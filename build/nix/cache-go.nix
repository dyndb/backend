{ pkgs, cachefile ? "cache.tar.gz" }:
let

in drv:
drv.overrideAttrs (oldAttrs: {

  outputs =
    if oldAttrs ? outputs then
      oldAttrs.outputs ++ [ "cache" ]
    else [
      "out"
      "cache"
    ];

  preBuild = ''
    source $stdenv/setup
 
    if [[ -f "${cachefile}" ]]; then
      echo ""########################### Found cache, extract it "###########################"
      mkdir -vp $(go env GOCACHE)
      tar -C $(go env GOCACHE) -xf ${cachefile}
    fi

    ${if oldAttrs ? preBuild then oldAttrs.preBuild else ""} 
  '';

  postBuild = ''
    echo "########################### create cache ###########################"
    mkdir -p "$cache"
    OLDPWD=$PWD
    cd $(go env GOCACHE)
    tar -C $(go env GOCACHE) -caf $cache/cache.tar.gz ./*
    cd $OLDPWD

    ${if oldAttrs ? postBuild then oldAttrs.postBuild else ""} 
  '';

})
