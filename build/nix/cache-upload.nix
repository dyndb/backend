# this is a wrapper to upload an cache-file to cache heavy builds
{ pkgs
, username ? ""
, password ? ""
}:
let

in

drv:
pkgs.stdenv.mkDerivation {
  name = "${drv.name}";
  #outputHashAlgo = "sha256";
  #outputHash = "";

  builder = pkgs.writeText "builder.sh" ''
    source $stdenv/setup
    #${pkgs.curl}/bin/curl -v -u ${username}:${password} --upload-file
    ${pkgs.rsync}/bin/rsync -av --progress ${drv.out} $out --exclude cache.tar.gz
  '';

  nativeBuildInputs = [ pkgs.cacert pkgs.curl ];
  preferLocalBuild = true;
}
  