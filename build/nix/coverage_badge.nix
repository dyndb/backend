{
  # core-stuff
  pkgs
  # packages
, coreutils ? pkgs.coreutils
, gnugrep ? pkgs.gnugrep
, go ? pkgs.go
  # options
, src
, version ? "dev"
, coverage
}:
let
  devpython = pkgs.python3.withPackages (packages: with packages; [ virtualenv pip setuptools wheel packaging anybadge ]);
in
pkgs.stdenv.mkDerivation {
  pname = "badge-coverage";
  inherit version;
  inherit src;

  nativeBuildInputs = [
    coreutils
    gnugrep
    go
    devpython
  ];

  outputs = [ "out" ];

  dontStrip = true;

  buildPhase = ''
        export GOCACHE=$TMPDIR/go-cache
        cat ${coverage}/coverage.out
        ${go}/bin/go tool cover -func ${coverage}/coverage.out | grep total | tr -d "\t" | tr -d "total:(statements)" | tr -d "%" > $TMPDIR/coverage.sum
        ${devpython}/bin/anybadge --label=coverage --value=$(cat $TMPDIR/coverage.sum) --suffix='%' --overwrite --file=coverage.svg \
    	  50=red 60=orange 80=yellow 100=green
  '';
  installPhase = ''
    mkdir $out
    cp -v $TMPDIR/coverage.sum $out/coverage.sum
    cp -v coverage.svg $out/coverage.svg
  '';


}
