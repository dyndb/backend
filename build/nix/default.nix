{ pkgs ? import (import ./nixpkgs.nix) { }
  # , ui ? ../../internal/ui/dist

  # released-ui
, ui ? (import "${import ./ui.nix}/build/nix/default.nix" {
    inherit pkgs;
  }).package

  #   # for local testing:
  # , ui ? (import /mnt/synced/develop/gitlab.com/dyndb/ui/build/nix/default.nix {
  #     inherit pkgs;
  #   }).package

, version ? "0.70.1"
}:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;


  ################################### Binary ###################################
  # src = lib.sources.trace (pkgs.nix-gitignore.gitignoreSourcePure [
  src = pkgs.nix-gitignore.gitignoreSourcePure [
    ".gitlab-ci.yml"
    ".gitignore"
    ".vscode"
    ".*"

    "build/"
    "docs/"

    # we not need nix-related stuff
    "flake*"
    "*.nix"

    # we not need documentation
    "*.adoc"
    "*.xml"

    # we not need makefiles
    "Makefile"
    "*.mk"
  ]
    (pkgs.lib.cleanSource ./../..);


  # the cache-addon
  use-cache-go = import ./cache-go.nix {
    inherit pkgs;

    # you can use a local cache-file
    #cachefile = ./cache.tar.gz;

    # but normally we use an downloaded one
    #cachefile = goBuildCacheFile;
  };

  coverage = import ./coverage.nix { inherit pkgs src; };
  coverage-badge = import ./coverage_badge.nix { inherit pkgs src coverage; };

  # the binary
  binary = import ./package.nix {
    inherit pkgs src ui;
    version = version;
  };

  binaryWithDebug = import ./package.nix {
    inherit pkgs ui;
    inherit src;
    version = version;
    debugging = "yes";
  };

  binaryLocal = import ./package.nix {
    inherit pkgs;
    inherit src;
    version = version;
    ui = ui;
  };


  # We provide an nixos-module
  module = import ./module.nix;


in
{

  # test
  coverage = coverage;
  badge = pkgs.symlinkJoin {
    name = "badge";
    paths = [
      coverage-badge
    ];
  };


  package = use-cache-go binary;
  package-debug = use-cache-go binaryWithDebug;

  module = module;


  test = import ./test.nix;


  # nix-shell -A badges 
  #badges = callPackage ./build/badges.nix { version = packageVersion; };
}
