{ lib, config, pkgs, ... }:

# see
# https://github.com/microsoft/vscode/blob/27a61bd9852cb8b808af99f0acedd3b5d3b9afd5/src/vs/server/serverEnvironmentService.ts#L12-L59

with lib;
let

  cfg = config.services.dyndb;
  defaultUser = "dyndb";
  defaultGroup = defaultUser;
  #defaultPackage = pkgs.callPackage ../default.nix { };



in
{
  ###### interface
  options = {
    services.dyndb = {
      enable = mkEnableOption "dyndb";

      package = mkOption {
        #default = defaultPackage.package;
        description = "The package to use";
        type = types.package;
      };

      user = mkOption {
        type = types.str;
        default = defaultUser;
        example = "yourUser";
        description = ''
          The user to run this.
          By default, a user named <literal>${defaultUser}</literal> will be created.
        '';
      };

      group = mkOption {
        type = types.str;
        default = defaultGroup;
        example = "yourGroup";
        description = ''
          The group to run this.
          By default, a group named <literal>${defaultGroup}</literal> will be created.
        '';
      };

      dataDir = mkOption {
        type = types.path;
        default = "/var/lib/dyndb";
        example = "/home/yourUser";
      };

      configPath = mkOption {
        default = "";
        description =
          "The path to folders that contain dyndb-configs";
        type = types.str;
      };
    };
  };

  ###### implementation

  config = mkIf cfg.enable {

    users.users = mkIf (cfg.user == defaultUser) {
      "${defaultUser}" =
        {
          isSystemUser = true;
          group = cfg.group;
          home = cfg.dataDir;
          createHome = true;
          description = "dyndb user";
        };
    };

    users.groups = mkIf (cfg.group == defaultGroup) {
      "${defaultGroup}" = {
        name = "dyndb";
      };
    };

    systemd.services.dyndb = mkIf cfg.enable {
      description = "dyndb";
      wantedBy = [ "multi-user.target" ];
      after = [ "network-online.target" "postgresql.service" ];
      environment = { };

      restartTriggers = [
        "${cfg.configPath}"
      ];

      serviceConfig = {
        ExecStart = "${cfg.package}/bin/dyndb -d ${cfg.configPath}";
        ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
        User = cfg.user;
        Restart = "on-failure";

        # Used as root directory
        RuntimeDirectory = "dyndb";

        # This currently not work :(
        #RootDirectory = "/run/dyndb";
        #BindReadOnlyPaths = [
        #  (dirOf cfg.configFile)
        #  "/etc/ssl"
        #  "/etc/static/ssl"
        #  "/etc/resolv.conf"
        #  "${configFile}"
        #  "${cfg.package}/bin/dyndb"
        #];
        #ReadWritePaths = [
        #  (dirOf cfg.dataDir)
        #];

        # Some security
        #CapabilityBoundingSet = [ "CAP_NET_BIND_SERVICE" ];
        #DevicePolicy = "closed";
        #LockPersonality = true;
        #MemoryDenyWriteExecute = true;
        #NoNewPrivileges = true;
        #ProtectHome = "read-only";
        #PrivateDevices = true;
        #PrivateMounts = true;
        #PrivateTmp = true;
        #PrivateUsers = true;
        #ProtectClock = true;
        #ProtectControlGroups = true;
        #ProtectHostname = true;
        #ProtectKernelLogs = true;
        #ProtectKernelModules = true;
        #ProtectKernelTunables = true;
        #ProtectSystem = "full";
        #RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
        #RestrictNamespaces = true;
        #RestrictRealtime = true;
        #RestrictSUIDSGID = true;
        #SystemCallArchitectures = "native";

        # This is broken and need rework
        #SystemCallFilter = [ "@system-service" "~@resources" ];
        UMask = "0077";
      };

    };

  };

  meta.maintainers = with maintainers; [ stackshadow ];
}
