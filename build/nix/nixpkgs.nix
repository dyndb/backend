builtins.fetchTarball {
  # Descriptive name to make the store path easier to identify
  name = "nixos-unstable-2024-02-23";
  # Commit hash for nixos-unstable as of 2018-09-12
  url = "https://github.com/nixos/nixpkgs/archive/cbc4211f0afffe6dfd2478a62615dd5175a13f9a.tar.gz";
  # Hash obtained using `nix-prefetch-url --unpack <url>`
  sha256 = "1zhf4102f3a09ri3c2gsn6sjhb72cz4jd65srmz31g4l034gzcv6";
}



