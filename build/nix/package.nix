{ pkgs
, src
, version ? "dev"
, vendorHash ? null
, debugging ? ""
, homepage ? "https://gitlab.com/dyndb/backend"
, ui ? ""
}:
let
  lib = pkgs.lib;
  inherit (pkgs) buildGoModule;


  ldflagsDebug =
    if debugging == "" then [
      "-s"
      "-w"
    ] else [ ];

in
buildGoModule {
  pname = "dyndb";
  inherit version src;

  outputs = [ "out" ];

  excludedPackages = "test";
  subPackages = [ "cmd/dyndb" ];

  inherit vendorHash;
  proxyVendor = true;

  dontStrip = if debugging == "" then false else true;
  ldflags = ldflagsDebug ++ [ "-X gitlab.com/nerdhaltig/dyndb/backend/internal/config.Version=${version}" ];


  preBuild = ''
    if [ "${ui}" != "" ]; then
      cp -Rv ${ui}/* ./internal/ui/dist
      ls -ahl ./internal/ui/dist
    fi
  '';

  meta = with lib; {
    description = "dyndb - your db";
    license = licenses.mit;
    homepage = homepage;
    maintainers = with maintainers; [ stackshadow ];
  };
}
