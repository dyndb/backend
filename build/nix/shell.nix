{}:
let
  pkgs = import (import ./nixpkgs.nix) { };

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    nixos-generators

    stdenv
    shadow
    util-linux
    git
    git-secret
    gnupg
    curl

    pkg-config
    go
    zsh
    figlet

    python39Packages.packaging
    python39Packages.anybadge

    docker-client
  ];
  shellHook = ''
    export NIX_BUILD_CORES=4
    export IN_NIX_SHELL=true

    figlet "DynDB - BuildSystem"
  '';
}

