# nix build --file ./test.nix
{ pkgs, modulesPath, ... }:
let



  # Single source of truth for all tutorial constants
  database = "postgres";
  schema = "api";
  table = "todos";
  username = "authenticator";
  password = "mysecretpassword";
  webRole = "web_anon";
  postgrestPort = 3000;


in
{

  imports = [
    (modulesPath + "/virtualisation/qemu-vm.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  virtualisation =
    {
      graphics = false;
      cores = 4;
      memorySize = 1024;
    };

  # spare some space and build time
  documentation.enable = false;


  # Open the default port for `postgrest` in the firewall
  networking.firewall.allowedTCPPorts = [ postgrestPort ];

  services.postgresql = {
    enable = true;

    initialScript = pkgs.writeText "initialScript.sql" ''
      create schema ${schema};

      create role ${webRole} nologin;

      grant usage on schema ${schema} to ${webRole};
      grant select on ${schema}.${table} to ${webRole};

      create role ${username} inherit login password '${password}';
      grant ${webRole} to ${username};
    '';
  };

  users = {
    mutableUsers = false;

    users = {
      # For ease of debugging the VM as the `root` user
      root.password = "";

      # Create a system user that matches the database user so that we
      # can use peer authentication.  The tutorial defines a password,
      # but it's not necessary.
      "${username}" = {
        group = "${username}";
        isSystemUser = true;
      };
    };
  };

}
 