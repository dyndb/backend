# ./postgrest-tutorial.nix
# nix build --file ./vm.nix
# QEMU_NET_OPTS='hostfwd=tcp::3000-:3000' result/bin/run-nixos-vm
{ system }:
let
  # For extra determinism
  nixpkgs = import ./repo-nixos-22.11-2023-01-15.nix;
  pkgs = import nixpkgs { inherit system; };




  nixos =
    import "${nixpkgs}/nixos" {
      #system = "x86_64-linux";
      inherit system;


      configuration = {

        imports = [
          (nixpkgs + "/nixos/modules/virtualisation/qemu-vm.nix")
          (nixpkgs + "/nixos/modules/profiles/qemu-guest.nix")
        ];

        virtualisation =
          {
            graphics = false;
            cores = 4;
            memorySize = "1G";
          };

        # spare some space and build time
        documentation.enable = false;

      } // import ./test-configuration.nix { inherit pkgs; };
    };

in
nixos.vm
