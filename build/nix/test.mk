.ONSHELL:


test-nixos: in-nix-shell ## test in nixos-based-vm
	@if [[ ! -z $$IN_NIX_SHELL ]]; then \
		echo -e "$(DEBUG) Build binary with nix $(NORMAL)" ;\
		nix-build ${dirBuildNix}default.nix \
		--argstr system ${SYSTEM} \
		--arg pkgs 'import (import ${dirBuildNix}repo-nixos-22.11-2023-01-15.nix) {  }' \
		-o result-release \
		--show-trace \
		-A test ;\
	fi

test-vm: in-nix-shell ## start a nixos vm
	@if [[ ! -z $$IN_NIX_SHELL ]]; then \
		echo -e "$(DEBUG) Build vm with nix $(NORMAL)" ;\
		CMD=$$(nixos-generate -f vm -c ${dirBuildNix}test-configuration.nix --show-trace) ;\
		$$CMD ;\
	fi

#		nix --extra-experimental-features nix-command build --argstr system ${SYSTEM} --system ${SYSTEM} --file ${dirBuildNix}test-vm.nix ;\
#		result/bin/run-nixos-vm ;\
