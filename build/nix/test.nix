# nix build --file ./test.nix

let

  # For extra determinism
  nixpkgs = import ./repo-nixos-22.11-2023-01-15.nix;
  pkgs = import nixpkgs { };

  # Single source of truth for all tutorial constants
  database = "postgres";
  schema = "api";
  table = "todos";
  username = "authenticator";
  password = "mysecretpassword";
  webRole = "web_anon";
  postgrestPort = 3000;


in
pkgs.nixosTest
  ({
    name = "test";
    #system = "x86_64-linux";

    nodes = {
      server = { config, pkgs, ... }: {
        # Open the default port for `postgrest` in the firewall
        networking.firewall.allowedTCPPorts = [ postgrestPort ];

        services.postgresql = {
          enable = true;

          initialScript = pkgs.writeText "initialScript.sql" ''
            create schema ${schema};

            create role ${webRole} nologin;

            grant usage on schema ${schema} to ${webRole};
            grant select on ${schema}.${table} to ${webRole};

            create role ${username} inherit login password '${password}';
            grant ${webRole} to ${username};
          '';
        };

        users = {
          mutableUsers = false;

          users = {
            # For ease of debugging the VM as the `root` user
            root.password = "";

            # Create a system user that matches the database user so that we
            # can use peer authentication.  The tutorial defines a password,
            # but it's not necessary.
            "${username}" = {
              group = "${username}";
              isSystemUser = true;
            };
          };
        };

      };

      client = { };
    };

    testScript =
      ''
        import json
        import sys

        start_all()


      '';
  })
