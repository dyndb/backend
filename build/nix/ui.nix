builtins.fetchGit {
  name = "dyndb-ui";
  url = "https://gitlab.com/dyndb/ui.git";
  ref = "main";
  rev = "42e2af77adf2411ffe85d7e771d5a72dcb1e05a1";
}

#, UISrc ? /mnt/synced/develop/gitlab.com/dyndb/ui
