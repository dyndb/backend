{ system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
, lib ? pkgs.lib
, config
, ...
}:
let

  ################################### Package ###################################

  package = import ../../default.nix { };

  configAsYaml =
    pkgs.writeText "dyndb.yml" (lib.generators.toYAML { } {
      loglevel = "debug";
      terminal = true;

      limits = {
        requestTimeout = "30s";
        tokenTimeout = "5m";
      };

      server = {
        url = "http://0.0.0.0:5555";
        prefix = "/api";
      };

      postgresql = {
        url = "postgres:///var/run/postgresql";
        dbname = "dyndb";
      };

      provisioning = {
        tenant = "default";
        username = "demo";
        password = "demo";
      };

    });

in
{


  environment.systemPackages = with pkgs; [
    coreutils
    bash
    systemd
  ];

  imports = [
    package.module
  ];

  # postgresl
  services.postgresql = {
    enable = true;

    package = pkgs.postgresql_14;

    ensureDatabases = [
      "dyndb"
    ];

    ensureUsers = [
      {
        name = "dyndb";
        ensurePermissions = {
          # "DATABASE ${secrets.postgresql.nixcloud.dbname}" = "ALL PRIVILEGES";
          "ALL TABLES IN SCHEMA public" = "ALL PRIVILEGES";
          "DATABASE dyndb" = "ALL PRIVILEGES";
        };
      }
    ];
  };

  systemd.services.dyndb.after = [ "postgresql.service" ];

  # dyndb-config
  services.dyndb = {
    enable = true;
    package = package.local;
    configFile = "${configAsYaml}";
  };

  networking.firewall.allowedTCPPorts = [ 5555 ];

}
