# nix-build '<nixpkgs/nixos>' -A vm --system x86_64-linux --arg configuration ./local

{ nixpkgs ? <nixpkgs>
, configuration ? ../configuration.nix
, system ? builtins.currentSystem
, lib ? nixpkgs.lib
, ...
}:

with lib;
{
  imports = [
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    <nixpkgs/nixos/modules/virtualisation/qemu-vm.nix>
    ./configuration.nix
  ];


  services.qemuGuest.enable = true;

  # fileSystems."/" = {
  #   device = "/dev/disk/by-label/nixos";
  #   fsType = "ext4";
  #   autoResize = true;
  # };

  # boot = {
  #   growPartition = true;
  #   kernelParams = [ "console=ttyS0" "boot.shell_on_fail" ];
  # };

  virtualisation = {
    diskSize = 3000; # MB
    memorySize = 1024; # MB
    writableStoreUseTmpfs = false;
    bootDevice = "/dev/vda";
    resolution =
      {
        x = 2048;
        y = 768;
      };
    forwardPorts = [
      # forward local port 2222 -> 22, to ssh into the VM
      { from = "host"; host.port = 5555; guest.port = 5555; }
    ];
  };

  # we could alternatively hook root or a custom user
  # to some ssh key pair
  users.extraUsers.root.password = ""; # oops
  users.mutableUsers = false;
}
