package inits

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitAttachments(ctr goactor.Concentrator, connections connections.Management) {
	attachmentSaveActor := attachment.NewSaveActor(ctr)
	attachmentOpenActor := attachment.NewOpenActor(ctr)
	attachmentDeleteActor := attachment.NewDeleteActor(ctr)
	attachmentPreviewOpenActor := attachment.NewPreviewOpenActor(ctr)

	if !config.Values.Development {
		attachmentSaveActor.Spawn(10)
		attachmentOpenActor.Spawn(10)
		attachmentDeleteActor.Spawn(10)
		attachmentPreviewOpenActor.Spawn(10)
	}
}
