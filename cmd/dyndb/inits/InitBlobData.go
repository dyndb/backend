package inits

import (
	"net/url"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitBlobData(ctr goactor.Concentrator, connections connections.Management, blobDataManager blobstorage.Management) {

	var err error

	// blob - actor
	defaultBucketActor := blobstorage.NewDefaultBucketActor(ctr, blobDataManager)
	blobDataSaveActor := blobstorage.NewSaveActor(ctr, blobDataManager)
	blobDataPreviewSaveActor := blobstorage.NewPreviewSaveActor(ctr, blobDataManager)
	blobDataOpenActor := blobstorage.NewOpenActor(ctr, blobDataManager)
	blobDataPreviewOpenActor := blobstorage.NewPreviewOpenActor(ctr, blobDataManager)
	blobDataDeleteActor := blobstorage.NewDeleteActor(ctr, blobDataManager)

	if !config.Values.Development {
		defaultBucketActor.Spawn(10)
		blobDataSaveActor.Spawn(10)
		blobDataPreviewSaveActor.Spawn(10)
		blobDataOpenActor.Spawn(10)
		blobDataPreviewOpenActor.Spawn(10)
		blobDataDeleteActor.Spawn(10)
	}

	// register buckets
	for BucketName, BucketInfo := range config.Values.StorageBuckets {
		if !BucketInfo.Enabled {
			continue
		}

		if BucketInfo.Type == "file" {
			newFileBucket := blobstorage.NewBlobStorageFilesystemAdapter(
				BucketName, BucketInfo.Path,
			)

			err = blobDataManager.BucketRegister(BucketName, newFileBucket)
		}

		if BucketInfo.Type == "s3" {

			// parse the url-string
			var url *url.URL
			url, err = url.Parse(BucketInfo.Path)
			if err != nil {
				panic(err)
			}

			opts := blobstorage.NewS3StorageOpts{
				Name:     BucketName,
				Endpoint: url.Host,
			}

			opts.AccessKeyID = url.User.Username()
			opts.AccessKey, _ = url.User.Password()

			opts.UseSSL = BucketInfo.SSL
			opts.IgnoreSelfSignedCert = BucketInfo.SelfSignedCert
			opts.EncryptionKey = BucketInfo.EncryptionKey

			if opts.EncryptionKey != "" {
				opts.UseSSL = true
			}

			opts.TmpPath = config.Values.Server.TempFolder + "/cache/s3"

			var newS3Bucket ports.BlobStorageAdapter
			newS3Bucket, err = blobstorage.NewBlobStorageS3Adapter(opts)

			if err == nil {
				err = blobDataManager.BucketRegister(BucketName, newS3Bucket)
			}
		}

		if err == nil {
			if BucketInfo.Default {
				err = blobDataManager.BucketDefaultSet(BucketName)
			}
		}

		if err != nil {
			panic(err)
		}
	}
}
