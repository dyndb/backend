package inits

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitBlobMetadata(ctr goactor.Concentrator, connections connections.Management) {
	blobMetadataPersistance := blobmetadata.NewPersistanceAdapter(connections)
	blobMetadataManager := blobmetadata.NewManager(blobMetadataPersistance)

	blobMetadataSaveActor := blobmetadata.NewSaveActor(ctr, blobMetadataManager)
	blobMetadataListActor := blobmetadata.NewListActor(ctr, blobMetadataManager)
	blobMetadataSearchActor := blobmetadata.NewSearchActor(ctr, blobMetadataManager)
	blobMetadataDeleteActor := blobmetadata.NewDeleteActor(ctr, blobMetadataManager)

	if !config.Values.Development {
		blobMetadataSaveActor.Spawn(10)
		blobMetadataListActor.Spawn(10)
		blobMetadataSearchActor.Spawn(10)
		blobMetadataDeleteActor.Spawn(10)
	}

}
