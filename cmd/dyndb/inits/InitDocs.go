package inits

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitDocs(ctr goactor.Concentrator, connections connections.Management) {
	docManagement := document.NewDocManagementUseCase(connections)
	docSearch := document.NewSearchUseCase(connections)

	documentsSaveActor := document.NewSaveActor(ctr, docManagement)
	documentsGetActor := document.NewGetActor(ctr, docManagement)
	documentsListActor := document.NewListActor(ctr, docManagement)
	documentsSearchActor := document.NewSearchActor(ctr, docSearch)
	documentsActor := document.NewDeleteActor(ctr, docManagement)
	documentsLinkActor := document.NewLinkActor(ctr, docManagement)

	if !config.Values.Development {
		documentsSaveActor.Spawn(10)
		documentsGetActor.Spawn(10)
		documentsListActor.Spawn(10)
		documentsSearchActor.Spawn(10)
		documentsActor.Spawn(10)
		documentsLinkActor.Spawn(10)
	}
}
