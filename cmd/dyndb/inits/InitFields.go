package inits

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitFields(ctr goactor.Concentrator, connections connections.Management) {
	fieldsPersistance := fields.NewDbPersistancePort(connections)
	fieldsSaveActor := fields.NewFieldsSaveActor(ctr, fieldsPersistance)
	if !config.Values.Development {
		fieldsSaveActor.Spawn(10)
	}
}
