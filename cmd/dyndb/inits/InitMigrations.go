package inits

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	connectionsDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/connections/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration/usecase"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/templates"
	"gitlab.com/nerdhaltig/gopersist"
)

func InitMigrations(ctxCancel context.Context, connections connections.Management) {

	// we run migrations
	migrationService := migration.NewMigrationService(connections)

	err := connections.WithTenants(func(connection gopersist.PersistancePort, tenantInfos *connectionsDomain.TenantInfo) (err error) {

		migrationService.Migrate(usecase.MigrateOpts{
			Ctx:          ctxCancel,
			Tenant:       connection.Tenant(),
			Section:      "fields",
			Migrations:   &fields.Migrations,
			Force:        config.Values.Postgresql.ForceMigration,
			ForceVersion: config.Values.Postgresql.ForceMigrationVersion,
		})

		migrationService.Migrate(usecase.MigrateOpts{
			Ctx:          ctxCancel,
			Tenant:       connection.Tenant(),
			Section:      "templates",
			Migrations:   &templates.Migrations,
			Force:        config.Values.Postgresql.ForceMigration,
			ForceVersion: config.Values.Postgresql.ForceMigrationVersion,
		})

		migrationService.Migrate(usecase.MigrateOpts{
			Ctx:          ctxCancel,
			Tenant:       connection.Tenant(),
			Section:      "blobmetadata",
			Migrations:   &blobmetadata.Migrations,
			Force:        config.Values.Postgresql.ForceMigration,
			ForceVersion: config.Values.Postgresql.ForceMigrationVersion,
		})

		migrationService.Migrate(usecase.MigrateOpts{
			Ctx:          ctxCancel,
			Tenant:       connection.Tenant(),
			Section:      "documents",
			Migrations:   &document.Migrations,
			Force:        config.Values.Postgresql.ForceMigration,
			ForceVersion: config.Values.Postgresql.ForceMigrationVersion,
		})

		return
	})
	if err != nil {
		panic(err)
	}
}
