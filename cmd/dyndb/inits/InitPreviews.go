package inits

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitPreviews(ctr goactor.Concentrator, connections connections.Management, blobDataManager blobstorage.Management) {

	// blob-preview
	blobPreviewer := blobpreview.NewService(config.Values.Gotenberg.URL)

	// and our attachment-actor which glues metadata and blobstorage together
	attachmentActor := blobpreview.NewGenerateActor(ctr, blobPreviewer, time.Minute*5)
	if !config.Values.Development {
		attachmentActor.Spawn(10)
	}

}
