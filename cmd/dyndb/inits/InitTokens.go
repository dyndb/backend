package inits

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func InitTokens(ctr goactor.Concentrator) {

	refreshTokenGenerator := tokens.NewJWTokenGeneratorAdapter("dyndb", config.Values.Security.SigningKey,
		time.Second*time.Duration(config.Values.Security.TokenTimeout),
		time.Second*time.Duration(config.Values.Security.TokenTimeoutOffset),
	)

	accessTokenGenerator := tokens.NewJWTokenGeneratorAdapter("dyndb", config.Values.Security.SigningKey,
		time.Second*time.Duration(config.Values.Security.TokenTimeout),
		time.Second*15,
	)

	tokenManager := tokens.NewTokenManagementUseCase(
		refreshTokenGenerator,
		accessTokenGenerator,
	)

	// actors
	tokenCreateActor := tokens.NewAuthCreateActor(ctr, tokenManager)
	tokenValidateActor := tokens.NewAuthValidateActor(ctr, tokenManager)
	if !config.Values.Development {
		tokenCreateActor.Spawn(10)
		tokenValidateActor.Spawn(10)
	}
}
