package inits

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user"
	userDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func InitUsers(ctr goactor.Concentrator, connections connections.Management) {

	userPersistance := user.NewUserPersistanceAdapter(connections)

	userSaveActor := user.NewSaveActor(ctr, userPersistance)
	userLoginActor := user.NewLoginActor(ctr, userPersistance)
	userInfoActor := user.NewUserInfoActor(ctr)
	if !config.Values.Development {
		userSaveActor.Spawn(10)
		userLoginActor.Spawn(10)
		userInfoActor.Spawn(10)
	}

	// add tenant/user if provided in config
	for newTenant, newUsers := range config.Values.Provisioning {

		// create tenant
		connections.TenantRegister(newTenant)

		for NewUserName, newUser := range newUsers {

			if !newUser.Enabled {
				continue
			}

			var userSaved *userDomain.UserSaved = &userDomain.UserSaved{}

			log.Debug().
				Str("tenant", newTenant).
				Str("username", NewUserName).
				Msg("Update User")

			_, err := ctr.WaitForMessages(nil, concentrator.Job{
				Message: &userDomain.UserSaveRequest{
					Tenant:   newTenant,
					Username: NewUserName,
					Password: newUser.Password,
				},
				ExpectedReturnMessage: &userSaved,
			})
			if err != nil {
				panic(err)
			}

		}
	}

	authCreateActor := auth.NewAuthCreateActor(ctr)
	authLoginActor := auth.NewAuthLoginActor(ctr)

	if !config.Values.Development {
		authCreateActor.Spawn(10)
		authLoginActor.Spawn(10)
	}
}
