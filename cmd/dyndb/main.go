package main

import (
	"context"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/cmd/dyndb/inits"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/cli"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/folders"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/fiber"
	restactor "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/services/resttoactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/services/routeattachments"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/services/routeauth"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/services/routedocuments"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/services/routefields"
	"gitlab.com/nerdhaltig/goactor/v2"

	goactorv1 "gitlab.com/nerdhaltig/goactor"
	"gitlab.com/nerdhaltig/gopersist"
)

func main() {

	// we need a context to cancel go-subroutines
	ctxCancel, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctx := appcontext.New(ctxCancel)
	var err error

	// read from cli
	cli.Read()

	// read config-file
	config.Init(ctx)

	// debug
	if config.Values.LogLevel != "debug" {
		goactorv1.DisableLogger()
	}

	// folders
	tempFolder := folders.NewFoldersAdapter()
	tempFolder.CreateIfMissing(config.Values.Server.TempFolder)
	//tempFolder.PeriodicDelete()

	// connections
	connectionsCounter := 5
	postgresqlPool := gopersist.NewPoolService(connectionsCounter, time.Second*time.Duration(config.Values.Server.RequestTimeout))
	for connectionIndex := connectionsCounter; connectionIndex > 0; connectionIndex-- {

		conn := gopersist.NewPostgresqlPersistAdapter(config.Values.Postgresql.URL)
		err = conn.Login(ctxCancel,
			config.Values.Postgresql.DBName,
			config.Values.Postgresql.Username,
			config.Values.Postgresql.Password,
		)
		if err != nil {
			break
		}

		postgresqlPool.Add(conn)
	}
	connections := connections.NewConnectionManagement(postgresqlPool)
	if err != nil {
		panic(err)
	}

	inits.InitMigrations(ctxCancel, connections)

	// ############################################## init ##############################################

	ctr := goactor.NewConcentrator(time.Second * 30)
	// rest-actor

	// tokens
	inits.InitTokens(ctr)

	// users
	inits.InitUsers(ctr, connections)

	// docs
	inits.InitDocs(ctr, connections)

	// fields
	inits.InitFields(ctr, connections)

	// blob - manager
	blobDataManager := blobstorage.NewBlobManagement()

	// blobs - data
	inits.InitBlobData(ctr, connections, blobDataManager)

	// blobs - metadata
	inits.InitBlobMetadata(ctr, connections)

	// attachment ( blob data+metadata )
	inits.InitAttachments(ctr, connections)

	// attachment ( preview )
	inits.InitPreviews(ctr, connections, blobDataManager)

	// imports
	// if cli.Values.ImportFolder != "" && cli.Values.ImportTenant != "" && cli.Values.ImportBucket != "" {
	// 	importfolder.ImportFromFolder(
	// 		workflowmanager, attachmentActor,
	// 		cli.Values.ImportFolder, cli.Values.ImportTenant, cli.Values.ImportBucket,
	// 	)
	// }

	// rest
	restToActor := restactor.NewRestToActorService(ctr, config.Values.Server.Domain)

	server := fiber.NewService(config.Values.Server.URL, config.Version)

	// register routes
	routeauth.Register(server.V1, restToActor)
	routedocuments.Register(server.V1, restToActor)
	routefields.Register(server.V1, restToActor)
	routeattachments.Register(server.V1, restToActor)

	server.Serve()

}
