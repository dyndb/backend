
docker-start: ## start additional containers for local development
	docker-compose \
	--project-name dyndb \
	--file deployment/docker-compose/minio.docker-compose.yml \
	up -d

docker-stop:
	docker-compose \
	--project-name dyndb \
	--file deployment/docker-compose/minio.docker-compose.yml \
	down --volumes
	