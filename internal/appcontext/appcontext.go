package appcontext

type Context interface {
	WithContext
	WithPanicAndStatus
	WithStatus
	WithLogger
}
