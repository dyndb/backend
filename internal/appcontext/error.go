package appcontext

import (
	"runtime"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type WithPanic interface {
	PanicOnErr(err error)
	ErrorMessage() string
}

type WithPanicAndStatus interface {
	WithPanic
	PanicWithStatusErr(status int, err error)
}

// Panic will set the message-level to panic and panic
func (c *appdata) PanicOnErr(err error) {
	if err != nil {

		if c.statuscode == 0 || c.statuscode == 200 {
			c.statuscode = 500
		}

		c.err = err
		c.logger.WithLevel(zerolog.PanicLevel).Msg(c.err.Error())
		panic(c)
	} else {
		c.statuscode = 200
	}
}

func (c *appdata) PanicWithStatusErr(status int, err error) {
	if err != nil {
		c.statuscode = status
		c.err = err
		c.logger.WithLevel(zerolog.PanicLevel).Msg(c.err.Error())
		panic(c)
	} else {
		c.statuscode = 200
		c.err = nil
	}
}

func (c *appdata) ErrorMessage() string {
	return c.err.Error()
}

// Recover try to recover
// return the status as http-status-code ( 200 == OK )
// and errorMessage.
// If no panic exist, errorMessage is "" and status is 200
func Recover(recoverValue interface{}) (status int, errorMessage string) {

	var statusCode int = 500
	var errorText string

	switch recoverValue := recoverValue.(type) {
	case string:
		errorText = recoverValue
	case error:
		errorText = recoverValue.Error()
	case Context:
		errorText = recoverValue.ErrorMessage()
		statusCode = recoverValue.Status()
	}

	buf := make([]byte, 1024)
	buf = buf[:runtime.Stack(buf, false)]

	if statusCode == 0 {
		statusCode = 500
	}

	log.Error().Str("stack", string(buf)).Msg(errorText)
	return statusCode, errorText
}
