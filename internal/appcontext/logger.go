package appcontext

import "github.com/rs/zerolog"

type WithLogger interface {
	Logger() zerolog.Context
	LoggerSet(logger zerolog.Context)

	Debug() *zerolog.Event
	Error() *zerolog.Event
}

func (c *appdata) Logger() zerolog.Context {
	return c.logger.With()
}

func (c *appdata) LoggerSet(logger zerolog.Context) {
	c.logger = logger.Logger()
}

// Debug return an zerolog debugger
func (c *appdata) Debug() *zerolog.Event {
	return c.logger.Debug()
}

func (c *appdata) Error() *zerolog.Event {
	return c.logger.Error()
}
