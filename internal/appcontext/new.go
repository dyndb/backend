package appcontext

import (
	"context"
	"runtime"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type ContextKey string

const AppContextKey = ContextKey("dyndb")

type appdata struct {
	ctx    context.Context
	logger zerolog.Logger

	tenant string

	statuscode int
	err        error
}

func New(ctx context.Context) (newAppContext Context) {

	newAppData := &appdata{
		ctx:    ctx,
		logger: log.Logger,
	}
	newAppData.ctx = context.WithValue(ctx, AppContextKey, newAppContext)

	return newAppData
}

func WithBackground() (newCtx context.Context, newAppContext Context) {
	newCtx = context.Background()
	newAppContext = &appdata{
		ctx:    newCtx,
		logger: log.Logger,
	}

	newCtx = context.WithValue(newCtx, AppContextKey, newAppContext)
	return
}

func FromContext(ctx context.Context) Context {

	newEvent := &appdata{
		ctx:    ctx,
		logger: log.Logger,
	}

	//if log.Logger.GetLevel() == zerolog.DebugLevel {
	_, filename, line, _ := runtime.Caller(1)
	newEvent.logger = newEvent.logger.With().
		Str("filename", filename).
		Int("line", line).
		Logger()
	//}

	return newEvent

}
