package appcontext

import (
	"errors"
	"time"
)

type WithContext interface {
	Deadline() (deadline time.Time, ok bool)
	Done() <-chan struct{}
	Err() error
	Value(key any) any
}

// Satisfy context
func (c *appdata) Deadline() (deadline time.Time, ok bool) {
	return c.ctx.Deadline()
}
func (c *appdata) Done() <-chan struct{} {
	return c.ctx.Done()
}
func (c *appdata) Err() error {

	contextErr := c.ctx.Err()
	if contextErr == nil {
		if c.ErrorMessage() != "" {
			contextErr = errors.New(c.ErrorMessage())
		}
	}

	return contextErr
}
func (c *appdata) Value(key any) any {
	return c.ctx.Value(key)
}
