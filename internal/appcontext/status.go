package appcontext

type WithStatus interface {
	StatusSet(statuscode int)
	Status() (statuscode int)
}

func (c *appdata) StatusSet(statuscode int) {
	c.statuscode = statuscode
}

func (c *appdata) Status() (statuscode int) {
	return c.statuscode
}
