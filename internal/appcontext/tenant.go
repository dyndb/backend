package appcontext

func (c *appdata) TenantSet(tenant string) {
	c.tenant = tenant
}

func (c *appdata) Tenant() (tenant string) {
	return c.tenant
}
