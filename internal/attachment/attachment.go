package attachment

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/service/attachmentactors"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func NewSaveActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return attachmentactors.NewUploadActor(ctr)
}

func NewOpenActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return attachmentactors.NewOpenActor(ctr)
}

func NewDeleteActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return attachmentactors.NewDeleteActor(ctr)
}

func NewPreviewOpenActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return attachmentactors.NewPreviewOpenActor(ctr)
}
