package domain

import (
	blobmetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	blobPreviewDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	blobstorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

type AttachmentUploadRequest struct {
	Tenant             string
	BucketName         string // optional, will use default if empty
	OriginalFilename   string
	DisplayText        string
	DownloadedFilePath string
}

type AttachmentUploaded struct {
	BlobID     blobstorageDomain.BlobID
	MetadataID blobmetadataDomain.MetadataID
}

type AttachmentDeleteRequest struct {
	Tenant     string
	MetadataID string
}

type AttachmentDeleted struct{}

type AttachmentOpenRequest struct {
	Tenant     string
	BucketName string
	BlobID     blobstorageDomain.BlobID
}

type AttachmentOpened struct {
	Metadata blobmetadataDomain.Metadata
	FilePath string
}

type PreviewOpenRequest struct {
	Tenant     string
	BucketName string
	BlobID     blobstorageDomain.BlobID
}

type PreviewOpened struct {
	State    blobPreviewDomain.PreviewState
	FilePath string
}
