package attachmentactors

import (
	"github.com/pkg/errors"
	attachmentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	blobmetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	blobdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewDeleteActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("attachmentDeleteActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		// 1. Request metadata
		case *attachmentDomain.AttachmentDeleteRequest:

			// tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			metadataID := blobmetadataDomain.MetadataID(msg.MetadataID)
			if err == nil {
				err = metadataID.Validate()
			}

			// Load metadata
			metadataList := &blobmetadataDomain.BlobMetadataList{}
			if err == nil {
				_, err = ctr.WaitForMessages(ctx, concentrator.Job{
					Message: &blobmetadataDomain.BlobMetadataListRequest{
						Tenant:      msg.Tenant,
						MetadataIDs: []blobmetadataDomain.MetadataID{metadataID},
					},
					ExpectedReturnMessage: &metadataList,
				})
			}

			// current meta-data
			var currentMetadata blobmetadataDomain.Metadata
			if err == nil {
				err = errors.WithStack(errors.New("metadata not found"))
				for _, currentMetadata = range metadataList.List {
					err = nil
					break
				}
			}

			blobID := blobdomain.BlobID(currentMetadata.BlobID)
			if err == nil {
				err = blobID.Validate()
			}

			if err == nil {
				_, err = ctr.WaitForMessages(ctx,
					concentrator.Job{
						// delete metadata
						Message: &blobmetadataDomain.BlobMetadataDeleteRequest{
							Tenant:     msg.Tenant,
							MetadataID: metadataID,
						},
					},
					concentrator.Job{
						// delete blob
						Message: &blobdomain.BlobDeleteRequest{
							Tenant:     msg.Tenant,
							BucketName: currentMetadata.BucketName,
							BlobID:     blobdomain.BlobID(currentMetadata.BlobID),
						},
					},
				)
			}

			if err == nil {
				returnMessage = append(returnMessage, &attachmentDomain.AttachmentDeleted{})
			}
		}
		return
	})

	ctr.HookActor(actor, &attachmentDomain.AttachmentDeleteRequest{})

	return
}
