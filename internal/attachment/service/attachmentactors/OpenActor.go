package attachmentactors

import (
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	blobmetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	blobstorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

func NewOpenActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("attachmentOpenActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		case *domain.AttachmentOpenRequest:

			metadataList := &blobmetadataDomain.BlobMetadataList{}

			_, err = ctr.WaitForMessages(ctx, concentrator.Job{
				Message: &blobmetadataDomain.BlobMetadataListRequest{
					Tenant:  msg.Tenant,
					BlobIDs: []string{msg.BlobID.String()},
				},
				ExpectedReturnMessage: &metadataList,
			})

			// current meta-data
			err = errors.WithStack(errors.New("metadata not found"))
			var currentMetadata blobmetadataDomain.Metadata
			for _, currentMetadata = range metadataList.List {
				err = nil
				break
			}

			// Request - blobOpened
			var blobOpened *blobstorageDomain.BlobOpened
			if err == nil {
				blobOpened = &blobstorageDomain.BlobOpened{}

				_, err = ctr.WaitForMessages(ctx, concentrator.Job{
					Message: &blobstorageDomain.BlobOpenRequest{
						Tenant:     msg.Tenant,
						BucketName: msg.BucketName,
						BlobID:     msg.BlobID,
					},
					ExpectedReturnMessage: &blobOpened,
				})
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.AttachmentOpened{
					Metadata: currentMetadata,
					FilePath: blobOpened.FilePath,
				})
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.AttachmentOpenRequest{})

	return
}
