package attachmentactors

import (
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	blobpreviewDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	blobstorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func NewPreviewOpenActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("previewOpenActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		// 1. Request preview from blob storage
		case *domain.PreviewOpenRequest:

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			// open it from blob storage
			previewOpened := &blobstorageDomain.PreviewOpened{}
			if err == nil {
				_, err = ctr.WaitForMessages(ctx, concentrator.Job{
					Message: &blobstorageDomain.PreviewOpenRequest{
						Tenant:     msg.Tenant,
						BucketName: msg.BucketName,
						BlobID:     msg.BlobID,
					},
					ExpectedReturnMessage: &previewOpened,
				})
			}

			// preview exist ?
			if err == nil {
				if previewOpened.FilePath != "" {
					returnMessage = append(returnMessage, &domain.PreviewOpened{
						State:    blobpreviewDomain.PreviewStateIsDone,
						FilePath: previewOpened.FilePath,
					})
					return
				}
			}

			// open source document
			blobOpenResp := &blobstorageDomain.BlobOpened{}

			if err == nil {
				_, err = ctr.WaitForMessages(ctx, concentrator.Job{
					Message: &blobstorageDomain.BlobOpenRequest{
						Tenant:     msg.Tenant,
						BucketName: msg.BucketName,
						BlobID:     msg.BlobID,
					},
					ExpectedReturnMessage: &blobOpenResp,
				})
			}

			// create preview
			previewGenerateResp := &blobpreviewDomain.Generated{}

			if err == nil {
				_, err = ctr.WaitForMessages(ctx, concentrator.Job{
					Message: &blobpreviewDomain.GenerateRequest{
						ReqID:          msg.BlobID.String(),
						SourceFilePath: blobOpenResp.FilePath,
					},
					ExpectedReturnMessage: &previewGenerateResp,
				})
			}

			if err == nil {
				switch previewGenerateResp.State {

				case blobpreviewDomain.PreviewStateIsOngoing:
					returnMessage = append(returnMessage, &domain.PreviewOpened{
						State: blobpreviewDomain.PreviewStateIsOngoing,
					})
					return

				case blobpreviewDomain.PreviewStateIsDone:

					previewSaveRes := &blobstorageDomain.PreviewSaved{}

					_, err = ctr.WaitForMessages(ctx, concentrator.Job{
						Message: &blobstorageDomain.PreviewSaveRequest{
							Tenant:         msg.Tenant,
							BucketName:     msg.BucketName,
							BlobID:         msg.BlobID,
							SourceFilePath: previewGenerateResp.FilePath,
						},
						ExpectedReturnMessage: &previewSaveRes,
					})

					if err == nil {
						returnMessage = append(returnMessage, &domain.PreviewOpened{
							State:    blobpreviewDomain.PreviewStateIsDone,
							FilePath: previewGenerateResp.FilePath,
						})
					}
					return

				default:
					err = errors.WithStack(errors.New("programming error"))
				}
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.PreviewOpenRequest{})

	return
}
