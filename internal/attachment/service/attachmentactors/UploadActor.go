package attachmentactors

import (
	"github.com/pkg/errors"
	attachmentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	metadatadomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	blobStorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	blobdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewUploadActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	var defaultBucketName string

	// we request the default bucket-name
	result := &blobStorageDomain.BlobStorageDefaultName{}
	_, err := ctr.WaitForMessages(nil, concentrator.Job{
		Message:               &blobStorageDomain.BlobStorageDefaultNameRequest{},
		ExpectedReturnMessage: &result,
	})
	if err != nil {
		panic(err)
	}

	// now we know the default bucket-name
	defaultBucketName = result.Name

	actor = goactor.NewActor("attachmentUploadActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		case *attachmentDomain.AttachmentUploadRequest:

			// get the tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if !ctx.As(&AuthInfo) {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
				if err == nil {
					msg.Tenant = AuthInfo.Tenant
				}
			}

			if err == nil {

				if msg.BucketName == "" {
					msg.BucketName = defaultBucketName
				}

				// read some stuff from
				newBlobID := blobdomain.NewBlobID(msg.DownloadedFilePath)

				// blob metadata store
				newMetadata := metadatadomain.Metadata{
					BlobID:           newBlobID.String(),
					DisplayText:      msg.DisplayText,
					BucketName:       msg.BucketName,
					FileNameOriginal: msg.OriginalFilename,
				}
				newMetadata.ReadFromFile(msg.DownloadedFilePath)

				// blob data store
				BlobSaved := &blobdomain.BlobSaved{}
				MetadataSaved := &metadatadomain.BlobMetadataSaved{}

				_, err = ctr.WaitForMessages(ctx,
					concentrator.Job{
						Message: &blobdomain.BlobSaveRequest{
							Tenant:         msg.Tenant,
							BucketName:     msg.BucketName,
							BlobID:         newBlobID,
							SourceFilePath: msg.DownloadedFilePath,
						},
						ExpectedReturnMessage: &BlobSaved,
					},
					concentrator.Job{
						Message: &metadatadomain.BlobMetadataSaveRequest{
							Tenant:   msg.Tenant,
							Metadata: newMetadata,
						},
						ExpectedReturnMessage: &MetadataSaved,
					},
				)

				if err == nil {
					returnMessage = append(returnMessage,
						&attachmentDomain.AttachmentUploaded{
							BlobID:     BlobSaved.BlobID,
							MetadataID: MetadataSaved.MetadataID,
						},
					)
				}
			}
		}

		return
	})

	// we create an hook and want to listen if somebody register an new blob-storage
	ctr.HookActor(actor, &attachmentDomain.AttachmentUploadRequest{})

	return
}
