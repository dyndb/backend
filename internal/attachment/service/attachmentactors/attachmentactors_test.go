package attachmentactors_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	attachmentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/service/attachmentactors"
	blobMetadataTests "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/tests"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/filesystem"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/blobactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/management"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func TestActors(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	// root directory
	var projectRoot string
	projectRoot, _ = testtools.FindProjectRoot()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testtenant",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	// we need an global concentrator
	ctr := goactor.NewConcentrator(time.Second * 30)

	// metadata - deps
	blobMetadataTests.PrepareBlobMetadataActors(ctr, connections)

	// data - deps
	blobStorageManager := management.NewBlobManagement()
	localBlobStorageAdapter := filesystem.NewBlobStorageAdapter("local", "./testfolder")
	blobStorageManager.BucketRegister(localBlobStorageAdapter.Name(), localBlobStorageAdapter)

	// data - actors
	blobactors.NewSaveActor(ctr, blobStorageManager)
	blobactors.NewOpenActor(ctr, blobStorageManager)
	blobactors.DefaultBucketActor(ctr, blobStorageManager)
	// blobDeleteActor := blobactors.NewDeleteActor(blobStorageManager)

	// attachment - actors
	attachmentactors.NewUploadActor(ctr)
	attachmentactors.NewOpenActor(ctr)

	// save
	attachmentSaveResp := &attachmentDomain.AttachmentUploaded{}

	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &attachmentDomain.AttachmentUploadRequest{
			Tenant:             "testtenant",
			BucketName:         "local",
			OriginalFilename:   "Makefile",
			DisplayText:        "Core Makefile",
			DownloadedFilePath: projectRoot + "/Makefile",
		},
		ExpectedReturnMessage: &attachmentSaveResp,
	})
	assert.NoError(t, err)
	assert.NotEmpty(t, attachmentSaveResp.BlobID)
	assert.NotEmpty(t, attachmentSaveResp.MetadataID)

}
