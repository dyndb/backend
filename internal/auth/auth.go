package auth

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/services/actorforauth"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func NewAuthCreateActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return actorforauth.NewAuthCreateActor(ctr)
}

func NewAuthLoginActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return actorforauth.NewAuthLoginActor(ctr)
}

func NewUserInfoActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return actorforauth.NewUserInfoActor(ctr)
}
