package domain

type UserCreateRequest struct {
	Tenant   string
	Username string
	Password string
}

// AuthValidated return the tenant/username of an valid token
//
// and new refresh/access token if requested
type UserCreated struct {
	Tenant   string
	Username string
}

type LoginRequest struct {
	Tenant   string
	Username string
	Password string
}

type LoggedIn struct{}

type UserInfoRequest struct{}

// AuthValidated return the tenant/username of an valid token
//
// and new refresh/access token if requested
type UserInfo struct {
	Tenant   string `json:"tenant"`
	Username string `json:"username"`
}
