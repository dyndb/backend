package actorforauth

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/domain"
	userDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewAuthCreateActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("authCreateActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.UserCreateRequest:

			var saved *userDomain.UserSaved = &userDomain.UserSaved{}

			_, err = ctr.WaitForMessages(nil,
				concentrator.Job{
					Message: &userDomain.UserSaveRequest{
						Tenant:   msg.Tenant,
						Username: msg.Username,
						Password: msg.Password,
					},
					ExpectedReturnMessage: &saved,
				},
			)

			if err == nil && msg.Tenant == saved.Tenant && msg.Username == saved.Username {
				returnMessage = append(returnMessage, &domain.UserCreated{
					Tenant:   msg.Tenant,
					Username: msg.Username,
				})
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.UserCreateRequest{})

	return
}
