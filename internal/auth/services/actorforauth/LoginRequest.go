package actorforauth

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/domain"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	userDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewAuthLoginActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("authLoginActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.LoginRequest:

			var validated *tokenDomain.AuthValidated = &tokenDomain.AuthValidated{}
			var loggedin *userDomain.UserLoggedIn = &userDomain.UserLoggedIn{}

			_, err = ctr.WaitForMessages(ctx,
				concentrator.Job{
					Message: &tokenDomain.AuthCreateRequest{
						Tenant:   msg.Tenant,
						Username: msg.Username,
					},
					ExpectedReturnMessage: &validated,
				},
				concentrator.Job{
					Message: &userDomain.UserLoginRequest{
						Tenant:   msg.Tenant,
						Username: msg.Username,
						Password: msg.Password,
					},
					ExpectedReturnMessage: &loggedin,
				},
			)

			if err == nil {
				returnMessage = append(returnMessage, &domain.LoggedIn{})
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.LoginRequest{})

	return
}
