package actorforauth

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewUserInfoActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("userInfoActpr", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch ctx.Message().(type) {

		case *domain.UserInfoRequest:

			returnMessage = append(returnMessage, &domain.UserInfo{})

		}
		return
	})

	ctr.HookActor(actor, &domain.UserInfoRequest{})

	return
}
