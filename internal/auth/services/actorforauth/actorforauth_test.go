package actorforauth_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/tests"
	authTests "gitlab.com/nerdhaltig/dyndb/backend/internal/auth/tests"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	tokenTests "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/tests"
	userTests "gitlab.com/nerdhaltig/dyndb/backend/internal/user/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func TestLogin(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	// we need an global concentrator
	ctr := goactor.NewConcentrator(time.Second * 30)

	// prepare all actors
	userTests.PrepareUserActors(ctr, connections)
	tokenTests.PrepareTokenActors(ctr)
	authTests.PrepareAuthActors(ctr)

	tests.CRUDWithActors(t, ctr, "testdyndb")

}
