//go:build !production

package tests

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/services/actorforauth"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func PrepareAuthActors(ctr goactor.Concentrator) {
	actorforauth.NewAuthCreateActor(ctr)
	actorforauth.NewAuthLoginActor(ctr)
	actorforauth.NewUserInfoActor(ctr)
}
