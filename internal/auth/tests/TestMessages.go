//go:build !production

package tests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/auth/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func CRUDWithActors(t *testing.T, ctr goactor.Concentrator, tenantName string) {

	userCreated := &domain.UserCreated{}
	_, err := ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.UserCreateRequest{
			Tenant:   tenantName,
			Username: "special.user",
			Password: "special.password",
		},
		ExpectedReturnMessage: &userCreated,
	})
	assert.NoError(t, err)
	assert.Equal(t, "special.user", userCreated.Username)

	// try to login
	loggedIn := &domain.LoggedIn{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.LoginRequest{
			Tenant:   tenantName,
			Username: "special.user",
			Password: "special.wrongpw",
		},
		ExpectedReturnMessage: &loggedIn,
	})
	assert.Error(t, err)

	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.LoginRequest{
			Tenant:   tenantName,
			Username: "special.user",
			Password: "special.password",
		},
		ExpectedReturnMessage: &loggedIn,
	})
	assert.NoError(t, err)

}
