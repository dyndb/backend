package blobmetadata

import "gitlab.com/nerdhaltig/dyndb/backend/internal/migration/domain"

var v1 domain.MigrationJobs = domain.MigrationJobs{
	Version: 1,
	Jobs: []domain.MigrationJob{
		{
			Action: "create_collection",
			DocID:  "attachments",
		},
	},
}

var Migrations []domain.MigrationJobs = []domain.MigrationJobs{
	v1,
}
