//go:build !production

package persistance

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

func TestBlobMetadataAdd(t *testing.T, tenantName string, metadatapersistance ports.BlobMetadataPersistance) {

	var err error

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	ctx := appcontext.New(ctxBg)

	// delete all attachments
	var allAttachments domain.MetadataList
	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	if err == nil {
		for attachmentID := range allAttachments {
			metadatapersistance.Delete(ctx, tenantName, attachmentID)
		}
	}

	// add an attachment
	err = metadatapersistance.Save(ctx, tenantName, domain.NewMetadataID(), domain.Metadata{
		DisplayText:      "Datasheet 1",
		FileNameOriginal: "data1.xml",
		BucketName:       "local",
		BlobID:           "0000-0000-0000-0001",
		//DocID:            []string{},
	})
	assert.NoError(t, err)

	// list attachments
	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(allAttachments))

	// Add a second attachment
	err = metadatapersistance.Save(ctx, tenantName, domain.NewMetadataID(), domain.Metadata{
		BucketName:       "local",
		DisplayText:      "Datasheet 2",
		FileNameOriginal: "data2.xml",
		BlobID:           "0000-0000-0000-0002",
	})
	assert.NoError(t, err)

	// list attachments
	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(allAttachments))

	// Add a third attachment
	//var dataSheet2AttachmentID domain.MetadataID
	err = metadatapersistance.Save(ctx, tenantName, domain.NewMetadataID(), domain.Metadata{
		BucketName:       "local",
		DisplayText:      "Datasheet 3",
		FileNameOriginal: "data3.xml",
		BlobID:           "0000-0000-0000-0003",
	})
	assert.NoError(t, err)

	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(allAttachments))
}

func TestBlobMetadataGet(t *testing.T, tenantName string, metadatapersistance ports.BlobMetadataPersistance) {

	var err error

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	ctx := appcontext.New(ctxBg)

	// list attachments
	var allAttachments domain.MetadataList

	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.True(t, len(allAttachments) >= 3)

	// Get an non existing attachment
	var existingAttachments domain.MetadataList
	existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByIDs: []domain.MetadataID{domain.NewMetadataID()}})
	assert.NoError(t, err)
	assert.Equal(t, 0, len(existingAttachments))

	metadataArray := []domain.MetadataID{}
	for metadataID := range allAttachments {
		metadataArray = append(metadataArray, metadataID)
	}
	metadataArray = metadataArray[1:]

	// Get multiple
	existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByIDs: metadataArray})
	assert.NoError(t, err)
	assert.Equal(t, len(allAttachments)-1, len(existingAttachments))

	// get by blobid
	for _, metadata := range allAttachments {

		existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByBlobIDs: []string{metadata.BlobID}})
		assert.NoError(t, err)

		for _, existingMetadata := range existingAttachments {
			assert.Equal(t, metadata.DisplayText, existingMetadata.DisplayText)
			break
		}

		break
	}

	// get by words
	existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByWords: []string{"Datasheet 1"}})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(existingAttachments))

	existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByWords: []string{"Datasheet", "1"}})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(existingAttachments))

	existingAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000, ByWords: []string{"Datasheet"}})
	assert.NoError(t, err)
	assert.Equal(t, 3, len(existingAttachments))

}

func TestBlobMetadataDelete(t *testing.T, tenantName string, metadatapersistance ports.BlobMetadataPersistance) {

	var err error

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	ctx := appcontext.New(ctxBg)

	// list attachments
	var allAttachments domain.MetadataList

	allAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.True(t, len(allAttachments) >= 3)

	var firstAttachmentID domain.MetadataID
	for metadataID := range allAttachments {
		firstAttachmentID = metadataID
		break
	}

	// Delete an attachment
	metadatapersistance.Delete(ctx, tenantName, firstAttachmentID)

	// check that its deleted
	var listedAttachments domain.MetadataList
	listedAttachments, err = metadatapersistance.List(ctx, ports.ListOpts{Tenant: tenantName, PageLimit: 1000})
	assert.NoError(t, err)
	assert.Equal(t, len(allAttachments)-1, len(listedAttachments))
}
