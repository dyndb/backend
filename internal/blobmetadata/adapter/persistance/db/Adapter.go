package db

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
)

type metadataStorage struct {
	connections    connections.Management
	collectionName string
}

func NewBlobMetaPersistance(connections connections.Management, collectionName string) ports.BlobMetadataPersistance {
	return &metadataStorage{
		connections:    connections,
		collectionName: collectionName,
	}
}
