package db

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *metadataStorage) Delete(ctx context.Context, tenant string, MetadataID domain.MetadataID) (err error) {
	err = svc.connections.WithTenant(tenant, svc.collectionName, func(connection gopersist.PersistancePort) (err error) {
		return connection.Delete(ctx, MetadataID.String())
	})
	return
}
