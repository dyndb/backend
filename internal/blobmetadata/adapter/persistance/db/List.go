package db

import (
	"context"
	"fmt"
	"html"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *metadataStorage) List(ctx context.Context, opts ports.ListOpts) (attachments domain.MetadataList, err error) {

	attachments = make(domain.MetadataList)

	err = svc.connections.WithTenant(opts.Tenant, svc.collectionName, func(connection gopersist.PersistancePort) (err error) {

		var connectionWithSQL gopersist.PersistancePortWithSQL
		var connectionWithSQLSupported bool

		if connectionWithSQL, connectionWithSQLSupported = connection.(gopersist.PersistancePortWithSQL); !connectionWithSQLSupported {
			err = errors.New("connection not support searching")
			err = errors.WithStack(err)
		}

		var filter = ""

		// ids
		for curIndex, curID := range opts.ByIDs {
			if curID.IsEmpty() {
				continue
			}
			if len(opts.ByIDs) > 1 && curIndex == 0 {
				filter += "( "
			}

			if len(opts.ByIDs) > 1 && curIndex > 0 {
				filter += " OR "
			}

			filter += fmt.Sprintf("docid = '%v'", curID.String())
		}
		if len(opts.ByIDs) > 1 {
			filter += " )"
		}

		// AND
		if len(opts.ByBlobIDs) > 0 {
			if filter != "" {
				filter += " AND "
			}
		}

		// all documents without an blob-id
		for curBlobIndex, curBlobID := range opts.ByBlobIDs {
			if curBlobID == "" {
				continue
			}
			if len(opts.ByBlobIDs) > 1 && curBlobIndex == 0 {
				filter += "( "
			}

			if len(opts.ByBlobIDs) > 1 && curBlobIndex > 0 {
				filter += " OR "
			}

			filter += fmt.Sprintf("data->>'blobid' = '%s'", curBlobID)
		}
		if len(opts.ByBlobIDs) > 1 {
			filter += " )"
		}

		// words
		for curWordIndex, curWord := range opts.ByWords {

			if len(opts.ByWords) > 1 && curWordIndex == 0 {
				filter += "( "
			}

			if len(opts.ByWords) > 1 && curWordIndex > 0 {
				filter += " AND "
			}

			curWord = html.EscapeString(curWord)
			curWord = strings.ToLower(curWord)

			filter += fmt.Sprintf("lower(data::text) like '%%%s%%'", curWord)
		}
		if len(opts.ByWords) > 1 {
			filter += " )"
		}

		if filter == "" {
			filter = "1=1"
		}

		filter += " ORDER BY data->>'uploaddatetime' DESC"

		if err == nil {
			var MetadataID domain.Metadata
			err = connectionWithSQL.QueryStruct(ctx, &opts.PageKey, opts.PageLimit, filter /*+" ORDER BY data->>'uploaddatetime' ASC"*/, &MetadataID, func(docID interface{}) (interrupt bool) {
				var newMetadata domain.MetadataID
				newMetadata, err = domain.NewMetadataIDFromString(docID.(string))
				if err == nil {
					attachments[newMetadata] = MetadataID
				}
				return
			})
		}

		return
	})

	return
}
