package db

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *metadataStorage) Save(ctx context.Context, tenant string, attachmentID domain.MetadataID, attachment domain.Metadata) (err error) {
	err = svc.connections.WithTenant(tenant, svc.collectionName, func(connection gopersist.PersistancePort) (err error) {
		err = connection.Save(ctx, attachmentID.String(), attachment)
		return
	})

	return
}
