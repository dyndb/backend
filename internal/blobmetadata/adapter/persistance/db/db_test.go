//go:build !production

package db_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/adapter/persistance"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/adapter/persistance/db"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestIt(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	metadata := db.NewBlobMetaPersistance(connections, "attachments")
	_ = metadata
	persistance.TestBlobMetadataAdd(t, "testdyndb", metadata)
	persistance.TestBlobMetadataGet(t, "testdyndb", metadata)
	persistance.TestBlobMetadataDelete(t, "testdyndb", metadata)
}
