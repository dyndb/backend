package blobmetadata

import (
	dbpersistance "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/adapter/persistance/db"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/service/blobmetadataactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/service/manager"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

type Metadata domain.Metadata

type PersistancePort ports.BlobMetadataPersistance
type Manager usecase.BlobMetadataManager

func NewPersistanceAdapter(connections connections.Management) PersistancePort {
	return dbpersistance.NewBlobMetaPersistance(connections, "attachments")
}

func NewManager(persistance PersistancePort) Manager {
	return manager.NewMetadataManager(persistance)
}

// actors
func NewSaveActor(ctr goactor.Concentrator, metadata Manager) (actor goactor.Actor) {
	return blobmetadataactors.NewSaveActor(ctr, metadata)
}

func NewListActor(ctr goactor.Concentrator, metadata Manager) (actor goactor.Actor) {
	return blobmetadataactors.NewListActor(ctr, metadata)
}

func NewSearchActor(ctr goactor.Concentrator, metadata Manager) (actor goactor.Actor) {
	return blobmetadataactors.NewSearchActor(ctr, metadata)
}

func NewDeleteActor(ctr goactor.Concentrator, metadata Manager) (actor goactor.Actor) {
	return blobmetadataactors.NewDeleteActor(ctr, metadata)
}
