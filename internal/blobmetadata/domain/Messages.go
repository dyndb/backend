package domain

type BlobMetadataSaveRequest struct {
	Tenant     string     // if not set, try to get it from AuthValidated{}
	MetadataID MetadataID // if not set, will be generated
	Metadata   Metadata
}

type BlobMetadataSaved struct {
	MetadataID MetadataID
}

type BlobMetadataListRequest struct {
	Tenant      string       // if not set, try to get it from AuthValidated{}
	MetadataIDs []MetadataID // get metadata by metadata-ids ( optional )
	BlobIDs     []string     // get metadata by blob-ids ( optional )

	// paging
	PageKey   string // use "" to start from beginning
	PageLimit int64
}

type BlobMetadataList struct {
	List        MetadataList
	NextPageKey string
}

type BlobMetadataSearchRequest struct {
	Tenant      string // if not set, try to get it from AuthValidated{}
	SearchWords []string
	PageKey     string
	PageLimit   int64
}

type BlobMetadataDeleteRequest struct {
	Tenant     string // if not set, try to get it from AuthValidated{}
	MetadataID MetadataID
}

type BlobMetadataDeleted struct {
	MetadataID MetadataID
}
