package domain

import (
	"os"
	"time"

	"github.com/h2non/filetype"
	"github.com/h2non/filetype/types"
)

type Metadata struct {
	DisplayText string `json:"displayText,omitempty"` // the visible text

	FileNameOriginal string    `json:"filenameoriginal,omitempty"` // the original filename with extension
	FileSizeBytes    int64     `json:"bytes,omitempty"`            // the filesize in bytes
	Mimetype         string    `json:"mimetype,omitempty"`         // the mimetype if the file
	UploadDateTime   time.Time `json:"uploaddatetime,omitempty"`

	BucketName string `json:"blobbucket,omitempty"` // in which bucket this attachments is saved
	BlobID     string `json:"blobid,omitempty"`     // blob id inside the bucked. This is mainly the sha-checksum

}

// ReadFromFile read the following infos from an local file
//
// - FileSizeBytes
//
// - Mimetype
func (meta *Metadata) ReadFromFile(filePath string) {

	fileInfo, err := os.Stat(filePath)
	if err == nil {

		meta.FileSizeBytes = fileInfo.Size()

		// Open a file descriptor
		var file *os.File
		file, err = os.Open(filePath)

		head := make([]byte, 261)
		if err == nil {
			// We only have to pass the file header = first 261 bytes
			_, err = file.Read(head)
		}

		var kind types.Type
		if err == nil {
			kind, err = filetype.Match(head)
		}

		if err == nil {
			meta.Mimetype = kind.MIME.Value
		}
	}

}

type MetadataList map[MetadataID]Metadata
