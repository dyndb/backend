package domain

import (
	"strings"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

type MetadataID string

func (s *MetadataID) IsEmpty() bool {
	return *s == ""
}

func (s *MetadataID) Generate() {
	*s = MetadataID("bmta-" + uuid.Must(uuid.NewRandom()).String())
}

func (s MetadataID) String() string {
	return string(s)
}

func (s MetadataID) Validate() (err error) {
	if !strings.HasPrefix(string(s), "bmta-") {
		err = errors.WithStack(errors.New("not a id for metadata"))
	}
	return
}

func NewMetadataIDFromString(id string) (newID MetadataID, err error) {
	newID = MetadataID(id)
	err = newID.Validate()
	return
}

func NewMetadataID() (newID MetadataID) {
	newID.Generate()
	return
}

func IsMetadataID(id string) bool {
	return strings.HasPrefix(id, "bmta-")
}
