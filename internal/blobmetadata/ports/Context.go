package ports

import "gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"

type Context interface {
	appcontext.WithContext
	appcontext.WithPanic
}
