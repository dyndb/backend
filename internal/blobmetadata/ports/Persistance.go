package ports

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
)

type ListOpts struct {
	Tenant string

	// Filters
	ByIDs     []domain.MetadataID
	ByBlobIDs []string
	ByWords   []string

	// paging
	PageKey   string
	PageLimit int64
}

type BlobMetadataPersistance interface {
	List(ctx context.Context, opts ListOpts) (attachments domain.MetadataList, err error)
	Save(ctx context.Context, tenant string, attachmentID domain.MetadataID, attachment domain.Metadata) (err error)
	Delete(ctx context.Context, tenant string, attachmentID domain.MetadataID) (err error)
}
