package blobmetadataactors

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewDeleteActor(ctr goactor.Concentrator, metadata usecase.BlobMetadataManager) (actor goactor.Actor) {

	actor = goactor.NewActor("blobMetadataDeleteActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.BlobMetadataDeleteRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			if err == nil {
				err = msg.MetadataID.Validate()
			}

			if err == nil {
				err = metadata.Delete(cancelCtx, msg.Tenant, msg.MetadataID)
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.BlobMetadataDeleted{
					MetadataID: msg.MetadataID,
				})
			}
		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.BlobMetadataDeleteRequest{})

	return
}
