package blobmetadataactors

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewListActor(ctr goactor.Concentrator, metadata usecase.BlobMetadataManager) (actor goactor.Actor) {

	actor = goactor.NewActor("blobMetadataListActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.BlobMetadataListRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &domain.BlobMetadataList{List: make(domain.MetadataList)}

			// MetadataID
			if err == nil {
				var attachmentsFromBlobIDs domain.MetadataList
				attachmentsFromBlobIDs, err = metadata.List(cancelCtx, ports.ListOpts{Tenant: msg.Tenant, ByIDs: msg.MetadataIDs, PageLimit: msg.PageLimit})
				if err == nil {
					for attachmentID, attachment := range attachmentsFromBlobIDs {
						result.List[attachmentID] = attachment
					}
				}
			}

			// BlobIDs
			if err == nil {
				var attachmentsFromBlobIDs domain.MetadataList
				attachmentsFromBlobIDs, err = metadata.List(cancelCtx, ports.ListOpts{Tenant: msg.Tenant, ByBlobIDs: msg.BlobIDs, PageLimit: msg.PageLimit})
				if err == nil {
					for attachmentID, attachment := range attachmentsFromBlobIDs {
						result.List[attachmentID] = attachment
					}
				}
			}

			// DocIDs
			if err == nil {
				var attachmentsFromDocIDs domain.MetadataList
				attachmentsFromDocIDs, err = metadata.List(cancelCtx, ports.ListOpts{Tenant: msg.Tenant, PageLimit: msg.PageLimit})
				if err == nil {
					for attachmentID, attachment := range attachmentsFromDocIDs {
						result.List[attachmentID] = attachment
					}
				}
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}
		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.BlobMetadataListRequest{})

	return
}
