package blobmetadataactors

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewSaveActor(ctr goactor.Concentrator, metadata usecase.BlobMetadataManager) (actor goactor.Actor) {

	actor = goactor.NewActor("blobMetadataSaveActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.BlobMetadataSaveRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			var newAttachmentID domain.MetadataID
			if err == nil {
				newAttachmentID, err = metadata.Save(
					cancelCtx,
					msg.Tenant,
					msg.MetadataID,
					msg.Metadata,
				)
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.BlobMetadataSaved{
					MetadataID: newAttachmentID,
				})
			}
		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.BlobMetadataSaveRequest{})

	return
}
