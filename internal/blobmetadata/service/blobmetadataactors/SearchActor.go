package blobmetadataactors

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewSearchActor(ctr goactor.Concentrator, metadata usecase.BlobMetadataManager) (actor goactor.Actor) {

	actor = goactor.NewActor("blobMetadataSearchActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.BlobMetadataSearchRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &domain.BlobMetadataList{}

			if err == nil {
				result.List, err = metadata.Search(
					cancelCtx,
					msg.Tenant,
					&msg.PageKey, msg.PageLimit,
					msg.SearchWords,
				)
				result.NextPageKey = msg.PageKey
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}
		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.BlobMetadataSearchRequest{})

	return
}
