package blobmetadataactors_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/tests"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func TestSave(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	// we need an global concentrator
	ctr := goactor.NewConcentrator(time.Second * 30)

	// Prepare actors
	tests.PrepareBlobMetadataActors(ctr, connections)

	// test save
	tests.CRUDWithActors(t, ctr, "testdyndb")

}
