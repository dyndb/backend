package manager

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
)

func (s *serviceData) Delete(ctx context.Context, tenant string, MetadataID domain.MetadataID) (err error) {
	return s.persistance.Delete(ctx, tenant, MetadataID)
}
