package manager

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

func (s *serviceData) List(ctx context.Context, opts ports.ListOpts) (attachments domain.MetadataList, err error) {
	return s.persistance.List(ctx, opts)
}
