package manager

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

func (s *serviceData) ListFromID(ctx context.Context, tenant string, MetadataIDs []domain.MetadataID) (attachments domain.MetadataList, err error) {
	return s.persistance.List(ctx, ports.ListOpts{
		Tenant: tenant,
		ByIDs:  MetadataIDs,
	})
}
