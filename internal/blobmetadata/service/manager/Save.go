package manager

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

// Save implements usecase.BlobMetadataManager.
func (s *serviceData) Save(ctx context.Context, tenant string, attachmentID domain.MetadataID, attachment domain.Metadata) (newAttachmentID domain.MetadataID, err error) {

	// validation
	/*
		if err == nil && attachment.BucketName == "" {
			err = errors.WithStack(errors.New("BucketName missing"))
		}

		if err == nil && attachment.BlobID == "" {
			err = errors.WithStack(errors.New("BlobID missing"))
		}
	*/
	if err == nil {
		if !attachmentID.IsEmpty() {

			var attachments domain.MetadataList
			attachments, err = s.persistance.List(ctx, ports.ListOpts{Tenant: tenant, ByIDs: []domain.MetadataID{attachmentID}})

			if err == nil {
				for _, existingAttachment := range attachments {

					var tmpJsonData []byte
					tmpJsonData, err = json.Marshal(attachment)
					if err == nil {
						err = json.Unmarshal(tmpJsonData, &existingAttachment)
					}

					if err == nil {
						attachment = existingAttachment
					}

					break
				}
			}
		} else {
			attachmentID.Generate()
		}
	}

	// update
	if err == nil {
		attachment.UploadDateTime = time.Now()
		err = s.persistance.Save(ctx, tenant, attachmentID, attachment)
	}

	if err == nil {
		newAttachmentID = attachmentID
	}

	return
}
