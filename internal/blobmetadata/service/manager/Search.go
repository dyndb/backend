package manager

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

func (s *serviceData) Search(ctx context.Context, tenant string, pagekey *string, pagelimit int64, words []string) (attachments domain.MetadataList, err error) {
	return s.persistance.List(ctx, ports.ListOpts{
		Tenant:  tenant,
		ByWords: words,
	})
}
