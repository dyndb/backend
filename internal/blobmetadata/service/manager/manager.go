package manager

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
)

type serviceData struct {
	persistance ports.BlobMetadataPersistance
}

func NewMetadataManager(persistance ports.BlobMetadataPersistance) usecase.BlobMetadataManager {

	newServiceData := &serviceData{
		persistance: persistance,
	}

	return newServiceData
}
