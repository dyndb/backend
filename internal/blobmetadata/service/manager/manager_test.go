package manager_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/tests"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestManager(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	// metadata
	metadataPersistance := blobmetadata.NewPersistanceAdapter(connections)
	metadataManager := blobmetadata.NewManager(metadataPersistance)

	tests.TestBlobMetadataPatch(t, "testdyndb", metadataManager)
}
