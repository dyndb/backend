//go:build !production

package tests

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func PrepareBlobMetadataActors(ctr goactor.Concentrator, connections connections.Management) {

	// metadata
	metadataPersistance := blobmetadata.NewPersistanceAdapter(connections)
	metadataManager := blobmetadata.NewManager(metadataPersistance)

	// actors
	blobmetadata.NewSaveActor(ctr, metadataManager)
	blobmetadata.NewListActor(ctr, metadataManager)
	blobmetadata.NewDeleteActor(ctr, metadataManager)
	blobmetadata.NewSearchActor(ctr, metadataManager)

}
