//go:build !production

package tests

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/usecase"
)

func TestBlobMetadataPatch(t *testing.T, tenantName string, manager usecase.BlobMetadataManager) {

	var err error

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	ctx := appcontext.New(ctxBg)

	var firstAttachmentID domain.MetadataID
	firstAttachmentID, err = manager.Save(ctx, tenantName, domain.NewMetadataID(), domain.Metadata{
		DisplayText:      "Datasheet 99",
		FileNameOriginal: "data99.xml",
		BucketName:       "local",
		BlobID:           "0000-0000-0000-0099",
		//DocID:            []string{},
	})
	assert.NoError(t, err)

	// we "patch" an attachment
	_, err = manager.Save(ctx, tenantName, firstAttachmentID, domain.Metadata{
		DisplayText: "Datasheet Second Version",
	})
	assert.NoError(t, err)

	// we read it
	var existingAttachments domain.MetadataList
	existingAttachments, err = manager.List(ctx, ports.ListOpts{Tenant: tenantName, ByIDs: []domain.MetadataID{firstAttachmentID}})
	assert.NoError(t, err)

	// compare it
	assert.Equal(t, "Datasheet Second Version", existingAttachments[firstAttachmentID].DisplayText)

}
