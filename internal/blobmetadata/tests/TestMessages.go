//go:build !production

package tests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	blobDataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func CRUDWithActors(
	t *testing.T,
	ctr goactor.Concentrator,
	tenantName string,
) (
	MetadataID blobMetadataDomain.MetadataID,
	err error,
) {

	// root directory
	var projectRoot string
	projectRoot, _ = tests.FindProjectRoot()

	// read id from file ( basically a hash )
	newBlobID := blobDataDomain.NewBlobID(projectRoot + "/go.mod")
	assert.NoError(t, newBlobID.Validate())

	// Create metadata from local file
	newMetadata := blobMetadataDomain.Metadata{
		BlobID:           newBlobID.String(),
		DisplayText:      "The core module file",
		BucketName:       "local",
		FileNameOriginal: "go.mod",
	}
	newMetadata.ReadFromFile(projectRoot + "./go.mod")

	// try to save it
	newSaveResp := &blobMetadataDomain.BlobMetadataSaved{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataSaveRequest{
			Tenant:     tenantName,
			Metadata:   newMetadata,
			MetadataID: blobMetadataDomain.NewMetadataID(),
		},
		ExpectedReturnMessage: &newSaveResp,
	})
	assert.NoError(t, err)
	assert.False(t, newSaveResp.MetadataID.IsEmpty())

	// list it
	newListResp := &blobMetadataDomain.BlobMetadataList{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataListRequest{
			Tenant: tenantName,
			MetadataIDs: []blobMetadataDomain.MetadataID{
				newSaveResp.MetadataID,
			},
		},
		ExpectedReturnMessage: &newListResp,
	})
	assert.NoError(t, err)
	assert.Len(t, newListResp.List, 1)
	assert.Equal(t, "The core module file", newListResp.List[newSaveResp.MetadataID].DisplayText)

	// find it via blob-id
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataListRequest{
			Tenant:  tenantName,
			BlobIDs: []string{newMetadata.BlobID},
		},
		ExpectedReturnMessage: &newListResp,
	})
	assert.NoError(t, err)
	assert.Len(t, newListResp.List, 1)
	assert.Equal(t, "The core module file", newListResp.List[newSaveResp.MetadataID].DisplayText)

	// search it
	newSearchResp := &blobMetadataDomain.BlobMetadataList{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataSearchRequest{
			Tenant:      tenantName,
			SearchWords: []string{"noexist"},
			PageLimit:   9999,
		},
		ExpectedReturnMessage: &newSearchResp,
	})
	assert.NoError(t, err)
	assert.Len(t, newSearchResp.List, 0)

	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataSearchRequest{
			Tenant:      tenantName,
			SearchWords: []string{"core"},
			PageLimit:   9999,
		},
		ExpectedReturnMessage: &newSearchResp,
	})
	assert.NoError(t, err)
	assert.Len(t, newSearchResp.List, 1)

	// delete all
	messages := []concentrator.Job{}
	for MetadataID := range newListResp.List {
		messages = append(messages, concentrator.Job{
			Message: &blobMetadataDomain.BlobMetadataDeleteRequest{
				Tenant:     tenantName,
				MetadataID: MetadataID,
			},
		})
	}
	ctr.WaitForMessages(nil, messages...)

	// list should be empty
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &blobMetadataDomain.BlobMetadataListRequest{
			Tenant: tenantName,
			MetadataIDs: []blobMetadataDomain.MetadataID{
				newSaveResp.MetadataID,
			},
		},
		ExpectedReturnMessage: &newListResp,
	})
	assert.NoError(t, err)
	assert.Len(t, newListResp.List, 0)

	return
}
