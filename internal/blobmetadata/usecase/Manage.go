package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/ports"
)

type BlobMetadataManager interface {
	List(ctx context.Context, opts ports.ListOpts) (attachments domain.MetadataList, err error)

	Save(ctx context.Context, tenant string, attachmentID domain.MetadataID, attachment domain.Metadata) (newAttachmentID domain.MetadataID, err error)
	Delete(ctx context.Context, tenant string, MetadataID domain.MetadataID) (err error)

	Search(ctx context.Context, tenant string, pagekey *string, pagelimit int64, words []string) (attachments domain.MetadataList, err error)
}
