package blobpreview

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/services/blobpreviewactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/services/gotenberg"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
)

type States domain.PreviewState
type GeneratePreviewUsecase usecase.FileGeneratePreview

func NewService(baseurl string) GeneratePreviewUsecase {
	return gotenberg.New(baseurl, map[string]string{
		".go": ".txt",
	})
}

// actors

func NewGenerateActor(
	ctr goactor.Concentrator,
	previewer GeneratePreviewUsecase,
	timeout time.Duration,
) (actor goactor.Actor) {
	return blobpreviewactors.NewGenerateActor(ctr, previewer, timeout)
}
