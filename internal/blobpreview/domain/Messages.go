package domain

type GenerateRequest struct {
	ReqID          string // an reqID to identify the request
	SourceFilePath string
}

type Generated struct {
	State    PreviewState
	FilePath string
}
