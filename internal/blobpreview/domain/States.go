package domain

type PreviewState int

const PreviewStateUnknown PreviewState = PreviewState(0)
const PreviewStateIsMissing PreviewState = PreviewState(1)
const PreviewStateIsOngoing PreviewState = PreviewState(2)
const PreviewStateIsDone PreviewState = PreviewState(3)
const PreviewStateErr PreviewState = PreviewState(4)
