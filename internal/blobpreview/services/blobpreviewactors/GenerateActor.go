package blobpreviewactors

import (
	"context"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewGenerateActor(
	ctr goactor.Concentrator,
	previewer usecase.FileGeneratePreview,
	timeout time.Duration,
) (actor goactor.Actor) {

	actor = goactor.NewActor("previewGenerateActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		case *domain.GenerateRequest:

			reqID := domain.PreviewReqID(msg.ReqID)

			ctx, ctxCancel := context.WithTimeout(context.Background(), timeout)
			defer ctxCancel()

			var previewFilePath string
			previewFilePath, err = previewer.GeneratePreviewRequest(ctx, reqID, msg.SourceFilePath, "/tmp/")

			if err == nil {
				returnMessage = append(returnMessage, &domain.Generated{
					State:    domain.PreviewStateIsDone,
					FilePath: previewFilePath,
				})
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.GenerateRequest{})

	return
}
