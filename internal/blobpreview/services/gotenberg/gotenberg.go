package gotenberg

import (
	"bytes"
	"context"
	"errors"
	"io"
	"io/fs"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/usecase"
)

type serviceData struct {
	baseurl string

	ongoingLock sync.RWMutex
	ongoing     map[domain.PreviewReqID]chan interface{}

	extensionRemap map[string]string
}

// New create a new previewer based on gotenberg
// osFileTimeout: Time returned files by this package are open, if time is up os.File will be closed !
func New(baseurl string, extensionsMap map[string]string) (previewer usecase.FileGeneratePreview) {

	previewer = &serviceData{
		baseurl: baseurl,

		ongoingLock: sync.RWMutex{},
		ongoing:     make(map[domain.PreviewReqID]chan interface{}),

		extensionRemap: extensionsMap,
	}

	// /health
	ctx, ctxCancel := context.WithTimeout(context.Background(), time.Second*30)
	defer ctxCancel()

	r, _ := http.NewRequestWithContext(ctx, "GET", baseurl+"/health", nil)
	client := &http.Client{}
	resp, err := client.Do(r)

	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 200 {
		panic(errors.New("service unavailable"))
	}

	return
}

// GeneratePreview implements usecase.FileGeneratePreview.
func (svc *serviceData) GeneratePreviewRequest(ctx context.Context, reqID domain.PreviewReqID, sourceFilePath string, targetFolder string) (previewFilePath string, err error) {

	// prepare
	targetFolder, _ = strings.CutSuffix(targetFolder, "/")

	// vars
	sourceFileName := filepath.Base(sourceFilePath)
	sourceFileExt := filepath.Ext(sourceFilePath)
	previewFilePath = targetFolder + "/" + sourceFileName + ".preview.pdf"

	// ###################################### Locking ######################################
	svc.ongoingLock.Lock()
	waitChan, exist := svc.ongoing[reqID]
	if !exist {
		waitChan = make(chan interface{})
		svc.ongoing[reqID] = waitChan
		svc.ongoingLock.Unlock()
	} else {
		// wait close of channel
		svc.ongoingLock.Unlock()
		<-svc.ongoing[reqID]
		return
	}

	defer func() {
		svc.ongoingLock.Lock()
		close(waitChan)
		delete(svc.ongoing, reqID)
		svc.ongoingLock.Unlock()
	}()

	// ###################################### Remap extension ######################################
	// remap extensions
	if newSourceFileExt, exist := svc.extensionRemap[sourceFileExt]; exist {
		newSourceFilePath := targetFolder + "/" + strings.Replace(sourceFileName, sourceFileExt, newSourceFileExt, 1)
		os.Remove(newSourceFilePath)
		err = os.Link(sourceFilePath, newSourceFilePath)
		defer os.Remove(newSourceFilePath)
		sourceFilePath = newSourceFilePath
	}

	// multipart writer
	body := &bytes.Buffer{}
	var writer *multipart.Writer
	if err == nil {
		writer = multipart.NewWriter(body)
	}

	// Open the source
	var sourceFile *os.File
	if err == nil {
		sourceFile, err = os.Open(sourceFilePath)
	}

	// add file
	if err == nil {
		var part io.Writer
		part, err = writer.CreateFormFile("file", sourceFilePath)
		if err == nil {
			_, err = io.Copy(part, sourceFile)
		}
	}

	// add option
	if err == nil {
		err = writer.WriteField("pdfa", "PDF/A-1b")
	}

	// finish
	if err == nil {
		err = writer.Close()
	}

	var resp *http.Response
	if err == nil {
		r, _ := http.NewRequestWithContext(ctx, "POST", svc.baseurl+"/forms/libreoffice/convert", body)
		r.Header.Add("Content-Type", writer.FormDataContentType())

		client := &http.Client{}
		resp, err = client.Do(r)
	}

	var respFileData []byte
	if err == nil {
		respFileData, err = io.ReadAll(resp.Body)
	}

	if err == nil {
		if resp.StatusCode != 200 {
			err = errors.New(string(respFileData))
		}
	}

	if err == nil {
		err = os.WriteFile(
			previewFilePath,
			respFileData,
			fs.FileMode(0777),
		)
	}

	return
}
