package gotenberg_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/services/gotenberg"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

/*
curl \
--request POST http://localhost:3000/forms/libreoffice/convert \
--form files=@/path/to/file.docx \
--form landscape=true \
--form nativePageRanges=1-5 \
*/

func TestSync(t *testing.T) {

	var syncer chan interface{} = make(chan interface{})

	go func() {
		log.Debug().Msg("1: start")
		<-syncer
		log.Debug().Msg("1: end")
	}()
	go func() {
		log.Debug().Msg("2: start")
		<-syncer
		log.Debug().Msg("2: end")
	}()

	time.Sleep(time.Second * 3)
	close(syncer)
	time.Sleep(time.Second * 1)
}

func TestConvert(t *testing.T) {

	projectRoot, err := tools.FindProjectRoot()
	if err != nil {
		t.FailNow()
	}

	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*30)
	defer ctxCancel()

	newConverter := gotenberg.New("http://localhost:3000", map[string]string{
		".go": ".txt",
	})

	curBlobFileName := projectRoot + "/internal/tests/testassets/image.png"

	var previewFilePath string
	go func() {
		if err == nil {
			previewFilePath, err = newConverter.GeneratePreviewRequest(ctxTimeout, domain.PreviewReqID(curBlobFileName), curBlobFileName, "/tmp/")
			assert.NoError(t, err)
			assert.NotEmpty(t, previewFilePath)
		}
	}()

	time.Sleep(time.Millisecond * 10)

	previewFilePath, err = newConverter.GeneratePreviewRequest(ctxTimeout, domain.PreviewReqID(curBlobFileName), curBlobFileName, "/tmp/")
	assert.NoError(t, err)
	assert.NotNil(t, previewFilePath)

	os.Remove(previewFilePath)

	// wait until finish

	/*
		file, _ := os.Open(projectRoot + "/internal/tests/testassets/test.txt")
		defer file.Close()

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)

		part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
		io.Copy(part, file)

		writer.WriteField("pdfa", "PDF/A-1b")

		writer.Close()

		r, _ := http.NewRequest("POST", "http://localhost:3000/forms/libreoffice/convert", body)
		r.Header.Add("Content-Type", writer.FormDataContentType())
		client := &http.Client{}

		var resp *http.Response
		resp, _ = client.Do(r)

		var respFileData []byte
		respFileData, _ = io.ReadAll(resp.Body)

		os.WriteFile(
			"/tmp/test.pdf",
			respFileData,
			fs.FileMode(0777),
		)
	*/
}
