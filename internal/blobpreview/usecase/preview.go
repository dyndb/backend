package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobpreview/domain"
)

type FileGeneratePreview interface {
	GeneratePreviewRequest(ctx context.Context, reqID domain.PreviewReqID, sourceFilePath string, targetFolder string) (previewFilePath string, err error)
}
