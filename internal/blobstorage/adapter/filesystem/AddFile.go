package filesystem

import (
	"os"
	"path/filepath"

	"github.com/gabriel-vasile/mimetype"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

func (svc *bucketAdapter) AddFile(tenant string, subDir string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error) {

	// validation
	if err == nil && tenant == "" {
		err = errors.New("tenant missing")
		err = errors.WithStack(err)
	}
	if err == nil && sourceFilePath == "" {
		err = errors.New("sourceFilePath missing")
		err = errors.WithStack(err)
	}

	// we need an absolut path
	if err == nil {
		sourceFilePath, err = filepath.Abs(sourceFilePath)
	}

	// ext
	sourceFileExt := filepath.Ext(sourceFilePath)
	if sourceFileExt == "" {
		log.Warn().Str("filepath", sourceFilePath).Msg("extension missing, try to detect it")

		var mtype *mimetype.MIME
		mtype, err = mimetype.DetectFile(sourceFilePath)
		if err == nil {
			sourceFileExt = mtype.Extension()
		}

		if sourceFileExt == "" {
			err = errors.New("extension missing and can not detected")
			err = errors.WithStack(err)
		}
	}

	// we need the temp-path
	targetPath := svc.pathDynDB + "/" + tenant + "/" + subDir

	// create folders
	if err == nil {
		err = os.MkdirAll(targetPath, 0700)
		err = errors.WithStack(err)
	}

	// generade blobid ( hash )
	if err == nil {
		if blobID.IsEmpty() {
			blobID.Generate(sourceFilePath)
		}
		newBlobID = blobID
	}

	// we copy the file with extension
	if err == nil {
		err = tools.FileCopyChunked(sourceFilePath, targetPath+"/"+blobID.String()+sourceFileExt)
	}

	// link it
	if err == nil {
		err = tools.FileLink(targetPath+"/"+blobID.String()+sourceFileExt, targetPath+"/"+blobID.String())
	}

	return
}
