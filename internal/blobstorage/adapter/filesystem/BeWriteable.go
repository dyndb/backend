package filesystem

import (
	"os"
)

func (svc *bucketAdapter) BeWriteable() (err error) {

	err = os.MkdirAll(svc.pathDynDB, 0700)

	var file *os.File
	if err == nil {
		file, err = os.CreateTemp(svc.pathDynDB, "writeable")
	}

	if err == nil {
		defer file.Close()
		os.Remove(file.Name())
	}

	return
}
