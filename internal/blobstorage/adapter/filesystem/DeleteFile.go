package filesystem

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/errwrap"
)

func (svc *bucketAdapter) DeleteFile(tenant string, subDir string, blobID domain.BlobID) (err error) {

	err = blobID.Validate()
	err = errwrap.WithStack(err)

	var linkFilePath string
	if err == nil {
		linkFilePath = fmt.Sprintf("%s/%s/%s/%s", svc.pathDynDB, tenant, subDir, blobID.String())
	}

	var tergetFilePath string
	if err == nil {
		tergetFilePath, err = filepath.EvalSymlinks(linkFilePath)
	}

	if err == nil {
		err = os.Remove(linkFilePath)
	}
	if err == nil {
		err = os.Remove(tergetFilePath)
	}

	// file not exist -> deleted
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			err = nil
		}
	}

	return
}
