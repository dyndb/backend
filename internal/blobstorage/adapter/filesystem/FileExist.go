package filesystem

import (
	"fmt"
	"os"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/errwrap"
)

func (svc *bucketAdapter) FileExist(tenant string, subDir string, blobID domain.BlobID) (exist bool, err error) {

	err = blobID.Validate()
	err = errwrap.WithStack(err)

	var tergetFilePath string
	if err == nil {
		tergetFilePath = fmt.Sprintf("%s/%s/%s/%s", svc.pathDynDB, tenant, subDir, blobID.String())
	}

	if err == nil {
		file, err := os.OpenFile(tergetFilePath, os.O_RDONLY, 0)
		if err == nil {
			file.Close()
			exist = true
		}
	}

	return
}
