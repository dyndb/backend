package filesystem

import (
	"os"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
	"gitlab.com/nerdhaltig/errwrap"
)

func (svc *bucketAdapter) OpenFile(tenant string, subDir string, blobID domain.BlobID) (filePath string, err error) {

	err = blobID.Validate()
	err = errwrap.WithStack(err)

	blobsPath := svc.pathDynDB + "/" + tenant + "/" + subDir
	blobSourcePath := blobsPath + "/" + blobID.String()

	blobSourcePathExist := tools.FileExist(blobSourcePath)
	if !blobSourcePathExist {
		err = os.ErrNotExist
	}

	// read target file of link
	if err == nil {
		filePath, err = os.Readlink(blobSourcePath)
	}

	if err == nil {
		filePath = blobsPath + "/" + filePath
	}

	if err != nil {
		filePath = ""
	}
	return
}
