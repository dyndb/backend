package filesystem

import (
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/errwrap"
)

type bucketAdapter struct {
	name      string
	pathDynDB string
}

// NewBlobStorageAdapter can save blobs ( files ) to an local filesystem
func NewBlobStorageAdapter(name, path string) ports.BlobStorageAdapter {
	if name == "" || path == "" {
		panic("name or path can not be empty")
	}

	var err error
	newBucketAdapter := &bucketAdapter{
		name: name,
	}

	// create absolut paths
	if err == nil {
		path, err = filepath.Abs(path)
	}

	if err == nil {
		newBucketAdapter.pathDynDB = strings.TrimRight(path, "/") + "/dyndb"
	}

	// create folder if needed
	if err == nil {
		err = os.MkdirAll(path, 0700)
	}

	// check if folder is writeable
	if err == nil {
		err = newBucketAdapter.BeWriteable()
	}

	errwrap.PanicOnError(err)
	return newBucketAdapter
}
