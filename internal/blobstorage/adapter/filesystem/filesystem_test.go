package filesystem_test

import (
	"io/fs"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/filesystem"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/tests"
)

func TestFilesystemCopy(t *testing.T) {
	var err error
	var exist bool

	bucketSubPath := "blobs2"
	bucket := filesystem.NewBlobStorageAdapter("test", "./")

	// create a temp-file
	err = os.MkdirAll("./dyndb/test/tmp", 0700)
	assert.NoError(t, err)

	file, err := os.CreateTemp("./dyndb/test/tmp", "dyndbblobtest")
	assert.NoError(t, err)
	defer file.Close()

	// add the file
	var srcFileAbsolut string
	srcFileAbsolut, err = filepath.Abs(file.Name())
	assert.NoError(t, err)

	var blobID domain.BlobID
	blobID, err = bucket.AddFile("test", bucketSubPath, srcFileAbsolut, "")
	assert.NoError(t, err)
	assert.False(t, blobID.IsEmpty())

	// the file should exist
	exist, err = bucket.FileExist("test", bucketSubPath, blobID)
	assert.True(t, exist)
	assert.NoError(t, err)

	// the file should be a link
	var fileInfo fs.FileInfo
	fileInfo, err = os.Lstat("./dyndb/test/" + bucketSubPath + "/" + blobID.String())
	assert.NoError(t, err)
	assert.True(t, fileInfo.Mode()&os.ModeSymlink == os.ModeSymlink)

	// delete the file
	err = bucket.DeleteFile("test", bucketSubPath, domain.BlobID(blobID.String()))
	assert.NoError(t, err)

	// the file should  not exist
	exist, err = bucket.FileExist("test", bucketSubPath, blobID)
	assert.False(t, exist)
	assert.NoError(t, err)

}

func TestOpenFile(t *testing.T) {

	// everything from /tmp will be copyed, everything else will symlinked
	bucket := filesystem.NewBlobStorageAdapter("test", "./")

	tests.TestOpenFile(t, bucket)
}
