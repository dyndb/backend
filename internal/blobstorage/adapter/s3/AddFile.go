package s3

import (
	"context"
	"path/filepath"

	"github.com/gabriel-vasile/mimetype"
	"github.com/minio/minio-go/v7"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

// AddFile implements ports.BlobStorageAdapter.
func (adp *bucketAdapter) AddFile(tenant string, subDir string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error) {

	ctx := context.Background()

	bucketName := "dyndb" + tenant

	// create bucket if needed
	exists, errBucketExists := adp.minioClient.BucketExists(ctx, bucketName)
	if errBucketExists == nil && !exists {
		err = adp.minioClient.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "local"})
	}

	// generade blobid ( hash )
	if err == nil {
		if blobID.IsEmpty() {
			blobID.Generate(sourceFilePath)
		}
		newBlobID = blobID
	}

	// read mimetype
	var mtype *mimetype.MIME
	if err == nil {
		mtype, err = mimetype.DetectFile(sourceFilePath)
		if err != nil {
			mtype = &mimetype.MIME{}
			err = nil
		}
	}

	// ext
	sourceFileExt := filepath.Ext(sourceFilePath)

	objectName := subDir + "/" + newBlobID.String()

	// Upload the test file with FPutObject
	var info minio.UploadInfo
	if err == nil {
		info, err = adp.minioClient.FPutObject(ctx, bucketName, objectName, sourceFilePath,
			minio.PutObjectOptions{
				ContentType:          mtype.String(),
				ServerSideEncryption: adp.encryption,
				UserMetadata: map[string]string{
					"Extension": sourceFileExt,
				},
			},
		)
		_ = info
	}

	return
}
