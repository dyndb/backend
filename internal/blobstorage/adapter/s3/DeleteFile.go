package s3

import (
	"context"

	"github.com/minio/minio-go/v7"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

// DeleteFile implements ports.BlobStorageAdapter.
func (adp *bucketAdapter) DeleteFile(tenant string, subDir string, blobID domain.BlobID) (err error) {
	ctx := context.Background()

	bucketName := "dyndb" + tenant
	objectName := subDir + "/" + blobID.String()

	err = adp.minioClient.RemoveObject(ctx, bucketName, objectName, minio.RemoveObjectOptions{})

	return
}
