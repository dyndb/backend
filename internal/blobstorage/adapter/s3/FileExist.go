package s3

import (
	"context"

	"github.com/minio/minio-go/v7"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/errwrap"
)

// FileExist implements ports.BlobStorageAdapter.
func (adp *bucketAdapter) FileExist(tenant string, subDir string, blobID domain.BlobID) (exist bool, err error) {
	ctx := context.Background()

	err = blobID.Validate()
	err = errwrap.WithStack(err)

	bucketName := "dyndb" + tenant

	// create bucket if needed
	if err == nil {
		exists, errBucketExists := adp.minioClient.BucketExists(ctx, bucketName)
		if errBucketExists == nil && !exists {
			err = adp.minioClient.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "local"})
		}
	}

	objectName := subDir + "/" + blobID.String()

	if err == nil {
		_, err = adp.minioClient.StatObject(ctx, bucketName, objectName,
			minio.GetObjectOptions{
				ServerSideEncryption: adp.encryption,
			},
		)

	}

	if err == nil {
		exist = true
	} else {
		if err.Error() == "The specified key does not exist." {
			err = nil
		}
	}

	return
}
