package s3

import (
	"context"

	"github.com/minio/minio-go/v7"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

// OpenFile implements ports.BlobStorageAdapter.
func (adp *bucketAdapter) OpenFile(tenant string, subDir string, blobID domain.BlobID) (filePath string, err error) {
	ctx := context.Background()

	bucketName := "dyndb" + tenant
	objectName := subDir + "/" + blobID.String()

	var attr minio.ObjectInfo
	attr, err = adp.minioClient.StatObject(ctx, bucketName, objectName, minio.GetObjectOptions{
		ServerSideEncryption: adp.encryption,
	})

	var extension string
	if err == nil {
		extension = attr.UserMetadata["Extension"]
	}

	// download blob
	if err == nil {
		filePath = adp.tmpPath + "/" + attr.Key + extension
		err = adp.minioClient.FGetObject(ctx, bucketName, objectName, filePath, minio.GetObjectOptions{
			ServerSideEncryption: adp.encryption,
		})
	}

	if err != nil {
		filePath = ""
	}
	return
}
