package s3

import (
	"context"
	"crypto/tls"
	"encoding/base64"
	"net/http"
	"os"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio-go/v7/pkg/encrypt"
)

type bucketAdapter struct {
	name string

	minioClient *minio.Client
	encryption  encrypt.ServerSide
	tmpPath     string // where we temporary download from s3
}

type NewS3StorageOpts struct {
	Name string

	Endpoint             string
	AccessKeyID          string
	AccessKey            string
	UseSSL               bool
	IgnoreSelfSignedCert bool
	EncryptionKey        string // can be empty to disable encryption

	// when open a file, it will be first downloaded to there !
	// it will not be cleaned automatically !!!
	// defaults to /tmp/dyndb/cache/s3
	TmpPath string
}

func NewS3Storage(opts NewS3StorageOpts) (adapter ports.BlobStorageAdapter, err error) {

	adp := &bucketAdapter{
		name:    opts.Name,
		tmpPath: opts.TmpPath,
	}

	// temporary folder
	if adp.tmpPath == "" {
		adp.tmpPath = "/tmp/dyndb/cache/s3"
	}
	err = os.MkdirAll(adp.tmpPath, 0700)

	// encryption key
	if err == nil && opts.EncryptionKey != "" {
		len := base64.StdEncoding.DecodedLen(len(opts.EncryptionKey))
		tmpBytes := make([]byte, len)

		var decodedBytes int
		decodedBytes, err = base64.StdEncoding.Decode(tmpBytes, []byte(opts.EncryptionKey))

		encryptionKey := make([]byte, decodedBytes)
		copy(encryptionKey, tmpBytes[:decodedBytes])

		if err == nil {
			adp.encryption, err = encrypt.NewSSEC(encryptionKey)
		}
		if err != nil {
			panic(err)
		}
	}

	// Initialize minio client object.
	if err == nil {
		minioOptions := &minio.Options{
			Creds:  credentials.NewStaticV4(opts.AccessKeyID, opts.AccessKey, ""),
			Secure: opts.UseSSL,
		}

		if opts.IgnoreSelfSignedCert {
			minioOptions.Secure = true
			minioOptions.Transport = &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
		}

		adp.minioClient, err = minio.New(opts.Endpoint, minioOptions)
	}

	// check connection
	if err == nil {
		_, err = adp.minioClient.BucketExists(context.Background(), "dummy")
	}

	return adp, err
}

// Name implements ports.BlobStorageAdapter.
func (b *bucketAdapter) Name() string {
	return b.name
}

// BeWriteable implements ports.BlobStorageAdapter.
func (b *bucketAdapter) BeWriteable() (err error) {
	return nil
}
