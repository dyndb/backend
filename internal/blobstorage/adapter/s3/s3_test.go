package s3_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/s3"
	blobTests "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/tests"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestOpenFile(t *testing.T) {

	url, err := tests.MinioPrepare(t)
	if err != nil {
		panic(err)
	}

	// 	String cmdTpl = "mc config host add myminio http://host.testcontainers.internal:%s %s %s --api s3v4 && "
	// 	+ "mc admin user add myminio %s %s readwrite";
	// String cmd = String.format(cmdTpl, mappedPort, ADMIN_ACCESS_KEY, ADMIN_SECRET_KEY, USER_ACCESS_KEY, USER_SECRET_KEY);
	bucket, err := s3.NewS3Storage(s3.NewS3StorageOpts{
		Endpoint: url,
		//Endpoint:    "127.0.0.1:9000",
		AccessKeyID: "localtest",
		AccessKey:   "localpassword",

		UseSSL:               true,
		IgnoreSelfSignedCert: true,
		// cat /dev/urandom | head -c 32 | base64 -w 0
		EncryptionKey: "dftlruBd60abE7E0vu+w+hgPcz/HzGRyQXweYVQgNzw=",
	})
	assert.NoError(t, err)
	assert.NotNil(t, bucket)

	blobTests.TestOpenFile(t, bucket)

}
