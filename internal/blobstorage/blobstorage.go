package blobstorage

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/filesystem"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/s3"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/blobactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/management"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
)

// Storage backends
type Adapter ports.BlobStorageAdapter

func NewBlobStorageFilesystemAdapter(name, path string) Adapter {
	return filesystem.NewBlobStorageAdapter(name, path)
}

type NewS3StorageOpts s3.NewS3StorageOpts

func NewBlobStorageS3Adapter(opts NewS3StorageOpts) (adapter ports.BlobStorageAdapter, err error) {
	return s3.NewS3Storage(s3.NewS3StorageOpts{
		Name:                 opts.Name,
		Endpoint:             opts.Endpoint,
		AccessKeyID:          opts.AccessKeyID,
		AccessKey:            opts.AccessKey,
		UseSSL:               opts.UseSSL,
		IgnoreSelfSignedCert: opts.IgnoreSelfSignedCert,
		EncryptionKey:        opts.EncryptionKey,
		TmpPath:              opts.TmpPath,
	})
}

// Manager
type Management usecase.BlobManagement

func NewBlobManagement() Management {
	return management.NewBlobManagement()
}

// actors
func NewDefaultBucketActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.DefaultBucketActor(ctr, blobs)
}

func NewSaveActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.NewSaveActor(ctr, blobs)
}

func NewPreviewSaveActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.NewPreviewSaveActor(ctr, blobs)
}

func NewOpenActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.NewOpenActor(ctr, blobs)
}

func NewPreviewOpenActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.NewPreviewOpenActor(ctr, blobs)
}

func NewDeleteActor(ctr goactor.Concentrator, blobs Management) (actor goactor.Actor) {
	return blobactors.NewDeleteActor(ctr, blobs)
}
