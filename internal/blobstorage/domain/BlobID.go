package domain

import (
	"strings"

	"github.com/codingsince1985/checksum"
	"github.com/pkg/errors"
)

type BlobID string

func (s *BlobID) IsEmpty() bool {
	return *s == ""
}

func (s *BlobID) Generate(filePath string) {
	sha256, err := checksum.SHA256sum(filePath)
	if err == nil {
		*s = BlobID("blob-" + sha256)
	}
}

func (s BlobID) String() string {
	return string(s)
}

func (s BlobID) Validate() (err error) {
	if !strings.HasPrefix(string(s), "blob-") {
		err = errors.WithStack(errors.New("not a id for a blob"))
	}
	return
}

// NewBlobIDFromString load the string and valide the blob-id
func NewBlobIDFromString(id string) (newID BlobID, err error) {
	newID = BlobID(id)
	err = newID.Validate()
	return
}

// NewBlobID generate a new blob-id
func NewBlobID(filePath string) (newID BlobID) {
	newID.Generate(filePath)
	return
}
