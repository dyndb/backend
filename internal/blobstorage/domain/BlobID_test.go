package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

func TestBlobID(t *testing.T) {

	projectRoot, err := tools.FindProjectRoot()
	if err != nil {
		t.FailNow()
	}

	curID := NewBlobID(projectRoot + "/internal/blobstorage/domain/BlobID.go")
	secondID := NewBlobID(projectRoot + "/internal/blobstorage/domain/BlobID.go")
	assert.Equal(t, curID.String(), secondID.String())
}
