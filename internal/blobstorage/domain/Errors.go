package domain

import "github.com/pkg/errors"

var (
	ErrMissingBucketName = errors.New("no bucket selected")
	ErrBucketMissing     = errors.New("bucket not exist")
)
