package domain

// Request name of default bucket-name
type BlobStorageDefaultNameRequest struct{}
type BlobStorageDefaultName struct {
	Name string
}

type BlobSaveRequest struct {
	Tenant         string
	BucketName     string
	BlobID         BlobID
	SourceFilePath string
}

type BlobSaved struct {
	BlobID BlobID
}

type BlobOpenRequest struct {
	Tenant     string
	BucketName string
	BlobID     BlobID
}

type BlobOpened struct {
	FilePath string
}

type BlobDeleteRequest struct {
	Tenant     string
	BucketName string
	BlobID     BlobID
}
type BlobDeleted struct{}

type PreviewSaveRequest struct {
	Tenant         string
	BucketName     string
	BlobID         BlobID
	SourceFilePath string
}

type PreviewSaved struct {
	BlobID BlobID
}

type PreviewOpenRequest struct {
	Tenant     string
	BucketName string
	BlobID     BlobID
}

type PreviewOpened struct {
	FilePath string
}
