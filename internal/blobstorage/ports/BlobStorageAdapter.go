package ports

import "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"

type BlobStorageAdapter interface {
	Name() string

	BeWriteable() (err error)

	// AddFile add an file to blob-storage
	//
	// if blobID is empty, we create a new ID and return it in newBlobID
	// if sourceFilePath is in temp-directory, we copy the file, because it will be deleted
	AddFile(tenant string, subDir string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error)

	FileExist(tenant string, subDir string, blobID domain.BlobID) (exist bool, err error)

	// OpenFile try to open a blob inside subDir + Tenant
	// if the blob not exist, file will be nil and err also
	OpenFile(tenant string, subDir string, blobID domain.BlobID) (filePath string, err error)

	DeleteFile(tenant string, subDir string, blobID domain.BlobID) (err error)
}
