package blobactors

import (
	blobStorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func DefaultBucketActor(ctr goactor.Concentrator, blobs usecase.BlobManagement) (actor goactor.Actor) {

	actor = goactor.NewActor("BlobStorageDefaultNameActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch ctx.Message().(type) {

		case *blobStorageDomain.BlobStorageDefaultNameRequest:

			result := &blobStorageDomain.BlobStorageDefaultName{}

			// Get
			if err == nil {
				result.Name = blobs.BucketDefaultName()
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}

		}
		return
	})

	ctr.HookActor(actor, &blobStorageDomain.BlobStorageDefaultNameRequest{})

	return
}
