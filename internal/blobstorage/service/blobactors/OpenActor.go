package blobactors

import (
	"github.com/pkg/errors"
	blobStorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewOpenActor(ctr goactor.Concentrator, blobs usecase.BlobManagement) (actor goactor.Actor) {

	actor = goactor.NewActor("blobDataOpenActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *blobStorageDomain.BlobOpenRequest:

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &blobStorageDomain.BlobOpened{}

			// Get
			if err == nil {
				result.FilePath, err = blobs.GetFile(msg.Tenant, msg.BucketName, msg.BlobID)
				err = errwrap.WithStack(err)
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}

		}
		return
	})

	ctr.HookActor(actor, &blobStorageDomain.BlobOpenRequest{})

	return
}
