package blobactors

import (
	"github.com/pkg/errors"
	blobStorageDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewPreviewOpenActor(ctr goactor.Concentrator, blobs usecase.BlobManagement) (actor goactor.Actor) {

	actor = goactor.NewActor("blobDataPreviewOpenActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *blobStorageDomain.PreviewOpenRequest:
			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &blobStorageDomain.PreviewOpened{}

			// Get
			if err == nil {
				result.FilePath, _ = blobs.GetPreview(msg.Tenant, msg.BucketName, msg.BlobID)
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}

		}
		return
	})

	ctr.HookActor(actor, &blobStorageDomain.PreviewOpenRequest{})

	return
}
