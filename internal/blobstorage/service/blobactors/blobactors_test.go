package blobactors_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/adapter/filesystem"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/blobactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/service/management"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func TestAll(t *testing.T) {

	projectRoot, _ := tests.FindProjectRoot()

	// we need a manager for blob-storages
	blobStorageManager := management.NewBlobManagement()
	localBlobStorageAdapter := filesystem.NewBlobStorageAdapter("local", "./testfolder")
	blobStorageManager.BucketRegister(localBlobStorageAdapter.Name(), localBlobStorageAdapter)
	blobStorageManager.BucketDefaultSet(localBlobStorageAdapter.Name())

	// we need an concentrator
	ctr := goactor.NewConcentrator(time.Second * 15)

	// we need our actors
	blobactors.NewSaveActor(ctr, blobStorageManager)
	blobactors.NewOpenActor(ctr, blobStorageManager)
	blobactors.NewDeleteActor(ctr, blobStorageManager)
	blobactors.DefaultBucketActor(ctr, blobStorageManager)

	// add an blob
	saveResp := &domain.BlobSaved{}
	_, err := ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.BlobSaveRequest{
			Tenant:         "testtenant",
			BlobID:         domain.NewBlobID(projectRoot + "/Makefile"),
			SourceFilePath: projectRoot + "/Makefile",
		},
		ExpectedReturnMessage: &saveResp,
	})
	assert.NoError(t, err)

	// get wrong the blob back
	openResp := &domain.BlobOpened{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.BlobOpenRequest{
			Tenant: "notexist",
			BlobID: saveResp.BlobID,
		},
		ExpectedReturnMessage: &openResp,
	})
	assert.Error(t, err)

	// now from the correct tenant
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.BlobOpenRequest{
			Tenant: "testtenant",
			BlobID: saveResp.BlobID,
		},
		ExpectedReturnMessage: &openResp,
	})
	assert.NoError(t, err)

	// delete
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.BlobDeleteRequest{
			Tenant: "testtenant",
			BlobID: saveResp.BlobID,
		},
	})
	assert.NoError(t, err)

	// should not exist anymore
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.BlobOpenRequest{
			Tenant: "testtenant",
			BlobID: saveResp.BlobID,
		},
		ExpectedReturnMessage: &openResp,
	})
	assert.Error(t, err)
}
