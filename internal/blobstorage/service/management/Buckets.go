package management

import (
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/errwrap"
)

func (svc *serviceData) BucketRegister(BucketName string, adapter ports.BlobStorageAdapter) (err error) {

	if BucketName == "" {
		err = domain.ErrMissingBucketName
		err = errwrap.WithStack(err)
	}

	if err == nil {
		svc.buckets[BucketName] = adapter
	}

	return
}

func (svc *serviceData) BucketDefaultName() string {
	return svc.bucketDefault
}

// BucketDefaultSet implements usecase.BlobManagement.
func (svc *serviceData) BucketDefaultSet(BucketName string) (err error) {
	svc.bucketsLock.Lock()
	defer svc.bucketsLock.Unlock()

	if svc.bucketDefault == "" {
		svc.bucketDefault = BucketName
	} else {
		err = errors.WithStack(fmt.Errorf("an default Bucket '%s' is already set", svc.bucketDefault))
	}

	return
}

func (svc *serviceData) BucketGet(BucketName string) (adapter ports.BlobStorageAdapter, err error) {

	// lock
	svc.bucketsLock.RLock()
	defer svc.bucketsLock.RUnlock()

	if BucketName == "" {
		BucketName = svc.bucketDefault
	}

	if BucketName == "" {
		err = domain.ErrMissingBucketName
		err = errwrap.WithStack(err)
	}

	// check if requested bucket exist
	if err == nil {
		var adapterExist bool
		adapter, adapterExist = svc.buckets[BucketName]
		if !adapterExist {
			err = domain.ErrBucketMissing
		}
	}

	return
}
