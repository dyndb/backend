package management

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
)

func (svc *serviceData) AddPreview(tenant string, BucketName string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	var exist bool
	if err == nil {
		exist, err = blobStorageAdapter.FileExist(tenant, svc.defaultPreviewSubDir, blobID)
	}

	if err == nil && !exist {
		newBlobID, err = blobStorageAdapter.AddFile(tenant, svc.defaultPreviewSubDir, sourceFilePath, blobID)
	}

	return
}

func (svc *serviceData) GetPreview(tenant string, BucketName string, blobID domain.BlobID) (filePath string, err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	if err == nil {
		filePath, err = blobStorageAdapter.OpenFile(tenant, svc.defaultPreviewSubDir, blobID)
	}

	return
}
