package management

import (
	"sync"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/usecase"
)

type serviceData struct {
	bucketDefault string
	bucketsLock   sync.RWMutex
	buckets       map[string]ports.BlobStorageAdapter

	defaultBlobSubDir    string
	defaultPreviewSubDir string
	defaultTempSubDir    string
}

func NewBlobManagement() usecase.BlobManagement {
	return &serviceData{
		buckets: make(map[string]ports.BlobStorageAdapter),

		defaultBlobSubDir:    "blobs",
		defaultPreviewSubDir: "previews",
		defaultTempSubDir:    "temp",
	}
}

// AddFile implements usecase.BlobManagement.
func (svc *serviceData) AddFile(tenant string, BucketName string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	if err == nil {
		newBlobID, err = blobStorageAdapter.AddFile(tenant, svc.defaultBlobSubDir, sourceFilePath, blobID)
	}

	return
}

// DeleteFile implements usecase.BlobManagement.
func (svc *serviceData) DeleteFile(tenant string, BucketName string, blobID domain.BlobID) (err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	if err == nil {
		err = blobStorageAdapter.DeleteFile(tenant, svc.defaultBlobSubDir, blobID)
	}

	return
}

// FileExist implements usecase.BlobManagement.
func (svc *serviceData) FileExist(tenant string, BucketName string, blobID domain.BlobID) (exist bool, err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	if err == nil {
		exist, err = blobStorageAdapter.FileExist(tenant, svc.defaultBlobSubDir, blobID)
	}

	return
}

// GetFile implements usecase.BlobManagement.
func (svc *serviceData) GetFile(tenant string, BucketName string, blobID domain.BlobID) (filePath string, err error) {
	// get the storage adapter
	var blobStorageAdapter ports.BlobStorageAdapter
	blobStorageAdapter, err = svc.BucketGet(BucketName)

	if err == nil {
		filePath, err = blobStorageAdapter.OpenFile(tenant, svc.defaultBlobSubDir, blobID)
	}

	return
}
