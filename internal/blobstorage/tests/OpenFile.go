//go:build !production

package tests

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/codingsince1985/checksum"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

func TestOpenFile(t *testing.T, bucket ports.BlobStorageAdapter) {
	var err error
	var exist bool

	projectRoot, err := tools.FindProjectRoot()
	if err != nil {
		panic(err)
	}

	// everything from /tmp will be copyed, everything else will symlinked
	bucketSubPath := "blobs3"

	// the file should not exist and give an error, because id is invalid
	exist, err = bucket.FileExist("test", bucketSubPath, "randomid")
	assert.False(t, exist)
	assert.Error(t, err)

	// add the file
	var srcSHA256sum string
	var srcFileAbsolut string
	srcFileAbsolut, err = filepath.Abs(projectRoot + "/internal/blobstorage/tests/OpenFile.go")
	assert.NoError(t, err)
	srcSHA256sum, err = checksum.SHA256sum(srcFileAbsolut)
	assert.NoError(t, err)

	var blobID domain.BlobID
	blobID, err = bucket.AddFile("test", bucketSubPath, srcFileAbsolut, "")
	assert.NoError(t, err)
	assert.False(t, blobID.IsEmpty())

	// the file should exist
	exist, err = bucket.FileExist("test", bucketSubPath, blobID)
	assert.True(t, exist)
	assert.NoError(t, err)

	// try to open if
	openedFile, openedFileErr := bucket.OpenFile("test", bucketSubPath, blobID)
	assert.NotNil(t, openedFile)
	assert.FileExists(t, openedFile)
	assert.NoError(t, openedFileErr)

	// compare file
	var targetSHA256sum string
	targetSHA256sum, err = checksum.SHA256sum(openedFile)
	assert.NoError(t, err)
	assert.Equal(t, srcSHA256sum, targetSHA256sum)

	// delete the file
	err = bucket.DeleteFile("test", bucketSubPath, blobID)
	assert.NoError(t, err)

	// the file should not exist
	exist, err = bucket.FileExist("test", bucketSubPath, blobID)
	assert.False(t, exist)
	assert.NoError(t, err)

	// try to open if
	openedFile, openedFileErr = bucket.OpenFile("test", bucketSubPath, blobID)
	assert.Empty(t, openedFile)
	assert.Error(t, openedFileErr)

	os.Remove(openedFile)

}
