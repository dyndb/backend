package usecase

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/ports"
)

type BlobManagement interface {

	// Register a bucket if not exist.
	// return err if an bucket already exist with the same name
	BucketRegister(BucketName string, adapter ports.BlobStorageAdapter) (err error)

	// Return the name of the default bucket or "" if no default bucket is set
	BucketDefaultName() string

	// Sets the default bucket if not already set.
	// @return err if an bucket is already set as default
	BucketDefaultSet(BucketName string) (err error)

	// Get an bucket from a name.
	// return err if bucket not exist
	BucketGet(BucketName string) (adapter ports.BlobStorageAdapter, err error)

	// AddFile will add a file to the blob-storage to an specific bucket.
	// if blobID is empty, a new blob-id will be generated
	AddFile(tenant string, BucketName string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error)

	AddPreview(tenant string, BucketName string, sourceFilePath string, blobID domain.BlobID) (newBlobID domain.BlobID, err error)

	FileExist(tenant string, BucketName string, blobID domain.BlobID) (exist bool, err error)

	GetFile(tenant string, BucketName string, blobID domain.BlobID) (filePath string, err error)

	GetPreview(tenant string, BucketName string, blobID domain.BlobID) (filePath string, err error)

	DeleteFile(tenant string, BucketName string, blobID domain.BlobID) (err error)
}
