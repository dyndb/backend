package cli

import (
	"os"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/joho/godotenv"
)

// CLICommon holds common stuff
type CLICommon struct {
	ConfigFolder string `help:"The folder that contains configs in *.yml" short:"d" env:"CONFIGFOLDER" default:"./"`
	ImportTenant string `help:"Tenant name for imported folder" short:"t"  default:""`
	ImportFolder string `help:"Folder to import" short:"f"  default:""`
	ImportBucket string `help:"Import files to bucket" short:"b"  default:""`
}

// CLIApp contains all app-parameter
type CLIApp struct {
	CLICommon
}

// CLI is the overall cli-struct
var Values CLIApp
var ctxKong *kong.Context

func Read() {

	// we not init on cmd-line-test
	if isTest() {
		return
	}

	// load dotenv
	godotenv.Load()

	// ########################## Command line parse ##########################
	ctxKong = kong.Parse(&Values,
		kong.Name("dyndb"),
		kong.Description("your dynamic db"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: false,
		}),
	)

	ctxKong.ApplyDefaults()
	//ctxKong.Bind(Data)
}

func isTest() bool {
	for _, flag := range os.Args {
		if strings.Contains(flag, "-test.run") || strings.Contains(flag, "-test.v") {
			return true
		}

	}
	return false
}
