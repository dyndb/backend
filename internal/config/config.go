package config

import (
	"os"
	"strings"

	"github.com/mcuadros/go-defaults"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/cli"

	"gopkg.in/yaml.v2"
)

// Version represents the current version
var Version string = "development edition"

type ConfigStruct struct {
	Development bool   `yaml:"dev" default:"false"`
	LogLevel    string `yaml:"loglevel" default:"info"`
	Terminal    bool   `yaml:"terminal" default:"true"`

	Limits struct {
		RequestTimeout    int `yaml:"requestTimeout" default:"15"`
		RequestHeaderSize int `yaml:"requestHeaderSize" default:"102400"` // max upload size

		// Timeout of an token.
		//
		// the token will tbe rotated after  Timeout / 3 after an request
		// the frontend is allowed to poll an endpoint to refresh the token after some time :) ( polling )
		TokenTimeout int `yaml:"tokenTimeout" default:"180"`
	} `yaml:"limits"`

	Security struct {
		TokenTimeout       int    `yaml:"tokenTimeout" default:"30"`
		TokenTimeoutOffset int    `yaml:"tokenTimeoutOffset" default:"300"`
		SigningKey         string `yaml:"signingKey" default:"dummy"`
	} `yaml:"security"`

	Server struct {
		URL            string `yaml:"url" default:"http://127.0.0.1:3535"`
		Domain         string `yaml:"domain" default:"localhost"` // for cookies
		Prefix         string `yaml:"prefix" default:"/api"`
		RequestTimeout int    `yaml:"requestTimeoutSeconds" default:"5"`
		TempFolder     string `yaml:"tempfolder" default:"/tmp/dyndb"`
	} `yaml:"server"`

	Postgresql struct {
		URL                   string `yaml:"url" default:"postgres:///var/run/postgresql"`
		DBName                string `yaml:"dbname" default:"dyndb"`
		Username              string `yaml:"username"`
		Password              string `yaml:"password"`
		ForceMigration        bool   `yaml:"forceMigration"`
		ForceMigrationVersion int    `yaml:"forceMigrationVersion" default:"0"`
	} `yaml:"postgresql"`

	// Redis struct {
	// 	URL      string `yaml:"url" default:"127.0.0.1:6379"`
	// 	Username string `yaml:"username"`
	// 	Password string `yaml:"password"`
	// } `yaml:"redis"`

	Provisioning map[string]map[string]struct {
		Enabled     bool   `yaml:"enabled"`
		DisplayText string `yaml:"displayText"`
		Password    string `yaml:"password"`
	} `yaml:"provisioning"`

	//CouchDB struct {
	//	URL string `yaml:"url" default:"http://localhost:5984"` // the url to couchdb
	//} `yaml:"couchdb"`

	StorageBuckets map[string]struct {
		Enabled        bool   `yaml:"enabled"`
		Default        bool   `yaml:"default"`
		Type           string `yaml:"type"`
		Path           string `yaml:"path"`
		EncryptionKey  string `yaml:"encryptionkey"`
		SSL            bool   `yaml:"ssl"`
		SelfSignedCert bool   `yaml:"selfsigned"`
	} `yaml:"buckets"`

	Gotenberg struct {
		URL string `yaml:"url" default:"" example:"http://localhost:3000"`
	} `yaml:"gotenberg"`
}

var Values ConfigStruct

func Init(ctx appcontext.Context) {

	files, err := LoadYamlFiles(cli.Values.ConfigFolder)
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		err = ReadFile(ctx, file)
		if err != nil {
			panic(err)
		}
	}

	Apply()
}

func LoadYamlFiles(basePath string) (yamlFiles []string, err error) {

	// Öffne den Ordner
	files, err := os.ReadDir(basePath)
	if err != nil {
		return nil, err
	}

	// Iteriere über jede Datei im Ordner
	for _, file := range files {

		// Ignoriere Verzeichnisse
		if file.IsDir() {
			continue
		}

		// Überprüfe, ob die Datei eine YAML-Datei ist
		if !strings.HasPrefix(file.Name(), ".") && (strings.HasSuffix(file.Name(), ".yaml") || strings.HasSuffix(file.Name(), ".yml")) {
			if !strings.Contains(file.Name(), ".override.") {
				yamlFiles = append(yamlFiles, basePath+"/"+file.Name())
			}
		}
	}

	// check for overrides
	for _, yamlFile := range yamlFiles {

		if strings.HasSuffix(yamlFile, ".yml") {
			yamlOverrideFileName := strings.Replace(yamlFile, ".yml", ".override.yml", 1)
			if _, statErr := os.Stat(yamlOverrideFileName); statErr == nil {
				yamlFiles = append(yamlFiles, yamlOverrideFileName)
			}
		}

		if strings.HasSuffix(yamlFile, ".yaml") {
			yamlOverrideFileName := strings.Replace(yamlFile, ".yaml", ".override.yaml", 1)
			if _, statErr := os.Stat(yamlOverrideFileName); statErr == nil {
				yamlFiles = append(yamlFiles, yamlOverrideFileName)
			}
		}

	}

	return
}

func ReadFile(ctx appcontext.Context, filename string) (err error) {

	if filename == "" {
		err = errors.WithStack(errors.New("filename missing"))
	}

	// read the file
	var data []byte
	if err == nil {
		data, err = os.ReadFile(filename)
	}

	// parse it
	if err == nil {
		err = yaml.Unmarshal(data, &Values)
	}

	return
}

func Apply() {

	// apply defaults
	defaults.SetDefaults(&Values) //<-- This set the defaults values

	// setup debugger
	newDebugLevel, err := zerolog.ParseLevel(Values.LogLevel)
	if err == nil {
		zerolog.SetGlobalLevel(newDebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// add caller when on debug
	if Values.LogLevel == "debug" {
		log.Logger = log.Logger.With().Caller().Logger()
	}

	// terminal-flag
	if Values.Terminal {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

}
