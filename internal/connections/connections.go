package connections

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/services/management"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/usecases"
	"gitlab.com/nerdhaltig/gopersist"
)

type Management usecases.ConnectionsManagement

func NewConnectionManagement(pool gopersist.PoolManagementUseCase) Management {
	return management.NewConnectionManagementService(pool)
}
