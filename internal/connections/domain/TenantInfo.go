package domain

import "time"

type TenantInfo struct {
	Created time.Time `json:"created"`
}
