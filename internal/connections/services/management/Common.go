package management

import (
	"context"
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/usecases"
	"gitlab.com/nerdhaltig/gopersist"
)

type connectionsManagement struct {
	pool gopersist.PoolManagementUseCase
}

func NewConnectionManagementService(pool gopersist.PoolManagementUseCase) usecases.ConnectionsManagement {
	return &connectionsManagement{
		pool: pool,
	}
}

func (c *connectionsManagement) Add(connection gopersist.PersistancePort) {
	c.pool.Add(connection)
}

func (c *connectionsManagement) WithTenant(tenant, collection string, handler func(connection gopersist.PersistancePort) (err error)) (err error) {

	ctxCancel, Cancel := context.WithCancel(context.Background())
	defer Cancel()

	connection, err := c.pool.RequestRaw(ctxCancel)
	if err == nil {
		defer c.pool.Release(connection)
	}

	if err == nil {
		err = connection.SwitchTenant(ctxCancel, "tenant_"+tenant)
	}
	if err == nil {
		err = connection.SwitchCollection(ctxCancel, collection)
	}

	err = handler(connection)

	return
}

func (c *connectionsManagement) TenantRegister(tenant string) (err error) {
	ctxCancel, Cancel := context.WithCancel(context.Background())
	defer Cancel()

	connection, err := c.pool.RequestRaw(ctxCancel)
	if err == nil {
		defer c.pool.Release(connection)
	}

	// switch tenant
	if err == nil {
		if !connection.TenantExist(ctxCancel, "tenants") {
			err = connection.CreateTenant(ctxCancel, "tenants")
		}
	}
	if err == nil {
		err = connection.SwitchTenant(ctxCancel, "tenants")
	}

	// switch collection
	if err == nil {
		if !connection.CollectionExist(ctxCancel, "infos") {
			err = connection.CreateCollection(ctxCancel, "infos")
		}
	}
	if err == nil {
		err = connection.SwitchCollection(ctxCancel, "infos")
	}

	// create infos about the tenant
	var infosExist bool
	var infos domain.TenantInfo
	infosExist, err = connection.Get(ctxCancel, tenant, &infos)
	if !infosExist {
		err = connection.Save(ctxCancel, tenant, domain.TenantInfo{
			Created: time.Now(),
		})
	}

	// create the tenant-schema
	if !connection.TenantExist(ctxCancel, "tenant_"+tenant) {
		err = connection.CreateTenant(ctxCancel, "tenant_"+tenant)
	}

	return
}

// @TODO parallel calls until connection limits are reached
// @TODO paged loads of tenants to support more than 100 tenants
// @LIMIT tenants are currently limited to 100 !
func (c *connectionsManagement) WithTenants(handler func(connection gopersist.PersistancePort, tenantInfos *domain.TenantInfo) (err error)) (err error) {

	ctxCancel, Cancel := context.WithCancel(context.Background())
	defer Cancel()

	connection, err := c.pool.RequestRaw(ctxCancel)
	if err == nil {
		defer c.pool.Release(connection)
	}

	if !connection.TenantExist(ctxCancel, "tenants") {
		err = connection.CreateTenant(ctxCancel, "tenants")
	}
	if err == nil {
		err = connection.SwitchTenant(ctxCancel, "tenants")
	}

	if !connection.CollectionExist(ctxCancel, "infos") {
		err = connection.CreateCollection(ctxCancel, "infos")
	}
	if err == nil {
		err = connection.SwitchCollection(ctxCancel, "infos")
	}

	if err == nil {
		var tenantInfo domain.TenantInfo
		var startKey = "0"
		err = connection.ListStruct(ctxCancel, &startKey, 100, &tenantInfo, func(docID interface{}) (interrupt bool) {

			// switch to tenant
			err = connection.SwitchTenant(ctxCancel, docID.(string))

			// call the handler
			if err == nil {
				err = handler(connection, &tenantInfo)
			}

			if err == nil {
				err = connection.Save(ctxCancel, tenantInfo, tenantInfo)
			}

			return
		})
	}

	return
}
