package management_test

import (
	"context"
	"testing"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestManagement(t *testing.T) {

	ctxCancel, ctxCancelFunc := context.WithCancel(context.Background())
	defer ctxCancelFunc()

	connections, err := tests.PostgresqlPrepare(t, &tests.PostgresqlPrepareOpts{
		Ctx:         ctxCancel,
		Tenant:      "testtenant",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"account"},
	})
	if err != nil {
		panic(err)
	}

	tests.TestConnectionsManagement(t, connections)

}
