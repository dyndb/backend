package usecases

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

type ConnectionsManagement interface {
	Add(connection gopersist.PersistancePort)

	// TenantRegister register an new tenant
	TenantRegister(tenant string) (err error)

	WithTenant(tenant, collection string, handler func(connection gopersist.PersistancePort) (err error)) (err error)

	// WithTenants connect for every tenant an connection and run the handler
	//
	// This is mainly for db-migrations
	WithTenants(handler func(connection gopersist.PersistancePort, tenantInfos *domain.TenantInfo) (err error)) (err error)
}
