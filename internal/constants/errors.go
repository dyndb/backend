package constants

type DError struct {
	msg string
}

func (err DError) Error() string {
	return err.msg
}

var ERR_SESSION_MISSING DError = DError{msg: "session is missing, you are not logged in"}
var ERR_DOC_TEMPLATE_MISSING DError = DError{msg: "template missing"}
var ERR_DOC_ID_MISSING DError = DError{msg: "id missing"}
var ERR_DOC_REV_MISSING DError = DError{msg: "rev missing"}
var ERR_MIGRATION_NO_VERSION DError = DError{msg: "version not found in DB. Thats okay, we will do an migration."}

var LIMIT_MAX_TEMPLATES = 1024 // maximum amount of templates that this app can handle

var VIEWNAME_TEMPLATE = "dyndb_template"
var VIEWNAME_FIELD = "dyndb_field"
var VIEWNAME_LINK = "builtin_link"
var VIEWNAME_ORPHANED = "builtin_orphaned"
