package document

import (
	domainField "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	domainMigration "gitlab.com/nerdhaltig/dyndb/backend/internal/migration/domain"
)

var v1 domainMigration.MigrationJobs = domainMigration.MigrationJobs{
	Version: 1,
	Jobs: []domainMigration.MigrationJob{
		{
			Action:     "replace",
			DocID:      "doc-cac26763-01a5-4f01-88a2-54228ff6dfc1",
			Collection: "documents",
			Doc: domainField.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "_dyndb_interlinks",
				DisplayText: "Links",
				Hint:        "Links to other objects",
				Default:     "",
				Datatype:    "",
			},
		},
	},
}

var Migrations []domainMigration.MigrationJobs = []domainMigration.MigrationJobs{
	v1,
}
