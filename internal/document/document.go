package document

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/service/common"
	actorfordocuments "gitlab.com/nerdhaltig/dyndb/backend/internal/document/service/documentactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/service/search"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
)

// Document management ( save, delete, get)
type ManageUseCase usecase.DocumentManage

func NewDocManagementUseCase(connections connections.Management) DocumentService {
	return common.NewDocManagementUseCase(connections)
}

// Document search
type SearchUseCase usecase.DocumentSearch

type DocumentService usecase.DocumentService

func NewSearchUseCase(connections connections.Management) SearchUseCase {
	return search.NewSearchUseCase(connections)
}

// actors
func NewSaveActor(ctr goactor.Concentrator, docManagement ManageUseCase) (actor goactor.Actor) {
	return actorfordocuments.NewSaveActor(ctr, docManagement)
}

func NewGetActor(ctr goactor.Concentrator, docManagement ManageUseCase) (actor goactor.Actor) {
	return actorfordocuments.NewGetActor(ctr, docManagement)
}

func NewListActor(ctr goactor.Concentrator, docManagement ManageUseCase) (actor goactor.Actor) {
	return actorfordocuments.NewListActor(ctr, docManagement)
}

func NewSearchActor(ctr goactor.Concentrator, docSearch SearchUseCase) (actor goactor.Actor) {
	return actorfordocuments.NewSearchActor(ctr, docSearch)
}

func NewDeleteActor(ctr goactor.Concentrator, docManagement ManageUseCase) (actor goactor.Actor) {
	return actorfordocuments.NewDeleteActor(ctr, docManagement)
}

func NewLinkActor(ctr goactor.Concentrator, docManagement DocumentService) (actor goactor.Actor) {
	return actorfordocuments.NewLinkActor(ctr, docManagement)
}
