package domain

import (
	"strings"

	"github.com/pkg/errors"

	"github.com/google/uuid"
)

type DocID string

func (s *DocID) IsEmpty() bool {
	return *s == ""
}

func (s *DocID) Generate() {
	*s = DocID("doc-" + uuid.Must(uuid.NewRandom()).String())
}

func (s DocID) String() string {
	return string(s)
}

func (s DocID) Validate() (err error) {
	if !strings.HasPrefix(string(s), "doc-") {
		err = errors.WithStack(errors.New("not a id for metadata"))
	}
	return
}

func DocIDFromString(id string) (newID DocID, err error) {
	newID = DocID(id)
	err = newID.Validate()
	return
}

func NewDocID() (newID DocID) {
	newID.Generate()
	return
}

func IsDocID(id string) bool {
	return strings.HasPrefix(id, "doc-")
}
