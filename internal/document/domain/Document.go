package domain

import "encoding/json"

type GenericDocument struct {
	DisplayText string
	FaIcon      string
}

func (doc *GenericDocument) FromAny(srcDoc any) (err error) {
	var jsonBytes []byte
	jsonBytes, err = json.Marshal(srcDoc)
	if err == nil {
		err = json.Unmarshal(jsonBytes, doc)
	}
	return
}

type Document map[string]interface{}

func FromAny(doc any) (newDoc Document, err error) {

	newDoc = make(Document)

	var jsonBytes []byte
	jsonBytes, err = json.Marshal(doc)
	if err == nil {
		err = json.Unmarshal(jsonBytes, &newDoc)
	}

	return
}

// KeepOnly will remove all elements except fields
func (doc Document) KeepOnly(fields ...string) {
	for fieldNameInDoc := range doc {
		found := false
		for _, fieldNameToKeep := range fields {
			if fieldNameToKeep == fieldNameInDoc {
				found = true
			}
		}
		if !found {
			delete(doc, fieldNameInDoc)
		}
	}
}

// Ensure check if an field name exist, and set 'value' if not
func (doc Document) Ensure(fieldName string, value any) {
	_, exist := doc[fieldName]
	if !exist {
		doc[fieldName] = value
	}
}
