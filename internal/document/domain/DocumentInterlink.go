package domain

import (
	"encoding/json"
)

var InterLinkFieldName = "_dyndb_interlinks"

type InterLinks map[string]DocumentShort

type DocumentShort struct {
	DisplayText string `json:"displayText"`
	FaIcon      string `json:"faicon"`
}

type LinkAddOpts struct {
	ID          string
	DisplayText string
	FaIcon      string
}

func (doc *Document) InterlinkAdd(opts LinkAddOpts) (err error) {

	// read it
	var currentLinks InterLinks
	if currentLinksInterface, ok := (*doc)[InterLinkFieldName]; ok {
		var jsonBytes []byte

		jsonBytes, err = json.Marshal(currentLinksInterface)
		if err == nil {
			err = json.Unmarshal(jsonBytes, &currentLinks)
		}
	} else {
		currentLinks = make(InterLinks)
		(*doc)[InterLinkFieldName] = currentLinks
	}

	if err == nil {
		currentLinks[opts.ID] = DocumentShort{
			DisplayText: opts.DisplayText,
			FaIcon:      opts.FaIcon,
		}

		(*doc)[InterLinkFieldName] = currentLinks
	}

	return
}

func (doc *Document) Interlinks() (currentLinks InterLinks, err error) {

	// read it
	if currentLinksInterface, ok := (*doc)[InterLinkFieldName]; ok {
		var jsonBytes []byte

		jsonBytes, err = json.Marshal(currentLinksInterface)
		if err == nil {
			err = json.Unmarshal(jsonBytes, &currentLinks)
		}
	}

	return
}
