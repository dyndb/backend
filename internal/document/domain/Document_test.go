package domain_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
)

func TestDocument(t *testing.T) {

	type TestDocumentType struct {
		First  string
		Second string
		Third  string
		Fourth string
		Fifth  string
	}

	jsonData, jsonDataError := json.Marshal(&TestDocumentType{
		Third: "three",
	})
	assert.NoError(t, jsonDataError)

	var TestDoc domain.Document
	jsonDataError = json.Unmarshal(jsonData, &TestDoc)
	assert.NoError(t, jsonDataError)

	// keep fields
	TestDoc.KeepOnly("Third", "Fifth", "Not existing")
	assert.Len(t, TestDoc, 2)

	// not overwrite existing
	TestDoc.Ensure("Third", "no")
	assert.Equal(t, TestDoc["Third"], "three")

	// set non-existing
	TestDoc.Ensure("nine", "yes")
	assert.Equal(t, TestDoc["nine"], "yes")
}
