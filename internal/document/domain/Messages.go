package domain

type DocumentMessage struct {
	Tenant     string
	Collection string
	DocID      string
	Doc        interface{}
}

const (
	DocumentMessageCreated = "document/created"
	DocumentMessageSaved   = "document/saved"
	DocumentMessageDeleted = "document/deleted"
	DocumentMessageMoved   = "document/moved"
)

type DocumentSaveRequest struct {
	Tenant string
	DocID  string
	Doc    any
}

type DocumentSaved struct {
	DocID string
	Doc   any
}

// DocumentGetRequest requests a single document
type DocumentGetRequest struct {
	Tenant string
	DocID  string
}

type DocumenGetted struct {
	DocID string
	Doc   Document
}

type DocumentListRequest struct {
	Tenant       string
	TemplateName string
	PageKey      string
	PageLimit    int64
}

type DocumentList struct {
	PagedDocs   Documents
	NextPageKey string
}

type DocumentSearchRequest struct {
	Tenant       string
	TemplateName string
	SearchWords  []string
	PageKey      string
	PageLimit    int64
}

type DocumentSearched DocumentList

type DocumentDeleteRequest struct {
	Tenant string
	DocID  string
}

type DocumentDeleted Documents

type DocumentLinkRequest struct {
	Tenant string
	DocID  string

	LinkDocID string
	LinkDoc   GenericDocument
}

type DocumentLinked struct{}
