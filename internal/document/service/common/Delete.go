package common

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *manageDocumentUsecase) Delete(ctx context.Context, tenant string, documentID domain.DocID) (err error) {
	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {
		err = connection.Delete(ctx, documentID)
		return
	})

	return
}
