package common

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *manageDocumentUsecase) Fetch(ctx context.Context, tenant string, documentID domain.DocID, document interface{}) (exist bool, err error) {
	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		// get the document
		exist, err = connection.Get(ctx, documentID.String(), document)

		return
	})
	return
}
