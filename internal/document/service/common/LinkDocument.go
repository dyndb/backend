package common

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	"gitlab.com/nerdhaltig/gopersist"
)

// LinkDocument implements usecase.DocumentService.
func (svc *manageDocumentUsecase) LinkDocument(opts usecase.LinkDocumentOpts) (err error) {
	err = svc.connections.WithTenant(opts.Tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		var Doc domain.Document

		// get the document
		var exist bool
		exist, err = connection.Get(opts.Ctx, opts.DocID, &Doc)

		var curDoc domain.Document
		if err == nil && exist {
			curDoc, err = domain.FromAny(Doc)
		}

		if err == nil {
			err = curDoc.InterlinkAdd(domain.LinkAddOpts{
				ID:          opts.LinkDocID,
				DisplayText: opts.LinkDoc.DisplayText,
				FaIcon:      opts.LinkDoc.FaIcon,
			})
		}

		if err == nil {
			err = connection.Save(opts.Ctx, opts.DocID, curDoc)
		}

		return
	})
	return
}
