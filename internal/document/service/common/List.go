package common

import (
	"context"
	"fmt"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *manageDocumentUsecase) List(ctx context.Context, tenant, templateName string, pagekey *string, pagelimit int64) (pagedDocuments domain.Documents, err error) {

	pagedDocuments = make(domain.Documents)

	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		var connectionWithSQL gopersist.PersistancePortWithSQL
		var connectionSupportSQL bool
		if connectionWithSQL, connectionSupportSQL = connection.(gopersist.PersistancePortWithSQL); connectionSupportSQL {

			var newTempDoc domain.Document
			connectionWithSQL.QueryStruct(ctx, pagekey, pagelimit, fmt.Sprintf("data->>'_dyndb_templatename' = '%s'", templateName), &newTempDoc, func(docIDIf interface{}) (interrupt bool) {
				var docID domain.DocID
				docID, err = domain.DocIDFromString(docIDIf.(string))
				if err == nil {
					pagedDocuments[docID] = newTempDoc
				}

				return false
			})
		}

		return
	})

	return
}
