package common

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
)

type manageDocumentUsecase struct {
	connections connections.Management
}

func NewDocManagementUseCase(connections connections.Management) usecase.DocumentService {
	return &manageDocumentUsecase{
		connections: connections,
	}
}
