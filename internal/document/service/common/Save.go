package common

import (
	"context"
	"encoding/json"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *manageDocumentUsecase) Save(ctx context.Context, tenant, templateName string, documentID domain.DocID, document interface{}) (newDocumentID domain.DocID, err error) {
	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		// from json
		var jsonData []byte
		jsonData, err = json.Marshal(document)
		if err != nil {
			return err
		}

		// to map
		var docAsMap map[string]interface{}
		err = json.Unmarshal(jsonData, &docAsMap)
		if err != nil {
			return err
		}

		// set template-name
		if templateName != "" {
			docAsMap["_dyndb_templatename"] = templateName
		}

		// set template to unknown
		if _, exist := docAsMap["_dyndb_templatename"]; !exist {
			docAsMap["_dyndb_templatename"] = "dyndb_unknown"
		}

		// create a new document-id
		if documentID == "" || documentID == "-" { // create new
			newDocumentID = domain.NewDocID()
		} else {
			newDocumentID = documentID
		}

		err = connection.Save(ctx, newDocumentID, docAsMap)

		if err != nil {
			newDocumentID = domain.DocID("")
		}
		return
	})

	return
}
