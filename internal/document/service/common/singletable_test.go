package common

import (
	"context"
	"testing"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func TestExampleTestSuite(t *testing.T) {

	ctx, ctxCancel := context.WithCancel(context.Background())
	defer ctxCancel()

	connections, err := tests.PostgresqlPrepare(t, &tests.PostgresqlPrepareOpts{
		Ctx:         ctx,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"documents"},
	})
	if err != nil {
		panic(err)
	}

	connectionsManagement := NewDocManagementUseCase(connections)
	// tests.TestDocumentSave(t, connectionsManagement)
	// tests.TestDocumentList(t, connectionsManagement)
	// tests.TestDocumentCreate(t, connectionsManagement)
	tests.TestLinkAttachment(t, connectionsManagement)
}
