package actorfordocuments

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewDeleteActor(ctr goactor.Concentrator, docManagement usecase.DocumentManage) (actor goactor.Actor) {

	actor = goactor.NewActor("documentDeleteActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentDeleteRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			// docID
			var docID domain.DocID
			docID, err = domain.DocIDFromString(msg.DocID)

			if err == nil {
				err = docManagement.Delete(
					cancelCtx,
					msg.Tenant,
					docID,
				)
			}

			if err == nil {
				documents := make(domain.DocumentDeleted)
				documents[docID] = nil
				returnMessage = append(returnMessage, &documents)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentDeleteRequest{})

	return
}
