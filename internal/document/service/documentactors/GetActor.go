package actorfordocuments

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewGetActor(ctr goactor.Concentrator, docManagement usecase.DocumentManage) (actor goactor.Actor) {

	actor = goactor.NewActor("documentGetActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentGetRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			// docID
			var docID domain.DocID
			docID, err = domain.DocIDFromString(msg.DocID)

			result := &domain.DocumenGetted{}

			var exist bool
			if err == nil {
				result.DocID = msg.DocID
				exist, err = docManagement.Fetch(
					cancelCtx,
					msg.Tenant,
					docID, &result.Doc,
				)
			}

			if err == nil && exist {
				returnMessage = append(returnMessage, result)
			} else {
				err = errwrap.WithState(errors.New("not found"), http.StatusNotFound)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentGetRequest{})

	return
}
