package actorfordocuments

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewLinkActor(ctr goactor.Concentrator, docManagement usecase.DocumentService) (actor goactor.Actor) {

	actor = goactor.NewActor("documentLinkActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentLinkRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			// docID - validate
			_, err = domain.DocIDFromString(msg.DocID)

			if err == nil {
				docManagement.LinkDocument(usecase.LinkDocumentOpts{
					Ctx:    cancelCtx,
					Tenant: msg.Tenant,
					DocID:  msg.DocID,

					LinkDocID: msg.LinkDocID,
					LinkDoc:   msg.LinkDoc,
				})
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.DocumentLinked{})
			} else {
				err = errwrap.WithState(errors.New("not found"), http.StatusNotFound)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentLinkRequest{})

	return
}
