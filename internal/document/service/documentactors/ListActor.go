package actorfordocuments

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewListActor(ctr goactor.Concentrator, docManagement usecase.DocumentManage) (actor goactor.Actor) {

	actor = goactor.NewActor("documentListActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentListRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &domain.DocumentList{}

			if err == nil {
				result.NextPageKey = msg.PageKey
				result.PagedDocs, err = docManagement.List(
					cancelCtx,
					msg.Tenant, msg.TemplateName,
					&result.NextPageKey, msg.PageLimit,
				)
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentListRequest{})

	return
}
