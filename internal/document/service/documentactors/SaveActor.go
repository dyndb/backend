package actorfordocuments

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewSaveActor(ctr goactor.Concentrator, docManagement usecase.DocumentManage) (actor goactor.Actor) {

	actor = goactor.NewActor("documentSaveActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentSaveRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			var docID domain.DocID
			if msg.DocID == "" || msg.DocID == "-" {
				docID = domain.NewDocID()
			} else {
				docID, err = domain.DocIDFromString(msg.DocID)
			}

			if err == nil {
				_, err = docManagement.Save(
					cancelCtx,
					msg.Tenant, "",
					docID, msg.Doc,
				)
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.DocumentSaved{
					DocID: docID.String(),
					Doc:   msg.Doc,
				})
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentSaveRequest{})

	return
}
