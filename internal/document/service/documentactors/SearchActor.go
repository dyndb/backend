package actorfordocuments

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewSearchActor(ctr goactor.Concentrator, docSearch usecase.DocumentSearch) (actor goactor.Actor) {

	actor = goactor.NewActor("documentSearchActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.DocumentSearchRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			result := &domain.DocumentSearched{}

			if err == nil {
				result.NextPageKey = msg.PageKey
				result.PagedDocs, err = docSearch.SearchInTemplate(cancelCtx,
					msg.Tenant, msg.TemplateName,
					&result.NextPageKey, msg.PageLimit,
					msg.SearchWords,
				)
			}

			if err == nil {
				returnMessage = append(returnMessage, result)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.DocumentSearchRequest{})

	return
}
