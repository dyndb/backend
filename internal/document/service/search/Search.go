package search

import (
	"context"
	"errors"
	"fmt"
	"html"
	"strings"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist"
)

type searchService struct {
	connections connections.Management
}

func NewSearchUseCase(connections connections.Management) usecase.DocumentSearch {
	return &searchService{
		connections: connections,
	}
}

// SearchInTemplate implements usecase.SearchUseCase
func (svc *searchService) SearchInTemplate(
	ctx context.Context, tenant, templateName string,
	pagekey *string, pagelimit int64,
	words []string,
) (pagedDocuments domain.Documents, err error) {

	pagedDocuments = make(domain.Documents)

	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		var connectionWithSQL gopersist.PersistancePortWithSQL
		var connectionWithSQLSupported bool

		if connectionWithSQL, connectionWithSQLSupported = connection.(gopersist.PersistancePortWithSQL); !connectionWithSQLSupported {
			err = errors.New("connection not support searching")
			err = errwrap.WithStack(err)
		}

		if err == nil {
			var sqlQuery string = ""
			if err == nil {
				for _, word := range words {
					word = html.EscapeString(word)
					word = strings.ToLower(word)

					if sqlQuery != "" {
						sqlQuery = sqlQuery + " AND "
					}

					sqlQuery = sqlQuery + fmt.Sprintf("lower(data::text) like '%%%s%%'", word)
				}
			}

			if templateName != "" && templateName != "-" {
				if sqlQuery != "" {
					sqlQuery = sqlQuery + " AND "
				}
				sqlQuery = sqlQuery + fmt.Sprintf("data->>'_dyndb_templatename' = '%s'", templateName)
			}

			var newTempDoc domain.Document
			err = connectionWithSQL.QueryStruct(ctx, pagekey, pagelimit, sqlQuery, &newTempDoc, func(docIDIf interface{}) (interrupt bool) {
				var docID domain.DocID
				docID, err = domain.DocIDFromString(docIDIf.(string))
				if err == nil {
					pagedDocuments[docID] = newTempDoc
				}
				return
			})

		}

		return
	})

	return
}
