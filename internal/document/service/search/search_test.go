package search

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/service/common"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestSearch(t *testing.T) {

	ctx, ctxCancel := context.WithCancel(context.Background())
	defer ctxCancel()

	// setup postgresql
	connections, err := tests.PostgresqlPrepare(t, &tests.PostgresqlPrepareOpts{
		Ctx:         ctx,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"documents"},
	})
	if err != nil {
		panic(err)
	}

	documentManagement := common.NewDocManagementUseCase(connections)
	tests.TestDocumentCreate(t, documentManagement)

	searcher := NewSearchUseCase(connections)

	var partsPageKey string

	var documents domain.Documents
	documents, err = searcher.SearchInTemplate(ctx, "testdyndb", "", &partsPageKey, 1000, []string{"SECOND"})
	assert.NoError(t, err)
	assert.Len(t, documents, 1)

	documents, err = searcher.SearchInTemplate(ctx, "testdyndb", "", &partsPageKey, 1000, []string{"document"})
	assert.NoError(t, err)
	assert.Len(t, documents, 3)

	documents, err = searcher.SearchInTemplate(ctx, "testdyndb", "", &partsPageKey, 1000, []string{"nothing"})
	assert.NoError(t, err)
	assert.Len(t, documents, 0)
}
