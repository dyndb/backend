package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
)

type LinkDocumentOpts struct {
	Ctx    context.Context
	Tenant string

	DocID string

	LinkDocID string
	LinkDoc   domain.GenericDocument
}

type UnlinkDocumentOpts struct {
	Ctx    context.Context
	Tenant string

	DocID string

	UnLinkDocID string
}

type LinkToDocument interface {
	//  Save link two documents
	LinkDocument(opts LinkDocumentOpts) (err error)

	// Unlink will delete
	UnlinkDocument(opts UnlinkDocumentOpts) (err error)
}
