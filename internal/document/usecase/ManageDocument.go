package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
)

type DocumentManage interface {
	List(ctx context.Context, tenant, templateName string, pagekey *string, pagelimit int64) (pagedDocuments domain.Documents, err error)

	Save(ctx context.Context, tenant, templateName string, documentID domain.DocID, document interface{}) (newDocumentID domain.DocID, err error)

	// MustFetch get the document from the connection from the current collection
	// Will create a new doc if docID is "-" and return the new id as fetchedDocID.
	// Will inform the system via messaging that a document was created
	Fetch(ctx context.Context, tenant string, documentID domain.DocID, document interface{}) (exist bool, err error)

	Delete(ctx context.Context, tenant string, documentID domain.DocID) (err error)

	Move(ctx context.Context, tenant string, templateName string, documentID domain.DocID, templateNameTo string) (err error)
}
