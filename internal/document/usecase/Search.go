package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
)

type DocumentSearch interface {
	SearchInTemplate(
		ctx context.Context, tenant, templateName string,
		pagekey *string, pagelimit int64,
		words []string,
	) (pagedDocuments domain.Documents, err error)
}
