package fields

import (
	domainFields "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	domainMigration "gitlab.com/nerdhaltig/dyndb/backend/internal/migration/domain"
)

var v1 domainMigration.MigrationJobs = domainMigration.MigrationJobs{
	Version: 1,
	Jobs: []domainMigration.MigrationJob{
		{
			Action: "create_collection",
			DocID:  "documents",
		},
		{
			Action:     "replace",
			DocID:      "doc-4a07d731-d982-453b-ad58-46b0f737b9fe",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "_embedded",
				FaIcon:      "fasTag",
				DisplayText: "Embedded documents",
				Hint:        "Embedded documents in this document",
				Default:     "",
				Datatype:    "",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-f9cbd913-8c90-4979-9f43-e8f0f9fff4c3",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "_dyndb_templatename",
				FaIcon:      "fasTag",
				DisplayText: "Used template",
				Hint:        "Used template for this document, can be changed anytime",
				Default:     "",
				Datatype:    "templatename",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-0be51d14-a18c-4db9-a1aa-07097fde8fe7",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "_dyndb_builtin",
				FaIcon:      "fasLaptopCode",
				DisplayText: "Systemfield",
				Hint:        "Builtin system field/template",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-6454db5f-720f-4ad5-97c8-86bb273cbaa0",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "fieldName",
				FaIcon:      "fasTag",
				DisplayText: "Name of Field",
				Hint:        "Name of a field inside an document",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-ad617995-6629-42d9-9bfc-dfe780273a84",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "displayText",
				DisplayText: "Visible text of this",
				Hint:        "A short text that is be visible in the ui, can be changed every time",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-40b3335b-5065-40f4-a97b-c5e32a09b4fa",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "faicon",
				FaIcon:      "fasIcons",
				DisplayText: "Fontawesome-Icon",
				Hint:        "Icon-Name of fontawesome icon",
				Default:     "",
				Datatype:    "faicon",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-14b6768a-7325-4caf-b9c3-28ba43074aa1",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "hint",
				FaIcon:      "fasInfo",
				DisplayText: "Hint",
				Hint:        "Give a hint what to enter here",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-6d501f58-d602-43eb-ad07-bb5b48199967",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "datatype",
				FaIcon:      "fasAsterisk",
				DisplayText: "Datatype",
				Hint:        "Type of this input",
				Default:     "string",
				Datatype:    "datatype",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-9675ca4b-f5a9-4b17-b7e6-fb01f91f10fe",
			Collection: "documents",
			Doc: domainFields.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "unit",
				FaIcon:      "fasVial",
				DisplayText: "Unit",
				Hint:        "Unit for this field",
				Default:     "",
				Datatype:    "string",
			},
		},
	},
}

var Migrations []domainMigration.MigrationJobs = []domainMigration.MigrationJobs{
	v1,
}
