package db

import (
	"context"
	"errors"
	"fmt"
	"html"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	fieldsdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist"
)

// Fetch implements ports.FieldPersistance.
func (svc *serviceData) Fetch(ctx context.Context, tenant string, names []string) (fields map[domain.DocID]fieldsdomain.Field, err error) {

	fields = make(map[domain.DocID]fieldsdomain.Field)

	err = svc.connections.WithTenant(tenant, "documents", func(connection gopersist.PersistancePort) (err error) {

		var connectionWithSQL gopersist.PersistancePortWithSQL
		var connectionWithSQLSupported bool

		if connectionWithSQL, connectionWithSQLSupported = connection.(gopersist.PersistancePortWithSQL); !connectionWithSQLSupported {
			err = errors.New("connection not support searching")
			err = errwrap.WithStack(err)
		}

		var sqlQuery string = "( "
		if err == nil {

			for _, fieldName := range names {
				fieldName = html.EscapeString(fieldName)

				if sqlQuery != "( " {
					sqlQuery = sqlQuery + " OR "
				}

				sqlQuery = sqlQuery + fmt.Sprintf("data->>'fieldName' = '%s'", fieldName)
			}

			sqlQuery += " )"
		}

		if err == nil {

			var startkey string
			var field fieldsdomain.Field
			err = connectionWithSQL.QueryStruct(ctx, &startkey, 999999, sqlQuery, &field, func(docID interface{}) (interrupt bool) {

				docIDString, docIDStringConverted := docID.(string)
				if docIDStringConverted {
					docID, err := domain.DocIDFromString(docIDString)
					if err == nil {
						fields[docID] = field
					}
				}

				return false
			})

		}

		return
	})

	return
}
