package db_test

import (
	"context"
	"testing"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/adapter/persistance/db"
	fieldsTests "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/tests"
	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
)

func TestDB(t *testing.T) {

	ctx, ctxCancel := context.WithCancel(context.Background())
	t.Cleanup(func() {
		ctxCancel()
	})

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctx,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"documents"},
	})
	if err != nil {
		panic(err)
	}

	dbPersistance := db.NewFieldPersistanceAdapter(connections)

	fieldsTests.FieldsFetchTest(t, connections, dbPersistance)
}
