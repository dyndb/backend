package db

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/ports"
)

type serviceData struct {
	connections connections.Management
}

func NewFieldPersistanceAdapter(connections connections.Management) ports.FieldPersistance {
	return &serviceData{
		connections: connections,
	}
}
