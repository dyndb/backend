package domain

import (
	documentdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
)

type FieldsRequest struct {
	Tenant string   // optional, will be read from tokenDomain.AuthValidated
	DocIDs []string // Needed !
}

type Fields struct {
	Fields map[documentdomain.DocID]Field
}
