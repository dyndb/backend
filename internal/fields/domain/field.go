package domain

type Field struct {
	FieldName   string `json:"fieldName"`
	FaIcon      string `json:"faicon"`
	DisplayText string `json:"displayText"`
	Hint        string `json:"hint"`
	Default     string `json:"default"`
	Datatype    string `json:"datatype"`

	DynDBTemplateName string `json:"_dyndb_templatename"`
	DynDBBuiltin      bool   `json:"_dyndb_builtin"`
}
