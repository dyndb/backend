package fields

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/adapter/persistance/db"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/services/fieldsactor"
	"gitlab.com/nerdhaltig/goactor/v2"
)

type PersistancePort ports.FieldPersistance

func NewDbPersistancePort(connections connections.Management) PersistancePort {
	return db.NewFieldPersistanceAdapter(connections)
}

// actors

func NewFieldsSaveActor(ctr goactor.Concentrator, persistance ports.FieldPersistance) (actor goactor.Actor) {
	return fieldsactor.NewFieldsSaveActor(ctr, persistance)
}
