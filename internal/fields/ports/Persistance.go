package ports

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	fieldsdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
)

type FieldPersistance interface {
	Fetch(ctx context.Context, tenant string, names []string) (fields map[domain.DocID]fieldsdomain.Field, err error)
}
