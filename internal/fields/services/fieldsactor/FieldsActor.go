package fieldsactor

import (
	"context"

	"github.com/pkg/errors"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/ports"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewFieldsSaveActor(ctr goactor.Concentrator, persistance ports.FieldPersistance) (actor goactor.Actor) {

	actor = goactor.NewActor("FieldsSaveActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.FieldsRequest:

			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// get tenant
			if msg.Tenant == "" {
				var AuthInfo *tokenDomain.AuthValidated
				if ctx.As(&AuthInfo) {
					msg.Tenant = AuthInfo.Tenant
				} else {
					err = errors.WithStack(errors.New("unauthenticated"))
				}
			}

			var fields map[documentDomain.DocID]domain.Field
			if err == nil {
				fields, err = persistance.Fetch(cancelCtx, msg.Tenant, msg.DocIDs)
			}

			if err == nil {
				returnMessage = append(returnMessage, &domain.Fields{
					Fields: fields,
				})
			}
		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.FieldsRequest{})

	return
}
