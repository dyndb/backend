package tests

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	fieldsdomain "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/fields/ports"
	"gitlab.com/nerdhaltig/gopersist"
)

func FieldsFetchTest(t *testing.T, connections connections.Management, persistance ports.FieldPersistance) {

	ctx, ctxCancel := context.WithCancel(context.Background())
	defer ctxCancel()

	err := connections.WithTenant("testdyndb", "documents", func(connection gopersist.PersistancePort) (err error) {

		err = connection.Save(ctx, domain.NewDocID(), fieldsdomain.Field{
			FieldName:   "price",
			DisplayText: "Price",
		})
		assert.NoError(t, err)

		err = connection.Save(ctx, domain.NewDocID(), fieldsdomain.Field{
			FieldName:   "amount",
			DisplayText: "Amoun of parts",
		})
		assert.NoError(t, err)

		err = connection.Save(ctx, domain.NewDocID(), fieldsdomain.Field{
			FieldName:   "stars",
			DisplayText: "Rating",
		})
		assert.NoError(t, err)

		return
	})
	assert.NoError(t, err)

	existingFields, err := persistance.Fetch(ctx, "testdyndb", []string{"amount", "price", "notexist", "something"})
	assert.NoError(t, err)
	assert.Len(t, existingFields, 2)

}
