package folders

import (
	"context"
	"io/fs"
	"os"
	"path/filepath"
	"time"
)

type folderActor struct{}

type FolderPort interface {
	CreateIfMissing(folder string) (err error)
	PeriodicDelete(ctx context.Context, folder string, filesOlderThan time.Duration, checkPeriod time.Duration)
}

func NewFoldersAdapter() FolderPort {
	return &folderActor{}
}

func (adp *folderActor) CreateIfMissing(folder string) (err error) {

	if _, err = os.Stat(folder); os.IsNotExist(err) {
		err = os.MkdirAll(folder, 0700)
	}

	return
}

func (adp *folderActor) PeriodicDelete(ctx context.Context, folder string, filesOlderThan time.Duration, checkPeriod time.Duration) {

	filepath.WalkDir(folder, func(path string, d fs.DirEntry, err error) error {
		if d == nil || err != nil {
			return nil
		}

		fileInfo, err := d.Info()
		if fileInfo == nil || err != nil {
			return nil
		}

		if fileInfo.IsDir() {
			return nil
		}

		if time.Now().After(fileInfo.ModTime().Add(filesOlderThan)) {
			filePath := folder + "/" + fileInfo.Name()
			os.Remove(filePath)
		}

		return nil
	})

	time.Sleep(checkPeriod)

	go adp.PeriodicDelete(ctx, folder, filesOlderThan, checkPeriod)

}
