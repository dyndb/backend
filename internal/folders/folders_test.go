package folders_test

import (
	"context"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/folders"
)

func TestParallelRuns(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	folders := folders.NewFoldersAdapter()

	testpath, err := os.MkdirTemp("/tmp/", "integrationtest_*")
	assert.NoError(t, err)
	defer os.RemoveAll(testpath)
	folders.CreateIfMissing(testpath)

	for fileCounter := 100; fileCounter > 0; fileCounter-- {
		ioutil.TempFile(testpath, "file")
	}

	for fileCounter := 100; fileCounter > 0; fileCounter-- {
		go folders.PeriodicDelete(ctx, testpath, time.Second*3, time.Second*1)
	}

	time.Sleep(time.Second * 5)
	assert.NoFileExists(t, testpath)
}
