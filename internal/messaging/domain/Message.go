package domain

type Message struct {
	Topic   string
	Payload any
}
