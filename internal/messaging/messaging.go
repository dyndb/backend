package messaging

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/service/actor"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/service/inmem"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/usecase"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/goactor"
)

// ports

// PublishSubscribeUsecase represents an publish-subscribe-port
type PublishSubscribeUsecase usecase.PublishSubscribe

// NewInMemPublishSubscribe create the default publish/subscribe implementation
func NewInMemPublishSubscribe(name string) PublishSubscribeUsecase {
	return inmem.NewPublishSubscribe(name)
}

func RegisterActor(
	supervisor goactor.SuperVisor,
	subscriptions PublishSubscribeUsecase,
) {
	newActor := actor.NewMessagingActor(subscriptions)
	err := supervisor.Register(newActor)
	errwrap.PanicOnError(err)
}
