package actor

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/usecase"
	"gitlab.com/nerdhaltig/goactor"
)

type MessagingActor struct {
	subscriptions usecase.PublishSubscribe
}

func NewMessagingActor(
	subscriptions usecase.PublishSubscribe,
) goactor.Actor {

	actorData := &MessagingActor{
		subscriptions: subscriptions,
	}

	newActor := goactor.NewActor("messaging", actorData.handler)

	return newActor
}
