package actor

import (
	"gitlab.com/nerdhaltig/goactor"
)

type MessagePublishToTenant struct {
	Tenant  string
	Topic   string
	Message any
}

type MessagePublished struct{}

func (act *MessagingActor) OnPublishToTenant(ctx goactor.JobContext, msg *MessagePublishToTenant) (returnMessage any, err error) {

	act.subscriptions.Publish("tenant/"+msg.Tenant+"/all/"+msg.Topic, msg.Message)

	return &MessagePublished{}, nil
}
