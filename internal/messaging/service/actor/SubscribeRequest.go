package actor

import (
	"gitlab.com/nerdhaltig/goactor"
)

type MessageSubscriptionRequest struct {
	Tenant string
}

func (act *MessagingActor) OnSubscribeRequest(ctx goactor.JobContext, msg *MessageSubscriptionRequest) (returnMessage any, err error) {

	// // we need a tenant
	// var validated *actors.AuthValidated
	// if !ctx.As(&validated) {
	// 	return nil, errors.New("unauthenticated")
	// }
	// msg.Tenant = validated.Tenant

	// newSubscription := &MessageSubscription{
	// 	Tenant:   validated.Tenant,
	// 	Username: validated.Username,
	// 	Inbox:    make(chan domain.Message, 10),
	// 	stopPing: make(chan bool),
	// }

	// newSubscription.SubscriptionID = act.subscriptions.Subscribe(
	// 	"tenant/"+msg.Tenant+"/#",
	// 	func(SubMessageID, SubTopic string, SubPayload any) {
	// 		newSubscription.Inbox <- domain.Message{
	// 			Topic:   SubTopic,
	// 			Payload: SubPayload,
	// 		}
	// 	},
	// )

	// start the ping
	/*
		newSubscription.Ping(30*time.Second, func() {
			act.OnUnSubscribeRequest(
				ctx,
				&MessageUnSubscribeRequest{
					ActiveSubsctiption: newSubscription,
				},
			)
		})
	*/

	// return newSubscription, nil
	return
}
