package actor

import (
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/domain"
)

type MessageSubscription struct {
	SubscriptionID string
	Tenant         string
	Username       string
	Inbox          chan domain.Message
	stopPing       chan bool
}

func (sub *MessageSubscription) Stop() {
	sub.stopPing <- true
}

// Ping starts a ping in Background
func (sub *MessageSubscription) Ping(interval time.Duration, OnPingTimeout func()) {

	go func() {

		ticker := time.NewTicker(interval)

	tickerloop:
		for {
			select {
			case <-ticker.C:

				pingSuccess := make(chan bool, 1)
				pingTimeout := time.NewTimer(interval / 2)

				sub.Inbox <- domain.Message{
					Topic:   "ping",
					Payload: pingSuccess,
				}

				select {
				case <-pingSuccess:
					break
				case <-pingTimeout.C:
					log.Error().
						Str("id", sub.SubscriptionID).
						Msg("unsubscribe because of ping-timeout")
					defer OnPingTimeout()
					break tickerloop
				}

			case <-sub.stopPing:
				return
			}

		}

	}()

}
