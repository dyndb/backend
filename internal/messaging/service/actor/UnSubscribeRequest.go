package actor

import (
	"gitlab.com/nerdhaltig/goactor"
)

type MessageUnSubscribeRequest struct {
	ActiveSubsctiption *MessageSubscription
}

type MessageUnSubscribed struct {
	SubscriptionsCounter int
}

func (act *MessagingActor) OnUnSubscribeRequest(ctx goactor.JobContext, msg *MessageUnSubscribeRequest) (returnMessage any, err error) {

	// unsubscribe
	act.subscriptions.UnSubscribe(msg.ActiveSubsctiption.SubscriptionID)

	msg.ActiveSubsctiption.Stop()
	close(msg.ActiveSubsctiption.Inbox)
	close(msg.ActiveSubsctiption.stopPing)

	return &MessageUnSubscribed{
		SubscriptionsCounter: act.subscriptions.Counter(),
	}, nil
}
