package actor

import (
	"gitlab.com/nerdhaltig/goactor"
	"gitlab.com/nerdhaltig/goactor/goactorstate"
)

func (act *MessagingActor) handler(ctx goactor.JobContext) (returnMessage any, err error) {
	switch msg := ctx.Message().(type) {

	case *goactorstate.MessageTypesRequest:
		return &goactorstate.MessageTypes{
			Types: []interface{}{
				&MessageSubscriptionRequest{},
				&MessageUnSubscribeRequest{},
				&MessagePublishToTenant{},
			},
		}, nil

	case *MessageSubscriptionRequest:
		return act.OnSubscribeRequest(ctx, msg)

	case *MessageUnSubscribeRequest:
		return act.OnUnSubscribeRequest(ctx, msg)

	case *MessagePublishToTenant:
		return act.OnPublishToTenant(ctx, msg)
	}

	return
}
