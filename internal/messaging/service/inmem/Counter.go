package inmem

// Publish implements ports.PublishSubscribePort
func (adp *messagingData) Counter() int {
	return len(adp.subscribers)
}
