package inmem

import (
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/usecase"
)

// Publish implements ports.PublishSubscribePort
func (adp *messagingData) publish(publishTopic string, msgID string, payload any) {

	publishTopic = strings.Trim(publishTopic, "/")

	adp.lock.RLock()
	defer adp.lock.RUnlock()

	for _, subscriber := range adp.subscribers {

		if adp.topicMatcher.Match(publishTopic, subscriber.topic) {
			go subscriber.handler(msgID, publishTopic, payload)
			continue
		}

	}

}

// Publish implements ports.PublishSubscribePort
func (adp *messagingData) Publish(publishTopic string, payload any) (messageID string) {

	messageID = uuid.Must(uuid.NewRandom()).String()
	go adp.publish(publishTopic, messageID, payload)

	return
}

func (adp *messagingData) Respond(msgID string, payload any) {
	go adp.publish("/result/"+msgID, "", payload)
}

func (adp *messagingData) PublishWaitForResponse(
	publishTopic string, payload any,
	handlerFunc usecase.SubscribeHandler,
	timeout time.Duration,
) (err error) {

	responded := make(chan byte, 1)
	timeoutTimer := time.NewTimer(timeout)
	defer timeoutTimer.Stop()

	// we need a msgID to wait for respond
	newMsgID := uuid.Must(uuid.NewRandom()).String()

	// subscribe for respond
	subscriptionID := adp.Subscribe("/result/"+newMsgID, func(SubMessageID, SubTopic string, SubPayload any) {
		// call with responded payload
		go handlerFunc(SubMessageID, SubTopic, SubPayload)
		// stop our function
		responded <- 0
	})
	defer adp.UnSubscribe(subscriptionID) // remove temp subscription

	// publish
	go adp.publish(publishTopic, newMsgID, payload)

	// wait for response
	select {
	case <-timeoutTimer.C:
		err = errors.New("timeout")
		break
	case <-responded:

		err = nil
		break
	}

	return
}
