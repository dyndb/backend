package inmem

import (
	"strings"

	"github.com/google/uuid"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/usecase"
)

// Subscribe implements ports.PublishSubscribePort
func (adp *messagingData) Subscribe(
	subscribeTopic string,
	handlerFunc usecase.SubscribeHandler,
) (subscriptionID string) {

	subscriptionID = uuid.Must(uuid.NewRandom()).String()

	newSubscriber := &messagingSubscriber{
		topic:   strings.Trim(subscribeTopic, "/"),
		handler: handlerFunc,
	}

	adp.lock.Lock()
	adp.subscribers[subscriptionID] = newSubscriber
	adp.lock.Unlock()

	return
}

// UnSubscribe implements ports.PublishSubscribePort
func (adp *messagingData) UnSubscribe(subscriptionID string) {
	adp.lock.Lock()
	delete(adp.subscribers, subscriptionID)
	adp.lock.Unlock()

}

func (adp *messagingData) UnSubscribeTopic(Topic string) {
	adp.lock.Lock()

	for subID, sub := range adp.subscribers {
		if sub.topic == Topic {
			delete(adp.subscribers, subID)
			break
		}
	}

	adp.lock.Unlock()

}
