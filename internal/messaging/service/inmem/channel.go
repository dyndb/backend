package inmem

import (
	"sync"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/messaging/usecase"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/topic/adapter/mqtt"
	usecase1 "gitlab.com/nerdhaltig/dyndb/backend/internal/topic/ports"
)

type messagingData struct {
	name         string
	topicMatcher usecase1.Matcher

	subscribers map[string]*messagingSubscriber
	lock        sync.RWMutex
}

type messagingSubscriber struct {
	topic   string
	handler usecase.SubscribeHandler
}

type Message struct {
	SubscriptionID string
	ID             string
	Topic          string
	Payload        interface{}
}

func NewPublishSubscribe(name string) usecase.PublishSubscribe {
	return &messagingData{
		name:         name,
		topicMatcher: mqtt.NewTopicHandlerAdapter(),

		subscribers: make(map[string]*messagingSubscriber),
		lock:        sync.RWMutex{},
	}
}
