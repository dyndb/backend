package inmem

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestExactMatch(t *testing.T) {

	pubsub := NewPublishSubscribe("exact")

	lightsLock := sync.Mutex{}
	lightsWorks := false
	lightsLock.Lock()
	pubsub.Subscribe(
		"/home/livingroom/lights/window1/state",
		func(MessageID, Topic string, Payload any) {

			if Payload == "data" {
				lightsWorks = true
			}

			defer lightsLock.Unlock()
		},
	)

	pubsub.Publish("/home/livingroom/lights/window1/state", "data")
	lightsLock.Lock()
	assert.True(t, lightsWorks)

}

func BenchmarkOneSubscriberManyPublisher(b *testing.B) {

	pubsub := NewPublishSubscribe("exact")
	pubsub.Subscribe(
		"/home/livingroom/lights/window1/state",
		func(MessageID, Topic string, Payload any) {},
	)

	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		pubsub.Publish("/home/livingroom/lights/window1/state", "data")

	}
}

func BenchmarkOneSubscriberManyPublisherWithSleep(b *testing.B) {

	pubsub := NewPublishSubscribe("exact")
	pubsub.Subscribe(
		"/home/livingroom/lights/window1/state",
		func(MessageID, Topic string, Payload any) {
			time.Sleep(time.Minute * 1)
		},
	)

	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		pubsub.Publish("/home/livingroom/lights/window1/state", "data")

	}
}

func TestWildcard(t *testing.T) {

	pubsub := NewPublishSubscribe("wildcard")

	lightsLock := sync.Mutex{}
	lightsWorks := false
	lightsLock.Lock()
	pubsub.Subscribe(
		"/home/livingroom/lights/#",
		func(MessageID, Topic string, Payload any) {
			lightsWorks = true
			defer lightsLock.Unlock()
		},
	)

	pubsub.Publish("/home/livingroom/lights/window1/state", "")
	lightsLock.Lock()
	assert.True(t, lightsWorks)

	lightLock := sync.Mutex{}
	lightWorks := false
	lightLock.Lock()
	pubsub.Subscribe(
		"/home/livingroom/light/#",
		func(MessageID, Topic string, Payload any) {
			lightWorks = true
			defer lightLock.Unlock()
		},
	)

	lightsWorks = false
	lightWorks = false
	pubsub.Publish("/home/livingroom/light/window1/state", "")
	lightLock.Lock()
	assert.False(t, lightsWorks)
	assert.True(t, lightWorks)

}

func TestRespond(t *testing.T) {
	pubsub := NewPublishSubscribe("respond")

	// we subscribe to something
	pubsub.Subscribe(
		"home/door/command",
		func(MessageID, Topic string, Payload any) {
			if Payload == "open" {
				pubsub.Respond(MessageID, "true")
			}
		},
	)

	assert.Equal(t, 1, pubsub.Counter())

	// we request for something that not exist
	err := pubsub.PublishWaitForResponse(
		"home/window/command", "open",
		func(MessageID, Topic string, Payload any) {
			panic("this should not happen")
		}, time.Second*2)
	assert.Error(t, err)
	assert.Equal(t, 1, len(pubsub.(*messagingData).subscribers))

	err = pubsub.PublishWaitForResponse(
		"home/door/command", "open",
		func(MessageID, Topic string, Payload any) {
			if Payload != "true" {
				panic("payload is not true")
			}
		}, time.Second*2)
	assert.NoError(t, err)

	// only one subscription should be left
	assert.Equal(t, 1, len(pubsub.(*messagingData).subscribers))

}

type TestDataStruct struct {
	StringData string
}

func TestStruct(t *testing.T) {

	pubsub := NewPublishSubscribe("struct")

	lightsLock := sync.Mutex{}
	lightsWorks := false
	lightsLock.Lock()
	pubsub.Subscribe(
		"/home/livingroom/lights/window1/state",
		func(MessageID, Topic string, Payload any) {

			data, _ := Payload.(*TestDataStruct)

			if data.StringData == "testdata" {
				lightsWorks = true
			}

			defer lightsLock.Unlock()
		},
	)

	pubsub.Publish("/home/livingroom/lights/window1/state", &TestDataStruct{
		StringData: "testdata",
	})
	lightsLock.Lock()
	assert.True(t, lightsWorks)

}
