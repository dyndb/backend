package usecase

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
)

type PublishSubscribeContext interface {
	appcontext.WithContext
	appcontext.WithPanic
}

// SubscribeHandler will call, if an message is published.
//
// if msgID is set, the sender want an response. Send it even if you not have anything to not run in timeouts
type SubscribeHandler func(
	SubMessageID string,
	SubTopic string,
	SubPayload any,
)

type PublishSubscribe interface {
	Publish(publishTopic string, payload any) (messageID string)

	PublishWaitForResponse(
		publishTopic string, payload any,
		handlerFunc SubscribeHandler,
		timeout time.Duration,
	) (err error)

	Subscribe(
		subscribeTopic string,
		handlerFunc SubscribeHandler,
	) (subscriptionID string)
	Respond(msgID string, payload any)

	UnSubscribe(subscriptionID string)
	UnSubscribeTopic(Topic string)

	Counter() int
}
