package migration

import (
	"testing"

	_ "github.com/go-kivik/couchdb/v4" // The CouchDB driver

	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
}

func TestSuiteTests(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

// setup suite
func (suite *TestSuite) SetupSuite() {

}

func (suite *TestSuite) Test01_GetVersion() {

}

func (suite *TestSuite) TearDownSuite() {
}
