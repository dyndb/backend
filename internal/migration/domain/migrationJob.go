package domain

type MigrationJob struct {
	Action     string      `json:"action"`
	DocID      string      `json:"docid"`
	Collection string      `json:"collection"`
	Doc        interface{} `json:"document"`
}

type MigrationJobs struct {
	Version int
	Jobs    []MigrationJob
}
