package migration

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration/services/management/dbbased"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration/usecase"
)

type ManagementUseCase usecase.MigrationManagementUseCase
type MigrateOpts usecase.MigrateOpts

func NewMigrationService(connections connections.Management) ManagementUseCase {
	return dbbased.NewMigrationService(connections)
}
