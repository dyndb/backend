package dbbased

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration/usecase"
	"gitlab.com/nerdhaltig/gopersist"
)

type migrationService struct {
	connections connections.Management
}

type VersionDoc struct {
	No int `json:"dbversion"`
}

func NewMigrationService(
	connections connections.Management,
) usecase.MigrationManagementUseCase {
	return &migrationService{
		connections: connections,
	}
}

// Migrate implements ports.MigrationsService
func (svc *migrationService) Migrate(opts usecase.MigrateOpts) {

	// nothing should be done
	if opts.Migrations == nil {
		return
	}

	svc.connections.WithTenant(opts.Tenant, "migrations", func(connection gopersist.PersistancePort) (err error) {

		var versionDoc VersionDoc
		if !opts.Force {
			// create collection if needed
			if !connection.CollectionExist(opts.Ctx, "migrations") {
				err = connection.CreateCollection(opts.Ctx, "migrations")
			}

			// get version
			if err == nil {
				err = connection.SwitchCollection(opts.Ctx, "migrations")
			}

			if err == nil {
				connection.Get(opts.Ctx, opts.Section, &versionDoc)
			}
		} else {
			versionDoc.No = opts.ForceVersion
		}

		for _, migration := range *opts.Migrations {

			if migration.Version <= versionDoc.No {
				versionDoc.No++
				continue
			}

			log.Debug().Int("Version", migration.Version).Str("tenant", opts.Tenant).Str("section", opts.Section).Msg("Start Migration")
			for _, job := range migration.Jobs {

				switch job.Action {
				case "create_collection":
					if err == nil {
						if !connection.CollectionExist(opts.Ctx, job.DocID) {
							err = connection.CreateCollection(opts.Ctx, job.DocID)
						}
					}
				case "replace":
					if err == nil {
						err = connection.SwitchCollection(opts.Ctx, job.Collection)
					}
					if err == nil {
						err = connection.Delete(opts.Ctx, job.DocID)
					}
					if err == nil {
						err = connection.Save(opts.Ctx, job.DocID, job.Doc)
					}

				}

				if err != nil {
					break
				}
			}

			if err == nil {
				versionDoc.No++
				err = connection.SwitchCollection(opts.Ctx, "migrations")
			}
			if err == nil {
				err = connection.Save(opts.Ctx, opts.Section, versionDoc)
			}

			if err != nil {
				break
			}

			log.Debug().Int("Version", migration.Version).Str("tenant", opts.Tenant).Str("section", opts.Section).Msg("Finished")
		}

		return
	})
}
