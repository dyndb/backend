package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/migration/domain"
)

type MigrateOpts struct {
	Ctx        context.Context
	Tenant     string
	Section    string
	Migrations *[]domain.MigrationJobs

	Force        bool
	ForceVersion int
}

type MigrationManagementUseCase interface {
	Migrate(opts MigrateOpts)
}
