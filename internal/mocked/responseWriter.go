package mocked

import (
	"net/http"

	"github.com/google/uuid"
)

type ResponseWriter struct {
	id         string
	header     map[string][]string
	StatusCode int
}

var list map[string]*ResponseWriter = make(map[string]*ResponseWriter)

func (rw *ResponseWriter) Init() {
	if rw.header == nil {
		rw.header = make(map[string][]string)
	}

	if rw.id == "" {
		rw.id = uuid.Must(uuid.NewRandom()).String()
		list[rw.id] = rw
	}
}

func (rw ResponseWriter) Header() (header http.Header) {
	header = rw.header
	return
}

func (rw ResponseWriter) Write(data []byte) (count int, err error) {
	count = len(data)
	return
}

func (rw ResponseWriter) WriteHeader(statusCode int) {

	if rwp, exist := list[rw.id]; exist == true {
		rwp.StatusCode = statusCode
	}

	return
}
