package ports

import (
	"io"
	"mime/multipart"
)

type AttachmentsRepo interface {

	// CreateBucket create a new bucket at location
	//
	// the locations is heavily depended on the implementation, for example in mongodb, its just a bucketname
	CreateBucket(bucketID, location string)

	Save(bucketID, docID, filename string, file multipart.File)
	List(bucketID, docID string)
	Download(bucketID, docID, filename string) (file io.ReadCloser)
}
