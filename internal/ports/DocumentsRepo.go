package ports

import (
	"context"
)

// DocumentsRepo represents a single connection to the database and is bound to an user
type DocumentsRepo interface {

	// Create a new repo but without client connections
	Create() DocumentsRepo

	// Login to you database
	//
	// dbname: Can be selected only once ( for tenancy and security )
	//
	// defaultCollectionName: Can be changed anytime
	Login(ctx context.Context, dbname, defaultCollectionName, username, password string) (err error)

	// IsLoggedIn return if this connection is logged in to a db
	IsLoggedIn(ctx context.Context) bool

	// Logout from the DB, disconnect and free resources, so this repo can be used by somebody else
	Logout(ctx context.Context)

	// Exist will load an document if its exist, if not this function returned false
	//
	// If an error occurred, this will return false
	Exist(ctx context.Context, docID string, document interface{}) (exist bool)

	// MustLoad will load an document or panics if an error occurred
	MustLoad(ctx context.Context, docID string, document interface{})

	MustList(ctx context.Context, listType, templateid, pageKey string, limit int64) (docs []interface{}, newPageKey string)

	// MustNew will create a new empty document or panics if an error occurred
	MustNew(ctx context.Context, templateID string, draft bool) (newDocID string, newDoc interface{})

	MustSave(ctx context.Context, document interface{})

	// MustDelete will delete the latest rev of an docID or panics if an error occurred
	MustDelete(ctx context.Context, docID string)
}
