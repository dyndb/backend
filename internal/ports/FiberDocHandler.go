package ports

import "github.com/gofiber/fiber/v2"

// FiberDocHandler provide document handling
type FiberDocHandler interface {

	// expect parameter: type, limit
	// optional parameter ( should be set to - ): template, pagekey
	List(fctx *fiber.Ctx) error

	Create(fctx *fiber.Ctx) error
	Get(fctx *fiber.Ctx) error
	Save(fctx *fiber.Ctx) error
	Publish(fctx *fiber.Ctx) error
	Delete(fctx *fiber.Ctx) error
}
