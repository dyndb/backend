package domain

import "fmt"

type AttachmentLinks struct {
	Self     string `json:"self,omitempty"` // create a new empty doc
	Preview  string `json:"preview,omitempty"`
	Download string `json:"download,omitempty"`
	Delete   string `json:"delete,omitempty"`
}

func (links *AttachmentLinks) SetSelf(MetadataID string) (curLink *AttachmentLinks) {
	links.Self = fmt.Sprintf("/attachment/%s", MetadataID)
	return links
}

func (links *AttachmentLinks) SetPreview(BucketName, BlobID string) (curLink *AttachmentLinks) {
	links.Preview = fmt.Sprintf("/preview/%s/%s", BucketName, BlobID)
	return links
}

func (links *AttachmentLinks) SetDownload(BucketName, BlobID string) (curLink *AttachmentLinks) {
	links.Download = fmt.Sprintf("/attachment/%s/%s", BucketName, BlobID)
	return links
}

func (links *AttachmentLinks) SetDelete(MetadataID string) (curLink *AttachmentLinks) {
	links.Delete = fmt.Sprintf("/attachment/%s", MetadataID)
	return links
}
