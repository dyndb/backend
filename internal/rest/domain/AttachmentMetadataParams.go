package domain

import (
	"html"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/errwrap"
)

type AttachmentMetadataParams struct {
	MetadataID string
}

func (params *AttachmentMetadataParams) Read(fctx *fiber.Ctx) (err error) {

	// validator
	validate := validator.New()

	// MetadataID
	params.MetadataID = fctx.Params("metadataid")
	err = validate.Var(params.MetadataID, "required,excludesall=;")
	err = errwrap.WithStack(err)
	if err == nil {
		params.MetadataID = html.EscapeString(params.MetadataID)
	}

	return
}
