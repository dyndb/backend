package domain

import (
	"html"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/blobstorage/domain"
)

type BlobAndBucketParams struct {
	BucketName string
	BlobID     domain.BlobID
}

func (params *BlobAndBucketParams) Read(fctx *fiber.Ctx) (err error) {

	// validator
	validate := validator.New()

	// BucketName
	params.BucketName = fctx.Params("bucket", "-")
	err = validate.Var(params.BucketName, "required,excludesall=;")
	err = errors.WithStack(err)
	if err == nil {
		params.BucketName = html.EscapeString(params.BucketName)
	}

	// BucketName
	var BlobIDString string
	if err == nil {
		BlobIDString = fctx.Params("blobid", "-")
		err = validate.Var(BlobIDString, "required,excludesall=;")
		err = errors.WithStack(err)
	}
	if err == nil {
		BlobIDString = html.EscapeString(BlobIDString)
	}

	if err == nil {
		params.BlobID, err = domain.NewBlobIDFromString(BlobIDString)
	}

	return
}
