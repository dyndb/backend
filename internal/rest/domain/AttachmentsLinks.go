package domain

import "fmt"

type AttachmentsLinks struct {
	List   string `json:"list,omitempty"`
	Upload string `json:"upload,omitempty"`
}

func (links *AttachmentsLinks) SetList(BlobID string) (curLink *AttachmentsLinks) {
	links.List = fmt.Sprintf("/attachment/list/full/0/999/%s", BlobID)
	return links
}

func (links *AttachmentsLinks) SetUploadWithoutDoc(BucketName, BlobID string) (curLink *AttachmentsLinks) {
	links.Upload = "/attachment"
	return links
}

func (links *AttachmentsLinks) SetUploadToDoc(BlobID string) (curLink *AttachmentsLinks) {
	links.Upload = fmt.Sprintf("/attachment/%s", BlobID)
	return links
}
