package domain

import (
	"fmt"
)

type DocumentLinks struct {
	Self   string `json:"self,omitempty"`   // create a new empty doc
	Create string `json:"create,omitempty"` // create a new empty doc
	View   string `json:"view,omitempty"`   // view the document
	Edit   string `json:"edit,omitempty"`   // lock and get the complete document for edit
	Delete string `json:"delete,omitempty"` // Delete an document
	Save   string `json:"save,omitempty"`   // Save an document
	Link   string `json:"link,omitempty"`   // Link other elements to this doc
}

func (links *DocumentLinks) SetSelf(docid string) (curLink *DocumentLinks) {
	links.Self = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetCreate(docid string) (curLink *DocumentLinks) {
	links.Create = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetView(docid string) (curLink *DocumentLinks) {
	links.View = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetEdit(docid string) (curLink *DocumentLinks) {
	links.Edit = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetDelete(docid string) (curLink *DocumentLinks) {
	links.Delete = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetSave(docid string) (curLink *DocumentLinks) {
	links.Save = fmt.Sprintf("/document/%s", docid)
	return links
}

func (links *DocumentLinks) SetLink(docid string) (curLink *DocumentLinks) {
	links.Link = fmt.Sprintf("/link/document/%s", docid)
	return links
}
