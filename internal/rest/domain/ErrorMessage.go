package domain

import (
	"fmt"
	"strings"
)

type ErrorMessage struct {
	Messages []string `json:"messages"`
}

func ErrorMessageFromErr(err error) (msg ErrorMessage) {
	errorMessage := fmt.Sprintf("%+v", err)
	msg.Messages = strings.Split(errorMessage, "\n")
	return
}
