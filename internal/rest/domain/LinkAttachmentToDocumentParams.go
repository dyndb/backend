package domain

import (
	"html"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
)

type LinkAttachmentToDocumentParams struct {
	MetadataID  string `json:"metadataID"`
	DisplayText string `json:"displayText"`
	Mimetype    string `json:"mimetype,omitempty"`
}

func (params *LinkAttachmentToDocumentParams) Read(fctx *fiber.Ctx) (err error) {

	// validator
	validate := validator.New()

	// metadataid
	params.MetadataID = fctx.Params("metadataid", "-")
	err = validate.Var(params.MetadataID, "required,excludesall=;")
	err = errors.WithStack(err)
	if err == nil {
		params.MetadataID = html.EscapeString(params.MetadataID)
	}

	// docid
	err = fctx.BodyParser(params)

	if err == nil {
		err = validate.Var(params.MetadataID, "required,excludesall=;")
	}
	if err == nil {
		params.MetadataID = html.EscapeString(params.MetadataID)
	}

	if err == nil {
		err = validate.Var(params.DisplayText, "excludesall=;")
	}
	if err == nil {
		params.DisplayText = html.EscapeString(params.DisplayText)
	}
	if params.DisplayText == "" {
		params.DisplayText = "no display text"
	}

	if err == nil {
		err = validate.Var(params.Mimetype, "excludesall=;")
	}
	if err == nil {
		params.Mimetype = html.EscapeString(params.Mimetype)
	}

	return
}
