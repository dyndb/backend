package domain

import "gitlab.com/nerdhaltig/dyndb/backend/internal/templates/domain"

type CommonMetadata struct {
	TemplateDocID string `json:"tplDocID,omitempty"`

	// Layout of the document thats overwrite the template layout
	Layout []domain.TemplateLayout `json:"layout,omitempty"`
}
