package domain

import (
	"fmt"
	"strconv"
)

type PagedLinks struct {
	BasePath string `json:"-"`

	Self  string `json:"self,omitempty"` // link to current page
	First string `json:"first,omitempty"`
	Prev  string `json:"prev,omitempty"` // prev page if possible
	Next  string `json:"next,omitempty"` // next page if possible
}

func (links *PagedLinks) SetSelf(currentPageKey string, limit int) (curLink *PagedLinks) {
	if limit > 0 {
		links.Self = fmt.Sprintf("%s/%s/%v", links.BasePath, currentPageKey, limit)
	}
	return links
}

func (links *PagedLinks) SetFirst(limit int) (curLink *PagedLinks) {
	if limit > 0 {
		links.First = fmt.Sprintf("%s/0/%v", links.BasePath, limit)
	}
	return links
}

func (links *PagedLinks) SetPrev(currentPageKey string, limit int) (curLink *PagedLinks) {
	if currentPageKey != "" && limit > 0 {

		num, err := strconv.Atoi(currentPageKey)
		if err == nil {
			num = num - limit
			if num >= 0 {
				links.Prev = fmt.Sprintf("%s/%d/%v", links.BasePath, num, limit)
			}
		}

	}
	return links
}

func (links *PagedLinks) SetNext(pagekey string, limit int) (curLink *PagedLinks) {
	if pagekey != "" && limit > 0 {
		links.Next = fmt.Sprintf("%s/%s/%v", links.BasePath, pagekey, limit)
	}
	return links
}
