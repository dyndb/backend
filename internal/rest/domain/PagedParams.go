package domain

import (
	"html"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/errwrap"
)

type PagedParams struct {
	ListType     string
	TemplateName string
	PageKey      string
	PageLimit    int
}

func (params *PagedParams) Read(fctx *fiber.Ctx) (err error) {

	// validator
	validate := validator.New()

	// ListType
	params.ListType = fctx.Params("type", "full")
	err = validate.Var(params.ListType, "alphanum,excludesall=;")
	err = errwrap.WithStack(err)
	if err == nil {
		params.ListType = html.EscapeString(params.ListType)
	}

	// TemplateName
	if err == nil {
		params.TemplateName = fctx.Params("template", "-")
		err = validate.Var(params.TemplateName, "excludesall=;")
		err = errwrap.WithStack(err)
	}
	if err == nil {
		params.TemplateName = html.EscapeString(params.TemplateName)
	}

	// PageKey
	if err == nil {
		params.PageKey = fctx.Params("pagekey", "0")
		if params.PageKey == "-" {
			params.PageKey = "0"
		}
		err = validate.Var(params.PageKey, "numeric")
		err = errwrap.WithStack(err)
	}
	if err == nil {
		params.PageKey = html.EscapeString(params.PageKey)
	}

	// PageLimit
	if err == nil {
		params.PageLimit, err = fctx.ParamsInt("limit", 100)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		err = validate.Var(params.PageLimit, "numeric")
		err = errwrap.WithStack(err)
	}

	return
}
