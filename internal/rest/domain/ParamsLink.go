package domain

import (
	"html"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/errwrap"
)

type ParamsLinks struct {
	DocID    string
	SelfLink string `json:"_self"`
}

func (params *ParamsLinks) Read(fctx *fiber.Ctx) (err error) {

	// validator
	validate := validator.New()

	// DocID
	params.DocID = fctx.Params("docid", "-")
	err = validate.Var(params.DocID, "required,excludesall=;")
	err = errwrap.WithStack(err)

	if err == nil {
		params.DocID = html.EscapeString(params.DocID)
	}

	// payload
	if len(fctx.Body()) > 0 {
		err = fctx.BodyParser(&params)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		err = validate.Var(params.SelfLink, "required,excludesall=;")
		err = errwrap.WithStack(err)
	}

	return
}
