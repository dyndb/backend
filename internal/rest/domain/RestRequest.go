package domain

import "time"

// RestRequest is an incoming rest request that
// holds an RestMessage that should create a new workflow
type RestRequest struct {
	SkipAuth bool
	Timeout  time.Duration

	Message         any
	ResponseMessage any
}

type RestResult struct {
	Error   error
	Message any
}
