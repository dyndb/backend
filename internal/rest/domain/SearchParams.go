package domain

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/errwrap"
)

type SearchParams struct {
	Words []string `json:"words"`
}

func (params *SearchParams) Read(fctx *fiber.Ctx) (err error) {

	// payload
	if len(fctx.Body()) > 0 {
		err = fctx.BodyParser(params)
		err = errwrap.WithStack(err)
	}

	return
}
