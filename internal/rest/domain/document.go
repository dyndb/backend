package domain

import (
	"encoding/json"
	"strings"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/templates/domain"
)

type Document map[string]interface{}

func FromStruct(data interface{}) (newDoc Document) {

	newDoc = make(Document)
	dataJSON, dataJSONErr := json.Marshal(data)
	if dataJSONErr == nil {
		json.Unmarshal(dataJSON, &newDoc)
	}

	// we ensure that there is an display-text
	if _, exist := newDoc["displayText"]; !exist {
		newDoc["displayText"] = "displayText missing"
	}

	return
}

// AddLinks will add links to the document
//
// this will not delete existing links, they will be merged
func (doc Document) AddLinks(links interface{}) (err error) {

	// convert to map
	linksMap, ok := doc["_links"].(map[string]string)
	if !ok {
		linksMap = make(map[string]string)
	}

	var jsonData []byte
	jsonData, err = json.Marshal(links)
	if err == nil {
		err = json.Unmarshal(jsonData, &linksMap)
	}

	// write it back
	doc["_links"] = linksMap

	return
}

func (doc Document) SetEmbedded(docID string, ToEmbedd interface{}) {

	// convert to map
	linksMap, ok := doc["_embedded"].(map[string]interface{})
	if !ok {
		linksMap = make(map[string]interface{})
		doc["_embedded"] = linksMap
	}

	linksMap[docID] = ToEmbedd

}

func (doc Document) AddMetadata(data interface{}) (err error) {

	// convert to map
	linksMap, ok := doc["_metadata"].(map[string]string)
	if !ok {
		linksMap = make(map[string]string)
	}

	var jsonData []byte
	jsonData, err = json.Marshal(data)
	if err == nil {
		err = json.Unmarshal(jsonData, &linksMap)
	}

	// write it back
	doc["_metadata"] = linksMap

	return
}

func (doc Document) SetLayout(layout []domain.TemplateLayout) {
	// write it back
	doc["_layout"] = layout
}

// Keep remove all other fields except they starting with "_"
func (doc Document) Keep(fields []string) {

	for docField := range doc {

		if strings.HasPrefix(docField, "_") {
			continue
		}

		docFieldKeep := false
		for _, field := range fields {
			if field == docField {
				docFieldKeep = true
				break
			}
		}

		if !docFieldKeep {
			delete(doc, docField)
		}

	}

}

// Cleanup keep the pure document and remove _embedded, _links and _metadata
func (doc Document) Cleanup() {
	delete(doc, "_embedded")
	delete(doc, "_links")
	delete(doc, "_metadata")
}
