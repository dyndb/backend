package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func TestKeep(t *testing.T) {
	testDoc := make(domain.Document)
	testDoc["displayText"] = "test"
	testDoc["description"] = "description"
	testDoc["other"] = true

	testDoc.AddLinks(domain.DocumentLinks{})

	testDoc["_keep"] = "ui"

	testDoc.Keep([]string{"displayText", "description"})

	assert.Contains(t, testDoc, "displayText")
	assert.Contains(t, testDoc, "description")
	assert.Contains(t, testDoc, "_keep")
	assert.Contains(t, testDoc, "_links")
	assert.NotContains(t, testDoc, "other")
}
