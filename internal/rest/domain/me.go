package domain

type InfoLinks struct {
	TemplateNames  string `json:"templates"`
	DocumentSearch string `json:"documentsearch"`
}

// SetPagedDocNextPage set the link for first page
func (links *InfoLinks) SetGetTemplateList() {
	links.TemplateNames = "/documents/list/short/dyndb_template/0/10"
}

func (links *InfoLinks) SetDocumentSearch() {
	links.DocumentSearch = "/documents/search/short/0/10"
}
