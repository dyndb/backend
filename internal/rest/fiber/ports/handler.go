package ports

import "github.com/gofiber/fiber/v2"

type RoutePort interface {
	RegisterHandler(baseRoute fiber.Router)
}
