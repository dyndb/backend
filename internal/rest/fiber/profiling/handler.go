package profiling

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/fiber/ports"
)

// go tool pprof -http :4001 http://localhost:3535/debug/pprof/heap

type profilingHandler struct {
}

func NewProfilingHandler() ports.RoutePort {
	return &profilingHandler{}
}

func (h *profilingHandler) RegisterHandler(baseRoute fiber.Router) {
	baseRoute.Use(pprof.New())

}
