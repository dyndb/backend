package fiber

import (
	"net/http"
	"net/url"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/logger"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/fiber/profiling"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/ui"
	"gitlab.com/nerdhaltig/errwrap"
)

type server struct {
	host string
	app  *fiber.App
	V1   fiber.Router
}

func NewService(
	host string,
	version string,
) *server {

	newServer := &server{
		host: host,
	}

	newServer.app = fiber.New(fiber.Config{
		AppName: "dyndb " + version,

		BodyLimit:       100 * 1024 * 1024,
		WriteBufferSize: 100 * 1024 * 1024,
		ReadBufferSize:  config.Values.Limits.RequestHeaderSize,

		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's an fiber.*Error
			if e, ok := err.(*fiber.Error); ok {
				code = e.Code
			}

			// token expired
			if err.Error() == "unauthenticated" {
				code = http.StatusUnauthorized
			}

			// Return from handler
			return ctx.Status(code).SendString(err.Error())
		},
		EnablePrintRoutes: true,
		DisableKeepalive:  false,
	})

	// CORS for external resources
	newServer.app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowHeaders:     "Cache-Control",
		AllowCredentials: true,
	}))

	// recover on panics
	newServer.app.Use(func(fctx *fiber.Ctx) (err error) {
		defer func() {
			if r := recover(); r != nil {
				status, errorMessage := appcontext.Recover(r)
				fctx.Status(status).Send([]byte(errorMessage))
			}
		}()
		return fctx.Next()
	})

	// logger
	newServer.app.Use(logger.New())

	// serve the static ui files
	ui.ServeFiber(newServer.app)

	// /api/v1
	api := newServer.app.Group("/api") // /api

	newServer.V1 = api.Group("/v1")

	// are we up ?
	newServer.V1.Get("/up", func(c *fiber.Ctx) error {
		return c.SendStatus(fiber.StatusOK)
	})

	// doc handler
	// handler = doc.NewDocHandler(supervisor)
	// handler.RegisterHandler(newServer.V1)

	// profiling
	handler := profiling.NewProfilingHandler()
	handler.RegisterHandler(newServer.app)

	return newServer
}

func (srv *server) Serve() {
	parsedURL, err := url.Parse(srv.host)
	errwrap.PanicOnError(err)

	err = srv.app.Listen(parsedURL.Host)
	errwrap.PanicOnError(err)
}
