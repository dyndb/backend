package hal

// Ensure check if an field name exist, and set 'value' if not
func (doc Doc) Ensure(fieldName string, value any) {
	_, exist := doc[fieldName]
	if !exist {
		doc[fieldName] = value
	}
}
