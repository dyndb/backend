package hal

import "encoding/json"

type Doc map[string]interface{}

func FromStruct(links interface{}) (newHalDocument Doc, err error) {

	newHalDocument = make(Doc)

	if links != nil {
		var jsonData []byte
		jsonData, err = json.Marshal(links)
		if err == nil {
			err = json.Unmarshal(jsonData, &newHalDocument)
		}
	}

	return
}

// Cleanup keep the pure document and remove _metadata, _links etc.
func (doc Doc) Cleanup() {
	delete(doc, "_embedded")
	delete(doc, "_links")
	delete(doc, "_metadata")
}

func (doc Doc) AppendEmbeddedDocArray(docname string, subdoc Doc) {

	embeddedInterface, exist := doc["_embedded"]
	if !exist {
		embeddedInterface = make(map[string]interface{})
		doc["_embedded"] = embeddedInterface
	}
	embeddedDocsAll, _ := embeddedInterface.(map[string]interface{})
	embeddedDocsInterface, exist := embeddedDocsAll[docname]
	if !exist {
		embeddedDocsInterface = []map[string]interface{}{}
		embeddedDocsAll[docname] = embeddedDocsInterface
	}
	embeddedDocs, _ := embeddedDocsInterface.([]map[string]interface{})

	// append and write it back
	if subdoc != nil {
		embeddedDocs = append(embeddedDocs, subdoc)
	}

	embeddedDocsAll[docname] = embeddedDocs
}

func (doc Doc) AppendEmbeddedDoc(docname string, subdoc Doc) {

	embeddedInterface, exist := doc["_embedded"]
	if !exist {
		embeddedInterface = make(map[string]interface{})
		doc["_embedded"] = embeddedInterface
	}
	embeddedDocsAll, _ := embeddedInterface.(map[string]interface{})
	embeddedDocsAll[docname] = subdoc
}
