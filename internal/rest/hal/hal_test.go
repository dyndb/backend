package hal

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestLinkForDoc struct {
	Download string `json:"download,omitempty"`
	Upload   string `json:"upload,omitempty"`
}

type TestDoc struct {
	DisplayName string
}

func TestLinks(t *testing.T) {

	newDoc := map[string]string{
		"DisplayName": "Some text",
	}

	halDoc, err := FromStruct(newDoc)
	assert.NoError(t, err)

	err = halDoc.SetLinks(TestLinkForDoc{
		Download: "/download",
	})
	assert.NoError(t, err)

	err = halDoc.SetLinks(TestLinkForDoc{
		Upload: "/upload",
	})
	assert.NoError(t, err)

}

func TestEmbedding(t *testing.T) {

	newDoc := map[string]interface{}{
		"DisplayName": "Paging",
	}

	halDoc, err := FromStruct(newDoc)
	assert.NoError(t, err)

	halDoc.AppendEmbeddedDocArray("documents", map[string]interface{}{
		"username": "john doe",
	})
	halDoc.AppendEmbeddedDocArray("documents", map[string]interface{}{
		"username": "eva nobody",
	})

}
