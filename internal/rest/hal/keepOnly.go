package hal

// KeepOnly will remove all elements except fields
func (doc Doc) KeepOnly(fields ...string) {
	for fieldNameInDoc := range doc {
		found := false
		for _, fieldNameToKeep := range fields {
			if fieldNameToKeep == fieldNameInDoc {
				found = true
			}
		}
		if !found {
			delete(doc, fieldNameInDoc)
		}
	}
}
