package hal

import "encoding/json"

type Links map[string]interface{}

// SetLinks will set the links into the doc
func CreateLinks(doc map[string]interface{}) Links {
	var links Links = make(Links)
	doc["_links"] = links
	return links
}

func (doc Doc) PatchLinks(linksToPatch any) (err error) {

	var jsonData []byte

	currentLinks := doc["_links"]

	var currentLinksMap map[string]interface{}
	if err == nil {
		jsonData, err = json.Marshal(currentLinks)
	}
	if err == nil {
		err = json.Unmarshal(jsonData, &currentLinksMap)
	}

	var linksToPatchMap map[string]interface{}
	if err == nil {
		jsonData, err = json.Marshal(linksToPatch)
	}
	if err == nil {
		err = json.Unmarshal(jsonData, &linksToPatchMap)
	}

	if err == nil {
		for linkName, linkData := range linksToPatchMap {
			currentLinksMap[linkName] = linkData
		}
	}

	// write it back
	if err == nil {
		err = doc.setSection("_links", currentLinksMap)
	}

	return
}

func (doc Doc) SetLinks(links interface{}) (err error) {
	return doc.setSection("_links", links)
}
