package hal

type Metadata map[string]interface{}

/* type Metadata struct {
	TemplateName string `json:"templatename,omitempty"`
	Builtin      bool   `json:"builtin,omitempty"`
} */

// SetLinks will set the links into the doc
func CreateMetadata(doc map[string]interface{}) *Metadata {
	newMetadata := &Metadata{}
	doc["_metadata"] = newMetadata
	return newMetadata
}

func (doc Doc) SetMetadata(metadata interface{}) (err error) {
	return doc.setSection("_metadata", metadata)
}
