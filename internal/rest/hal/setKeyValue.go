package hal

import "encoding/json"

func (doc Doc) SetKeyValue(section string, key string, value interface{}) (err error) {

	var sectionMap map[string]interface{}

	// load current values
	sectionData, sectionDataExist := doc[section]
	if sectionDataExist {
		jsonData, _ := json.Marshal(sectionData)
		json.Unmarshal(jsonData, &sectionMap)
	} else {
		sectionMap = make(map[string]interface{})
	}

	sectionMap[key] = value
	return
}
