package hal

import "encoding/json"

func (doc Doc) setSection(section string, values interface{}) (err error) {

	var curDocValues map[string]interface{}
	var newDocValues map[string]interface{}

	// load current values
	curDocValuesInterface, curDocValuesExist := doc[section]
	if curDocValuesExist {
		var docLinksJSON []byte
		docLinksJSON, err = json.Marshal(curDocValuesInterface)
		if err == nil {
			err = json.Unmarshal(docLinksJSON, &curDocValues)
		}
	} else {
		curDocValues = make(map[string]interface{})
	}

	// convert new values
	if err == nil {
		var jsonData []byte
		jsonData, err = json.Marshal(values)

		if err == nil {
			newDocValues = make(map[string]interface{})
			err = json.Unmarshal(jsonData, &newDocValues)
		}
	}

	// update doc
	if err == nil {
		for name, url := range newDocValues {
			curDocValues[name] = url
		}
	}

	doc[section] = curDocValues

	return

}
