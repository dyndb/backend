package hallinks

import "fmt"

func (links HALLinks) SetAttachmentSelf(attachmentID string) HALLinks {
	links["self"] = fmt.Sprintf("/attachment/%s", attachmentID)
	return links
}

func (links HALLinks) SetAttachmentDownload(attachmentID string) HALLinks {
	links["download"] = fmt.Sprintf("/attachment/%s", attachmentID)
	return links
}

func (links HALLinks) SetAttachmentDelete(attachmentID string) HALLinks {
	links["delete"] = fmt.Sprintf("/attachment/%s", attachmentID)
	return links
}

func (links HALLinks) SetAttachmentsList(docID string) HALLinks {
	links["attmtlist"] = fmt.Sprintf("/attachments/list/%s", docID)
	return links
}

func (links HALLinks) SetAttachmentsUpload(docID string) HALLinks {
	links["attmtupload"] = fmt.Sprintf("/attachment/%s", docID)
	return links
}
