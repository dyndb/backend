package hallinks

import (
	"errors"
	"fmt"
	"strings"
)

type HALLinks map[string]string

// SetLinks will set the links into the doc
func Create(doc map[string]interface{}) HALLinks {
	var links HALLinks = make(HALLinks)
	doc["_links"] = links
	return links
}

func (links HALLinks) SetDocSelf(docID string) HALLinks {
	links["self"] = fmt.Sprintf("/document/%s", docID)
	return links
}

func (links HALLinks) DocSelf() string {
	return links["self"]
}

func DocSelfUnpack(link string) (docID string, err error) {

	link = strings.Trim(link, "/")
	values := strings.Split(link, "/")

	if len(values) == 0 {
		err = errors.New("link mismatch")
	}
	if values[0] != "document" {
		err = errors.New("link mismatch")
	}
	docID = values[1]

	return
}

func (links HALLinks) SetDocCreate() HALLinks {
	links["create"] = fmt.Sprintf("/document/-")
	return links
}

func (links HALLinks) SetDocDelete(docID string) HALLinks {
	links["delete"] = fmt.Sprintf("/document/%s", docID)
	return links
}

func (links HALLinks) SetDocSave(docID string) HALLinks {
	links["save"] = fmt.Sprintf("/document/%s", docID)
	return links
}
