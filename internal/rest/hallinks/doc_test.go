package hallinks_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hallinks"
)

func TestDocUnlink(t *testing.T) {

	var newDoc map[string]interface{} = make(map[string]interface{})
	newLinks := hallinks.Create(newDoc)

	newLinks.SetDocSelf("dummy")

	// unpack
	newDocSelfLinks := newLinks.DocSelf()
	curDocID, curDocErr := hallinks.DocSelfUnpack(newDocSelfLinks)
	assert.NoError(t, curDocErr)
	assert.Equal(t, "dummy", curDocID)
}
