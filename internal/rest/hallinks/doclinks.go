package hallinks

import (
	"fmt"
)

func (links HALLinks) SetDocLinksList(template, docid string) HALLinks {
	links["doclinkslist"] = fmt.Sprintf("/doclink/%s/%s", template, docid)
	return links
}

func (links HALLinks) SetDocLinksAdd(firstDocTemplate, firstDocID string) HALLinks {
	links["doclinksadd"] = fmt.Sprintf("/doclink/%s/%s", firstDocTemplate, firstDocID)
	return links
}

func (links HALLinks) SetDocLinksUnlink(firstDocTemplate, firstDocID, secondDocTemplate, secondDocID string) HALLinks {
	links["doclinkremove"] = fmt.Sprintf("/doclink/%s/%s/%s/%s", firstDocTemplate, firstDocID, secondDocTemplate, secondDocID)
	return links
}
