package hallinks

import (
	"fmt"
)

type PagedLinks struct {
	First string
	Prev  string
	Next  string
}

// SetPagedDocNextPage set the link for first page
func (links *PagedLinks) SetPagedDocFirstPage(listtype, template string, limit int) {
	links.First = fmt.Sprintf("/documents/list/%s/%s/0/%v", listtype, template, limit)
}

// SetPagedDocNextPage set the link for prev page
func (links *PagedLinks) SetPagedDocPrevPage(listtype, template, pagekey string, limit int) {
	links.Prev = fmt.Sprintf("/documents/list/%s/%s/%s/%v", listtype, template, pagekey, limit)
}

// SetPagedDocNextPage set the link for next page
func (links *PagedLinks) SetPagedDocNextPage(listtype, template, pagekey string, limit int) {
	links.Next = fmt.Sprintf("/documents/list/%s/%s/%s/%v", listtype, template, pagekey, limit)
}

// SetPagedDocNextPage set the link for first page
func (links HALLinks) SetPagedDocFirstPage(listtype, template string, limit int) {
	links["first"] = fmt.Sprintf("/documents/list/%s/%s/0/%v", listtype, template, limit)
}

// SetPagedDocNextPage set the link for prev page
func (links HALLinks) SetPagedDocPrevPage(listtype, template, pagekey string, limit int) {
	links["prev"] = fmt.Sprintf("/documents/list/%s/%s/%s/%v", listtype, template, pagekey, limit)
}

// SetPagedDocNextPage set the link for next page
func (links HALLinks) SetPagedDocNextPage(listtype, template, pagekey string, limit int) {
	links["next"] = fmt.Sprintf("/documents/list/%s/%s/%s/%v", listtype, template, pagekey, limit)
}

// SetPagedDocSearch set the link for searching for documents
func (links HALLinks) SetPagedDocSearch(listtype, template, pagekey string, limit int) HALLinks {
	links["search"] = fmt.Sprintf("/documents/search/%s/%s/%s/%v", listtype, template, pagekey, limit)
	return links
}
