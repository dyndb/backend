package resttests

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

type TestCases struct {
	Method         string
	Description    string // description of the test case
	Route          string // route path to test
	Body           string
	MultipartFiles []string
	ExpectedCode   int // expected HTTP status code
	ExpectedBody   string

	ResultStoreKey string // store result to an internal map with this key
	ResultUseKey   string // load data from the internal map as body
}

func TestEndpoints(t *testing.T, app *fiber.App, cases []TestCases) {

	var refreshToken string
	var accessToken string
	var curCookies []*http.Cookie = nil
	var resultData map[string][]byte = make(map[string][]byte)

	// Iterate through test single test cases
	for _, test := range cases {

		// request-body
		reqBody := []byte(test.Body)
		if test.ResultUseKey != "" {
			if tmpData, tmpDataExist := resultData[test.ResultUseKey]; tmpDataExist {
				reqBody = tmpData
			}
		}

		// Create a new http request with the route from the test case
		req := httptest.NewRequest(test.Method, test.Route, bytes.NewBuffer(reqBody))
		req.Header.Set("Content-Type", "application/json")

		if refreshToken != "" {
			req.Header.Set("X-Refresh-Token", refreshToken)
		}
		if accessToken != "" {
			req.Header.Set("X-Access-Token", accessToken)
		}

		if curCookies != nil {
			for _, curCookie := range curCookies {
				req.AddCookie(curCookie)
			}
		}

		if len(test.MultipartFiles) > 0 {

			var writer *multipart.Writer
			for _, path := range test.MultipartFiles {

				// multipart
				file, err := os.Open(path)
				if err != nil {
					continue
				}
				defer file.Close()

				body := &bytes.Buffer{}
				writer = multipart.NewWriter(body)
				part, err := writer.CreateFormFile("documents", filepath.Base(path))
				if err != nil {
					continue
				}

				io.Copy(part, file)
			}

			req.Header.Set("Content-Type", writer.FormDataContentType())
		}
		// Perform the request plain with the app,
		// the second argument is a request latency
		// (set to -1 for no latency)
		resp, _ := app.Test(req, -1)

		refreshToken = resp.Header.Get("X-Refresh-Token")
		accessToken = resp.Header.Get("X-Access-Token")

		// remember cookie if needed
		if len(resp.Cookies()) > 0 {
			curCookies = resp.Cookies()
		}

		// remember body if requested
		bodyData, _ := io.ReadAll(resp.Body)
		if test.ResultStoreKey != "" {
			resultData[test.ResultStoreKey] = bodyData
		}

		// Logging
		event := log.Debug()
		if len(bodyData) > 0 {
			if bodyData[0] == 'O' {
				event = event.Bytes("body", bodyData)
			} else {
				event = event.RawJSON("body", bodyData)
			}
		}
		event.Send()

		// Verify, if the status code is as expected
		assert.Equalf(t, test.ExpectedCode, resp.StatusCode, test.Description)

		// log respond
		if test.ExpectedBody != "" {
			assert.Equal(t, test.ExpectedBody, string(bodyData))
		}

	}

}
