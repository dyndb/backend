package restactor

import (
	"net/http"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	restDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func (svc *RestToActorService) SendToActorAndWaitForMessage(
	fctx *fiber.Ctx,
	newRequest *restDomain.RestRequest,
) (result restDomain.RestResult) {

	if newRequest.Timeout == 0 {
		newRequest.Timeout = time.Second * 30
	}

	// // get tokens
	AccessToken := fctx.Cookies("dyndb-token-access", "")
	RefreshToken := fctx.Cookies("dyndb-token-refresh", "")

	// ctx
	var ctx jobcontext.Ctx

	// authenticate
	var auth *tokenDomain.AuthValidated = &tokenDomain.AuthValidated{}
	if newRequest.SkipAuth {
		auth.Tenant = "dontuseme"
	} else {
		ctx, result.Error = svc.concentrator.WaitForMessages(nil, concentrator.Job{
			Message: &tokenDomain.AuthValidateRequest{
				AccessToken:  AccessToken,
				RefreshToken: RefreshToken,
			},
			ExpectedReturnMessage: &auth,
		})
	}

	// send request
	if result.Error == nil {
		ctx, result.Error = svc.concentrator.WaitForMessages(ctx,
			concentrator.Job{
				Message:               newRequest.Message,
				ExpectedReturnMessage: newRequest.ResponseMessage,
			},
		)
	}

	// read auth
	if result.Error == nil {
		if newRequest.SkipAuth {
			ctx.As(&auth)
		}
	}

	// set tokens
	if auth.AccessToken != "" {
		fctx.Cookie(&fiber.Cookie{
			Name:        "dyndb-token-access",
			Value:       auth.AccessToken,
			Domain:      svc.publicDomain,
			SessionOnly: true,
			SameSite:    "Strict",
		})
	}
	if auth.RefreshToken != "" {
		fctx.Cookie(&fiber.Cookie{
			Name:        "dyndb-token-refresh",
			Value:       auth.RefreshToken,
			Domain:      svc.publicDomain,
			SessionOnly: true,
			SameSite:    "Strict",
		})
	}

	if result.Error == nil {
		fctx.Status(http.StatusOK)
		result.Message = newRequest.ResponseMessage
		result.Error = nil
	} else {
		fctx.Status(http.StatusInternalServerError)
	}

	// check unauthenticated
	if result.Error != nil {
		errorString := result.Error.Error()
		if strings.Contains(errorString, "unauthenticated") {
			fctx.Status(http.StatusUnauthorized)
		}
	}

	return
}
