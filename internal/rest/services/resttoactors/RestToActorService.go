package restactor

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
)

type RestToActorService struct {
	concentrator goactor.Concentrator
	publicDomain string
}

func NewRestToActorService(concentrator goactor.Concentrator, PublicDomain string) usecase.RestToActor {
	return &RestToActorService{
		concentrator: concentrator,
		publicDomain: PublicDomain,
	}
}
