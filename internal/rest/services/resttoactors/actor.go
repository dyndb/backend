package restactor

import (
	"time"
)

type Actor struct {
}

// RestRequest is an incoming rest request that
// holds an RestMessage that should create a new workflow
type RestRequest struct {
	SkipAuth bool
	Timeout  time.Duration

	RefreshToken string
	AccessToken  string
	APIToken     string

	RestMessage any

	ExpectedMessageType any
	RestResult          chan RestResult
}

type RestRequestDone struct{}

type RestResult struct {
	HTTPState int
	Mimetype  string
	Message   any
	Error     error
	Stack     string

	RefreshToken string
	AccessToken  string
}

func NewRestActor() (actor *Actor) {

	// // create actor
	// actor = &Actor{
	// 	RestRequest:     &RestRequest{},
	// 	RestRequestDone: &RestRequestDone{},
	// }

	// // this actor use the RestMessage inside the RestRequest and return it
	// // normally you would check tokens, and so on...
	// actor.Actor = goactor.NewActor("rest", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

	// 	switch msg := ctx.Message().(type) {

	// 	// we ignore core messages
	// 	case
	// 		*actorstates.Initialized,
	// 		*actorstates.Started,
	// 		*actorstates.Stopped:
	// 		break

	// 	case error:

	// 		restRequest := &RestRequest{}
	// 		if ctx.As(&restRequest) {

	// 			// unauth-error
	// 			if msg.Error() == "unauthenticated" {
	// 				restRequest.RestResult <- RestResult{
	// 					HTTPState: http.StatusUnauthorized,
	// 					Message:   msg.Error(),
	// 					Error:     msg,
	// 				}
	// 			} else {
	// 				restRequest.RestResult <- RestResult{
	// 					HTTPState: http.StatusBadRequest,
	// 					Message:   msg.Error(),
	// 					Error:     msg,
	// 				}
	// 			}
	// 		}

	// 	case *RestRequest:

	// 		if msg.SkipAuth {
	// 			returnMessage = append(returnMessage, &actorfortokens.AuthValidated{
	// 				Tenant: "dontuseme",
	// 			})
	// 			returnMessage = append(returnMessage, msg.RestMessage)
	// 		} else {
	// 			returnMessage = append(returnMessage, &actorfortokens.AuthValidateRequest{
	// 				AccessToken:  msg.AccessToken,
	// 				RefreshToken: msg.RefreshToken,
	// 			})
	// 			returnMessage = append(returnMessage, msg.RestMessage)
	// 		}

	// 	case *RestRequestDone:

	// 		restRequest := &RestRequest{}
	// 		if !ctx.As(&restRequest) {
	// 			err = errors.WithStack(errors.New("not an rest request !"))
	// 			break
	// 		}

	// 		authenticated := &actorfortokens.AuthValidated{}
	// 		if !ctx.As(&authenticated) {
	// 			err = errors.WithStack(errors.New("not authenticated"))
	// 			break
	// 		}

	// 		// get answer
	// 		msgReflect := reflect.ValueOf(restRequest.ExpectedMessageType)
	// 		ReturnMessageTypeString := msgReflect.Type().String()
	// 		payload, payloadExist := ctx.MessageGet(ReturnMessageTypeString)

	// 		if payloadExist {
	// 			restRequest.RestResult <- RestResult{
	// 				HTTPState:    http.StatusOK,
	// 				AccessToken:  authenticated.AccessToken,
	// 				RefreshToken: authenticated.RefreshToken,
	// 				Message:      payload,
	// 			}
	// 		} else {
	// 			restRequest.RestResult <- RestResult{
	// 				HTTPState:    http.StatusNotFound,
	// 				AccessToken:  authenticated.AccessToken,
	// 				RefreshToken: authenticated.RefreshToken,
	// 				Error:        errors.WithStack(errors.New("answer not found")),
	// 			}
	// 		}

	// 	}

	// 	return
	// })

	return
}
