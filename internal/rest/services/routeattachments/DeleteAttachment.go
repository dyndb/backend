package routeattachments

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) DeleteAttachment(baseRoute fiber.Router) {
	baseRoute.Add("DELETE", "/attachment/:metadataid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.AttachmentMetadataParams{}
		err = params.Read(fctx)

		previewOpened := &domain.AttachmentDeleted{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &domain.AttachmentDeleteRequest{
					MetadataID: params.MetadataID,
				},
				ResponseMessage: &previewOpened,
			})
			err = result.Error
		}

		return err
	})

}
