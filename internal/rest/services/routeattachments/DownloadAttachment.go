package routeattachments

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) DownloadAttachment(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/attachment/:bucket/:blobid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.BlobAndBucketParams{}
		err = params.Read(fctx)

		attachmentOpened := &domain.AttachmentOpened{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &domain.AttachmentOpenRequest{
					BucketName: params.BucketName,
					BlobID:     params.BlobID,
				},
				ResponseMessage: &attachmentOpened,
			})
			err = result.Error
		}

		var attachmentFile *os.File
		if err == nil {
			attachmentFile, err = os.OpenFile(attachmentOpened.FilePath, os.O_RDONLY, 0)
		}

		if err == nil {
			// create hal-document
			return fctx.SendStream(attachmentFile)
		} else {
			return err
		}

	})

}
