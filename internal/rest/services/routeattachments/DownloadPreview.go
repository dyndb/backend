package routeattachments

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) DownloadPreview(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/preview/:bucket/:blobid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.BlobAndBucketParams{}
		err = params.Read(fctx)

		previewOpened := &domain.PreviewOpened{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &domain.PreviewOpenRequest{
					BucketName: params.BucketName,
					BlobID:     params.BlobID,
				},
				ResponseMessage: &previewOpened,
			})
			err = result.Error
		}

		var attachmentFile *os.File
		if err == nil {
			attachmentFile, err = os.OpenFile(previewOpened.FilePath, os.O_RDONLY, 0)
		}

		if err == nil {
			// create hal-document
			return fctx.SendStream(attachmentFile)
		} else {

			return err
		}

	})

}
