package routeattachments

import (
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hal"
)

func (route *routeData) GetAttachment(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/attachment/:metadataid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.AttachmentMetadataParams{}
		err = params.Read(fctx)

		// metadata-id
		var metadataID blobMetadataDomain.MetadataID
		if err == nil {
			metadataID, err = blobMetadataDomain.NewMetadataIDFromString(params.MetadataID)
		}

		// request-message
		var result myDomain.RestResult
		documentList := &blobMetadataDomain.BlobMetadataList{}

		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &blobMetadataDomain.BlobMetadataListRequest{
					MetadataIDs: []blobMetadataDomain.MetadataID{metadataID},
				},
				ResponseMessage: &documentList,
			})
			err = result.Error
		}

		metadata := documentList.List[metadataID]

		// create hal-document
		var halDoc hal.Doc
		if err == nil {
			halDoc, err = hal.FromStruct(documentList.List[metadataID])
			err = errors.WithStack(err)
		}

		// links
		if err == nil {
			attachmentLinks := myDomain.AttachmentLinks{}
			attachmentLinks.SetSelf(metadataID.String())
			attachmentLinks.SetPreview(metadata.BucketName, metadata.BlobID)
			attachmentLinks.SetDownload(metadata.BucketName, metadata.BlobID)
			attachmentLinks.SetDelete(metadataID.String())

			halDoc.SetLinks(attachmentLinks)
		}

		if err == nil {
			return fctx.JSON(halDoc)
		} else {
			return err
		}
	})

}
