package routeattachments

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/usecase"
)

// adapter
type routeData struct {
	restToActor usecase.RestToActor
}

func Register(
	baseRoute fiber.Router,
	restToActor usecase.RestToActor,
) {
	newRoute := &routeData{
		restToActor: restToActor,
	}

	newRoute.UploadAttachment(baseRoute)
	newRoute.ListAttachments(baseRoute)
	newRoute.Search(baseRoute)
	newRoute.GetAttachment(baseRoute)
	newRoute.DownloadAttachment(baseRoute)
	newRoute.DownloadPreview(baseRoute)
	newRoute.DeleteAttachment(baseRoute)
}
