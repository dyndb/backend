package routeattachments

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) Search(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/attachment/search/:type/:pagekey/:limit", func(fctx *fiber.Ctx) (err error) {

		// search-words
		paramsSearch := myDomain.SearchParams{}
		if err == nil {
			err = paramsSearch.Read(fctx)
		}

		// paged
		paramsPaged := myDomain.PagedParams{}
		if err == nil {
			err = paramsPaged.Read(fctx)
		}

		// request-message
		var result myDomain.RestResult
		documentList := &blobMetadataDomain.BlobMetadataList{}

		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &blobMetadataDomain.BlobMetadataSearchRequest{
					SearchWords: paramsSearch.Words,
					PageKey:     paramsPaged.PageKey,
					PageLimit:   int64(paramsPaged.PageLimit),
				},
				ResponseMessage: &documentList,
			})
			err = result.Error
		}

		// create hal-document
		halDoc := make(myDomain.Document)
		if err == nil {

			for metadataID, metadata := range documentList.List {

				attachmentLinks := myDomain.AttachmentLinks{}
				attachmentLinks.SetSelf(metadataID.String())
				attachmentLinks.SetPreview(metadata.BucketName, metadata.BlobID)
				attachmentLinks.SetDownload(metadata.BucketName, metadata.BlobID)
				attachmentLinks.SetDelete(metadataID.String())

				metadataDoc := myDomain.FromStruct(metadata)
				metadataDoc.AddLinks(attachmentLinks)
				halDoc.SetEmbedded(metadataID.String(), metadataDoc)
			}

			pagedLinks := &myDomain.PagedLinks{BasePath: fmt.Sprintf("/attachment/search/%s", paramsPaged.ListType)}
			pagedLinks.SetFirst(paramsPaged.PageLimit)
			pagedLinks.SetNext(documentList.NextPageKey, paramsPaged.PageLimit)
			err = halDoc.AddLinks(pagedLinks)
		}

		if err == nil {
			result.Message = halDoc
		} else {
			result.Message = myDomain.ErrorMessageFromErr(err)
		}

		return fctx.JSON(result.Message)
	})
}
