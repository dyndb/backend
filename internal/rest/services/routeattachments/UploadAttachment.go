package routeattachments

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/attachment/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/config"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) UploadAttachment(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/attachment", func(fctx *fiber.Ctx) (err error) {

		// Parse the multipart form:
		var form *multipart.Form
		form, err = fctx.MultipartForm()
		err = errors.WithStack(err)

		var result myDomain.RestResult
		var MetadataIds []string

		for _, MultipartFileHeader := range form.File {
			for _, MultipartFile := range MultipartFileHeader {

				fmt.Println(MultipartFile.Filename, MultipartFile.Size, MultipartFile.Header["Content-Type"][0])
				// => "tutorial.pdf" 360641 "application/pdf"

				tempFileName := uuid.Must(uuid.NewRandom()).String()
				tempFileName += path.Ext(MultipartFile.Filename)

				tempFilePath := strings.TrimRight(config.Values.Server.TempFolder, "/")
				tempFilePath += "/"
				tempFilePath += tempFileName

				var tempFile *os.File
				if err == nil {
					tempFile, err = os.Create(tempFilePath)
				}

				var filehandle multipart.File
				if err == nil {
					filehandle, err = MultipartFile.Open()
				}

				if err == nil {
					_, err = io.Copy(tempFile, filehandle)
				}

				attachmentUploaded := &domain.AttachmentUploaded{}
				if err == nil {
					result = route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
						Message: &domain.AttachmentUploadRequest{
							DownloadedFilePath: tempFilePath,
							DisplayText:        MultipartFile.Filename,
							OriginalFilename:   MultipartFile.Filename,
						},
						ResponseMessage: &attachmentUploaded,
					})
					err = result.Error
				}

				// remove file after successful add to storage
				if err == nil {
					os.Remove(tempFilePath)
				}

				if err == nil {
					MetadataIds = append(MetadataIds, attachmentUploaded.MetadataID.String())
				}

				if err != nil {
					break
				}

			}
		}

		if err == nil {
			return fctx.JSON(MetadataIds)
		} else {
			return err
		}

	})

}
