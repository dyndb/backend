package routeauth

import (
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	restDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	userDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
)

type CreateCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (route *routeData) CreateUser(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/login/new", func(fctx *fiber.Ctx) (err error) {

		var loginData CreateCredentials

		// parse logindata
		err = fctx.BodyParser(&loginData)
		err = errors.WithStack(err)

		// request-message
		var result restDomain.RestResult
		userSaved := &userDomain.UserSaved{}
		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &restDomain.RestRequest{
				Message: &userDomain.UserSaveRequest{
					Username: loginData.Username,
					Password: loginData.Password,
				},
				ResponseMessage: &userSaved,
			})
			err = result.Error
		}

		if err == nil {
			result.Message = userSaved
		} else {
			result.Message = restDomain.ErrorMessageFromErr(result.Error)
		}

		return fctx.JSON(result.Message)
	})
}
