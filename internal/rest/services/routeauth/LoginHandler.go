package routeauth

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	authDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/auth/domain"
	restDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/errwrap"
)

type LoginCredentials struct {
	Tenant   string `json:"tenant"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (route *routeData) LoginHandler(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/login", func(fctx *fiber.Ctx) (err error) {

		var loginData LoginCredentials

		// logindata
		err = fctx.BodyParser(&loginData)
		err = errwrap.WithStack(err)

		// default tenant
		if loginData.Tenant == "" {
			loginData.Tenant = "default"
		}

		var result restDomain.RestResult
		userLoggedIn := &authDomain.LoggedIn{}
		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &restDomain.RestRequest{
				SkipAuth: true, // we not check existing tokens
				Message: &authDomain.LoginRequest{
					Tenant:   loginData.Tenant,
					Username: loginData.Username,
					Password: loginData.Password,
				},
				ResponseMessage: &userLoggedIn,
			})
			err = result.Error
		}

		// if an error occured, we force 401
		if err != nil {
			log.Error().Err(err).Send()
			return fctx.SendStatus(http.StatusUnauthorized)
		}

		// return
		return fctx.JSON(result.Message)
	})
}
