package routeauth

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/usecase"
)

// adapter
type routeData struct {
	restToActor usecase.RestToActor
}

func Register(baseRoute fiber.Router, restToActor usecase.RestToActor) {
	newHandler := &routeData{
		restToActor: restToActor,
	}

	newHandler.UserInfo(baseRoute)
	newHandler.LoginHandler(baseRoute)
	newHandler.CreateUser(baseRoute)
}
