package routeauth

import (
	"github.com/gofiber/fiber/v2"
	restDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	userDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
)

func (route *routeData) UserInfo(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/me", func(fctx *fiber.Ctx) (err error) {

		userLoggedIn := &userDomain.UserInfo{}

		result := route.restToActor.SendToActorAndWaitForMessage(fctx, &restDomain.RestRequest{
			Message:         &userDomain.UserInfoRequest{},
			ResponseMessage: &userLoggedIn,
		})
		err = result.Error

		// create hal-document
		halDoc := make(restDomain.Document)
		if err == nil {
			halDoc.SetEmbedded("userinfo", userLoggedIn)

			halLinks := &restDomain.InfoLinks{}
			halLinks.SetGetTemplateList()
			halLinks.SetDocumentSearch()
			err = halDoc.AddLinks(halLinks)
		} else {
			halDoc.SetEmbedded("error", result.Error.Error())
		}

		if err == nil {
			result.Message = halDoc
		} else {
			result.Message = restDomain.ErrorMessageFromErr(result.Error)
		}

		return fctx.JSON(result.Message)
	})
}
