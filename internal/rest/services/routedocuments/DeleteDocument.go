package routedocuments

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) DeleteDocument(baseRoute fiber.Router) {
	baseRoute.Add("DELETE", "/document/:docid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.ParamsDoc{}
		err = params.Read(fctx)

		// request
		resultPagedDoc := &documentDomain.DocumentDeleted{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentDeleteRequest{
					DocID: params.DocID,
				},
				ResponseMessage: &resultPagedDoc,
			})
			err = result.Error
		}

		if err == nil {
			return fctx.SendStatus(http.StatusOK)
		} else {
			return err
		}
	})
}
