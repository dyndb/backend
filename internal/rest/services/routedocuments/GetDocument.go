package routedocuments

import (
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hal"
)

func (route *routeData) GetDocument(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/document/:docid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.ParamsDoc{}
		err = params.Read(fctx)

		// request
		resultPagedDoc := &documentDomain.DocumenGetted{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentGetRequest{
					DocID: params.DocID,
				},
				ResponseMessage: &resultPagedDoc,
			})
			err = result.Error
		}

		var halDoc hal.Doc
		if err == nil {
			halDoc, err = hal.FromStruct(resultPagedDoc.Doc)
			err = errors.WithStack(err)
		}

		// links
		if err == nil {
			docLinks := myDomain.DocumentLinks{}

			docLinks.
				SetSelf(resultPagedDoc.DocID).
				SetLink(resultPagedDoc.DocID).
				SetSave(resultPagedDoc.DocID)

			// if its an build doc, you can not delete it !
			isBuildIn, _ := halDoc["_dyndb_builtin"].(bool)
			if !isBuildIn {
				docLinks.SetDelete(params.DocID)
			}

			halDoc.SetLinks(docLinks)
		}

		// interlinks
		var interlinks documentDomain.InterLinks
		if err == nil {
			interlinks, err = resultPagedDoc.Doc.Interlinks()
		}
		for interlinkDocID, interlinkDoc := range interlinks {

			var interlinkHalDoc hal.Doc
			interlinkHalDoc, err = hal.FromStruct(interlinkDoc)

			// its an doc
			if documentDomain.IsDocID(interlinkDocID) {
				interlinkDocLinks := myDomain.DocumentLinks{}
				interlinkDocLinks.
					SetSelf(interlinkDocID)

				interlinkHalDoc.SetLinks(interlinkDocLinks)
			}

			// its an metadata
			if blobMetadataDomain.IsMetadataID(interlinkDocID) {
				interlinkDocLinks := myDomain.AttachmentLinks{}
				interlinkDocLinks.
					SetSelf(interlinkDocID)

				interlinkHalDoc.SetLinks(interlinkDocLinks)
			}

			halDoc.AppendEmbeddedDoc(interlinkDocID, interlinkHalDoc)
		}

		if err == nil {
			return fctx.JSON(halDoc)
		} else {
			return err
		}
	})
}
