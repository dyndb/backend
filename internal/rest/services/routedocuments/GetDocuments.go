package routedocuments

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hal"
)

func (route *routeData) GetDocuments(baseRoute fiber.Router) {
	baseRoute.Add("GET", "/documents/list/:type/:template/:pagekey/:limit", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.PagedParams{}
		err = params.Read(fctx)

		// request-message
		var result myDomain.RestResult
		documentList := &documentDomain.DocumentList{}

		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentListRequest{
					TemplateName: params.TemplateName,
					PageKey:      params.PageKey,
					PageLimit:    int64(params.PageLimit),
				},
				ResponseMessage: &documentList,
			})
			err = result.Error
		}

		// Document-map
		halPagedDoc := make(myDomain.Document)
		if err == nil {

			for pagedDocID, pagedDoc := range documentList.PagedDocs {

				var halDoc hal.Doc
				halDoc, err = hal.FromStruct(pagedDoc)
				if err == nil {
					docLinks := myDomain.DocumentLinks{}
					docLinks.SetSelf(pagedDocID.String())

					err = halDoc.SetLinks(docLinks)
				}

				// interlinks
				var interlinks documentDomain.InterLinks
				if err == nil {
					interlinks, err = pagedDoc.Interlinks()
				}
				for interlinkDocID, interlinkDoc := range interlinks {

					var interlinkHalDoc hal.Doc
					interlinkHalDoc, err = hal.FromStruct(interlinkDoc)

					// its an doc
					if documentDomain.IsDocID(interlinkDocID) {
						interlinkDocLinks := myDomain.DocumentLinks{}
						interlinkDocLinks.
							SetSelf(interlinkDocID)

						interlinkHalDoc.SetLinks(interlinkDocLinks)
					}

					// its an metadata
					if blobMetadataDomain.IsMetadataID(interlinkDocID) {
						interlinkDocLinks := myDomain.AttachmentLinks{}
						interlinkDocLinks.
							SetSelf(interlinkDocID)

						interlinkHalDoc.SetLinks(interlinkDocLinks)
					}

					halDoc.AppendEmbeddedDoc(interlinkDocID, interlinkHalDoc)
				}

				if err == nil {
					if params.ListType == "short" {
						halDoc.KeepOnly("_links", "_embedded", "displayText", "faicon", "tplName", "fieldName")
					}
					halDoc.Ensure("displayText", "No display-text")
				}

				if err == nil {
					halPagedDoc.SetEmbedded(pagedDocID.String(), halDoc)
				}

			}

			pagedLinks := &myDomain.PagedLinks{BasePath: fmt.Sprintf("/documents/list/%s/%s", params.ListType, params.TemplateName)}
			pagedLinks.SetSelf(params.PageKey, params.PageLimit)
			pagedLinks.SetFirst(params.PageLimit)
			pagedLinks.SetPrev(params.PageKey, params.PageLimit)
			pagedLinks.SetNext(documentList.NextPageKey, params.PageLimit)
			err = halPagedDoc.AddLinks(pagedLinks)

		}

		if err == nil {
			return fctx.JSON(halPagedDoc)
		} else {
			return err
		}

	})
}
