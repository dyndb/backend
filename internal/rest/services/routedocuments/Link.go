package routedocuments

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

func (route *routeData) LinkAttachment(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/link/document/:docid", func(fctx *fiber.Ctx) (err error) {

		// docIDParams
		docIDParams := myDomain.ParamsLinks{}
		err = docIDParams.Read(fctx)

		lastIndex := strings.LastIndex(docIDParams.SelfLink, "/")
		if lastIndex == -1 {
			err = errors.WithStack(errors.New("malformed self link"))
		}

		var elementID string
		if err == nil {
			elementID = docIDParams.SelfLink[lastIndex+1:]
		}

		var documentToLink any

		// its an document
		if documentDomain.IsDocID(elementID) {
			docResultPayload := &documentDomain.DocumenGetted{}
			docResult := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentGetRequest{
					DocID: elementID,
				},
				ResponseMessage: &docResultPayload,
			})
			err = docResult.Error

			if err == nil {
				documentToLink = docResultPayload.Doc
			}
		}

		// its an attachment
		if blobMetadataDomain.IsMetadataID(elementID) {
			var metadataID blobMetadataDomain.MetadataID
			metadataID, err = blobMetadataDomain.NewMetadataIDFromString(elementID)

			blobMetadataResultPayload := &blobMetadataDomain.BlobMetadataList{}
			if err == nil {
				blobMetadataResult := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
					Message: &blobMetadataDomain.BlobMetadataListRequest{
						MetadataIDs: []blobMetadataDomain.MetadataID{metadataID},
					},
					ResponseMessage: &blobMetadataResultPayload,
				})
				err = blobMetadataResult.Error
			}

			if err == nil {
				if len(blobMetadataResultPayload.List) == 0 {
					err = errors.WithStack(errors.New("metadata not found"))
				}
			}

			if err == nil {
				documentToLink = blobMetadataResultPayload.List[metadataID]
			}
		}

		// generate generic document to link
		var doc documentDomain.GenericDocument
		if err == nil {
			err = doc.FromAny(documentToLink)
		}

		// update doc
		if err == nil {
			docResultPayload := &documentDomain.DocumentLinked{}
			docResult := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentLinkRequest{
					DocID:     docIDParams.DocID,
					LinkDocID: elementID,
					LinkDoc:   doc,
				},
				ResponseMessage: &docResultPayload,
			})
			err = docResult.Error

		}

		return err
	})

}
