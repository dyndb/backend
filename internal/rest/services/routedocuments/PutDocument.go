package routedocuments

import (
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hal"
)

func (route *routeData) PutDocument(baseRoute fiber.Router) {
	baseRoute.Add("PUT", "/document/:docid", func(fctx *fiber.Ctx) (err error) {

		// params
		params := myDomain.ParamsDoc{}
		err = params.Read(fctx)

		// request
		resultPagedDoc := &documentDomain.DocumentSaved{}
		if err == nil {
			result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentSaveRequest{
					DocID: params.DocID,
					Doc:   params.Doc,
				},
				ResponseMessage: &resultPagedDoc,
			})
			err = result.Error
		}

		var halDoc hal.Doc
		if err == nil {
			halDoc, err = hal.FromStruct(resultPagedDoc.Doc)
			err = errors.WithStack(err)
		}

		// links
		if err == nil {
			docLinks := myDomain.DocumentLinks{}

			docLinks.
				SetSelf(resultPagedDoc.DocID).
				SetSave(resultPagedDoc.DocID)

			// if its an build doc, you can not delete it !
			isBuildIn, _ := halDoc["_dyndb_builtin"].(bool)
			if !isBuildIn {
				docLinks.SetDelete(params.DocID)
			}

			halDoc.SetLinks(docLinks)

			attachmentLinks := myDomain.AttachmentsLinks{}
			attachmentLinks.
				SetList(resultPagedDoc.DocID).
				SetUploadToDoc(resultPagedDoc.DocID)

			halDoc.SetLinks(attachmentLinks)
		}

		if err == nil {
			return fctx.JSON(halDoc)
		} else {
			return err
		}
	})
}
