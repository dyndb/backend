package routedocuments

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/usecase"
)

// adapter
type routeData struct {
	restToActor usecase.RestToActor
}

func Register(
	baseRoute fiber.Router,
	restToActor usecase.RestToActor,
) {
	newRoute := &routeData{
		restToActor: restToActor,
	}

	newRoute.GetDocuments(baseRoute)
	newRoute.GetDocument(baseRoute)
	newRoute.Search(baseRoute)
	newRoute.PutDocument(baseRoute)
	newRoute.DeleteDocument(baseRoute)
	newRoute.LinkAttachment(baseRoute)
}
