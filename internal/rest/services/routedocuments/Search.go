package routedocuments

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	documentDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/rest/hal"
)

func (route *routeData) Search(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/documents/search/:type/:pagekey/:limit", func(fctx *fiber.Ctx) (err error) {

		// search-words
		paramsSearch := myDomain.SearchParams{}
		if err == nil {
			err = paramsSearch.Read(fctx)
		}

		// paged
		paramsPaged := myDomain.PagedParams{}
		if err == nil {
			err = paramsPaged.Read(fctx)
		}

		// request-message
		var result myDomain.RestResult
		documentList := &documentDomain.DocumentSearched{}

		if err == nil {
			result = route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
				Message: &documentDomain.DocumentSearchRequest{
					TemplateName: paramsPaged.TemplateName,
					SearchWords:  paramsSearch.Words,
					PageKey:      paramsPaged.PageKey,
					PageLimit:    int64(paramsPaged.PageLimit),
				},
				ResponseMessage: &documentList,
			})
			err = result.Error
		}

		// create hal-document
		halDoc := make(myDomain.Document)
		if err == nil {

			for pagedDocID, pagedDoc := range documentList.PagedDocs {

				if paramsPaged.ListType == "short" {
					pagedDoc.KeepOnly("_links", "_embedded", "displayText", "faicon", "tplName", "fieldName")
				}
				pagedDoc.Ensure("displayText", "No display-text")

				var newHalDocument hal.Doc
				newHalDocument, err = hal.FromStruct(pagedDoc)
				if err == nil {
					docLinks := myDomain.DocumentLinks{}
					docLinks.SetSelf(pagedDocID.String())

					err = newHalDocument.SetLinks(docLinks)
				}

				if err == nil {
					halDoc.SetEmbedded(pagedDocID.String(), newHalDocument)
				}

			}

			pagedLinks := &myDomain.PagedLinks{BasePath: fmt.Sprintf("/documents/search/%s", paramsPaged.ListType)}
			pagedLinks.SetFirst(paramsPaged.PageLimit)
			pagedLinks.SetNext(documentList.NextPageKey, paramsPaged.PageLimit)
			err = halDoc.AddLinks(pagedLinks)
		}

		if err == nil {
			result.Message = halDoc
		} else {
			result.Message = myDomain.ErrorMessageFromErr(err)
		}

		return fctx.JSON(result.Message)

	})
}
