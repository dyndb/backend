package routefields

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	fieldDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	myDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
	"gitlab.com/nerdhaltig/errwrap"
)

type PagedParams struct {
	FieldNames []string `json:"fieldnames"`
}

func (params *PagedParams) Read(fctx *fiber.Ctx, templateRequired bool) (err error) {

	validate := validator.New()

	// payload
	if len(fctx.Body()) > 0 {
		err = fctx.BodyParser(params)
		err = errwrap.WithStack(err)
	}

	for _, fieldName := range params.FieldNames {
		err = validate.Var(fieldName, "required,excludesall=;")
		if err != nil {
			break
		}
	}

	return
}

func (route *routeData) GetFieldsByName(baseRoute fiber.Router) {
	baseRoute.Add("POST", "/fields/byname", func(fctx *fiber.Ctx) (err error) {

		// params
		params := PagedParams{}
		err = params.Read(fctx, true)

		// request-message
		documentList := &fieldDomain.Fields{}

		result := route.restToActor.SendToActorAndWaitForMessage(fctx, &myDomain.RestRequest{
			Message: &fieldDomain.FieldsRequest{
				DocIDs: params.FieldNames,
			},
			ResponseMessage: &documentList,
		})

		// create hal-document
		halDocs := make(myDomain.Document)
		if err == nil {

			for pagedDocID, pagedDoc := range documentList.Fields {

				halDoc := myDomain.FromStruct(pagedDoc)
				halDocLinks := myDomain.DocumentLinks{}
				halDocLinks.SetEdit(pagedDocID.String())
				halDocLinks.SetSave(pagedDocID.String())
				halDocLinks.SetDelete(pagedDocID.String())
				halDoc.AddLinks(halDocLinks)

				halDocs.SetEmbedded(pagedDocID.String(), halDoc)
			}
		}

		if err == nil {
			result.Message = halDocs
		} else {
			result.Message = myDomain.ErrorMessageFromErr(err)
		}

		return fctx.JSON(result.Message)
	})
}
