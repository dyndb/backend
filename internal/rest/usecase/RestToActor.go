package usecase

import (
	"github.com/gofiber/fiber/v2"
	restDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/rest/domain"
)

type RestToActor interface {

	// SendToActorAndWaitForMessage will
	//
	// - read / verify / update access tokens
	//
	// - send a message to an actor
	//
	// - wait for response / timeout / err
	//
	// - update cookies / state for fiber
	SendToActorAndWaitForMessage(
		fctx *fiber.Ctx,
		newRequest *restDomain.RestRequest,
	) (result restDomain.RestResult)
}
