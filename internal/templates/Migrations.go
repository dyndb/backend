package templates

import (
	domainField "gitlab.com/nerdhaltig/dyndb/backend/internal/fields/domain"
	domainMigration "gitlab.com/nerdhaltig/dyndb/backend/internal/migration/domain"
	domainTemplates "gitlab.com/nerdhaltig/dyndb/backend/internal/templates/domain"
)

var v1 domainMigration.MigrationJobs = domainMigration.MigrationJobs{
	Version: 1,
	Jobs: []domainMigration.MigrationJob{
		{
			Action:     "replace",
			DocID:      "doc-04a78cdd-e30e-46d3-8600-b66d665060df",
			Collection: "documents",
			Doc: domainField.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "tplName",
				DisplayText: "Template ID",
				Hint:        "Must be unique. Use lowercase",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-3551bfcf-5a37-412a-80de-6332efee1291",
			Collection: "documents",
			Doc: domainField.Field{
				DynDBTemplateName: "dyndb_field",
				DynDBBuiltin:      true,

				FieldName:   "description",
				FaIcon:      "fasIcons",
				DisplayText: "Description",
				Hint:        "Detail information about this",
				Default:     "",
				Datatype:    "string",
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-42cef0ca-3a82-47a6-ab1c-e1afae62e0ad",
			Collection: "documents",
			Doc: domainTemplates.Template{
				DynDBTemplateName: "dyndb_template",
				DynDBBuiltin:      true,

				Name:        "dyndb_template",
				DisplayText: "Template",
				Description: "A template that contain fields from where you create documents",
				DynDBLayout: []domainTemplates.TemplateLayout{
					{
						FieldName: "tplName",
					},
					{
						FieldName: "displayText",
					},
					{
						FieldName: "description",
					},
				},
			},
		},
		{
			Action:     "replace",
			DocID:      "doc-8ce7e3aa-98cb-455e-8ee2-5730f4686eae",
			Collection: "documents",
			Doc: domainTemplates.Template{
				DynDBTemplateName: "dyndb_template",
				DynDBBuiltin:      true,

				Name:        "dyndb_field",
				DisplayText: "Field",
				Description: "A single field inside an document",
				DynDBLayout: []domainTemplates.TemplateLayout{
					{
						FieldName: "fieldName",
					},
					{
						FieldName: "displayText",
					},
					{
						FieldName: "hint",
					},
					{
						FieldName: "datatype",
					},
					{
						FieldName: "unit",
					},
				},
			},
		},
	},
}

var Migrations []domainMigration.MigrationJobs = []domainMigration.MigrationJobs{
	v1,
}
