package domain

type Template struct {
	Name        string `json:"tplName"`
	DisplayText string `json:"displayText"`
	Description string `json:"description"`

	DynDBTemplateName string           `json:"_dyndb_templatename"`
	DynDBBuiltin      bool             `json:"_dyndb_builtin"`
	DynDBLayout       []TemplateLayout `json:"_dyndb_layout"`
}

type TemplateLayout struct {
	FieldName string `json:"fieldName"`
	X         int    `json:"x"`
	Y         int    `json:"y"`
	ColSpan   int    `json:"colspan"`
	RowSpan   int    `json:"rowspan"`
}
