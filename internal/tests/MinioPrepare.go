package tests

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests/testcontainer"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

type MinioPrepareOpts struct {
	Ctx         context.Context
	Tenant      string
	Collections []string

	Database string
	Username string
	Password string
}

func MinioPrepare2(t *testing.T) (url string, err error) {

	ctx, ctxCancel := context.WithTimeout(context.Background(), time.Second*30)
	defer ctxCancel()

	CI_DOCKER_NETWORK := os.Getenv("CI_DOCKER_NETWORK")

	var container *testcontainer.Container
	var hostname string
	var mappedPort int
	container, hostname, mappedPort, err = testcontainer.New(ctx, &testcontainer.DockerTesterOpts{
		ProjectName: "dyndb_minio",
		Image:       "quay.io/minio/minio:RELEASE.2024-07-04T14-25-45Z",
		NetworkName: CI_DOCKER_NETWORK,
		Port:        "9000",
		Environment: []string{
			"MINIO_VOLUMES=/data",
		},
		Command:         []string{"server", "/data"},
		StopBeforeStart: true,
	})
	if err == nil {
		t.Cleanup(func() {
			container.Stop()
		})
	}

	// start the container
	var logMessages chan string
	if err == nil {
		logMessages, err = container.Start()
	}

	if err == nil {
		err = container.WaitForLog(logMessages, "1 Online, 0 Offline.", time.Minute*5)
	}

	if err == nil {
		url = fmt.Sprintf("%s:%v", hostname, mappedPort)
	}

	return
}

func MinioPrepare(t *testing.T) (minioURL string, err error) {

	ctx := context.Background()

	var projectRoot string
	projectRoot, err = tools.FindProjectRoot()
	if err != nil {
		panic(err)
	}

	// MinIO Container mit SSL starten
	req := testcontainers.ContainerRequest{
		Image:        "quay.io/minio/minio:RELEASE.2024-07-04T14-25-45Z",
		ExposedPorts: []string{"9000/tcp", "9001/tcp"},
		Cmd:          []string{"server", "/data", "--console-address", ":9001", "--certs-dir", "/home"},
		Files: []testcontainers.ContainerFile{
			{
				HostFilePath:      projectRoot + "/deployment/docker-compose/certs/public.crt",
				ContainerFilePath: "/home/public.crt",
				FileMode:          0644,
			},
			{
				HostFilePath:      projectRoot + "/deployment/docker-compose/certs/private.key",
				ContainerFilePath: "/home/private.key",
				FileMode:          0600,
			},
		},
		Env: map[string]string{
			"MINIO_ROOT_USER":     "localtest",
			"MINIO_ROOT_PASSWORD": "localpassword",
		},
		WaitingFor: wait.ForListeningPort("9000/tcp"),
	}

	minioContainer, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		log.Fatal(err)
	}
	t.Cleanup(func() {
		minioContainer.Terminate(ctx)
	})

	// MinIO Adresse abrufen
	host, err := minioContainer.Host(ctx)
	if err != nil {
		log.Fatal(err)
	}

	port, err := minioContainer.MappedPort(ctx, "9000")
	if err != nil {
		log.Fatal(err)
	}

	minioURL = fmt.Sprintf("%s:%s", host, port.Port())

	return
}
