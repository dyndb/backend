package tests

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests/testcontainer"
	"gitlab.com/nerdhaltig/gopersist"
)

type PostgresqlPrepareOpts struct {
	Ctx         context.Context
	Tenant      string
	Collections []string

	Database string
	Username string
	Password string
}

func PostgresqlPrepare(t *testing.T, opts *PostgresqlPrepareOpts) (management connections.Management, err error) {

	CI_DOCKER_NETWORK := os.Getenv("CI_DOCKER_NETWORK")

	var container *testcontainer.Container
	var hostname string
	var mappedPort int
	container, hostname, mappedPort, err = testcontainer.New(opts.Ctx, &testcontainer.DockerTesterOpts{
		ProjectName: "dyndb_postgresql",
		Image:       "postgres:14-alpine",
		NetworkName: CI_DOCKER_NETWORK,
		Port:        "5432",
		Environment: []string{
			"POSTGRES_DB=" + opts.Database,
			"POSTGRES_PASSWORD=" + opts.Password,
			"POSTGRES_USER=" + opts.Username,
		},
		StopBeforeStart: true,
	})
	if err == nil {
		t.Cleanup(func() {
			container.Stop()
		})
	}

	// start the container
	var logMessages chan string
	if err == nil {
		logMessages, err = container.Start()
	}

	if err == nil {
		err = container.WaitForLog(logMessages, "[1] LOG:  database system is ready to accept connections", time.Minute*5)
	}

	if err == nil {

		dsn := fmt.Sprintf("postgres://%s:%d", hostname, mappedPort)

		tenant := "tenant_" + opts.Tenant

		pool := gopersist.NewPoolService(5, time.Second*10)

		for connectionNo := 0; connectionNo < 5; connectionNo++ {
			connection := gopersist.NewPostgresqlPersistAdapter(dsn)
			err = connection.Login(context.Background(), opts.Database, opts.Username, opts.Password)
			if err != nil {
				return
			}
			pool.Add(connection)
		}

		management = connections.NewConnectionManagement(pool)

		management.WithTenant(tenant, "test", func(connection gopersist.PersistancePort) (err error) {
			if !connection.TenantExist(context.Background(), tenant) {
				connection.CreateTenant(context.Background(), tenant)
			}
			connection.SwitchTenant(context.Background(), tenant)

			for _, collection := range opts.Collections {
				if !connection.CollectionExist(context.Background(), collection) {
					connection.CreateCollection(context.Background(), collection)
				}
			}

			return
		})
	}

	return
}

type PostgresqlSuite struct {
	postgres    testcontainers.Container
	Connections connections.Management
}

func (suite *PostgresqlSuite) SetupTest(Tenant string, collections []string) {
	ctx := context.Background()

	// currently we only support testcontainers
	suite.postgres, suite.Connections = PostgresqlWithTestContainers(ctx, Tenant, collections)

}

func (suite *PostgresqlSuite) TearDownSuite() {
	ctx := context.Background()
	if suite.postgres != nil {
		suite.postgres.Terminate(ctx)
	}
}

func PostgresqlWithTestContainers(ctx context.Context, tenant string, collections []string) (
	container testcontainers.Container,
	management connections.Management,
) {
	var err error

	var postgresqlPort nat.Port
	postgresqlPort, err = nat.NewPort("tcp", "5432")
	if err != nil {
		panic(err)
	}

	req := testcontainers.ContainerRequest{
		Name:         "dyndb_test",
		Image:        "postgres:14-alpine",
		ExposedPorts: []string{postgresqlPort.Port() + "/" + postgresqlPort.Proto()},
		//WaitingFor:   wait.ForLog("database system is ready to accept connections"),
		WaitingFor: wait.ForListeningPort(postgresqlPort),
		Env: map[string]string{
			"POSTGRES_PASSWORD": "testcontpw",
			"POSTGRES_USER":     "testcontuser",
			"POSTGRES_DB":       "testdyndb",
		},
	}
	container, err = testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
		Reuse:            true,
	})
	if err != nil {
		panic(err)
	}

	var postgresEndpoint string
	postgresEndpoint, err = container.Endpoint(ctx, "")
	if err != nil {
		panic(err)
	}

	management, err = InitPostgresql(
		"postgresql://"+postgresEndpoint,
		"testdyndb", "testcontuser", "testcontpw",
		tenant, collections,
	)
	if err != nil {
		panic(err)
	}

	return
}

func InitPostgresql(dsn, db, user, pw, tenant string, collections []string) (management connections.Management, err error) {

	if dsn == "" {
		dsn = "postgres:///var/run/postgresql"
	}

	tenant = "tenant_" + tenant

	pool := gopersist.NewPoolService(5, time.Second*10)

	for connectionNo := 0; connectionNo < 5; connectionNo++ {
		connection := gopersist.NewPostgresqlPersistAdapter(dsn)
		err = connection.Login(context.Background(), db, user, pw)
		if err != nil {
			return
		}
		pool.Add(connection)
	}

	management = connections.NewConnectionManagement(pool)

	management.WithTenant(tenant, "test", func(connection gopersist.PersistancePort) (err error) {
		if !connection.TenantExist(context.Background(), tenant) {
			connection.CreateTenant(context.Background(), tenant)
		}
		connection.SwitchTenant(context.Background(), tenant)

		for _, collection := range collections {
			if !connection.CollectionExist(context.Background(), collection) {
				connection.CreateCollection(context.Background(), collection)
			}
		}

		return
	})

	return
}
