package tests

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
)

type TestDocument struct {
	UUID        string `json:"name"`
	Description string `json:"description"`
}

func TestDocumentCreate(t *testing.T, connections usecase.DocumentManage) {
	var err error

	_, err = connections.Save(
		context.Background(),
		"testdyndb", "", "",
		TestDocument{
			UUID:        uuid.Must(uuid.NewRandom()).String(),
			Description: "The first test document",
		},
	)
	assert.NoError(t, err)

	_, err = connections.Save(
		context.Background(),
		"testdyndb", "", "",
		TestDocument{
			UUID:        uuid.Must(uuid.NewRandom()).String(),
			Description: "The second test document",
		},
	)
	assert.NoError(t, err)

	// this should create a new document
	TestDocumentUUID := uuid.Must(uuid.NewRandom()).String()
	var documentID domain.DocID = domain.NewDocID()
	documentID, err = connections.Save(
		context.Background(),
		"testdyndb", "", "",
		TestDocument{
			UUID:        TestDocumentUUID,
			Description: "The third test document",
		},
	)
	assert.NoError(t, err)
	assert.NotEqual(t, "-", documentID)

	// get the document
	var exist bool
	var testDoc TestDocument
	exist, err = connections.Fetch(context.Background(), "testdyndb", documentID, &testDoc)
	assert.NoError(t, err)
	assert.True(t, exist)
	assert.Equal(t, testDoc.Description, "The third test document")

}
