package tests

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
)

func TestDocumentList(t *testing.T, documents usecase.DocumentManage) {
	var err error

	// check that we have only 2 element in the list
	var partsPageKey string
	var parts domain.Documents

	parts, err = documents.List(context.Background(), "testdyndb", "parts", &partsPageKey, 2)
	assert.NoError(t, err)
	assert.Len(t, parts, 2)

	parts, err = documents.List(context.Background(), "testdyndb", "parts", &partsPageKey, 1)
	assert.NoError(t, err)
	assert.Len(t, parts, 1)

	parts, err = documents.List(context.Background(), "testdyndb", "parts", &partsPageKey, 99)
	assert.NoError(t, err)
	assert.Len(t, parts, 1)

}
