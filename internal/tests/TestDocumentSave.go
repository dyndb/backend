package tests

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
)

func TestDocumentSave(t *testing.T, documents usecase.DocumentManage) {
	var err error

	type TestDocument struct {
		Name string `json:"name"`
	}

	var documentID domain.DocID = domain.NewDocID()
	_, err = documents.Save(
		context.Background(),
		"testdyndb", "devices", documentID, TestDocument{
			Name: "calculator",
		},
	)
	assert.NoError(t, err)

	// check that we have only 1 element in the list
	var devicePageKey string
	var devices domain.Documents
	devices, err = documents.List(context.Background(), "testdyndb", "devices", &devicePageKey, 1000)
	assert.NoError(t, err)
	assert.Len(t, devices, 1)

	var docIDToDelete domain.DocID
	for counter := 1; counter <= 5; counter++ {
		documentID = domain.NewDocID()

		docIDToDelete, err = documents.Save(
			context.Background(),
			"testdyndb", "parts", documentID, TestDocument{
				Name: fmt.Sprintf("resistor%d", counter),
			},
		)
		assert.NoError(t, err)
	}

	// check that we have only 5 element in the list
	var partsPageKey string
	var parts domain.Documents
	parts, err = documents.List(context.Background(), "testdyndb", "parts", &partsPageKey, 1000)
	assert.NoError(t, err)
	assert.Len(t, parts, 5)

	// Delete a element
	err = documents.Delete(context.Background(), "testdyndb", docIDToDelete)
	assert.NoError(t, err)

	// check that we have only 4 element in the list
	parts, err = documents.List(context.Background(), "testdyndb", "parts", &partsPageKey, 1000)
	assert.NoError(t, err)
	assert.Len(t, parts, 4)
}
