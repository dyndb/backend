package tests

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	blobMetadataDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/blobmetadata/domain"
	docDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/document/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/document/usecase"
)

func TestLinkAttachment(t *testing.T, documents usecase.DocumentService) {
	var err error

	type TestDocument struct {
		Name string `json:"name"`
	}

	// create some documents
	_, err = documents.Save(context.Background(),
		"testdyndb", "invoices", "8ce8b3c0-3242-49b5-b2ca-1174eb19ff83", TestDocument{
			Name: "invoice ssd",
		})
	assert.NoError(t, err)

	_, err = documents.Save(context.Background(),
		"testdyndb", "invoices", "f2ffd811-4e15-4637-a039-53cad315d627", TestDocument{
			Name: "invoice display",
		})
	assert.NoError(t, err)

	documents.LinkDocument(usecase.LinkDocumentOpts{
		Ctx:       context.Background(),
		Tenant:    "testdyndb",
		DocID:     "f2ffd811-4e15-4637-a039-53cad315d627",
		LinkDocID: blobMetadataDomain.NewMetadataID().String(),
		LinkDoc: docDomain.GenericDocument{
			DisplayText: "A document",
			FaIcon:      "",
		},
	})
}
