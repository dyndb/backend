package tests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func TestConnectionsManagement(t *testing.T, service connections.Management) {

	err := service.TenantRegister("first")
	assert.NoError(t, err)

	err = service.TenantRegister("second")
	assert.NoError(t, err)

	err = service.TenantRegister("third")
	assert.NoError(t, err)

	var counter int = 0
	service.WithTenants(func(connection gopersist.PersistancePort, tenantInfos *domain.TenantInfo) (err error) {
		counter++
		return
	})

	assert.Equal(t, 3, counter)

}
