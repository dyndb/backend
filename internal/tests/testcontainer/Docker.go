package testcontainer

import (
	"context"
	"fmt"
	"os/exec"
	"strconv"

	"github.com/rs/zerolog/log"
)

type Container struct {
	ProjectName string

	cmd       *exec.Cmd
	ctx       context.Context
	cmdCancel context.CancelFunc
}

type DockerTesterOpts struct {
	ProjectName string
	Image       string
	Environment []string

	// NetworkName (optional) for using docker-networks
	// will be passed as environment variable CT_DOCKER_NETWORK to you docker-compose files
	NetworkName string

	// Port you would like to have
	Port string

	Command []string

	StopBeforeStart bool
}

func New(ctx context.Context, Opts *DockerTesterOpts) (newDockerTester *Container, hostname string, mappedPort int, err error) {

	newDockerTester = &Container{
		ProjectName: Opts.ProjectName,
	}
	newDockerTester.ctx, newDockerTester.cmdCancel = context.WithCancel(ctx)

	if Opts.NetworkName != "" {
		mappedPort, err = strconv.Atoi(Opts.Port)
		Opts.Port = ""
	}

	// hostname
	if Opts.NetworkName == "" {
		hostname = "127.0.0.1"
	} else {
		hostname = Opts.ProjectName
	}

	if err == nil {
		if Opts.StopBeforeStart {
			stopCmd := exec.Command("docker", "rm", "-f", Opts.ProjectName)
			err = stopCmd.Run()
		}
	}

	// command
	if err == nil {
		cmdParams := []string{}
		cmdParams = append(cmdParams, "run", "--rm")
		cmdParams = append(cmdParams, "--name", Opts.ProjectName)
		if Opts.NetworkName != "" {
			cmdParams = append(cmdParams, "--network", Opts.NetworkName)
		}
		for _, env := range Opts.Environment {
			cmdParams = append(cmdParams, "-e", env)
		}

		// port
		if Opts.Port != "" {
			for mappedPort = 11000; mappedPort < 13000; mappedPort++ {
				if newDockerTester.checkPort(mappedPort) {
					break
				}
			}

			cmdParams = append(cmdParams, "-p", fmt.Sprintf("%d:%s", mappedPort, Opts.Port))
		}

		// image
		cmdParams = append(cmdParams, Opts.Image)

		if len(Opts.Command) > 0 {
			cmdParams = append(cmdParams, Opts.Command...)
		}

		newDockerTester.cmd = exec.CommandContext(newDockerTester.ctx, "docker", cmdParams...)
		newDockerTester.cmd.Env = Opts.Environment

		log.Info().Str("cmd", newDockerTester.cmd.String()).Msg("Prepared command")
	}

	return
}
