package testcontainer

import (
	"bufio"
	"io"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

func (ct *Container) Start() (logMessages chan string, err error) {

	var stdoutReader io.ReadCloser
	if err == nil {
		stdoutReader, err = ct.cmd.StdoutPipe()
	}

	var stderrReader io.ReadCloser
	if err == nil {
		stderrReader, err = ct.cmd.StderrPipe()
	}

	log.Info().Str("cmd", ct.cmd.String()).Send()

	if err == nil {
		err = ct.cmd.Start()
		if ct.cmd.Process == nil {
			err = errors.WithStack(errors.New("process not started"))
		}
	}

	if err == nil {
		logMessages = make(chan string, 10)
		stdoutBufferReader := bufio.NewReader(stdoutReader)
		go func() {
			for {
				str, err := stdoutBufferReader.ReadString('\n')
				if err != nil {
					ct.Stop()
					return
				}
				logMessages <- str
			}
		}()

		stderrBufferReader := bufio.NewReader(stderrReader)
		go func() {
			for {
				str, err := stderrBufferReader.ReadString('\n')
				if err != nil {
					ct.Stop()
					return
				}
				logMessages <- str
			}
		}()
	}

	return
}
