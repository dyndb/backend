package testcontainer

import "os/exec"

func (ct *Container) Stop() {
	stopCmd := exec.Command("docker", "rm", "-f", ct.ProjectName)
	stopCmd.Run()

	ct.cmdCancel()
}
