package testcontainer

import (
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

func (ct *Container) WaitForLog(logMessages chan string, logentry string, timeout time.Duration) (err error) {

	readyTimeout := time.NewTimer(timeout)

waitForLogLoop:
	for {
		select {
		case msg := <-logMessages:
			log.Debug().Str("stdout", msg).Send()
			if strings.Contains(msg, logentry) {
				break waitForLogLoop
			}
		case <-readyTimeout.C:
			err = errors.WithStack(errors.New("timeout"))
			break waitForLogLoop
		case <-ct.ctx.Done():
			err = errors.WithStack(errors.New("process was cancelled"))
			break waitForLogLoop
		}
	}

	return
}
