package testcontainer

import (
	"fmt"
	"net"
)

func (ct *Container) checkPort(port int) (available bool) {

	ln, err := net.Listen("tcp", ":"+fmt.Sprintf("%d", port))
	if err == nil {
		ln.Close()
		available = true
	}

	return
}
