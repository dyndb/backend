package jwtoken

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func (adp *jwtokenService) Must(args ...interface{}) []interface{} {
	for _, arg := range args {
		switch value := arg.(type) {
		case error:
			if value != nil {
				panic(value)
			}
		}
	}

	return args
}

func (adp *jwtokenService) CreateToken(createRequestInfos domain.TokenInfo) (signed domain.SignedToken, err error) {

	// Create the Claims
	claims := &CustomClaims{
		Tenant:           createRequestInfos.Tenant,
		UserID:           createRequestInfos.Username,
		RefreshInSeconds: int64(adp.timeout.Seconds()),
		AdittionalInfos:  createRequestInfos.AdditionalInfos,
		RegisteredClaims: jwt.RegisteredClaims{
			ID:        uuid.Must(uuid.NewRandom()).String(),
			Subject:   createRequestInfos.ID,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(adp.timeout).Add(adp.timeoutOffset)),
			Issuer:    adp.issuer,
		},
	}

	// create jwt
	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign the token and get the string for it
	var signedString string
	signedString, err = accessToken.SignedString([]byte(adp.signingKey))
	if err == nil {
		signed = domain.SignedToken(signedString)
	}

	return
}

func (adp *jwtokenService) MustCreateToken(createRequestInfos domain.TokenInfo) (signed domain.SignedToken) {
	args := adp.Must(adp.CreateToken(createRequestInfos))
	signed = args[0].(domain.SignedToken)
	return
}
