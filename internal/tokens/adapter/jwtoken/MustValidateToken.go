package jwtoken

import (
	"errors"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func (adp *jwtokenService) ValidateToken(signed domain.SignedToken) (infos domain.TokenInfo, err error) {

	// get access token
	if signed == "" {
		err = errors.New("token missing")
	}

	// is the token valid ?
	tokenClaims := &CustomClaims{}
	if err != nil {
		return
	}

	_, err = jwt.ParseWithClaims(string(signed), tokenClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(adp.signingKey), nil
	},
		jwt.WithLeeway(adp.timeoutOffset),
		jwt.WithValidMethods([]string{
			jwt.SigningMethodHS256.Alg(),
		}),
	)
	if err != nil {
		return
	}

	infos = domain.TokenInfo{
		ID:              tokenClaims.Subject,
		Tenant:          tokenClaims.Tenant,
		Username:        tokenClaims.UserID,
		AdditionalInfos: tokenClaims.AdittionalInfos,
	}

	return
}
