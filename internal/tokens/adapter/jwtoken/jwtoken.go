package jwtoken

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/ports"
)

type jwtokenService struct {
	issuer        string
	signingKey    string
	timeout       time.Duration
	timeoutOffset time.Duration
}

func NewTokenGeneratorAdapter(
	issuer string,
	signingKey string,
	timeout time.Duration,
	timeoutOffset time.Duration,
) ports.TokenGeneratorPort {

	return &jwtokenService{
		issuer:        issuer,
		signingKey:    signingKey,
		timeout:       timeout,
		timeoutOffset: timeoutOffset,
	}
}

type CustomClaims struct {
	Tenant           string            `json:"tenant"`
	UserID           string            `json:"userid"`
	RefreshInSeconds int64             `json:"refresh_in_seconds"`
	AdittionalInfos  map[string]string `json:"adittional,omitempty"`
	jwt.RegisteredClaims
}
