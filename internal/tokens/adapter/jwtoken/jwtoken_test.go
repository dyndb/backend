package jwtoken

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func TestAccessToken(t *testing.T) {

	var err error
	var infos domain.TokenInfo
	var signedString domain.SignedToken

	tokenAdapter := NewTokenGeneratorAdapter(
		"integrationtest",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*2,
		0,
	)

	signedString, err = tokenAdapter.CreateToken(domain.TokenInfo{
		ID:       "access",
		Tenant:   "testtenant",
		Username: "testuser",
	})
	assert.Nil(t, err)

	// token should be valid
	infos, err = tokenAdapter.ValidateToken(signedString)
	assert.Nil(t, err)
	assert.Equal(t, "access", infos.ID)
	assert.Equal(t, "testtenant", infos.Tenant)
	assert.Equal(t, "testuser", infos.Username)

	// we test an empty token
	_, err = tokenAdapter.ValidateToken(domain.SignedToken(""))
	assert.NotNil(t, err)

	// we test timeout of token
	time.Sleep(time.Second * 8)
	infos, err = tokenAdapter.ValidateToken(signedString)
	assert.NotNil(t, err)

}

func TestAdditionalInfos(t *testing.T) {

	var err error
	var infos domain.TokenInfo
	var signedString domain.SignedToken

	tokenAdapter := NewTokenGeneratorAdapter(
		"integrationtest",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*2,
		0,
	)

	signedString, err = tokenAdapter.CreateToken(domain.TokenInfo{
		ID:       "access",
		Tenant:   "testtenant",
		Username: "testuser",
		AdditionalInfos: map[string]string{
			"info1": "data1",
			"info2": "data2",
		},
	})
	assert.Nil(t, err)

	infos, err = tokenAdapter.ValidateToken(signedString)
	assert.Nil(t, err)
	assert.NotNil(t, infos.AdditionalInfos)
	assert.NotEmpty(t, infos.AdditionalInfos)

}
