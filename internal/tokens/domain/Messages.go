package domain

// AuthCreateRequest will create new tokens for this tenant/username
type AuthCreateRequest struct {
	Tenant          string
	Username        string
	AdditionalInfos map[string]string
}

// AuthValidateRequest will validate refresh and access tokens
//
// If an refresh token was provided, new refresh and access tokens will be created if valid
type AuthValidateRequest struct {
	RefreshToken string
	AccessToken  string
}

type AuthInfoRequest struct{}

// AuthValidated return the tenant/username of an valid token
//
// and new refresh/access token if requested
type AuthValidated struct {
	Tenant       string
	Username     string
	RefreshToken string // empty if token not refreshed
	AccessToken  string // empty if token not refreshed

	AdditionalInfos map[string]string
}
