package domain

// SignedToken represents a signed token
type SignedToken string

// SignedTokenSet represents a signed refresh and access token
type SignedTokenSet struct {
	RefreshToken SignedToken
	AccessToken  SignedToken
}

// TokenInfo represent the infos about an token inside an signed token
type TokenInfo struct {
	ID              string
	Tenant          string
	Username        string
	AdditionalInfos map[string]string
}
