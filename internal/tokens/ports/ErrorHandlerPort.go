package ports

type ErrorHandlerPort interface {
	PanicOnError(err error)
	NewErrorWithUserInfos(err error, tenant string, username string) error
	NewErrorWithState(err error, state int) error
}
