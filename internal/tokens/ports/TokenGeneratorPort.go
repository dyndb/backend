package ports

import "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"

// stuff we provide outside our domain

// TokenGeneratorPort represents a thing that can generate tokens
type TokenGeneratorPort interface {
	CreateToken(createRequestInfos domain.TokenInfo) (signed domain.SignedToken, err error)
	ValidateToken(signed domain.SignedToken) (infos domain.TokenInfo, err error)
}
