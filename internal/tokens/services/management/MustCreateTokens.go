package management

import (
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func (mgmt *refreshUseCase) CreateTokens(tokenInfo domain.TokenInfo) (tokenSet domain.SignedTokenSet, err error) {

	// refresh token
	if err == nil {
		tokenInfo.ID = "refresh"
		tokenSet.RefreshToken, err = mgmt.refreshTokenAdapter.CreateToken(tokenInfo)
		if err != nil {
			err = errors.WithStack(err)
		}
	}

	// access token
	if err == nil {
		tokenInfo.ID = "access"
		tokenSet.AccessToken, err = mgmt.accessTokenAdapter.CreateToken(tokenInfo)
		if err != nil {
			err = errors.WithStack(err)
		}
	}

	// add a state and user-infos to error
	if err != nil {
		tokenSet.RefreshToken = ""
		tokenSet.AccessToken = ""
	}

	return
}
