package management

import (
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
)

func (mgmt *refreshUseCase) ValidateOrRefresh(tokenSet *domain.SignedTokenSet) (tokenInfo domain.TokenInfo, err error) {

	if tokenSet == nil {
		err = errors.WithStack(errors.New("set is empty"))
		tokenSet = &domain.SignedTokenSet{}
	}

	// handling error
	defer func() {
		// add a state and user-infos to error
		if err != nil {
			err = errors.WithStack(err)

			if tokenSet != nil {
				tokenSet.RefreshToken = ""
				tokenSet.AccessToken = ""
			}
			tokenInfo = domain.TokenInfo{}
		}
	}()

	// this create a new refresh and access token
	if tokenSet.RefreshToken != "" {
		if err == nil {
			tokenInfo, err = mgmt.refreshTokenAdapter.ValidateToken(tokenSet.RefreshToken)
			if err != nil {
				err = errors.WithStack(err)
			}
		}

		if err == nil {
			var newTokenSet domain.SignedTokenSet
			newTokenSet, err = mgmt.CreateTokens(tokenInfo)
			if err != nil {
				err = errors.WithStack(err)
			} else {
				tokenSet.AccessToken = newTokenSet.AccessToken
				tokenSet.RefreshToken = newTokenSet.RefreshToken
			}
		}
	}

	// this just validate the token
	if err == nil {
		tokenInfo, err = mgmt.accessTokenAdapter.ValidateToken(tokenSet.AccessToken)
		if err != nil {
			err = errors.WithStack(err)
		}
	}

	// clear tokenSet if no new tokens requested
	if err == nil {
		if tokenSet.RefreshToken == "" {
			tokenSet.AccessToken = ""
		}
	}

	return
}
