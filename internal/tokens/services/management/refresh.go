package management

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/usecase"
)

type refreshUseCase struct {
	refreshTokenAdapter ports.TokenGeneratorPort
	accessTokenAdapter  ports.TokenGeneratorPort
}

func New(
	refreshTokenAdapter ports.TokenGeneratorPort,
	accessTokenAdapter ports.TokenGeneratorPort,

) usecase.TokenManagement {
	return &refreshUseCase{
		refreshTokenAdapter: refreshTokenAdapter,
		accessTokenAdapter:  accessTokenAdapter,
	}
}
