package management_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/adapter/jwtoken"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/services/management"
)

func TestCreateAndValidate(t *testing.T) {

	refreshTokenAdapter := jwtoken.NewTokenGeneratorAdapter(
		"testissuer",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*30,
		0,
	)

	accessTokenAdapter := jwtoken.NewTokenGeneratorAdapter(
		"testissuer",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*10,
		time.Second*10,
	)

	refresher := management.New(
		refreshTokenAdapter,
		accessTokenAdapter,
	)

	newTokenInfo := domain.TokenInfo{
		Tenant:   "testtenant",
		Username: "testuser",
	}
	newTokenSet, err := refresher.CreateTokens(newTokenInfo)
	assert.Nil(t, err)
	assert.NotEmpty(t, newTokenSet.AccessToken)
	assert.NotEmpty(t, newTokenSet.RefreshToken)

	// we set nil
	var invalidTokenInfo domain.TokenInfo
	invalidTokenInfo, err = refresher.ValidateOrRefresh(nil)
	assert.Error(t, err)
	assert.Empty(t, invalidTokenInfo.Tenant)
	assert.Empty(t, invalidTokenInfo.Username)

	// validate access-token - no need to refresh
	var validateTokenInfo domain.TokenInfo
	validateTokenSet := &domain.SignedTokenSet{
		AccessToken: newTokenSet.AccessToken,
	}
	validateTokenInfo, err = refresher.ValidateOrRefresh(validateTokenSet)
	assert.Nil(t, err)
	assert.Equal(t, newTokenInfo.Tenant, validateTokenInfo.Tenant)
	assert.Equal(t, newTokenInfo.Username, validateTokenInfo.Username)
	assert.Empty(t, validateTokenSet.AccessToken)
	assert.Empty(t, validateTokenSet.RefreshToken)

	// create
	validateTokenSet = &domain.SignedTokenSet{
		AccessToken:  newTokenSet.AccessToken,
		RefreshToken: newTokenSet.RefreshToken,
	}
	validateTokenInfo, err = refresher.ValidateOrRefresh(validateTokenSet)
	assert.Nil(t, err)
	assert.Equal(t, newTokenInfo.Tenant, validateTokenInfo.Tenant)
	assert.Equal(t, newTokenInfo.Username, validateTokenInfo.Username)
	assert.NotEqual(t, newTokenSet.AccessToken, validateTokenSet.AccessToken)
	assert.NotEqual(t, newTokenSet.RefreshToken, validateTokenSet.RefreshToken)

}
