package tokenactors

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewAuthCreateActor(ctr goactor.Concentrator, manager usecase.TokenManagement) (actor goactor.Actor) {

	actor = goactor.NewActor("authCreateActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.AuthCreateRequest:

			if msg.AdditionalInfos == nil {
				msg.AdditionalInfos = make(map[string]string)
			}

			var newTokenSet domain.SignedTokenSet
			newTokenSet, err = manager.CreateTokens(
				domain.TokenInfo{
					Tenant:          msg.Tenant,
					Username:        msg.Username,
					AdditionalInfos: msg.AdditionalInfos,
				},
			)

			if err == nil {
				returnMessage = append(returnMessage, &domain.AuthValidated{
					Tenant:       msg.Tenant,
					Username:     msg.Username,
					RefreshToken: string(newTokenSet.RefreshToken),
					AccessToken:  string(newTokenSet.AccessToken),
				})
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.AuthCreateRequest{})

	return
}
