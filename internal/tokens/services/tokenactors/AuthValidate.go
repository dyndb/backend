package tokenactors

import (
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewAuthValidateActor(ctr goactor.Concentrator, manager usecase.TokenManagement) (actor goactor.Actor) {

	actor = goactor.NewActor("authValidateActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.AuthInfoRequest:
			validated := &domain.AuthValidated{}
			if !ctx.As(&validated) {
				err = errors.WithStack(errors.New("unauthenticated"))
			} else {
				returnMessage = append(returnMessage, validated)
			}

		case *domain.AuthValidateRequest:

			var newTokenSet domain.SignedTokenSet = domain.SignedTokenSet{
				RefreshToken: domain.SignedToken(msg.RefreshToken),
				AccessToken:  domain.SignedToken(msg.AccessToken),
			}
			var tokenInfo domain.TokenInfo
			tokenInfo, err = manager.ValidateOrRefresh(&newTokenSet)

			if err == nil {
				returnMessage = append(returnMessage, &domain.AuthValidated{
					Tenant:          tokenInfo.Tenant,
					Username:        tokenInfo.Username,
					RefreshToken:    string(newTokenSet.RefreshToken),
					AccessToken:     string(newTokenSet.AccessToken),
					AdditionalInfos: tokenInfo.AdditionalInfos,
				})
			}

			if err != nil {
				log.Error().Err(err).Send()
				err = errors.WithStack(errors.New("unauthenticated"))
			}

		}
		return
	})

	ctr.HookActor(actor, &domain.AuthValidateRequest{})

	return
}
