//go:build !production

package tests

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/adapter/jwtoken"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/services/management"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/services/tokenactors"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func PrepareTokenActors(ctr goactor.Concentrator) {

	refreshTokenAdapter := jwtoken.NewTokenGeneratorAdapter(
		"testissuer",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*30,
		0,
	)

	accessTokenAdapter := jwtoken.NewTokenGeneratorAdapter(
		"testissuer",
		uuid.Must(uuid.NewRandom()).String(),
		time.Second*10,
		time.Second*10,
	)

	refresher := management.New(
		refreshTokenAdapter,
		accessTokenAdapter,
	)

	// actors
	tokenactors.NewAuthCreateActor(ctr, refresher)
	tokenactors.NewAuthValidateActor(ctr, refresher)

}
