package tokens

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/adapter/jwtoken"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/services/management"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/services/tokenactors"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/usecase"
	"gitlab.com/nerdhaltig/goactor/v2"
)

// we proxy our objects from sub-packages

// ports
type GeneratorPort ports.TokenGeneratorPort

func NewJWTokenGeneratorAdapter(issuer string, signingKey string, timeout time.Duration, timeoutOffset time.Duration) GeneratorPort {
	return jwtoken.NewTokenGeneratorAdapter(issuer, signingKey, timeout, timeoutOffset)
}

type ManagementUseCase usecase.TokenManagement

func NewTokenManagementUseCase(
	refreshTokenAdapter GeneratorPort,
	accessTokenAdapter GeneratorPort,
) ManagementUseCase {
	return management.New(refreshTokenAdapter, accessTokenAdapter)
}

// actors
func NewAuthCreateActor(ctr goactor.Concentrator, manager ManagementUseCase) (actor goactor.Actor) {
	return tokenactors.NewAuthCreateActor(ctr, manager)
}

func NewAuthValidateActor(ctr goactor.Concentrator, manager ManagementUseCase) (actor goactor.Actor) {
	return tokenactors.NewAuthValidateActor(ctr, manager)
}
