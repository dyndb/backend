package usecase

import "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"

type TokenManagement interface {
	ValidateOrRefresh(tokenSet *domain.SignedTokenSet) (tokenInfo domain.TokenInfo, err error)
	CreateTokens(tokenInfo domain.TokenInfo) (tokenSet domain.SignedTokenSet, err error)
}
