package tools

import (
	"io"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/errwrap"
)

// FileCopyChunked copy the file, create parent folders if needed
// if target file already exist, this will do nothing
func FileCopyChunked(sourceFilePath, targetFilePath string) (err error) {

	// open the file
	var sourceFile *os.File
	if err == nil {
		sourceFile, err = os.Open(sourceFilePath)
		err = errors.WithStack(err)
	}

	// create target folder if needed
	if err == nil {
		err = os.MkdirAll(filepath.Dir(targetFilePath), 0700)
		err = errwrap.WithStack(err)
	}

	// check if file already exist
	if FileExist(targetFilePath) {
		log.Warn().Stack().Str("filepath", targetFilePath).Msg("blob already exist")
		return
	}

	// create output file
	var targetFile *os.File
	if err == nil {
		targetFile, err = os.Create(targetFilePath)
		err = errors.WithStack(err)
	}

	if err == nil {
		// close fo on exit and check for its returned error
		defer func() {
			err = targetFile.Close()
		}()

		// make a buffer to keep chunks that are read
		var sourceBufReaded int
		sourceBuf := make([]byte, 1024)
		for {
			// read a chunk
			sourceBufReaded, err = sourceFile.Read(sourceBuf)
			if err != nil && err != io.EOF {
				break
			}
			if sourceBufReaded == 0 {
				break
			}

			// write a chunk
			if _, err = targetFile.Write(sourceBuf[:sourceBufReaded]); err != nil {
				break
			}
		}
	}

	return
}
