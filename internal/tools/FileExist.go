package tools

import "os"

func FileExist(filePath string) (exist bool) {

	// check if the blob already exist ( do nothing ! )
	// Dateiinfo abrufen
	_, err := os.Stat(filePath)

	// Überprüfen, ob ein Fehler aufgetreten ist und ob die Datei existiert
	if err == nil {
		return true // Datei existiert
	}
	if os.IsNotExist(err) {
		return false // Datei existiert nicht
	}

	return false
}
