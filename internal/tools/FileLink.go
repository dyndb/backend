package tools

import (
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/errwrap"
)

func FileLink(source, target string) (err error) {

	// create target folder if needed
	if err == nil {
		err = os.MkdirAll(filepath.Dir(target), 0700)
		err = errwrap.WithStack(err)
	}

	// check if file already exist
	if FileExist(target) {
		log.Warn().Stack().Str("filepath", target).Msg("link already exist")
		return
	}

	// we need an abs path
	if err == nil {
		source, err = filepath.Abs(source)
	}

	//
	if err == nil {
		var relFileName string
		relFileName, err = filepath.Rel(filepath.Dir(target), source)
		os.Symlink(relFileName, target)
	}

	return
}
