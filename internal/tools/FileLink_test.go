package tools_test

import (
	"io/fs"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tools"
)

func TestLinking(t *testing.T) {

	absPath, err := filepath.Abs("./FileLink.go")
	assert.NoError(t, err)

	os.Remove("/tmp/FileLink.go")

	err = tools.FileLink(absPath, "/tmp/FileLink.go")
	assert.NoError(t, err)

	err = tools.FileLink(absPath, "/tmp/FileLink.go")
	assert.NoError(t, err)

	// check if its an link
	var fileInfo fs.FileInfo
	fileInfo, err = os.Lstat("/tmp/FileLink.go")
	assert.NoError(t, err)
	assert.Equal(t, fileInfo.Mode().String(), "Lrwxrwxrwx")
}
