package tools

import (
	"fmt"
	"os"
	"path/filepath"
)

func FindProjectRoot() (path string, err error) {

	// Starte die Suche im aktuellen Arbeitsverzeichnis
	currentDir, err := os.Getwd()
	if err != nil {
		return "", err
	}

	currentDir, err = filepath.Abs(currentDir)
	if err != nil {
		return "", err
	}

	// Durchsuche übergeordnete Ordner
	for {
		// Überprüfe, ob eine go.mod-Datei im aktuellen Verzeichnis vorhanden ist
		goModPath := filepath.Join(currentDir, "go.mod")
		if _, err = os.Stat(goModPath); err == nil {
			path = filepath.Dir(goModPath)
			break
		}

		// Gehe zum übergeordneten Verzeichnis
		parentDir := filepath.Dir(currentDir)

		// Überprüfe, ob das übergeordnete Verzeichnis das Wurzelverzeichnis ist
		if parentDir == currentDir {
			path = ""
			err = fmt.Errorf("go.mod-Datei wurde nicht gefunden")
			break
		}

		// Aktualisiere das aktuelle Verzeichnis zum übergeordneten Verzeichnis
		currentDir = parentDir
	}

	return
}
