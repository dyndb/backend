package tools

import (
	"context"

	"gitlab.com/nerdhaltig/gopersist"
	"gitlab.com/nerdhaltig/gopersist/persistance"
)

func WithTenantConnection(
	ctx context.Context, pool gopersist.PoolManagementUseCase,
	tenant string, handler func(connection persistance.Port),
) {

	pool.WithConnection(ctx, "tenant_"+tenant, "unknown", func(ctx context.Context, connection persistance.Port) {
		handler(connection)
	})

}
