package mqtt

import (
	"strings"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/topic/ports"
)

type topicHandlerAdapter struct{}

func NewTopicHandlerAdapter() ports.Matcher {
	return &topicHandlerAdapter{}
}

func (adp *topicHandlerAdapter) Match(topic, matcher string) bool {
	if topic == matcher {
		return true
	}

	// check for wildcard
	if strings.Contains(matcher, "#") {

		publishTopicRunes := strings.Split(topic, "/")
		subscribeTopicRunes := strings.Split(matcher, "/")

		match := false
		for index := 0; true; index++ {
			match = false

			// boundry check
			if index >= len(publishTopicRunes) {
				break
			}
			if index >= len(subscribeTopicRunes) {
				break
			}

			publishTopicRune := publishTopicRunes[index]
			subscribeTopicRune := subscribeTopicRunes[index]

			if subscribeTopicRune == "#" {
				match = true
				break
			}

			if publishTopicRune == subscribeTopicRune {
				match = true
			}

			if !match {
				break
			}
		}

		if match {
			return true
		}

	}

	return false
}
