package mqtt

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTopics(t *testing.T) {

	topicMatcher := NewTopicHandlerAdapter()

	assert.True(t, topicMatcher.Match("/boat/sensor/temp", "#"))
	assert.True(t, topicMatcher.Match("/boat/sensor/temp", "/boat/#"))
	assert.True(t, topicMatcher.Match("/boat/sensor/temp", "/boat/sensor/#"))
	assert.False(t, topicMatcher.Match("/boat/sensor/temp", "/boat/sensors/#"))
	assert.True(t, topicMatcher.Match("/boat/sensor/temp", "/boat/sensor/temp"))

}
