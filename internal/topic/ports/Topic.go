package ports

type Matcher interface {
	Match(topic, matcher string) bool
}
