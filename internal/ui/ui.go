package ui

import (
	"embed"
	"io/fs"
	"net/http"
	"path"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/filesystem"
)

//go:embed all:dist
var webUIRoot embed.FS

type webUIFS struct {
	content embed.FS
}

func (c webUIFS) Open(name string) (fs.File, error) {
	return c.content.Open(path.Join("dist", name))
}

func ServeFiber(app *fiber.App) {
	/*
		app.Get("/ui/*", func(fctx *fiber.Ctx) (err error) {

			urlPath := fctx.Path()
			urlPath = strings.Replace(urlPath, "/ui", "", 1)

			fileName := path.Join("dist", urlPath)

			var file fs.File
			file, err = webUIRoot.Open(fileName)
			return fctx.SendStream(file)

			//err = fctx.SendFile(config.Data.Path.Fileserver + "/" + filename)
			//tools.DeleteFileIfExist(fctx.Context(), config.Data.Path.Fileserver+"/"+filename)

		})
	*/

	app.Use("/ui/", func(c *fiber.Ctx) error {

		// Only GET !
		if c.Method() != "GET" {
			return c.SendStatus(fiber.StatusMethodNotAllowed)
		}

		return filesystem.New(filesystem.Config{
			Browse:     true,
			Root:       http.FS(webUIRoot),
			PathPrefix: "dist",
		})(c)
	})

}
