package user_adapter_persistance_db

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/ports"
)

type adapterData struct {
	connections    connections.Management
	collectionName string
}

func New(
	connections connections.Management,
) ports.UserPersistance {

	newAuthService := adapterData{
		connections:    connections,
		collectionName: "account",
	}

	return &newAuthService
}
