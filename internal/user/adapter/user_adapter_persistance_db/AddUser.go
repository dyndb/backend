package user_adapter_persistance_db

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/errwrap"
	"golang.org/x/crypto/bcrypt"
)

func (svc *adapterData) AddUser(ctx context.Context, tenant, username, password string) (err error) {

	// encrypt passwort
	var encryptedPassword []byte
	encryptedPassword, err = bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		err = errwrap.WithStack(err)
		return err
	}

	err = svc.UpdateUserInfo(ctx, tenant, username, domain.Credentials{
		EncryptedPassword: string(encryptedPassword),
	})

	return
}
