package user_adapter_persistance_db

import (
	"context"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/gopersist"
	"golang.org/x/crypto/bcrypt"
)

// MustLogin implements ports.FiberAuthService
func (svc *adapterData) CheckPassword(ctx context.Context, tenant, username, password string) (infos domain.UserInfos, err error) {
	err = svc.connections.WithTenant(tenant, svc.collectionName, func(connection gopersist.PersistancePort) (err error) {

		// add 401 if something goes wrong
		defer func() {
			if err != nil {
				err = errors.WithStack(err)
			} else {
				log.Debug().Msg("Login was successful")
			}
		}()

		// get credentials
		var AccountCredentials domain.Credentials

		var exist bool
		exist, err = connection.Get(ctx, username, &AccountCredentials)
		if !exist {
			err = errors.New("user not exist")
		}

		// check password
		if err == nil {
			err = bcrypt.CompareHashAndPassword([]byte(AccountCredentials.EncryptedPassword), []byte(password))
		}

		// return infos
		if err == nil {
			infos.DisplayText = AccountCredentials.DisplayText
		}

		return
	})

	return
}
