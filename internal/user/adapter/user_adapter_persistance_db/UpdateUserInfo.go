package user_adapter_persistance_db

import (
	"context"
	"encoding/json"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/gopersist"
)

func (svc *adapterData) UpdateUserInfo(ctx context.Context, tenant, username string, infos domain.Credentials) (err error) {

	err = svc.connections.WithTenant(tenant, svc.collectionName, func(connection gopersist.PersistancePort) (err error) {

		if !connection.CollectionExist(ctx, svc.collectionName) {
			err = connection.CreateCollection(ctx, svc.collectionName)
		}

		// get credentials + update
		var AccountCredentials domain.Credentials
		var AccountCredentialsExist bool
		if err == nil {
			AccountCredentialsExist, err = connection.Get(ctx, username, &AccountCredentials)
			if AccountCredentialsExist {
				var tmpJsonBytes []byte
				tmpJsonBytes, err = json.Marshal(infos)
				if err == nil {
					err = json.Unmarshal(tmpJsonBytes, &AccountCredentials)
				}
			} else {
				AccountCredentials = infos
			}
		}

		// save it back
		if err == nil {
			err = connection.Save(ctx, username, AccountCredentials)
		}

		return
	})

	return

}
