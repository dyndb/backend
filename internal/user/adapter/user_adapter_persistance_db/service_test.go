package user_adapter_persistance_db_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/appcontext"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/adapter/user_adapter_persistance_db"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/ports"
)

type TestSuite struct {
	suite.Suite

	ctx       context.Context
	ctxCancel context.CancelFunc
	service   ports.UserPersistance
}

type TestCarDocument struct {
	Vendor string `json:"vendor"`
	Speed  int64  `json:"speed"`
}

func TestIt(t *testing.T) {
	suite.Run(t, new(TestSuite))
}

func (suite *TestSuite) SetupSuite() {

	var ctxTimeout context.Context
	ctxTimeout, suite.ctxCancel = context.WithTimeout(context.Background(), time.Minute*2)
	suite.ctx = appcontext.FromContext(ctxTimeout)

	connections, err := tests.PostgresqlPrepare(suite.T(), &tests.PostgresqlPrepareOpts{
		Ctx:         ctxTimeout,
		Tenant:      "testtenant",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"account"},
	})
	if err != nil {
		panic(err)
	}

	// create our service we want to test
	suite.service = user_adapter_persistance_db.New(connections)
}

func (suite *TestSuite) Test01_Login() {

	var err error
	var infos domain.UserInfos

	// add a new user
	suite.Assert().NoError(suite.service.AddUser(suite.ctx, "testtenant", "johndoe", "supersecret"))

	// try to login to an not known user
	_, err = suite.service.CheckPassword(suite.ctx, "testtenant", "unknown", "supersecret")
	suite.Assert().Error(err)
	suite.Assert().Empty(infos.DisplayText)

	// correct user, wrong password
	_, err = suite.service.CheckPassword(suite.ctx, "testtenant", "johndoe", "wrongpassword")
	suite.Assert().Error(err)

	// correct user, correct passsword
	_, err = suite.service.CheckPassword(suite.ctx, "testtenant", "johndoe", "supersecret")
	suite.Assert().NoError(err)

}

func (suite *TestSuite) Test02_UserInfos() {

	var err error
	var infos domain.UserInfos

	// add a new user
	suite.Assert().NoError(suite.service.AddUser(suite.ctx, "testtenant", "jeanne", "supersecret"))

	// update infos
	err = suite.service.UpdateUserInfo(suite.ctx, "testtenant", "jeanne", domain.Credentials{
		DisplayText: "Jeanne d'Arc",
	})
	suite.Assert().NoError(err)

	// try to login
	infos, err = suite.service.CheckPassword(suite.ctx, "testtenant", "jeanne", "supersecret")
	suite.Assert().NoError(err)
	suite.Assert().Equal("Jeanne d'Arc", infos.DisplayText)
}

func (suite *TestSuite) TearDownSuite() {

}
