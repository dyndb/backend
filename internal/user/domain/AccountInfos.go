package domain

import "time"

type AccountInfos struct {
	Tenant    string
	Username  string
	LoginTime time.Time
}
