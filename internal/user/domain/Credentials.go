package domain

type UserInfos struct {
	DisplayText string `json:"displayText,omitempty"`
}

type Credentials struct {
	DisplayText string `json:"displayText,omitempty"`

	EncryptedPassword string `json:"encryptedPassword,omitempty"`
}
