package domain

type UserInfoRequest struct{}

type UserInfo struct {
	Username    string
	DisplayText string
}

type UserLoginRequest struct {
	Tenant   string
	Username string
	Password string
}

type UserLoggedIn struct {
	Tenant   string
	Username string
}

type UserSaveRequest struct {
	Tenant      string
	Username    string
	DisplayText string
	Password    string
}
type UserSaved struct {
	Tenant   string
	Username string
}
