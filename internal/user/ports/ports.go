package ports

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
)

type UserPersistance interface {
	AddUser(ctx context.Context, tenant, username, password string) (err error)

	// CheckPassword check a password for an user
	//
	// the tokenclient is needed to check if user was already logged in and to store the token to an client ( normally your http-response )
	CheckPassword(ctx context.Context, tenant, username, password string) (infos domain.UserInfos, err error)

	UpdateUserInfo(ctx context.Context, tenant, username string, infos domain.Credentials) (err error)
}
