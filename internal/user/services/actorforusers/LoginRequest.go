package actorforusers

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/ports"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewLoginActor(ctr goactor.Concentrator, userPersistance ports.UserPersistance) (actor goactor.Actor) {

	actor = goactor.NewActor("loginActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.UserLoginRequest:
			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			_, err = userPersistance.CheckPassword(cancelCtx,
				msg.Tenant, msg.Username, msg.Password,
			)

			if err == nil {
				returnMessage = append(returnMessage,
					&domain.UserLoggedIn{
						Tenant:   msg.Tenant,
						Username: msg.Username,
					},
				)
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.UserLoginRequest{})

	return
}
