package actorforusers

import (
	"context"

	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/ports"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewSaveActor(ctr goactor.Concentrator, userPersistance ports.UserPersistance) (actor goactor.Actor) {

	actor = goactor.NewActor("userSaveActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {

		case *domain.UserSaveRequest:
			cancelCtx, cancel := context.WithCancel(context.Background())
			defer cancel()

			err = userPersistance.AddUser(cancelCtx,
				msg.Tenant, msg.Username, msg.Password,
			)

			if err == nil {
				returnMessage = append(returnMessage, &domain.UserSaved{
					Tenant:   msg.Tenant,
					Username: msg.Username,
				})
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.UserSaveRequest{})

	return
}
