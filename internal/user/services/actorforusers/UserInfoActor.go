package actorforusers

import (
	"github.com/pkg/errors"
	tokenDomain "gitlab.com/nerdhaltig/dyndb/backend/internal/tokens/domain"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func NewUserInfoActor(ctr goactor.Concentrator) (actor goactor.Actor) {

	actor = goactor.NewActor("userInfoActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch ctx.Message().(type) {

		case *domain.UserInfoRequest:

			var AuthInfo *tokenDomain.AuthValidated
			if ctx.As(&AuthInfo) {

				returnMessage = append(returnMessage, &domain.UserInfo{
					Username: AuthInfo.Username,
				})

			} else {
				err = errors.WithStack(errors.New("unauthenticated"))
			}

		}
		return
	})

	// register it
	ctr.HookActor(actor, &domain.UserInfoRequest{})

	return
}
