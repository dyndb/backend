package actorforusers_test

import (
	"context"
	"testing"
	"time"

	testtools "gitlab.com/nerdhaltig/dyndb/backend/internal/tests"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/tests"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func TestActors(t *testing.T) {

	ctxBg, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()

	connections, err := testtools.PostgresqlPrepare(t, &testtools.PostgresqlPrepareOpts{
		Ctx:         ctxBg,
		Tenant:      "testdyndb",
		Database:    "integrationtest",
		Username:    "dbtestuser",
		Password:    "dntestpw",
		Collections: []string{"attachments"},
	})
	if err != nil {
		panic(err)
	}

	// we need an global concentrator
	ctr := goactor.NewConcentrator(time.Second * 30)

	// prepare actors
	tests.PrepareUserActors(ctr, connections)

	user.TestActors(t, ctr, "testdyndb")

}
