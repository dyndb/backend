//go:build !production

package tests

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/services/actorforusers"
	"gitlab.com/nerdhaltig/goactor/v2"
)

func PrepareUserActors(ctr goactor.Concentrator, connections connections.Management) {

	// persistance in DB
	persistanceAdapter := user.NewUserPersistanceAdapter(connections)

	// create adapters
	actorforusers.NewSaveActor(ctr, persistanceAdapter)
	actorforusers.NewLoginActor(ctr, persistanceAdapter)

}
