package user

import (
	"gitlab.com/nerdhaltig/dyndb/backend/internal/connections"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/adapter/user_adapter_persistance_db"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/ports"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/services/actorforusers"
	"gitlab.com/nerdhaltig/goactor/v2"
)

// ports
type PersistancePort ports.UserPersistance

// adapters

// services

func NewUserPersistanceAdapter(connections connections.Management) PersistancePort {
	return user_adapter_persistance_db.New(connections)
}

// actors
func NewSaveActor(ctr goactor.Concentrator, userPersistance PersistancePort) (actor goactor.Actor) {
	return actorforusers.NewSaveActor(ctr, userPersistance)
}

func NewLoginActor(ctr goactor.Concentrator, userPersistance PersistancePort) (actor goactor.Actor) {
	return actorforusers.NewLoginActor(ctr, userPersistance)
}

func NewUserInfoActor(ctr goactor.Concentrator) (actor goactor.Actor) {
	return actorforusers.NewUserInfoActor(ctr)
}
