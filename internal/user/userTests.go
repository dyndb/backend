//go:build !production

package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerdhaltig/dyndb/backend/internal/user/domain"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
)

func TestActors(
	t *testing.T,
	ctr goactor.Concentrator,
	Tenant string,
) {

	// Create a new Login
	_, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Message: &domain.UserSaveRequest{
				Tenant:      Tenant,
				Username:    "first",
				DisplayText: "first user",
				Password:    "firstpassword",
			},
		},
		concentrator.Job{
			Message: &domain.UserSaveRequest{
				Tenant:      Tenant,
				Username:    "second",
				DisplayText: "second user",
				Password:    "secondpassword",
			},
		},
	)
	assert.NoError(t, err)

	// wrong username / correct password
	loggedIn := &domain.UserLoggedIn{}
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.UserLoginRequest{
			Tenant:   Tenant,
			Username: "blabla",
			Password: "secondpassword",
		},
		ExpectedReturnMessage: &loggedIn,
	})
	assert.Error(t, err)

	// correct username / correct password
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &domain.UserLoginRequest{
			Tenant:   Tenant,
			Username: "second",
			Password: "secondpassword",
		},
		ExpectedReturnMessage: &loggedIn,
	})
	assert.NoError(t, err)
	assert.NotEmpty(t, loggedIn.Tenant)
	assert.NotEmpty(t, loggedIn.Username)

}
