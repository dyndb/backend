package ports

// SessionRepo store multiple sessions
type SessionRepo interface {

	// Create will create an empty session
	//
	// if the session already exist, nothing will be done
	Create(sessionID string) (err error)

	// Exist will check if an session exist
	//
	//  Returns an error if the session cannot be found
	Exist(sessionID string) (exist bool, err error)

	// Delete will delete the session
	//
	//  Returns an error if the session cannot be found
	Delete(sessionID string) (err error)

	// Value will [READ] an key from a session
	Value(sessionID, key string, value interface{}) (err error)

	// ValueStore will store an value to an session.
	//
	//  Returns an error if the session cannot be found
	ValueStore(sessionID, key string, value interface{}) (err error)

	// ValueDelete delete an value from an session
	//
	//  Returns an error if the session cannot be found
	ValueDelete(sessionID, key string) (err error)

	// Will iterate over all sessions
	Iterate(valuekey string, output chan<- SessionInfo)

	Close() (err error)
}

type SessionInfo struct {
	ID    string
	Value interface{}
}
