package ports

import (
	"context"
	"time"
)

type TokenRepo interface {

	// will cleanup invalid tokens, delete the session and inform about the deleted session id.
	// This function is BLOCKING, so use go ...
	CleanupLoop(ctx context.Context, intervall time.Duration, deletedSessionsIDs chan<- string)

	// New will create a new token and invalidate the old/active one !
	New(sessionID string) (tokenID string, err error)

	// Refresh the token if the token is valid.
	// If the token is invalid newToken will be ""
	Refresh(curTokenID string) (sessionID string, tokenAlreadyRefreshed bool, newTokenID string)
	IsValid(curTokenID string) (sessionID string, deadline time.Time, refreshed, valid bool)

	// #################### Stats ####################
	Count() int

	// #################### Persistance ####################

	// Load all tokens from your backend storage
	Load(storage SessionRepo)

	// will save the tokens to your storage engine in a loop - BLOCKING !
	FlushLoop(ctx context.Context, storage SessionRepo, intervall time.Duration)

	// Flush all tokens to you backend storage
	Flush(storage SessionRepo)
}
