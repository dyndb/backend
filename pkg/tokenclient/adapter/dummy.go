package adapter

import (
	"time"

	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenclient/ports"
)

type dummyClient struct {
	accessToken  string
	refreshToken string
}

func NewDummyAdapter() ports.TokenClientPort {
	return &dummyClient{}
}

func (cl *dummyClient) RefreshTokenGet(prefix string) (refreshToken string) {
	return cl.refreshToken
}

func (cl *dummyClient) RefreshTokenSet(prefix string, path string, timeout time.Duration, refreshToken string) {
	cl.refreshToken = refreshToken
}

// GetToken implements tokenclient.Adapter
func (cl *dummyClient) AccessTokenGet(prefix string) (accessToken string) {
	return cl.accessToken
}

// SetToken implements tokenclient.Adapter
func (cl *dummyClient) AccessTokenSet(prefix string, path string, timeout time.Duration, accessToken string) {
	cl.accessToken = accessToken
}
