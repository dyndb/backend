package adapter

import (
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenclient/ports"
)

type fiberCookieAdapter struct {
	fctx *fiber.Ctx
}

func NewFiberCookieAdapter(fctx *fiber.Ctx) ports.TokenClientPort {

	return &fiberCookieAdapter{
		fctx: fctx,
	}
}

func (cl *fiberCookieAdapter) RefreshTokenGet(prefix string) (refreshToken string) {
	return cl.fctx.Cookies(prefix+"token-refresh", "")
}

func (cl *fiberCookieAdapter) RefreshTokenSet(prefix string, path string, timeout time.Duration, refreshToken string) {

	newCookie := &fiber.Cookie{
		Name:    prefix + "token-refresh",
		Secure:  true,
		Path:    path,
		Expires: time.Now().Add(timeout),
		Value:   refreshToken,
	}

	cl.fctx.Cookie(newCookie)
	cl.fctx.Response().Header.Set("x-"+prefix+"token-refresh-timeout", fmt.Sprintf("%f", timeout.Seconds()))

}

// GetToken implements tokenclient.Adapter
func (cl *fiberCookieAdapter) AccessTokenGet(prefix string) (accessToken string) {
	return cl.fctx.Cookies(prefix+"token-access", "")
}

// SetToken implements tokenclient.Adapter
func (cl *fiberCookieAdapter) AccessTokenSet(prefix string, path string, timeout time.Duration, accessToken string) {

	// create cookie
	newCookie := &fiber.Cookie{
		Name:    prefix + "token-access",
		Secure:  true,
		Path:    path,
		Expires: time.Now().Add(timeout),
		Value:   accessToken,
	}

	cl.fctx.Cookie(newCookie)
	cl.fctx.Response().Header.Set("x-"+prefix+"token-access-timeout", fmt.Sprintf("%f", timeout.Seconds()))
}
