package ports

import "time"

type FiberAdapter interface {
}

type TokenClientPort interface {
	RefreshTokenGet(prefix string) (refreshToken string)
	RefreshTokenSet(prefix string, path string, timeout time.Duration, refreshToken string)

	AccessTokenGet(prefix string) (accessToken string)
	AccessTokenSet(prefix string, path string, timeout time.Duration, accessToken string)
}
