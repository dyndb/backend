package tokenclient

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenclient/adapter"
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenclient/ports"
)

type Port ports.TokenClientPort

func NewFiberCookieAdapter(fctx *fiber.Ctx) Port {
	return adapter.NewFiberCookieAdapter(fctx)
}

func NewDummyAdapter() Port {
	return adapter.NewDummyAdapter()
}
