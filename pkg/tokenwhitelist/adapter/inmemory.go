package adapter

import (
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenwhitelist/ports"
)

type tokenWhitelistAdapter struct {
	storage map[string]map[string]bool
}

func NewTokenWhitelistInMemoryAdapter() ports.TokenWhitelistPort {
	return &tokenWhitelistAdapter{
		storage: make(map[string]map[string]bool),
	}
}

// Add implements ports.TokenWhitelistPort
func (adp *tokenWhitelistAdapter) Add(group string, id string) {
	if id == "" {
		return
	}

	var exist bool
	var signatures map[string]bool

	if signatures, exist = adp.storage[group]; !exist {
		signatures = make(map[string]bool)
		adp.storage[group] = signatures
	}

	signatures[id] = true
}

// Exist implements ports.TokenWhitelistPort
func (adp *tokenWhitelistAdapter) Exist(group string, id string) bool {
	var exist bool
	var signatures map[string]bool

	if signatures, exist = adp.storage[group]; !exist {
		signatures = make(map[string]bool)
		adp.storage[group] = signatures
	}

	_, signatureExist := signatures[id]
	return signatureExist
}

// Remove implements ports.TokenWhitelistPort
func (adp *tokenWhitelistAdapter) Remove(group string, id string) {
	var exist bool
	var signatures map[string]bool

	if signatures, exist = adp.storage[group]; !exist {
		signatures = make(map[string]bool)
		adp.storage[group] = signatures
	}

	delete(signatures, id)
}

// RemoveAll implements ports.TokenWhitelistPort
func (adp *tokenWhitelistAdapter) RemoveAll(group string) {
	delete(adp.storage, group)
}
