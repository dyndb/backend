package adapter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWhitelist(t *testing.T) {

	whitelistAdapter := NewTokenWhitelistInMemoryAdapter()

	assert.False(t, whitelistAdapter.Exist("default", "sig1"))
	whitelistAdapter.Add("default", "sig1")
	assert.True(t, whitelistAdapter.Exist("default", "sig1"))
	whitelistAdapter.Add("default", "sig2")
	assert.True(t, whitelistAdapter.Exist("default", "sig2"))
	whitelistAdapter.Remove("default", "sig1")
	assert.False(t, whitelistAdapter.Exist("default", "sig1"))
	whitelistAdapter.RemoveAll("default")
	assert.False(t, whitelistAdapter.Exist("default", "sig2"))

}
