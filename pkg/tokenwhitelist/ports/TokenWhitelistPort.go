package ports

type TokenWhitelistPort interface {
	// Add a token-id to the whitelist
	Add(group, id string)

	// Exist will check if a token-id is whitelisted
	Exist(group, id string) bool

	// Remove a token-id from the whitelist
	Remove(group, id string)

	// Remove all token-ids from the whitelist
	// This usual happens, if a not whitelisted refresh-token was provided
	RemoveAll(group string)
}
