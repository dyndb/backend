package tokenwhitelist

import (
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenwhitelist/adapter"
	"gitlab.com/nerdhaltig/dyndb/backend/pkg/tokenwhitelist/ports"
)

type PersistancePort ports.TokenWhitelistPort

func NewTokenWhitelistInMemoryAdapter() PersistancePort {
	return adapter.NewTokenWhitelistInMemoryAdapter()
}
