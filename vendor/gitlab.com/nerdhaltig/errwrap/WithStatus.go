package errwrap

type ErrorWithState interface {
	State() int

	Error() string
	Unwrap() error
}

type errState struct {
	ErrState int   `json:"state"`
	Err      error `json:"-"` // The original error. Same reason as above.
}

func WithState(err error, state int) error {
	if err == nil {
		return nil
	}

	return errState{
		ErrState: state,
		Err:      err,
	}
}

func (err errState) State() int {
	return err.ErrState
}
func (err errState) Error() string {
	if err.Err != nil {
		return err.Err.Error()
	}
	return ""
}

func (err errState) Unwrap() error {
	return err.Err // Returns inner error
}
