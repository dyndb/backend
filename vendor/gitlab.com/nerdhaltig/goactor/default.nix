{ pkgs ? import
    (builtins.fetchTarball
      "https://github.com/NixOS/nixpkgs/archive/aa8aa7e2ea35ce655297e8322dc82bf77a31d04b.tar.gz"
    )
    { }
}:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

  version = "0.50.0";

  ################################### Binary ###################################
  src = pkgs.nix-gitignore.gitignoreSourcePure [
    ".gitlab-ci.yml"
    ".gitignore"
    "build/"
    "flake*"
    "*.adoc"
    "*.xml"
    "*.nix"
  ]
    (pkgs.lib.cleanSource ./.);

  coverage = import ./build/nix/coverage.nix { inherit pkgs src; };

  badge-test = import ./build/nix/badge.nix {
    inherit pkgs;
    valuefile = "${pkgs.writeText "$out/test" "10.0"}";
    badgename = "test";
  };

  badge-coverage = import ./build/nix/badge.nix {
    inherit pkgs;
    valuefile = "${coverage}/coverage.sum";
    badgename = "coverage";
  };

  badge = pkgs.symlinkJoin {
    name = "badges";
    paths = [ badge-coverage ];
  };

  report = import ./build/nix/report.nix { inherit pkgs src; };

in
{
  inherit
    coverage
    badge-test
    badge-coverage
    badge
    report;
}
