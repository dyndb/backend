# nix --extra-experimental-features "nix-command flakes" build  .#coverage
# nix --extra-experimental-features "nix-command flakes" build  .#coverage_badge
{
  inputs = {

    # we would like to use the current packages for building the docker-image
    #nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs.url = "https://github.com/NixOS/nixpkgs/archive/aa8aa7e2ea35ce655297e8322dc82bf77a31d04b.tar.gz";

    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

      in
      {
        packages = import ./default.nix { inherit pkgs; };

        # devShells.default = import ./shell2.nix { inherit pkgs; };

      });
}
