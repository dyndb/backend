package goactor

import (
	"gitlab.com/nerdhaltig/goactor/internal/actor"
	"gitlab.com/nerdhaltig/goactor/internal/jobcontext"
	"gitlab.com/nerdhaltig/goactor/logger"
	"gitlab.com/nerdhaltig/goactor/supervisor"
	"gitlab.com/nerdhaltig/goactor/types"
)

type Actor types.Actor
type JobContext types.JobContext

// NewActor create a new actor with an handling function.
//
// This will NOT spawn go-routines, call .Spawn() to do this.
func NewActor(name string, handler func(ctx JobContext) (msg any, err error)) Actor {
	return actor.New(name, func(ctx types.JobContext) (returnMessage any, err error) {
		return handler(ctx)
	})
}

// NewEmptyJobContext return an empty context with an actor.
//
// Use this if you would like to create an error handler,
// of an final actor that receive all response messages.
func NewEmptyJobContext(actor Actor) JobContext {
	return jobcontext.First(actor, nil)
}

// supervisor
type Opt types.Opt
type SuperVisor types.SuperVisor

func NewSupervisor(options ...types.Opt) SuperVisor {
	return supervisor.New(options...)
}

// supervisor - options
func OptionInitialWorkerCount(count int) types.Opt {
	return supervisor.OptionInitialWorkerCount(count)
}

// logger
func LoggerSet(log logger.Logger) {
	logger.SetLogger(log)
}

func DisableLogger() {
	logger.SetLogger(&logger.NoLogger{})
}
