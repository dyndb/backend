package actor

import (
	"errors"
	"sync"
	"time"

	"gitlab.com/nerdhaltig/goactor/types"
)

type Actor struct {
	lock sync.RWMutex

	name string

	inbox       chan types.JobContext
	initialized bool

	workerCount        int
	workerCountReached sync.WaitGroup // wait to worker count is reached
	workerCountWant    int            // wanted worker amount
	workerCancel       chan interface{}

	handler func(ctx types.JobContext) (returnMessage any, err error)
}

func New(name string, handler func(ctx types.JobContext) (returnMessage any, err error)) *Actor {

	// create a new actor
	newActor := &Actor{
		name:    name,
		handler: handler,

		workerCancel: make(chan interface{}, 1),
	}

	return newActor
}

func (act *Actor) Name() string {
	return act.name
}

func (act *Actor) Stop(timeout time.Duration) (err error) {

	// timeout
	timerTimeout := time.NewTimer(timeout)

	// ensure processes
	var processesReached chan interface{} = make(chan interface{}, 1)
	go func() {
		act.lock.Lock()
		processesToSpawn := act.workerCount * -1
		act.lock.Unlock()

		act.Spawn(processesToSpawn)
		processesReached <- nil
	}()

	select {
	case <-processesReached:
		break
	case <-timerTimeout.C:
		err = errors.New("timeout on ensuring processes")
	}
	close(processesReached)

	return
}
