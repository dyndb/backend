package actor

import "gitlab.com/nerdhaltig/goactor/types"

func (act *Actor) CallHandler(ctx types.JobContext) (returnMessage any, err error) {
	return act.handler(ctx)
}
