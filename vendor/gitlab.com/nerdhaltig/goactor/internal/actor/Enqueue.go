package actor

import (
	"gitlab.com/nerdhaltig/goactor/internal/jobcontext"
	"gitlab.com/nerdhaltig/goactor/types"
)

func (act *Actor) Enqueue(msg any) {
	act.EnqueueJob(jobcontext.First(act, msg))
}

func (act *Actor) EnqueueJob(ctx types.JobContext) {
	if act.inbox == nil {
		act.messageHandler(ctx)
	} else {
		act.inbox <- ctx
	}
}
