package actor

func (act *Actor) InboxCounter() (usage, capacity int) {
	return len(act.inbox), cap(act.inbox)
}
