package actor

import (
	"errors"
	"fmt"
	"runtime/debug"

	"gitlab.com/nerdhaltig/goactor/goactorerror"
	"gitlab.com/nerdhaltig/goactor/logger"
	"gitlab.com/nerdhaltig/goactor/types"
)

func (act *Actor) messageHandler(ctx types.JobContext) {
	func() {

		// send error on panic
		defer func() {
			if recoverInterface := recover(); recoverInterface != nil {

				var outErr error

				switch recoverData := recoverInterface.(type) {
				case error:
					outErr = goactorerror.NewErrorWrapper(recoverData, ctx.Message())
					logger.Log.Error(recoverData)
				case string:
					newError := errors.New(recoverData)
					outErr = goactorerror.NewErrorWrapper(errors.New(recoverData), ctx.Message())
					logger.Log.Error(newError)
				default:
					newError := fmt.Errorf("%v", recoverData)
					outErr = goactorerror.NewErrorWrapper(newError, ctx.Message())
					logger.Log.Error(newError)
				}

				debug.PrintStack()

				//logger.Log.Error(errors.New(string(debug.Stack())))

				ctx.Return(outErr)
			}
		}()

		// call the handler
		logger.Log.Debug(fmt.Sprintf("[%s]: call handler(%s)", act.Name(), ctx.MessageType()))
		returnMessage, err := act.handler(ctx)

		// return
		if err == nil {
			if returnMessage != nil {
				ctx.Return(returnMessage)
			}
		} else {
			logger.Log.Error(err)
			ctx.Return(goactorerror.NewErrorWrapper(err, ctx.Message()))
		}

	}()
}
