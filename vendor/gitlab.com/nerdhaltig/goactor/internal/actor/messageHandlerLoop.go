package actor

import (
	"fmt"
	"time"

	"gitlab.com/nerdhaltig/goactor/goactorstate"
	"gitlab.com/nerdhaltig/goactor/internal/jobcontext"
	"gitlab.com/nerdhaltig/goactor/logger"
)

func (act *Actor) messageHandlerLoop() {
	act.lock.RLock()
	logger.Log.Debug(fmt.Sprintf("spawning process %d", act.workerCount+1))
	act.lock.RUnlock()

	// decrease worker counter
	// and ensure processes
	defer func() {
		act.lock.Lock()
		act.workerCount--
		act.lock.Unlock()
		act.ensureProcesses()
	}()

	// Send the stopped-message
	defer act.handler(jobcontext.First(act, &goactorstate.Stopped{}))

	// the initial message
	act.lock.RLock()
	if !act.initialized {
		act.lock.RUnlock()

		act.handler(jobcontext.First(act, &goactorstate.Initialized{}))

		act.lock.Lock()
		act.initialized = true
		act.lock.Unlock()
	} else {
		act.lock.RUnlock()
	}

	// process counter + Initialized
	act.lock.Lock()
	act.workerCount++
	act.lock.Unlock()

	// Send the started-message
	act.handler(jobcontext.First(act, &goactorstate.Started{}))

	// ensure additional processes
	act.ensureProcesses()

messageLoop:
	for {
		select {

		case msg := <-act.inbox:
			if msg == nil {
				time.Sleep(time.Second * 1)
				continue
			}

			act.messageHandler(msg)

		case <-act.workerCancel:
			break messageLoop
		}

	}

}
