package jobcontext

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/types"
)

// JobContext represent the chain of messages
type Context struct {
	first *Context
	prev  *Context

	actor   types.Actor
	msg     any
	msgType string

	// indicates that this context is an return context
	isReturnContext bool
}

// First create the first job-context
func First(actor types.Actor, msg any) (newContext *Context) {

	// new context
	newContext = &Context{
		actor:   actor,
		msg:     msg,
		msgType: "nil",
	}

	// this is the first context
	newContext.first = newContext

	// read message-type
	if msg != nil {
		msgReflect := reflect.ValueOf(msg)
		newContext.msgType = msgReflect.Type().String()
	}

	return newContext
}

func (ctx *Context) Actor() types.Actor {
	return ctx.actor
}
