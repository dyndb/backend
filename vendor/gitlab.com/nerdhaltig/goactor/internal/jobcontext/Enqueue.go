package jobcontext

import (
	"errors"
	"fmt"

	"gitlab.com/nerdhaltig/goactor/logger"
)

// EnqueueOrigin will fork the context and enqueue the context to the actor
func (ctx *Context) EnqueueOrigin(msg any) (err error) {
	if ctx.first != nil {

		newContext := ctx.Next(ctx.first.actor, msg)

		logger.Log.Info(fmt.Sprintf("[%s] %s -> %s: send message %s", ctx.actor.Name(), ctx.actor.Name(), ctx.first.actor.Name(), newContext.msgType))

		newContext.Publish()
		return nil
	}

	return errors.New("no origin actor available")
}

// Publish enqueue the context to an actor
func (ctx *Context) Publish() (err error) {
	if ctx.actor != nil {
		ctx.actor.EnqueueJob(ctx)
		return nil
	}

	return errors.New("no actor available")
}

// Egal wo du stehst auf deinem Pfad, egal wie du dort hin gekomment bist, jeder Schritt vorwärts ist deine Wahl
