package jobcontext

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/types"
)

func (ctx *Context) Fork(actor types.Actor, msg any) (newContext *Context) {

	// we create a copy
	newContext = &Context{
		first:   ctx.first,
		prev:    ctx.prev,
		actor:   ctx.actor,
		msg:     msg,
		msgType: "nil",
	}

	if actor != nil {
		newContext.actor = actor
	}

	// read message-type
	if msg != nil {
		msgReflect := reflect.ValueOf(msg)
		newContext.msgType = msgReflect.Type().String()
	}

	return
}
