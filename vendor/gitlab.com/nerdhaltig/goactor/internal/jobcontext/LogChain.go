package jobcontext

import (
	"fmt"

	"gitlab.com/nerdhaltig/goactor/logger"
)

func (ctx *Context) LogChain() {

	output := ""
	first := false
	for ctx != nil {

		if first {
			output = " -> " + output
		} else {
			first = true
		}

		output = fmt.Sprintf("%s", ctx.actor.Name()) + output

		ctx = ctx.prev
	}

	logger.Log.Debug(output)
}
