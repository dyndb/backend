package jobcontext

import (
	"reflect"
)

func (ctx *Context) Message() (msg any) {
	msg = ctx.msg
	return
}

func (ctx *Context) MessageType() string {
	return ctx.msgType
}

func (ctx *Context) As(msgtype any) (exist bool) {

	msgReflect := reflect.ValueOf(msgtype)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = string(msgRelflectType[1:])

	// we search for the message
	for curCtx := ctx.prev; curCtx != nil; curCtx = curCtx.prev {

		if curCtx.msgType == msgRelflectType {
			msgReflect.Elem().Set(reflect.ValueOf(curCtx.msg))
			return true
		}

	}

	return
}
