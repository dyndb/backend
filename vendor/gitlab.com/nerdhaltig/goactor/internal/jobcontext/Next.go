package jobcontext

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/types"
)

func (ctx *Context) Next(actor types.Actor, msg any) (newContext *Context) {

	// new context
	newContext = &Context{
		first:   ctx.first,
		prev:    ctx,
		actor:   actor,
		msg:     msg,
		msgType: "nil",
	}

	// read message-type
	if msg != nil {
		msgReflect := reflect.ValueOf(msg)
		newContext.msgType = msgReflect.Type().String()
	}

	return
}
