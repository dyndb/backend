package jobcontext

import (
	"errors"
	"fmt"

	"gitlab.com/nerdhaltig/goactor/logger"
)

func (ctx *Context) Return(msg any) (err error) {

	originContext := ctx
	for {
		if originContext == nil {
			break
		}
		if !originContext.isReturnContext {
			break
		}
		originContext = originContext.prev
	}

	if originContext.prev != nil {

		/*
			newContext := ctx.prev.Fork(nil, msg)
			newContext.Publish()
		*/
		newContext := originContext.prev.Next(originContext.prev.actor, msg)
		newContext.isReturnContext = true

		ctxActorName := "unknown"
		if ctx.Actor() != nil {
			ctxActorName = ctx.Actor().Name()
		}
		newContextActorName := "unknown"
		if newContext.actor != nil {
			newContextActorName = newContext.actor.Name()
		}

		logger.Log.Info(fmt.Sprintf("[%s] %s <- %s: return message %s", ctxActorName, newContextActorName, ctxActorName, newContext.msgType))

		newContext.Publish()
		return nil
	}

	return errors.New("no prev context")
}
