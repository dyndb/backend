package logger

import (
	"github.com/rs/zerolog"
	zerologger "github.com/rs/zerolog/log"
)

type Logger interface {
	Debug(text string)
	Info(message string)
	Warning(message string)
	Error(err error)
}

type DefaultLogger struct{}

func (log *DefaultLogger) Debug(message string) {
	zerologger.Debug().Msg(message)
}

func (log *DefaultLogger) Info(message string) {
	zerologger.Info().Msg(message)
}

func (log *DefaultLogger) Warning(message string) {
	zerologger.Warn().Msg(message)
}

func (log *DefaultLogger) Error(err error) {
	zerologger.Error().Msg(err.Error())
}

var Log Logger = &DefaultLogger{}

func SetLogger(log Logger) {
	Log = log
}

func init() {
	zerolog.CallerSkipFrameCount = 3
}
