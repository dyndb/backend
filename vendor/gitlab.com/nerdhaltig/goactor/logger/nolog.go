package logger

type NoLogger struct{}

func (log *NoLogger) Debug(message string) {}

func (log *NoLogger) Info(message string) {}

func (log *NoLogger) Warning(message string) {}

func (log *NoLogger) Error(err error) {}
