package supervisor

func (sv *supervisor) Enqueue(messages ...interface{}) {
	for _, message := range messages {
		if message == nil {
			continue
		}

		sv.actor.Enqueue(message)
	}
}
