package supervisor

import "gitlab.com/nerdhaltig/goactor/types"

type OptInitialWorkerCount struct {
	Count int
}

func OptionInitialWorkerCount(count int) types.Opt {
	return &OptInitialWorkerCount{
		Count: count,
	}
}
