package supervisor

import (
	"fmt"

	"gitlab.com/nerdhaltig/goactor/logger"
	"gitlab.com/nerdhaltig/goactor/types"
)

func (sv *supervisor) Print() {

	sv.messageTypeActors.ForEach(func(s string, a []types.Actor) {

		message := s + ": "
		messageFirst := true
		for _, curActor := range a {

			if !messageFirst {
				message += ", "
			}
			messageFirst = false

			message += fmt.Sprintf("%p (%s)", &curActor, curActor.Name())
		}
		logger.Log.Debug(message)
	})

}
