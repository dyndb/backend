package supervisor

import "gitlab.com/nerdhaltig/goactor/internal/actor"

func (sv *supervisor) ProcessCounter() (counter int) {
	return sv.actor.(*actor.Actor).ProcessCounter()
}
