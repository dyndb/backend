package supervisor

import (
	"fmt"
	"reflect"

	"gitlab.com/nerdhaltig/goactor/goactorstate"
	"gitlab.com/nerdhaltig/goactor/internal/actor"
	"gitlab.com/nerdhaltig/goactor/internal/jobcontext"
	"gitlab.com/nerdhaltig/goactor/logger"
	"gitlab.com/nerdhaltig/goactor/types"
)

func (sv *supervisor) Register(actorToRegister types.Actor, messageTypes ...interface{}) (err error) {

	// check if actor already exist
	if _, exist := sv.allActors.Get(actorToRegister.Name()); exist {
		return fmt.Errorf("actor '%s' already exist", actorToRegister.Name())
	}
	sv.allActors.Set(actorToRegister.Name(), actorToRegister)

	// add message types
	err = sv.RegisterMessageTypes(actorToRegister, messageTypes...)
	if err != nil {
		return err
	}

	// get message types directly from actor
	messageTypesRequest := &goactorstate.MessageTypesRequest{}
	var responseTypesAny any

	// call the actor directly - synchron !
	responseTypesAny, err = actorToRegister.(*actor.Actor).CallHandler(jobcontext.First(sv.actor, messageTypesRequest))

	// Handle the response
	switch responseTypes := responseTypesAny.(type) {
	case *goactorstate.MessageTypes:
		sv.RegisterMessageTypes(actorToRegister, responseTypes.Types...)
	}

	return
}

func (sv *supervisor) RegisterMessageTypes(actor types.Actor, messageTypes ...interface{}) (err error) {

	// get message-types that actor will handle
	for _, messageTypeInterface := range messageTypes {
		if messageTypeInterface == nil {
			continue
		}

		msgReflect := reflect.ValueOf(messageTypeInterface)
		messageType := msgReflect.Type().String()

		logger.Log.Debug(fmt.Sprintf("[%s][%s]: register type: %s", sv.actor.Name(), actor.Name(), messageType))

		actorsArray, actorsExist := sv.messageTypeActors.Get(messageType)
		if !actorsExist {
			actorsArray = []types.Actor{}
		}

		actorsArray = append(actorsArray, actor)

		sv.messageTypeActors.Set(messageType, actorsArray)
	}

	return
}
