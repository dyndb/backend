package supervisor

func (sv *supervisor) SpawnMore(name string, processCounter int) {
	if actor, exist := sv.allActors.Get(name); exist {
		actor.Spawn(processCounter)
	}
}
