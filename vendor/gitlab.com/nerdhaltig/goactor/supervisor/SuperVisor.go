package supervisor

import (
	"gitlab.com/nerdhaltig/goactor/internal/actor"
	"gitlab.com/nerdhaltig/goactor/internal/safemap"
	"gitlab.com/nerdhaltig/goactor/types"
)

type supervisor struct {
	actor types.Actor

	// messageTypeActors maps message types to multiple actors.
	messageTypeActors *safemap.SafeMap[string, []types.Actor]

	// allActors maps actor-names to actor.
	// this is to ensure that an actor only exist once
	allActors *safemap.SafeMap[string, types.Actor]
}

func New(options ...types.Opt) types.SuperVisor {

	// options
	var WantedProcesses int = 99

	// parse options
	for _, curOption := range options {
		switch typedOption := curOption.(type) {
		case *OptInitialWorkerCount:
			WantedProcesses = typedOption.Count

		}
	}

	// create supervisor
	newSupervisor := &supervisor{
		messageTypeActors: safemap.New[string, []types.Actor](),
		allActors:         safemap.New[string, types.Actor](),
	}

	// we need a queue with an amount of worker for this actor
	newSupervisor.actor = actor.New("supervisor", newSupervisor.handler)
	newSupervisor.actor.Spawn(WantedProcesses)

	return newSupervisor
}
