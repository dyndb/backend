package supervisor

import (
	"fmt"

	"gitlab.com/nerdhaltig/goactor/goactorstate"
	"gitlab.com/nerdhaltig/goactor/internal/jobcontext"
	"gitlab.com/nerdhaltig/goactor/logger"
	"gitlab.com/nerdhaltig/goactor/types"
)

func (sv *supervisor) handler(jobctx types.JobContext) (returnMessage any, err error) {

	// convert, because we would like to use "internal" functions
	ctx := jobctx.(*jobcontext.Context)

	message := ctx.Message()
	messageType := ctx.MessageType()

	// We ignore some internal message-types
	if messageType == "*goactorstate.Initialized" {
		return
	}
	if messageType == "*goactorstate.Started" {
		return
	}
	if messageType == "*goactorstate.Stopped" {
		return
	}

	logger.Log.Debug(fmt.Sprintf("[%s] %s -> ???: try to find an actor for message-type %s", sv.actor.Name(), ctx.Actor().Name(), messageType))

	// get actors that want this message
	actors, exist := sv.messageTypeActors.Get(messageType)
	if exist {
		if len(actors) > 0 {

			for _, registeredActor := range actors {

				subctx := ctx.Fork(registeredActor, message)

				logger.Log.Info(fmt.Sprintf("[%s] %s -> %s: actor is interested in this message", sv.actor.Name(), ctx.Actor().Name(), registeredActor.Name()))
				registeredActor.EnqueueJob(subctx)
			}

		}
	} else {
		logger.Log.Info(fmt.Sprintf("[%s] %s -> ???: nobody handled this message: %s", sv.actor.Name(), ctx.Actor().Name(), messageType))

		if messageType != "*goactorstate.OrphanMessage" {

			ctx.EnqueueOrigin(&goactorstate.OrphanMessage{
				Msg: ctx.Message(),
			})

		}
	}

	return
}
