package types

// ActorHandler do the whole work
type ActorHandler interface {

	// MessageTypes should return a list of messages this job can handle
	MessageTypes() []string

	// Handle the job.
	Handle(ctx JobContext) (msg any, err error)
}
