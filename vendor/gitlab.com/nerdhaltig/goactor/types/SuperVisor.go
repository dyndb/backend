package types

type SuperVisor interface {
	Register(actor Actor, messageTypes ...interface{}) (err error)

	SpawnMore(name string, processCounter int)
	ProcessCounter() (counter int)

	Enqueue(msg ...any)

	Print()
}
