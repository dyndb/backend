package types

import "time"

// Actor interface that represents what an job can do with an actor
type Actor interface {
	Name() string

	// Enqueue a new message to the actors inbox.
	// Without an prev-context
	Enqueue(msg any)

	// Enqueue a job.
	//
	// This is mainly for special stuff and should not be used in hour normal code
	EnqueueJob(ctx JobContext)

	// Spawn more processes.
	// if processCounter is negative, worker will reduce for this amount
	Spawn(processCounter int)

	// Stop all go subroutines or return an error if timeout occurred
	Stop(timeout time.Duration) (err error)
}
