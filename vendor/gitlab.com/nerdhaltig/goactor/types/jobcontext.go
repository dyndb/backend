package types

type JobContext interface {

	// Actor return the actor for the current context
	Actor() Actor

	// Message return the message for the current context
	Message() (msg any)

	// MessageType return the current type of message as string
	MessageType() string

	// As will load the type of msgtype from the context.
	// Every Enqueue() and Return() append a message to the context chain
	// and this messages can loaded with As()
	//
	// Example:
	//
	//  var message *customMessage
	//  if ctx.As(&message) {
	As(msgtype any) (exist bool)

	// Destroy will destroy current context.
	Destroy()

	// Enqueue a new message to first actor
	EnqueueOrigin(msg any) (err error)

	// Return a message to prev context, only if the prev context is not an returned context.
	// This ensures chaining of actors.
	Return(msg any) (err error)

	LogChain()
}
