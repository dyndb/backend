package actor

import (
	"fmt"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

func (act *actorData) Spawn(processCounter int) {

	// calculate wanted worker-count
	act.lock.Lock()
	act.workerCountWant = act.workerCountWant + processCounter
	act.workerCountReached.Add(1)
	act.lock.Unlock()

	// resize inbox-amount
	if act.workerCountWant > 0 {

		// we kill the inbox
		if act.inbox != nil {
			act.lock.Lock()
			oldCounter := act.workerCountWant
			act.workerCountWant = 0
			act.lock.Unlock()

			// stop all worker
			act.ensureProcesses()
			act.workerCountReached.Wait()

			act.workerCountReached.Add(1)
			act.lock.Lock()
			act.workerCountWant = oldCounter
			act.lock.Unlock()

			close(act.inbox)
			act.inbox = nil
		}

	}

	// inbox is not init
	if act.inbox == nil {
		act.inbox = make(chan jobcontext.Ctx, act.workerCountWant)
	}

	// ensure amount of go-subroutines
	act.ensureProcesses()
	act.workerCountReached.Wait()

	// if we dont want an inbox anymore ( direct actor )
	if act.workerCountWant == 0 {
		close(act.inbox)
		act.inbox = nil
	}
}

func (act *actorData) ensureProcesses() {

	act.lock.RLock()
	logger.Log.Debug(&logger.LoggerOpts{ActorName: act.name}, fmt.Sprintf("ensure processes, current:%d want:%d", act.workerCount, act.workerCountWant))
	additionalWorker := act.workerCountWant - act.workerCount
	act.lock.RUnlock()

	if additionalWorker < 0 {
		act.workerCancel <- nil
	}
	if additionalWorker > 0 {
		go act.inboxLoop()
	}
	if additionalWorker == 0 {
		act.workerCountReached.Done()
	}

}

func (act *actorData) ProcessCounter() (counter int) {
	// calculate wanted worker-count
	act.lock.Lock()
	counter = act.workerCountWant
	act.lock.Unlock()

	return
}
