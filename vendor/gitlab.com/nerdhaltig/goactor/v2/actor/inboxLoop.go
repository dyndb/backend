package actor

import (
	"fmt"
	"time"

	"gitlab.com/nerdhaltig/goactor/v2/actorstates"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

func (act *actorData) inboxLoop() {
	act.lock.RLock()
	logger.Log.Debug(&logger.LoggerOpts{ActorName: act.name}, fmt.Sprintf("spawning process %d", act.workerCount+1))
	act.lock.RUnlock()

	// decrease worker counter
	// and ensure processes
	defer func() {
		act.lock.Lock()
		act.workerCount--
		act.lock.Unlock()
		act.ensureProcesses()
	}()

	// Send the stopped-message
	defer act.handler(jobcontext.New(&actorstates.Stopped{}, nil))

	// the initial message
	act.lock.RLock()
	if !act.initialized {
		act.lock.RUnlock()

		act.handler(jobcontext.New(&actorstates.Initialized{}, nil))

		act.lock.Lock()
		act.initialized = true
		act.lock.Unlock()
	} else {
		act.lock.RUnlock()
	}

	// process counter + Initialized
	act.lock.Lock()
	act.workerCount++
	act.lock.Unlock()

	// Send the started-message
	act.handler(jobcontext.New(&actorstates.Started{}, nil))

	// ensure additional processes
	act.ensureProcesses()

messageLoop:
	for {
		select {

		case jobContext := <-act.inbox:

			// if message is nil, we ignore it
			if jobContext == nil {
				logger.Log.Debug(&logger.LoggerOpts{ActorName: act.name}, "message in inbox was nil")
				time.Sleep(time.Second * 1)
				continue
			}

			// call the message handler
			act.messageHandler(jobContext)

		case <-act.workerCancel:
			break messageLoop
		}

	}

}
