package actor

import (
	"time"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

// Actor interface that represents what an job can do with an actor
type Actor interface {
	Name() string

	// Enqueue a new message to the actors inbox.
	// Without an prev-context
	Enqueue(msg any, returnChannel chan []any) (ctx jobcontext.Ctx)

	// Enqueue a job to an actors inbox
	EnqueueJob(ctx jobcontext.Ctx)

	// Spawn more processes.
	// if processCounter is negative, worker will reduce for this amount
	Spawn(processCounter int)

	// ProcessCounter return the amount of created go-routines
	ProcessCounter() (counter int)

	// Stop all go subroutines or return an error if timeout occurred
	Stop(timeout time.Duration) (err error)
}
