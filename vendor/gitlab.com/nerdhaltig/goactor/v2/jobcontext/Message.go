package jobcontext

import (
	"reflect"
	"strings"
)

func (ctx *contextData) Message() (msg any) {
	msg = ctx.msg
	return
}

func (ctx *contextData) MessageType() string {
	return ctx.msgType
}

func (ctx *contextData) MessageStore(msg any) (messageType string) {
	messageType = reflect.TypeOf(msg).String()
	ctx.msgs.Set(messageType, msg)
	return
}

func (ctx *contextData) MessageGet(messageType string) (msg any, exist bool) {
	msg, exist = ctx.msgs.Get(messageType)
	return
}

func (ctx *contextData) As(msgtype any) (exist bool) {

	msgReflect := reflect.ValueOf(msgtype)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = strings.TrimPrefix(msgRelflectType, "*")

	value, exist := ctx.msgs.Get(msgRelflectType)
	if exist {
		msgReflect.Elem().Set(reflect.ValueOf(value))
	}

	return exist
}

func (ctx *contextData) MessageExtract(msgtype any) (exist bool) {

	msgReflect := reflect.ValueOf(msgtype)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = strings.TrimPrefix(msgRelflectType, "*")

	value, exist := ctx.msgs.Get(msgRelflectType)
	if exist {
		msgReflect.Elem().Set(reflect.ValueOf(value))
		ctx.msgs.Delete(msgRelflectType)
	}

	return exist
}
