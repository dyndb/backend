package jobcontext

func (ctx *contextData) ReturnChannel() (returnChannel chan []any) {
	return ctx.returnChannel
}

func (ctx *contextData) Return(msg []any, err error) {

	if msg == nil {
		msg = []any{}
	}

	if err != nil {
		msg = append(msg, &ctxError{err: err})
		ctx.ErrSet(err)
	}

	// inform the channel
	if ctx.returnChannel != nil {
		ctx.returnChannel <- msg
	}
}
