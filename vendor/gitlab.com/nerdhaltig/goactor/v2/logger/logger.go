package logger

import (
	"os"

	"github.com/rs/zerolog"
	zerologger "github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

type LoggerOpts struct {
	WorkflowID string
	ActorName  string
	StepNo     int
	StepLen    int
}

func (opts *LoggerOpts) Fill(logEntry *zerolog.Event) (newlogEntry *zerolog.Event) {
	newlogEntry = logEntry

	if opts.WorkflowID != "" {
		newlogEntry = newlogEntry.Str("workflowid", opts.WorkflowID)
	}
	if opts.ActorName != "" {
		newlogEntry = newlogEntry.Str("actor", opts.ActorName)
	}
	if opts.StepNo != 0 {
		newlogEntry = newlogEntry.Int("step", opts.StepNo)
	}
	if opts.StepLen != 0 {
		newlogEntry = newlogEntry.Int("stepend", opts.StepLen)
	}

	return
}

type Logger interface {
	OptsToContext(ctx jobcontext.Ctx, opts *LoggerOpts)
	OptsFromContext(ctx jobcontext.Ctx) (opts *LoggerOpts)
	Debug(opts *LoggerOpts, message string)
	Info(opts *LoggerOpts, message string)
	Warning(opts *LoggerOpts, message string)
	Error(opts *LoggerOpts, message string)
}

type DefaultLogger struct{}

func (log *DefaultLogger) OptsToContext(ctx jobcontext.Ctx, opts *LoggerOpts) {
	ctx.MessageStore(*opts)
}

func (log *DefaultLogger) OptsFromContext(ctx jobcontext.Ctx) (opts *LoggerOpts) {
	currentOpts := LoggerOpts{}
	if !ctx.As(&currentOpts) {
		ctx.MessageStore(currentOpts)
	}
	return &currentOpts
}

func (log *DefaultLogger) Debug(opts *LoggerOpts, message string) {
	logEntry := zerologger.Debug()

	logEntry = opts.Fill(logEntry)
	logEntry.Msg(message)
}

func (log *DefaultLogger) Info(opts *LoggerOpts, message string) {
	logEntry := zerologger.Info()

	logEntry = opts.Fill(logEntry)
	logEntry.Msg(message)
}

func (log *DefaultLogger) Warning(opts *LoggerOpts, message string) {
	logEntry := zerologger.Warn()

	logEntry = opts.Fill(logEntry)
	logEntry.Msg(message)
}

func (log *DefaultLogger) Error(opts *LoggerOpts, message string) {
	logEntry := zerologger.Error().Stack()

	logEntry = opts.Fill(logEntry)
	logEntry.Msg(message)
}

var Log Logger = &DefaultLogger{}

func SetLogger(log Logger) {
	Log = log
}

func init() {
	zerolog.CallerSkipFrameCount = 3
	zerologger.Logger = zerologger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
}
