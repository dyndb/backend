package safemap

import (
	"maps"
	"sync"
)

type SafeMap[KeyType comparable, ValueType any] struct {
	lock sync.RWMutex
	data map[KeyType]ValueType
}

func New[KeyType comparable, ValueType any]() *SafeMap[KeyType, ValueType] {
	return &SafeMap[KeyType, ValueType]{
		data: make(map[KeyType]ValueType),
	}
}

// ShallowClone just clones the safemap but not deep copy its contents
func (s *SafeMap[KeyType, ValueType]) ShallowClone() *SafeMap[KeyType, ValueType] {

	/* Before go 1.21
	newMap := &SafeMap[KeyType, ValueType]{
		data: make(map[KeyType]ValueType),
	}

	for key, value := range s.data {
		newMap.data[key] = value
	}
	*/

	newMap := &SafeMap[KeyType, ValueType]{
		data: maps.Clone(s.data),
	}

	return newMap
}

func (s *SafeMap[KeyType, ValueType]) Set(k KeyType, v ValueType) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.data[k] = v
}

func (s *SafeMap[KeyType, ValueType]) Get(key KeyType) (data ValueType, exist bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	val, ok := s.data[key]
	return val, ok
}

func (s *SafeMap[KeyType, ValueType]) Delete(key KeyType) {
	s.lock.Lock()
	defer s.lock.Unlock()
	delete(s.data, key)
}

func (s *SafeMap[KeyType, ValueType]) DeleteFirst() (deletedValue ValueType) {
	s.lock.Lock()
	defer s.lock.Unlock()

	for key := range s.data {
		deletedValue = s.data[key]
		delete(s.data, key)
		break
	}

	return
}

func (s *SafeMap[KeyType, ValueType]) Len() int {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return len(s.data)
}

func (s *SafeMap[KeyType, ValueType]) ForEach(f func(KeyType, ValueType)) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	for key, val := range s.data {
		f(key, val)
	}
}
