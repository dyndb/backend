package workflow

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

// Enqueue will enqueue a new message to the workflow
func (m *ManagerData) Enqueue(msg any) {

	msgReflect := reflect.ValueOf(msg)
	messageTypeString := msgReflect.Type().String()

	// we get the initial Route
	curChains, curChainsExist := m.Chains[messageTypeString]
	if curChainsExist {
		for _, curChain := range curChains {

			newJob := jobcontext.New(msg, make(chan []any, 10))
			newJob.MessageStore(curChain)

			m.actor.EnqueueJob(newJob)

		}
	}

}
