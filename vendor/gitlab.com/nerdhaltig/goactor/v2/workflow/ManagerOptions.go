package workflow

import (
	"time"

	"gitlab.com/nerdhaltig/goactor/v2/actor"
)

type ManagerOptions struct {
	SpawnProcesses int
	Timeout        time.Duration
	ErrorActor     actor.Actor
}
