package workflow

import (
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskCallback struct {
	callback func(ctx jobcontext.Ctx)
}

// Callback adds a function that get called, mainly for debugging purpose or do sync some stuff
func (w *WorkflowTaskChain) Callback(handler func(ctx jobcontext.Ctx)) *WorkflowTaskChain {

	newTask := &taskCallback{
		callback: handler,
	}

	w.tasks = append(w.tasks, newTask)
	return w
}

func (task *taskCallback) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	// logging
	logger.Log.Debug(logOpts, "Call callback...")
	task.callback(ctx)
	return
}
