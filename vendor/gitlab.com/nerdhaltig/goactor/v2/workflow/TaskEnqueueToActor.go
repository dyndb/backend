package workflow

import (
	"fmt"

	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskEnqueueToActor struct {
	Msg   any
	Actor actor.Actor
}

// EnqueueToActor send the msg to actor
func (w *WorkflowTaskChain) EnqueueToActor(msg any, actor actor.Actor) *WorkflowTaskChain {

	newTask := &taskEnqueueToActor{
		Msg:   msg,
		Actor: actor,
	}

	w.tasks = append(w.tasks, newTask)

	return w
}

func (task *taskEnqueueToActor) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	// logging
	logger.Log.Debug(logOpts, fmt.Sprintf("Call '%s'", task.Msg))

	// fork only with a new message
	ctx = ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
		WithMessage: task.Msg,
	})

	// enqueue to actor
	task.Actor.EnqueueJob(ctx)

	// it should send back on our ctx-return-channel
	expectedMessages = 1
	return
}
