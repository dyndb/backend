package workflow

import (
	"fmt"
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskExpectReturnMessages struct {
	messageTypes []string
}

// ExpectReturnMessageTypes only continue, if the expected messages-types are returned from
// previouse actors. If an message of messageTypes is missing, an error occures and the
// error actor will called if set.
func (w *WorkflowTaskChain) ExpectReturnMessageTypes(messageTypes ...any) *WorkflowTaskChain {

	newTask := &taskExpectReturnMessages{}

	for _, message := range messageTypes {
		msgReflect := reflect.ValueOf(message)
		newTask.messageTypes = append(newTask.messageTypes, msgReflect.Type().String())
	}

	w.tasks = append(w.tasks, newTask)
	return w
}

func (task *taskExpectReturnMessages) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	// logging
	logger.Log.Debug(logOpts, "Check return messages...")

	for _, messageType := range task.messageTypes {
		_, routeMessageExist := ctx.MessageGet(messageType)
		if !routeMessageExist {
			err = fmt.Errorf("missing message of type '%s'", messageType)
			break
		} else {
			logger.Log.Debug(logOpts, fmt.Sprintf("Check return messages... '%s' ... OK", messageType))
		}
	}

	if err == nil {
		logger.Log.Debug(logOpts, "Check return messages... OK")
	} else {
		logger.Log.Debug(logOpts, "Check return messages... FAILED")
	}

	return
}
