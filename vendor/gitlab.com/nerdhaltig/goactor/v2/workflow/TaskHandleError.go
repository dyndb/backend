package workflow

import (
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskHandleError struct{}

// HandleError will call the actor when an error occured inside an flow
func (w *WorkflowTaskChain) HandleError(actor actor.Actor) *WorkflowTaskChain {
	w.errorActor = actor
	return w
}

func (task *taskHandleError) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	return
}
