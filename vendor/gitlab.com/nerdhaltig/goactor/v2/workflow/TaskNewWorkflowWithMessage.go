package workflow

import (
	"fmt"
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskNewWorkflow struct {
	Msg     any
	MsgType string
}

// NewWorkflowWithMessage start a new workflow in the same context and wait for finished it
func (w *WorkflowTaskChain) NewWorkflowWithMessage(msg any) *WorkflowTaskChain {

	msgReflect := reflect.ValueOf(msg)

	newTask := &taskNewWorkflow{
		Msg:     msg,
		MsgType: msgReflect.Type().String(),
	}

	w.tasks = append(w.tasks, newTask)

	return w
}

func (task *taskNewWorkflow) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	// logging
	logger.Log.Debug(logOpts, fmt.Sprintf("New sub-workflow for '%s'", task.MsgType))

	// safe the message for later use
	ctx.MessageStore(task.Msg)

	// we get the initial Route
	curWorkflow, curWorkflowExist := manager.Chains[task.MsgType]
	if curWorkflowExist {
		for _, curSteps := range curWorkflow {

			newJob := ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
				WithMessage: task.Msg,
			})
			newJob.MessageStore(curSteps)

			manager.actor.EnqueueJob(newJob)
		}
	}

	expectedMessages = 1
	return
}
