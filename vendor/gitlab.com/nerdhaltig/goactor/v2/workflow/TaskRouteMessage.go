package workflow

import (
	"fmt"
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskRouteMessage struct {
	routes []*MessageToActor
}

type MessageToActor struct {

	// MessageType is an instance to an empty struct/*struct
	// If an message is enqueued to this workspace from this type,
	// it will be sended to the Actor
	MessageType       any
	messageTypeString string

	Actor actor.Actor

	// If set to true, only the current enqueued message will be used
	// if this is false ( default ) the prev messages in the context will be used ( if it exist )
	Direct bool
}

// RouteMessage will route return messages from prev actor to specific actors
//
// # Error handling
//
// - If an message from prev actor not exist, the workflow will continue.
//
// If you need to ensure that an prev actor return an message, use ExpectReturnMessageTypes()
func (w *WorkflowTaskChain) RouteMessage(routes []*MessageToActor) *WorkflowTaskChain {
	newTask := &taskRouteMessage{
		routes: routes,
	}

	for _, route := range newTask.routes {

		if route.MessageType != nil {
			msgReflect := reflect.ValueOf(route.MessageType)
			route.messageTypeString = msgReflect.Type().String()
		} else {
			route.messageTypeString = ""
		}

		if !w.registered {
			if route.messageTypeString != "" {
				w.mgr.Chains[route.messageTypeString] = append(w.mgr.Chains[route.messageTypeString], w)
				w.registered = true
			}
		}

	}

	w.tasks = append(w.tasks, newTask)
	return w
}

func (task *taskRouteMessage) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {
	// logging
	logger.Log.Debug(logOpts, "Route messages")

	expectedMessages = 0

	for _, route := range task.routes {

		// if messageType is present, we send only the message of this type to this actor
		if route.messageTypeString != "" {

			// get the message
			var message any
			var messageExist bool

			if route.Direct {
				if ctx.MessageType() == route.messageTypeString {
					messageExist = true
					message = ctx.Message()
				}
			} else {
				message, messageExist = ctx.MessageGet(route.messageTypeString)
			}

			if messageExist {
				logger.Log.Debug(logOpts, fmt.Sprintf("Route message of type '%s' to actor '%s'", route.messageTypeString, route.Actor.Name()))

				// fork context and call the actor
				newJobContext := ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
					WithMessage: message,
				})
				route.Actor.EnqueueJob(newJobContext)
				expectedMessages++
			}
		} else {
			logger.Log.Warning(logOpts, "No message-type for this route !")
		}

	}

	return
}
