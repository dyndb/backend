package workflow

import (
	"fmt"
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type taskRouteMessageToNewWorkflows struct {
	types map[string]interface{}
}

// RouteMessageToNewWorkflows enqueu 'MessageType' to new workflows
// this will wait until all workflows are done
func (w *WorkflowTaskChain) RouteMessageToNewWorkflows(MessageTypes ...any) *WorkflowTaskChain {

	// create the task
	newTask := &taskRouteMessageToNewWorkflows{
		types: make(map[string]interface{}),
	}

	// fill out type-strings
	for _, messageType := range MessageTypes {

		// get the string for this type
		msgReflect := reflect.ValueOf(messageType)
		newTask.types[msgReflect.Type().String()] = nil

	}

	w.tasks = append(w.tasks, newTask)
	return w
}

func (task *taskRouteMessageToNewWorkflows) execute(manager *ManagerData, logOpts *logger.LoggerOpts, ctx jobcontext.Ctx) (expectedMessages int, err error) {

	// logging
	logger.Log.Debug(logOpts, fmt.Sprintf("Create a new workflow from messages"))

	expectedMessages = 0

	for messageTypeString := range task.types {

		// get the message
		message, messageExist := ctx.MessageGet(messageTypeString)

		if messageExist {
			logger.Log.Debug(logOpts, fmt.Sprintf("Enqueue message of type '%s'", messageTypeString))

			// we get the initial Route
			curWorkflow, curWorkflowExist := manager.Chains[messageTypeString]
			if curWorkflowExist {
				for _, curSteps := range curWorkflow {

					newJob := ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
						WithMessage: message,
					})
					newJob.MessageStore(curSteps)

					logger.Log.Debug(logOpts, fmt.Sprintf("Enqueue workflow for '%s'", messageTypeString))
					manager.actor.EnqueueJob(newJob)
					expectedMessages++
				}

			}

		}

	}

	return
}
