package workflow

import (
	"errors"
	"fmt"
	"time"

	"github.com/teris-io/shortid"
	"gitlab.com/nerdhaltig/goactor/v2/actorstates"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

type ErrorSended struct{}

func (m *ManagerData) handler(ctx jobcontext.Ctx) (returnMessage []any, err error) {

	// Logging opts
	msgAny := ctx.Message()

	switch msg := msgAny.(type) {

	case *actorstates.Initialized,
		*actorstates.Started,
		*actorstates.Stopped:
		break

	default:

		// workflow-context
		var curChain *WorkflowTaskChain
		if !ctx.MessageExtract(&curChain) {
			panic(fmt.Sprintf("Message can not be handled by workflow-actor !. This is an library error, fixme ! %+v", msg))
		}

		// logger-options
		logOpts := &logger.LoggerOpts{
			ActorName: "workflow",
		}

		// workflowid
		shortID, shortErr := shortid.Generate()
		if shortErr == nil {
			logOpts.WorkflowID = shortID
		}

		// we need a new channel, that is only valid for this workflow
		// this prevents a bug that cause sub-workflows to finish parent-workflows
		var returnChannel chan []any = make(chan []any, cap(ctx.ReturnChannel()))
		ctx = ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
			WithChannel: returnChannel,
		})

		logOpts.StepLen = len(curChain.tasks)
		for stepIndex, task := range curChain.tasks {

			logOpts.StepNo = stepIndex + 1
			logger.Log.OptsToContext(ctx, logOpts)
			logger.Log.Debug(logOpts, "Step")

			err = ctx.Err()

			// there should be a task and no error
			if task == nil || err != nil {
				break
			}

			// we count the expected return-message-arrays
			var addExpectedMessages int
			var expectedMessages int

			// execute the task
			addExpectedMessages, err = task.execute(m, logOpts, ctx)
			expectedMessages = expectedMessages + addExpectedMessages

			// store returned messages
			for returnChannelIndex := 0; returnChannelIndex < expectedMessages; returnChannelIndex++ {
				timeout := time.NewTimer(m.Timeout)
				select {
				case msgs := <-returnChannel:
					for _, msg := range msgs {
						ctx.MessageStore(msg)
						returnMessage = append(returnMessage, msg)
					}
					continue
				case <-timeout.C:
					if err == nil {
						err = errors.New("timeout waiting for actor to return")
						ctx.ErrSet(err)
					}
				}
			}

			// call actors that handle errors
			err = ctx.Err()
			if err != nil {
				_, sended := ctx.MessageGet("*workflow.ErrorSended")
				if !sended {
					if curChain.errorActor != nil {
						// fork context and call the error-actor
						newJobContext := ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
							WithMessage: err,
						})
						curChain.errorActor.EnqueueJob(newJobContext)
						ctx.MessageStore(&ErrorSended{})
					} else {
						if m.errorActor != nil {
							// fork context and call the error-actor
							newJobContext := ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
								WithMessage: err,
							})
							m.errorActor.EnqueueJob(newJobContext)
							ctx.MessageStore(&ErrorSended{})
						}
					}
				}
			}

			if err != nil {
				break
			}

		}
	}

	return
}
