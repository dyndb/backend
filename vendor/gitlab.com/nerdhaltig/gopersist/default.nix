{ system ? builtins.currentSystem
, pkgs ? import (import ./nixpkgs.nix) { inherit system; }

}:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

  name = "gopersist";
  version = "0.21.1";

  ################################### Binary ###################################
  src = pkgs.nix-gitignore.gitignoreSourcePure [
    ".gitlab-ci.yml"
    ".gitignore"
    "build/"
    "doc/"
    "flake*"
    "*.adoc"
    "*.xml"
    "*.nix"
  ]
    (pkgs.lib.cleanSource ./.);

  #docker = import ./build/docker.nix { inherit pkgs; };

  report = import ./build/nix/report.nix { inherit pkgs name version src; };


  badge-coverage = import ./build/nix/badge.nix {
    inherit pkgs;
    valuefile = "${src}/coverage.sum";
    badgename = "coverage";
  };

in
{
  # $(nix-build -A docker --no-out-link) | docker load
  inherit
    report
    badge-coverage;
}
