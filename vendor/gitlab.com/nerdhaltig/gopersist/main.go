package gopersist

import (
	"time"

	"gitlab.com/nerdhaltig/gopersist/persistance"
	"gitlab.com/nerdhaltig/gopersist/persistance/ports"
	"gitlab.com/nerdhaltig/gopersist/pool"
)

// we proxy our objects from sub-packages

// ports - persistance

type PersistancePort persistance.Port
type PersistancePortWithSQL persistance.PortWithSQL
type PersistancePortWithSoftDelete persistance.PortWithSoftDelete
type PersistancePortWithSecurity persistance.PortWithSecurity
type PersistanceWithSearch persistance.PortWithSearch

// ports - combined
type PersistancePortWithSQLSoftDeleteSecurity ports.PersistancePortWithSQLSoftDeleteSecurity

// ports - service

type PoolManagementUseCase pool.PoolManagementUseCase

// adapters
func NewInmemoryPersistAdapter() PersistancePort { return persistance.NewInmemoryAdapter() }
func NewEncryptedPersistAdapter(secret string, storage PersistancePort) PersistancePort {
	return persistance.NewEncryptedPersistAdapter(secret, storage)
}
func NewPostgresqlPersistAdapter(dsn string) PersistancePortWithSQLSoftDeleteSecurity {
	return persistance.NewPostgresqlPersistAdapter(dsn)
}

func NewPoolService(size int, requestTimeout time.Duration) PoolManagementUseCase {
	return pool.NewPoolService(size, requestTimeout)
}
