package encrypt

import (
	"context"
	"encoding/json"
)

// Get implements ports.Documents
func (repo *encryptAdapterData) Get(ctx context.Context, documentID interface{}, document interface{}) (exist bool, err error) {

	var tempDoc Doc

	exist, err = repo.storage.Get(ctx, documentID, &tempDoc)

	if exist {
		err = decrypt(repo.secret, &tempDoc)
	}

	// unmashall json
	if exist && err == nil {
		err = json.Unmarshal([]byte(tempDoc.Data), document)
	}

	return
}
