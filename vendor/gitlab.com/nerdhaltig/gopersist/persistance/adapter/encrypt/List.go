package encrypt

import (
	"context"
)

// MustListAll implements ports.Documents
func (repo *encryptAdapterData) List(ctx context.Context, startkey *string, limit int64, callback func(docID interface{}, doc interface{}) (interrupt bool)) (err error) {
	var tempDocument interface{}
	return repo.ListStruct(ctx, startkey, limit, &tempDocument, func(docID interface{}) (interrupt bool) {
		return callback(docID, tempDocument)
	})
}
