package encrypt

import (
	"context"
	"encoding/json"
)

// MustListAll implements ports.Documents
func (repo *encryptAdapterData) ListStruct(ctx context.Context, startkey *string, limit int64, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error) {

	var tempDoc Doc

	return repo.storage.ListStruct(ctx, startkey, limit, &tempDoc, func(docID interface{}) (interrupt bool) {

		err = decrypt(repo.secret, &tempDoc)

		// unmashall json
		if err == nil {
			err = json.Unmarshal([]byte(tempDoc.Data), doc)
		}

		if err == nil {
			callback(docID)
		}

		return
	})

}
