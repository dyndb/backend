package encrypt

import "gitlab.com/nerdhaltig/gopersist/persistance/ports"

type PersistanceAdapter interface {
	ports.PersistancePort
}

type encryptAdapterData struct {
	storage ports.PersistancePort
	secret  string
}

type Doc struct {
	Type string
	Data string
}

// New create a new inmemory document-storage
func New(secret string, storage ports.PersistancePort) PersistanceAdapter {
	return &encryptAdapterData{
		storage: storage,
		secret:  secret,
	}
}

// Fork implements ports.Documents
func (repo *encryptAdapterData) Fork() interface{} {
	return &encryptAdapterData{
		storage: repo.storage,
		secret:  repo.secret,
	}
}
