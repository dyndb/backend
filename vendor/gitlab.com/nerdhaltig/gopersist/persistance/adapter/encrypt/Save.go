package encrypt

import (
	"context"
	"encoding/json"
)

// MustSave implements ports.Documents
func (repo *encryptAdapterData) Save(ctx context.Context, documentID interface{}, document interface{}) (err error) {

	// create json of our clear-text-document
	bytes, err := json.Marshal(document)
	if err != nil {
		panic(err.Error())
	}

	// create the doc
	var tempDoc Doc
	tempDoc.Data = string(bytes)

	// encrypt it
	err = encrypt("aes", repo.secret, &tempDoc)

	// store it
	if err == nil {
		err = repo.storage.Save(ctx, documentID, tempDoc)
	}

	return
}
