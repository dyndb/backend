package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
)

func hashPassword(input string) []byte {
	data := sha256.Sum256([]byte(input))
	return data[0:]
}

func encrypt(cipher, key string, document *Doc) (err error) {

	switch cipher {
	case "aes":
		document.Type = "aes"
		document.Data, err = encryptAES(key, document.Data)
	default:
		document.Type = "aes"
		document.Data, err = encryptAES(key, document.Data)
	}

	return
}

func decrypt(key string, document *Doc) (err error) {

	switch document.Type {
	case "aes":
		document.Data, err = decryptAES(key, document.Data)
	default:
		panic("unknown cipher")
	}

	return
}

func encryptAES(key string, data string) (base64String string, err error) {

	hashKey := hashPassword(key)

	var blockCipher cipher.Block
	blockCipher, err = aes.NewCipher(hashKey)

	var gcm cipher.AEAD
	if err == nil {
		gcm, err = cipher.NewGCM(blockCipher)
	}

	nonce := make([]byte, gcm.NonceSize())
	if err == nil {
		_, err = rand.Read(nonce)
	}

	if err == nil {
		ciphertext := gcm.Seal(nonce, nonce, []byte(data), nil)
		base64String = base64.StdEncoding.EncodeToString(ciphertext)
	}

	return
}

// Decrypt will decrypt base64 encoded data
func decryptAES(key, data string) (plaintext string, err error) {

	hashKey := hashPassword(key)

	var blockCipher cipher.Block
	blockCipher, err = aes.NewCipher(hashKey)

	var gcm cipher.AEAD
	if err == nil {
		gcm, err = cipher.NewGCM(blockCipher)
	}

	// decrypt base64
	var dataDecrypted []byte
	if err == nil {
		dataDecrypted, err = base64.StdEncoding.DecodeString(data)
	}

	var plaintextdata []byte
	if err == nil {
		nonce, ciphertext := dataDecrypted[:gcm.NonceSize()], dataDecrypted[gcm.NonceSize():]

		plaintextdata, err = gcm.Open(nil, nonce, []byte(ciphertext), nil)
	}

	if err == nil {
		plaintext = string(plaintextdata)
	}

	return
}
