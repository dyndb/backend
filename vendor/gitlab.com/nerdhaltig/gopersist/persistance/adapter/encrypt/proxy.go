package encrypt

import "context"

// proxy functions

func (repo *encryptAdapterData) Login(ctx context.Context, dbname, username, password string) (err error) {
	return repo.storage.Login(ctx, dbname, username, password)
}

func (repo *encryptAdapterData) TenantExist(ctx context.Context, tenant string) (exist bool) {
	return repo.storage.TenantExist(ctx, tenant)
}

func (repo *encryptAdapterData) CreateTenant(ctx context.Context, tenant string) (err error) {
	return repo.storage.CreateTenant(ctx, tenant)
}

func (repo *encryptAdapterData) SwitchTenant(ctx context.Context, tenant string) (err error) {
	return repo.storage.SwitchTenant(ctx, tenant)
}

func (repo *encryptAdapterData) Tenant() string {
	return repo.storage.Tenant()
}

func (repo *encryptAdapterData) CollectionExist(ctx context.Context, collection string) (exist bool) {
	return repo.storage.CollectionExist(ctx, collection)
}

func (repo *encryptAdapterData) CreateCollection(ctx context.Context, collection string) (err error) {
	return repo.storage.CreateCollection(ctx, collection)
}

// MustSwitchCollection implements ports.Documents
func (repo *encryptAdapterData) SwitchCollection(ctx context.Context, collection string) (err error) {
	return repo.storage.SwitchCollection(ctx, collection)
}

func (repo *encryptAdapterData) Collection() string {
	return repo.storage.Collection()
}

// IsLoggedIn implements ports.Documents
func (repo *encryptAdapterData) IsLoggedIn(ctx context.Context) bool {
	return repo.storage.IsLoggedIn(ctx)
}

func (repo *encryptAdapterData) Reconnect(ctx context.Context) (err error) {
	return repo.storage.Reconnect(ctx)
}

// Logout implements ports.Documents
func (repo *encryptAdapterData) Logout(ctx context.Context) {
	repo.storage.Logout(ctx)
}

// MustDelete implements ports.Documents
func (repo *encryptAdapterData) Delete(ctx context.Context, documentID interface{}) (err error) {
	return repo.storage.Delete(ctx, documentID)
}
