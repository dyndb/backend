package inmemory

import (
	"context"
	"errors"

	"gitlab.com/nerdhaltig/errwrap"
)

func (repo *inmemAdapterData) CollectionExist(ctx context.Context, collection string) (exist bool) {

	err := repo.validateCollection()
	if err == nil {
		_, exist = repo.memory[repo.dbname][repo.tenant][collection]
	}

	return
}

func (repo *inmemAdapterData) CreateCollection(ctx context.Context, collection string) (err error) {
	// validate
	err = repo.validateTenant()

	if collection == "" {
		err = errors.New("collection can not be empty")
		err = errwrap.WithStack(err)
	}

	if err == nil {
		repo.memory[repo.dbname][repo.tenant][collection] = []documentData{}
	}

	return
}

// MustSwitchCollection implements ports.Documents
func (repo *inmemAdapterData) SwitchCollection(ctx context.Context, collection string) (err error) {
	// validate
	err = repo.validateTenant()

	if collection == "" {
		err = errors.New("collection can not be empty")
		err = errwrap.WithStack(err)
	}

	if err == nil {
		repo.collection = collection
	}

	return
}

func (repo *inmemAdapterData) Collection() string {
	return repo.collection
}
