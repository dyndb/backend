package inmemory

import (
	"context"
)

// MustDelete implements ports.Documents
func (repo *inmemAdapterData) Delete(ctx context.Context, documentID interface{}) (err error) {
	// validate
	err = repo.validateTenant()

	if err == nil {
		db := repo.memory[repo.dbname]
		tenant := db[repo.tenant]
		collection := tenant[repo.collection]

		var docDataIndex int = -1
		var docData documentData
		for docDataIndex, docData = range collection {
			if docData.id == documentID {
				ret := make([]documentData, 0)
				ret = append(ret, collection[:docDataIndex]...)
				ret = append(ret, collection[docDataIndex+1:]...)
				tenant[repo.collection] = ret
				break
			}
		}
	}

	return
}
