package inmemory

import (
	"context"
	"encoding/json"

	"gitlab.com/nerdhaltig/errwrap"
)

// Get implements ports.Documents
func (repo *inmemAdapterData) Get(ctx context.Context, documentID interface{}, document interface{}) (exist bool, err error) {
	// validate
	err = repo.validateTenant()

	if err == nil {
		db := repo.memory[repo.dbname]
		tenant := db[repo.tenant]
		collection := tenant[repo.collection]

		// check if the document already exist
		var docData documentData
		for _, docData = range collection {
			if docData.id == documentID {
				exist = true
				break
			}
		}

		// check if the document already exist
		if exist {
			err = json.Unmarshal([]byte(docData.json), &document)
			err = errwrap.WithStack(err)
		}
	}

	return
}
