package inmemory

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

// MustListAll implements ports.Documents
func (repo *inmemAdapterData) ListStruct(ctx context.Context, startkey *string, limit int64, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error) {

	// validate
	if startkey == nil {
		panic(errors.New("startkey is nil"))
	}
	err = repo.validateCollection()

	if err == nil {
		var limitCounter int64
		var startKeyReached bool
		for _, documentData := range repo.memory[repo.dbname][repo.tenant][repo.collection] {

			if !startKeyReached {
				if fmt.Sprintf("%s", documentData.id) == *startkey {
					startKeyReached = true
					continue
				}
				if *startkey == "" || *startkey == "0" {
					startKeyReached = true
				}
			}
			if !startKeyReached {
				continue
			}

			err := json.Unmarshal([]byte(documentData.json), &doc)
			if err == nil {
				callback(documentData.id)
			}

			limitCounter++

			if limitCounter >= limit && limit > 0 {
				*startkey = fmt.Sprintf("%s", documentData.id)
				break
			} else {
				*startkey = ""
			}
		}
	}

	return
}
