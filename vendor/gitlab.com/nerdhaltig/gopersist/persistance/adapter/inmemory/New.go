package inmemory

import (
	"context"

	"gitlab.com/nerdhaltig/gopersist/persistance/ports"
)

type PersistanceAdapter interface {
	ports.PersistancePort
	ports.WithSecurity
}

type inmemAdapterData struct {
	dbname string

	ownerToken string
	tenant     string
	collection string

	memory documentsDBs
}
type documentData struct {
	id   interface{}
	json string
}
type documents []documentData
type documentsCollections map[string]documents
type documentsTenants map[string]documentsCollections
type documentsDBs map[string]documentsTenants

// New create a new inmemory document-storage
func New() PersistanceAdapter {
	return &inmemAdapterData{
		memory: make(documentsDBs),
	}
}

// Fork implements ports.Documents
func (repo *inmemAdapterData) Fork() interface{} {
	return &inmemAdapterData{
		memory: make(documentsDBs),
	}
}

// IsLoggedIn implements ports.Documents
func (*inmemAdapterData) IsLoggedIn(ctx context.Context) bool {
	return true
}

// Login implements ports.Documents
func (repo *inmemAdapterData) Login(ctx context.Context, dbname, username, password string) (err error) {
	repo.dbname = dbname

	repo.memory[repo.dbname] = make(documentsTenants)

	return
}

func (*inmemAdapterData) Reconnect(ctx context.Context) (err error) { return }

// Logout implements ports.Documents
func (*inmemAdapterData) Logout(ctx context.Context) {}
