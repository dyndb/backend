
// tag::features[]
:feature-description: Store your struct into memory. This is mainly for integration-tests because it will not persists you data
:feature-tenancy: ✅
:feature-query: ❌
:feature-softdelete: ❌
:feature-security: ✅
// end::features[]

include::../../../docs/_macros/_feature_table.adoc[]