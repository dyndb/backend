package inmemory

import (
	"context"
	"encoding/json"

	"gitlab.com/nerdhaltig/errwrap"
)

// MustSave implements ports.Documents
func (repo *inmemAdapterData) Save(ctx context.Context, documentID interface{}, document interface{}) (err error) {
	// validate
	err = repo.validateTenant()

	var documentAsJSON []byte

	if err == nil {
		documentAsJSON, err = json.Marshal(document)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		db := repo.memory[repo.dbname]
		tenant := db[repo.tenant]
		collection := tenant[repo.collection]

		// check if the document already exist
		var docExist bool = false
		var docDataIndex int = -1
		var docData documentData
		for docDataIndex, docData = range collection {
			if docData.id == documentID {
				docExist = true
				break
			}
		}

		if docExist {
			collection[docDataIndex] = documentData{
				id:   documentID,
				json: string(documentAsJSON),
			}
		} else {
			collection = append(collection, documentData{
				id:   documentID,
				json: string(documentAsJSON),
			})
		}

		tenant[repo.collection] = collection
	}

	return
}
