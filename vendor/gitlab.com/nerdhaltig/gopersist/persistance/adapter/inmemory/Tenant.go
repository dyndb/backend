package inmemory

import (
	"context"
	"fmt"

	"gitlab.com/nerdhaltig/errwrap"
)

// TenantExist implements ports.Documents
func (repo *inmemAdapterData) TenantExist(ctx context.Context, tenant string) (exist bool) {
	_, exist = repo.memory[repo.dbname][tenant]
	return
}

func (repo *inmemAdapterData) CreateTenant(ctx context.Context, tenant string) (err error) {
	err = repo.validateDB()
	if err == nil {
		if !repo.TenantExist(ctx, tenant) {
			repo.memory[repo.dbname][tenant] = make(documentsCollections)
		}
	}
	return
}

func (repo *inmemAdapterData) SwitchTenant(ctx context.Context, tenant string) (err error) {

	if err == nil {
		err = repo.validateDB()
	}

	// only switch if not owned by somebody
	if err == nil && repo.ownerToken != "" {
		err = fmt.Errorf("connection is owned by somebody else with tenant '%s' and can not be switched to '%s'", repo.tenant, tenant)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		repo.tenant = tenant
		repo.collection = ""
	}

	return
}

func (repo *inmemAdapterData) Tenant() string {
	return repo.tenant
}
