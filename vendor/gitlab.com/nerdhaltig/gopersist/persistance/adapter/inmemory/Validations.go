package inmemory

import (
	"errors"

	"gitlab.com/nerdhaltig/errwrap"
)

func (repo *inmemAdapterData) validateDB() (err error) {
	// dbname
	if repo.dbname == "" && err == nil {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if _, found := repo.memory[repo.dbname]; !found && err == nil {
		err = errwrap.WithStack(errors.New("db not exist"))
	}

	return
}

func (repo *inmemAdapterData) validateTenant() (err error) {
	// dbname
	err = repo.validateDB()

	// tenant
	if repo.tenant == "" && err == nil {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if _, found := repo.memory[repo.dbname][repo.tenant]; !found && err == nil {
		err = errwrap.WithStack(errors.New("tenant not exist in db"))
	}

	return
}

func (repo *inmemAdapterData) validateCollection() (err error) {

	//
	err = repo.validateTenant()

	// collection
	if repo.collection == "" && err == nil {
		err = errwrap.WithStack(errors.New("no collection selected"))
	}
	if _, found := repo.memory[repo.dbname][repo.tenant][repo.collection]; !found && err == nil {
		err = errwrap.WithStack(errors.New("collection not exist in db"))
	}

	return
}
