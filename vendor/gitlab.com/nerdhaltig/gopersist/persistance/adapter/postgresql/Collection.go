package postgresql

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/errwrap"
)

func (repo *postgresqlAdapterData) CollectionExist(ctx context.Context, collection string) (exist bool) {

	var err error

	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && collection == "" {
		err = errwrap.WithStack(errors.New("collection can not be empty"))
	}

	if err == nil {
		collection = strings.ReplaceAll(collection, ";", "")

		// query = fmt.Sprintf("SELECT tablename, indexname FROM pg_indexes WHERE schemaname = '%s'", repo.tenant)
		query := fmt.Sprintf("SELECT FROM pg_tables WHERE schemaname = '%s' AND tablename  = '%s';", repo.tenant, collection)
		rows := repo.dbpool.QueryRow(ctx, query)

		err = rows.Scan()
		err = errwrap.WithStack(err)
	}

	if err == nil {
		exist = !errors.Is(err, pgx.ErrNoRows)
	}

	return
}

func (repo *postgresqlAdapterData) CreateCollection(ctx context.Context, collection string) (err error) {

	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && collection == "" {
		err = errwrap.WithStack(errors.New("collection can not be empty"))
	}

	var query string
	if err == nil {
		collection = strings.ReplaceAll(collection, ";", "")

		query = fmt.Sprintf(queryTableCreate, repo.tenant, collection)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		query = fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s on %s.%s ( updated_at )", collection+"_index_updated_at", repo.tenant, collection)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		query = fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s on %s.%s ( created_at )", collection+"_index_created_at", repo.tenant, collection)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		query = fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s on %s.%s ( deleted_at )", collection+"_index_deleted_at", repo.tenant, collection)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		query = fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s on %s.%s using gin(data)", collection+"_index_data", repo.tenant, collection)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	return
}

func (repo *postgresqlAdapterData) SwitchCollection(ctx context.Context, collection string) (err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && collection == "" {
		err = errwrap.WithStack(errors.New("collection can not be empty"))
	}

	if repo.collection != collection {
		repo.collection = collection
	}

	return
}

func (repo *postgresqlAdapterData) Collection() string {
	return repo.collection
}
