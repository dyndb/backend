package postgresql

import (
	"context"
	"errors"
	"fmt"
	"html"
	"strconv"
	"strings"
)

// MustContain implements PersistanceAdapter
func (repo *postgresqlAdapterData) Contain(ctx context.Context, startkey *string, limit int64, words []string, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error) {
	if startkey == nil {
		panic(errors.New("startkey is nil"))
	}

	// vars
	var offset int64

	// vars - prepare
	if *startkey != "" {
		offset, err = strconv.ParseInt(*startkey, 10, 64)
	}

	var wordFilter string = ""
	if err == nil {
		for _, word := range words {
			word = html.EscapeString(word)
			word = strings.ToLower(word)

			if wordFilter != "" {
				wordFilter = wordFilter + " AND "
			}

			wordFilter = wordFilter + fmt.Sprintf("lower(data::text) like '%%%s%%'", word)
		}
	}

	// we will read 1 element more than requested, to detect if we need more
	query := fmt.Sprintf(queryFindDocuments, repo.tenant, repo.collection, wordFilter, offset, limit+1)

	var resultscounter int64
	resultscounter, err = repo.list(ctx, query, limit, doc, func(docID interface{}) (interrupt bool) {
		return callback(docID)
	})

	if resultscounter > limit {
		*startkey = fmt.Sprintf("%v", offset+limit)
	}

	return
}
