package postgresql

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/nerdhaltig/errwrap"
)

func (repo *postgresqlAdapterData) SetSoftDelete(enabled bool) {
	repo.softDeleteEnabled = enabled
}

func (repo *postgresqlAdapterData) mustDelete(ctx context.Context, documentID interface{}, softDelete bool) (err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && repo.collection == "" {
		err = errwrap.WithStack(errors.New("no collection selected"))
	}

	now := time.Now()

	if softDelete {
		query := fmt.Sprintf(querySoftDeleteDocument, repo.tenant, repo.collection)
		_, err = repo.dbpool.Exec(ctx, query, now, documentID)

	} else {
		query := fmt.Sprintf(queryDeleteDocument, repo.tenant, repo.collection)
		_, err = repo.dbpool.Exec(ctx, query, documentID)
	}

	return
}

// MustDelete implements ports.Documents
func (repo *postgresqlAdapterData) Delete(ctx context.Context, documentID interface{}) (err error) {
	return repo.mustDelete(ctx, documentID, repo.softDeleteEnabled)
}

func (repo *postgresqlAdapterData) DeleteForce(ctx context.Context, documentID interface{}) (err error) {
	return repo.mustDelete(ctx, documentID, false)
}
