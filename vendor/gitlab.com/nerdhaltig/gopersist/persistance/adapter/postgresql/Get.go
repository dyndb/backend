package postgresql

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist/persistance/domain"
)

// Exist implements ports.DocumentsRepo
func (repo *postgresqlAdapterData) Get(ctx context.Context, documentID interface{}, document interface{}) (exist bool, err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && repo.collection == "" {
		err = errwrap.WithStack(errors.New("no collection selected"))
	}

	var metadata domain.Document
	if err == nil {
		query := fmt.Sprintf(queryGetDocument, repo.tenant, repo.collection, documentID)
		row := repo.dbpool.QueryRow(ctx, query)

		err = row.Scan(&metadata.UUID, &metadata.CreatedAt, &metadata.UpdatedAt, &metadata.DeletedAt, &metadata.Deleted, document)
		if err != nil {
			if errors.Is(err, pgx.ErrNoRows) {
				err = nil
			} else {
				err = errwrap.WithStack(err)
			}
		} else {
			exist = true
		}

	}

	if err == nil {
		if metadata.IsDeleted() {
			// set the documentIDField in the result document
			//reflect.ValueOf(document).Elem().FieldByName(repo.documentIDField).Set(reflect.ValueOf(metadata.UUID))
			err = errors.New("deleted")
			err = errwrap.WithStack(err)
		}
	}

	return
}
