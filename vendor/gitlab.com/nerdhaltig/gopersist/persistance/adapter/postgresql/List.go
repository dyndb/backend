package postgresql

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist/persistance/domain"
)

func (repo *postgresqlAdapterData) list(ctx context.Context, query string, limit int64, doc interface{}, callback func(docID interface{}) (interrupt bool)) (counter int64, err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && repo.collection == "" {
		err = errwrap.WithStack(errors.New("no collection selected"))
	}

	// vars
	var rows pgx.Rows

	// query
	rows, err = repo.dbpool.Query(ctx, query)
	err = errwrap.WithStack(err)

	if err == nil {
		nextAvailable := rows.Next()
		for nextAvailable {
			counter++

			// we only need the data
			var document domain.Document
			err = rows.Scan(&document.UUID, &document.CreatedAt, &document.UpdatedAt, &document.DeletedAt, &document.Deleted, doc)

			if err == nil {
				// push
				if callback != nil {
					interrupt := callback(document.UUID)
					if interrupt {
						break
					}
				}
			}

			nextAvailable = rows.Next()
			if counter >= limit && limit > 0 {
				if nextAvailable {
					counter++
				}
				break
			}
		}

		rows.Close()
	}

	return
}

func (repo *postgresqlAdapterData) List(ctx context.Context, startkey *string, limit int64, callback func(docID interface{}, doc interface{}) (interrupt bool)) (err error) {

	// validate
	if startkey == nil {
		panic(errors.New("startkey is nil"))
	}

	// vars
	var offset int64

	// vars - prepare
	if *startkey != "" {
		offset, err = strconv.ParseInt(*startkey, 10, 64)
		err = errwrap.WithStack(err)
	}

	var resultCounter int64
	if err == nil {

		// we will read 1 element more than requested, to detect if we need more
		query := fmt.Sprintf(queryListDocuments, repo.tenant, repo.collection, offset, limit+1)

		var tempDoc interface{}

		resultCounter, err = repo.list(ctx, query, limit, &tempDoc, func(docID interface{}) (interrupt bool) {
			return callback(docID, tempDoc)
		})
		err = errwrap.WithStack(err)

	}

	if err == nil {
		if resultCounter > limit {
			*startkey = fmt.Sprintf("%v", offset+limit)
		} else {
			*startkey = ""
		}
	} else {
		*startkey = ""
	}

	return
}

func (repo *postgresqlAdapterData) ListDeleted(ctx context.Context, startkey *string, limit int64, callback func(docID interface{}, doc interface{}) (interrupt bool)) (err error) {

	if startkey == nil {
		panic(errors.New("startkey is nil"))
	}

	// vars
	var offset int64

	// vars - prepare
	if *startkey != "" {
		offset, err = strconv.ParseInt(*startkey, 10, 64)
		err = errwrap.WithStack(err)
	}

	query := fmt.Sprintf(queryListDeletedDocuments, repo.tenant, repo.collection, offset, limit+1)

	var tempDoc interface{}
	var resultCounter int64
	if err == nil {
		resultCounter, err = repo.list(ctx, query, limit, &tempDoc, func(docID interface{}) (interrupt bool) {
			return callback(docID, tempDoc)
		})
	}

	if err == nil {
		if resultCounter > limit {
			*startkey = fmt.Sprintf("%v", offset+limit)
		} else {
			*startkey = ""
		}
	} else {
		*startkey = ""
	}
	return
}
