package postgresql

import (
	"context"
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/jackc/pgx/v5"
)

// urlExample := "postgres://username:password@localhost:5432/database_name"
func (repo *postgresqlAdapterData) Login(ctx context.Context, dbname, username, password string) (err error) {

	var newDSN string

	parsedURL, err := url.Parse(repo.dsn)
	if err == nil {
		// socket-based connection
		if _, err := os.Stat(parsedURL.Path); err == nil {
			newDSN = fmt.Sprintf("%s:///%s?host=%s", parsedURL.Scheme, dbname, parsedURL.Path)
		} else {
			newDSN = fmt.Sprintf("%s://%s:%s@%s/%s", parsedURL.Scheme, username, password, parsedURL.Host, dbname)
		}

		repo.dbpool, err = pgx.Connect(ctx, newDSN)
	}

	if err == nil {
		dbname = strings.ReplaceAll(dbname, ";", "")
		repo.dsn = newDSN
		repo.dbname = dbname
	}

	return
}

// IsLoggedIn implements ports.Documents
func (*postgresqlAdapterData) IsLoggedIn(ctx context.Context) bool {
	panic("unimplemented")
}

func (repo *postgresqlAdapterData) Reconnect(ctx context.Context) (err error) {

	if repo.dbpool != nil {
		if repo.dbpool.IsClosed() {
			err = repo.dbpool.DeallocateAll(ctx)
			if err == nil {
				repo.dbpool, err = pgx.Connect(ctx, repo.dsn)
			}
		}
	}

	return
}

// Logout implements ports.Documents
func (repo *postgresqlAdapterData) Logout(ctx context.Context) {
	repo.dbpool.Close(ctx)
	repo.dbname = ""
	repo.tenant = ""
	repo.collection = ""
}
