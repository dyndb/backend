package postgresql

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/nerdhaltig/errwrap"
)

func (repo *postgresqlAdapterData) QueryStruct(ctx context.Context, startkey *string, limit int64, rawSQLFilter string, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error) {

	// validate
	if startkey == nil {
		panic(errors.New("startkey is nil"))
	}

	// vars
	var offset int64

	// vars - prepare
	if *startkey != "" {
		offset, err = strconv.ParseInt(*startkey, 10, 64)
		err = errwrap.WithStack(err)
	}

	var resultCounter int64
	if err == nil {

		// prevent SQL-Injection
		rawSQLFilter = strings.ReplaceAll(rawSQLFilter, ";", "")

		// we will read 1 element more than requested, to detect if we need more
		query := fmt.Sprintf(queryQueryDocuments, repo.tenant, repo.collection, rawSQLFilter, offset, limit+1)

		resultCounter, err = repo.list(ctx, query, limit, doc, func(docID interface{}) (interrupt bool) {
			return callback(docID)
		})
	}

	if err == nil {
		if resultCounter > limit {
			*startkey = fmt.Sprintf("%v", offset+limit)
		} else {
			*startkey = ""
		}
	} else {
		*startkey = ""
	}

	return
}
