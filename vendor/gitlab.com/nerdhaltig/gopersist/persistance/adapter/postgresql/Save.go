package postgresql

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"time"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist/persistance/domain"
)

func (repo *postgresqlAdapterData) exist(ctx context.Context, documentID interface{}) (exist bool, err error) {

	query := fmt.Sprintf(queryDocumentExist, repo.tenant, repo.collection, documentID)
	row := repo.dbpool.QueryRow(ctx, query)

	var metadata domain.Document
	err = row.Scan(&metadata.DeletedAt)

	// no rows exist ( so no error ! )
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}

	if err == nil {
		exist = true
	}
	return
}

// MustCreate implements ports.DocumentsRepo
func (repo *postgresqlAdapterData) Save(ctx context.Context, documentID interface{}, document interface{}) (err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}
	if err == nil && repo.tenant == "" {
		err = errwrap.WithStack(errors.New("no tenant selected"))
	}
	if err == nil && repo.collection == "" {
		err = errwrap.WithStack(errors.New("no collection selected"))
	}

	now := time.Now()

	// document need to be structured, for basic-types we get the error
	// "invalid input syntax for type json (SQLSTATE 22P02)"
	// @TODO maybe we will wrap basic types in a struct to archive this
	// until then, we only will support structs/maps
	documentType := reflect.ValueOf(document).Type()
	documentTypeKind := documentType.Kind().String()
	if documentTypeKind != "struct" && documentTypeKind != "map" {
		panic("document must be an struct or map")
	}

	// check if doc already exist
	var exist bool
	exist, err = repo.exist(ctx, documentID)
	err = errwrap.WithStack(err)

	if err == nil {
		if !exist {
			query := fmt.Sprintf(queryNewDocument, repo.tenant, repo.collection)
			_, err = repo.dbpool.Exec(ctx, query, documentID, now, now, time.Unix(0, 0), false, document)
		} else {
			query := fmt.Sprintf(queryUpdateDocument, repo.tenant, repo.collection)
			_, err = repo.dbpool.Exec(ctx, query, document, now, documentID)
		}
	}

	return
}
