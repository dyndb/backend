package postgresql

import (
	"context"
	"fmt"

	"gitlab.com/nerdhaltig/errwrap"
)

// MustLockTenant implements ports.AdapterWithEverything
func (adp *postgresqlAdapterData) LockTenant(ctx context.Context, tenant string, ownerToken string) (err error) {
	err = adp.SwitchTenant(ctx, tenant)
	if err == nil {
		adp.ownerToken = ownerToken
	}
	return
}

// MustReleaseTenant implements ports.AdapterWithEverything
func (adp *postgresqlAdapterData) ReleaseTenant(ctx context.Context, ownerToken string) (err error) {
	if adp.ownerToken != ownerToken {
		err = fmt.Errorf("connection is owned by somebody else with tenant '%s' can not be released", adp.tenant)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		adp.ownerToken = ""
	}
	return
}
