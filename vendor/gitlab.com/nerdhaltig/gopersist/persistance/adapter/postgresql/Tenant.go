package postgresql

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/errwrap"
)

// TenantExist implements ports.DocumentsWithSoftDelete
func (repo *postgresqlAdapterData) TenantExist(ctx context.Context, tenant string) (exist bool) {
	// validate
	if repo.dbname == "" {
		panic("not logged in")
	}

	// query
	tenant = strings.ReplaceAll(tenant, ";", "")
	query := fmt.Sprintf("SELECT schema_name FROM information_schema.schemata WHERE schema_name = '%s';", tenant)
	rows := repo.dbpool.QueryRow(ctx, query)

	err := rows.Scan()
	exist = !errors.Is(err, pgx.ErrNoRows)
	return
}

func (repo *postgresqlAdapterData) CreateTenant(ctx context.Context, tenant string) (err error) {

	var query string

	if repo.tenant != tenant {
		tenant = strings.ReplaceAll(tenant, ";", "")

		query = fmt.Sprintf(querySchemaCreate, tenant)
		_, err = repo.dbpool.Exec(ctx, query)
		err = errwrap.WithStack(err)
	}

	return
}

func (repo *postgresqlAdapterData) SwitchTenant(ctx context.Context, tenant string) (err error) {
	// validate
	if err == nil && repo.dbname == "" {
		err = errwrap.WithStack(errors.New("not logged in"))
	}

	// only switch if not owned by somebody
	if err == nil && repo.ownerToken != "" {
		err = fmt.Errorf("connection is owned by somebody else with tenant '%s' and can not be switched to '%s'", repo.tenant, tenant)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		repo.tenant = strings.ReplaceAll(tenant, ";", "")
		repo.collection = ""
	}

	return
}

func (repo *postgresqlAdapterData) Tenant() string {
	return repo.tenant
}
