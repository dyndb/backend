package postgresql

import (
	"net/url"

	"github.com/jackc/pgx/v5"
	"gitlab.com/nerdhaltig/gopersist/persistance/ports"
)

type PersistanceAdapter interface {
	ports.PersistancePort
	ports.WithSQL
	ports.WithSoftDelete
	ports.WithSecurity
	ports.WithSearch
}

type postgresqlAdapterData struct {
	dsn    string
	dbpool *pgx.Conn
	dbname string

	ownerToken string
	tenant     string
	collection string

	softDeleteEnabled bool // if this is enable, we not delete entrys in the DB directly
}

// create a Client connected to a postgresql server
func New(dsn string) PersistanceAdapter {
	return &postgresqlAdapterData{
		dsn: dsn,
	}
}

func (repo *postgresqlAdapterData) HostSet(hostname string) {

	parsedURL, err := url.Parse(repo.dsn)
	if err == nil {
		parsedURL.Host = hostname
	}

	repo.dsn = parsedURL.String()
}

// Fork implements ports.Documents
func (repo *postgresqlAdapterData) Fork() interface{} {
	return &postgresqlAdapterData{
		dsn: repo.dsn,
	}
}
