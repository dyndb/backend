package postgresql

const (
	querySchemaCreate         = "CREATE SCHEMA IF NOT EXISTS \"%s\""
	queryTableCreate          = "CREATE TABLE IF NOT EXISTS %s.%s ( docid varchar not null primary key, created_at timestamptz, updated_at timestamptz, deleted_at timestamptz, deleted bool, data jsonb DEFAULT null )"
	queryDocumentExist        = "SELECT deleted_at FROM %s.%s WHERE docid = '%v'"
	queryGetDocument          = "SELECT docid, created_at, updated_at, deleted_at, deleted, data FROM %s.%s WHERE deleted = false AND docid = '%v'"
	queryListDocuments        = "SELECT docid, created_at, updated_at, deleted_at, deleted, data FROM %s.%s WHERE deleted = false OFFSET %v LIMIT %v"
	queryListDeletedDocuments = "SELECT docid, created_at, updated_at, deleted_at, deleted, data FROM %s.%s WHERE deleted = true OFFSET %v LIMIT %v"
	queryQueryDocuments       = "SELECT docid, created_at, updated_at, deleted_at, deleted, data FROM %s.%s WHERE deleted = false AND %s OFFSET %v LIMIT %v"
	queryFindDocuments        = "SELECT docid, created_at, updated_at, deleted_at, deleted, data FROM %s.%s WHERE deleted = false AND %s OFFSET %v LIMIT %v"
	queryNewDocument          = "INSERT INTO %s.%s values($1, $2, $3, $4, $5, $6 )"
	queryUpdateDocument       = "UPDATE %s.%s SET data = $1, updated_at = $2 WHERE docid = $3"
	queryDeleteDocument       = "DELETE FROM %s.%s WHERE docid = $1"
	querySoftDeleteDocument   = "UPDATE %s.%s SET deleted = true, deleted_at = $1 WHERE docid = $2"
)
