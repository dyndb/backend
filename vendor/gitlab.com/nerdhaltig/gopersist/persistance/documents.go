package persistance

import (
	"gitlab.com/nerdhaltig/gopersist/persistance/adapter/encrypt"
	"gitlab.com/nerdhaltig/gopersist/persistance/adapter/inmemory"
	"gitlab.com/nerdhaltig/gopersist/persistance/adapter/postgresql"
	"gitlab.com/nerdhaltig/gopersist/persistance/ports"
)

// we proxy our objects from sub-packages

// ports
type Port ports.PersistancePort

// or you can check against "features"
type PortWithSQL ports.PersistancePortsWithSQL
type PortWithSoftDelete ports.PersistancePortsWithSoftDelete
type PortWithSecurity ports.PersistancePortsWithSecurity
type PortWithSearch ports.WithSearch
type PersistancePortWithSQLSoftDeleteSecurity ports.PersistancePortWithSQLSoftDeleteSecurity

// adapters
func NewInmemoryAdapter() inmemory.PersistanceAdapter { return inmemory.New() }

func NewEncryptedPersistAdapter(secret string, storage Port) encrypt.PersistanceAdapter {
	return encrypt.New(secret, storage)
}

func NewPostgresqlPersistAdapter(dsn string) postgresql.PersistanceAdapter {
	return postgresql.New(dsn)
}
