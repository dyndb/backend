package domain

import (
	"encoding/json"
	"time"
)

// Document - represents a datastore document
type Document struct {
	UUID      string      `json:"uuid" db:"uuid"`
	UpdatedAt time.Time   `json:"updated_at" db:"updated_at"`
	CreatedAt time.Time   `json:"created_at" db:"created_at"`
	DeletedAt time.Time   `json:"deleted_at" db:"deleted_at"`
	Deleted   bool        `json:"deleted" db:"deleted"`
	Data      interface{} `json:"data" db:"data"`
}

// Deleted - whether the documented deleted or not
func (d Document) IsDeleted() bool {
	return d.Deleted
}

func (d Document) ToDocument(document interface{}) {

	bytes, err := json.Marshal(d.Data)
	if err != nil {
		json.Unmarshal(bytes, document)
	}

}
