package ports

import (
	"context"
)

// Documents can store, load and find structs
type PersistancePort interface {

	// Login to you database
	//
	// dbname: Can be selected only once ( for tenancy and security )
	//
	// tenant: Can be selected only once
	//
	// collection: Can be changed anytime. Depending on the DB-Driver
	Login(ctx context.Context, dbname, username, password string) (err error)

	// TenantExist will return true if the tenant exist
	TenantExist(ctx context.Context, tenant string) (exist bool)

	// CreateTenant will create a tenant if not exist or return an error if something goes wrong
	CreateTenant(ctx context.Context, tenant string) (err error)

	// SwitchTenant will switch to another tenant.
	// Make sure, that the tenant exist if you call other functions, this function will not check if the tenant exist, to reduce DB-Load
	SwitchTenant(ctx context.Context, tenant string) (err error)

	// Tenant return the switched tenant
	Tenant() string

	// CollectionExist return if an collection exist
	CollectionExist(ctx context.Context, collection string) (exist bool)

	// CreateCollection will create a collection return an error if something goes wrong
	CreateCollection(ctx context.Context, collection string) (err error)

	// SwitchCollection will switch your document-collection inside an tenant
	// Make sure, that the collection exist if you call other functions, this function will not check if the collection exist, to reduce DB-Load
	SwitchCollection(ctx context.Context, collection string) (err error)

	// Collection return the switched collection
	Collection() string

	// IsLoggedIn return if this connection is logged in to a db
	IsLoggedIn(ctx context.Context) bool

	// Reconnect if not connected.
	// This function is connection save, if you are connected, nothing will be done.
	// The check, if an connection is connected, depends on the used backend.
	Reconnect(ctx context.Context) (err error)

	// Logout from the DB, disconnect and free resources, so this repo can be used by somebody else
	Logout(ctx context.Context)

	// Get will load an document.
	// It will return an error if something goes wrong.
	Get(ctx context.Context, documentID interface{}, document interface{}) (exist bool, err error)

	// List will list all not deleted documents.
	// If startkey is nil this function panics.
	// If the startkey is "", we start from the beginning.
	// startkey will be changed to the start of the next page. If startkey is "", then there are no pages anymore.
	List(ctx context.Context, startkey *string, limit int64, callback func(docID interface{}, doc interface{}) (interrupt bool)) (err error)

	// ListStruct will list all not deleted documents.
	// Doc must be a pointer to an structure of your final document.
	// For every doc in the db, callback is called and the document is written to doc.
	// If startkey is nil this function panics.
	// If the startkey is "", we start from the beginning.
	// startkey will be changed to the start of the next page. If startkey is "", then there are no pages anymore.
	ListStruct(ctx context.Context, startkey *string, limit int64, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error)

	// Save the `document`
	Save(ctx context.Context, documentID interface{}, document interface{}) (err error)

	// Delete will delete the document
	Delete(ctx context.Context, documentID interface{}) (err error)
}

type WithSQL interface {
	// QueryStruct will list all not deleted documents.
	// Doc must be a pointer to an structure of your final document.
	// For every doc in the db, callback is called and the document is written to doc.
	// rawSQLFilter contains the backend specific SQL-WHERE clause
	// If startkey is nil this function panics.
	// If the startkey is "", we start from the beginning.
	// startkey will be changed to the start of the next page. If startkey is "", then there are no pages anymore.
	QueryStruct(ctx context.Context, startkey *string, limit int64, rawSQLFilter string, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error)
}

type WithSearch interface {
	Contain(ctx context.Context, startkey *string, limit int64, words []string, doc interface{}, callback func(docID interface{}) (interrupt bool)) (err error)
}

type PersistancePortsWithSQL interface {
	PersistancePort
	WithSQL
}

type WithSoftDelete interface {

	// SetSoftDelete will enable/disable soft-delete, its disabled by default if the driver supports it !
	SetSoftDelete(enabled bool)

	ListDeleted(ctx context.Context, startkey *string, limit int64, callback func(docID interface{}, doc interface{}) (interrupt bool)) (err error)

	DeleteForce(ctx context.Context, documentID interface{}) (err error)
}

type PersistancePortsWithSoftDelete interface {
	PersistancePort
	WithSoftDelete
}

type WithSecurity interface {
	// LockTenant switch to a tenant and lock it.
	// only the one who knows the owner-token kann switch to another tenant ( first release it with ReleaseTenant() )
	LockTenant(ctx context.Context, tenant string, ownerToken string) (err error)
	ReleaseTenant(ctx context.Context, ownerToken string) (err error)
}

type PersistancePortsWithSecurity interface {
	PersistancePort
	WithSecurity
}

type PersistancePortWithSQLSoftDeleteSecurity interface {
	PersistancePort
	WithSQL
	WithSoftDelete
	WithSecurity
}
