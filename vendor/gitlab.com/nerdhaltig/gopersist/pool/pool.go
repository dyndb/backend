package pool

import (
	"time"

	"gitlab.com/nerdhaltig/gopersist/pool/service/management"
	"gitlab.com/nerdhaltig/gopersist/pool/usecase"
)

// we proxy our objects from sub-packages

// ports
type PoolManagementUseCase usecase.PoolManagement

func NewPoolService(size int, requestTimeout time.Duration) PoolManagementUseCase {
	return management.NewPoolService(size, requestTimeout)
}
