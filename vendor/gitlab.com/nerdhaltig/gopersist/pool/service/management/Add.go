package management

import "gitlab.com/nerdhaltig/gopersist/persistance"

// Add implements ports.ConnectionPool
func (svc *connectionPoolService) Add(connection persistance.Port) {
	svc.available <- connection
}
