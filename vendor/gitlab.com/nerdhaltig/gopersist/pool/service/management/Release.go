package management

import "gitlab.com/nerdhaltig/gopersist/persistance"

// Release implements ports.ConnectionPool
func (svc *connectionPoolService) Release(connection persistance.Port) {
	if connection != nil {

		// lock
		svc.availableLock.Lock()
		defer svc.availableLock.Unlock()

		// check if connection is unused
		if available, exists := svc.availableConnections[connection]; exists {
			if available {
				return
			} else {
				svc.availableConnections[connection] = true
			}
		} else {
			svc.availableConnections[connection] = true
		}

		// add it to channel
		svc.available <- connection
	}
}
