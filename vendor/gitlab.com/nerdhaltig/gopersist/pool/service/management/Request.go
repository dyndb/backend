package management

import (
	"context"
	"errors"

	"gitlab.com/nerdhaltig/errwrap"
	"gitlab.com/nerdhaltig/gopersist/persistance"
)

func (svc *connectionPoolService) RequestRaw(ctx context.Context) (connection persistance.Port, err error) {

	// new timeout context
	ctx, cancel := context.WithTimeout(ctx, svc.requestTimeout)
	defer cancel()

	select {

	// try to read a connection from available-stash
	case connection := <-svc.available:

		// remember that connection is used
		svc.availableLock.Lock()
		defer svc.availableLock.Unlock()
		svc.availableConnections[connection] = false

		// Reconnect ( if needed )
		err = connection.Reconnect(ctx)

		return connection, err

	// timeout ocurred
	case <-ctx.Done():
		return nil, errors.New("timeout waiting for an free connection")
	}

}

func (svc *connectionPoolService) WithConnection(ctx context.Context, tenant, collection string, handler func(ctx context.Context, connection persistance.Port)) {

	// get an connection from the pool
	connection, err := svc.RequestRaw(ctx)
	if err == nil {

		// on panic, we give the connection back
		defer func() {
			r := recover()
			svc.Release(connection)
			if r != nil {
				panic(r)
			}
		}()
	}

	if err == nil {
		// switch tenant
		err = connection.SwitchTenant(ctx, tenant)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		err = connection.SwitchCollection(ctx, collection)
		err = errwrap.WithStack(err)
	}

	if err == nil {
		handler(ctx, connection)
	}
}
