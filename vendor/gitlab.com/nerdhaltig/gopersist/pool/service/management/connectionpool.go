package management

import (
	"sync"
	"time"

	"gitlab.com/nerdhaltig/gopersist/persistance"
	"gitlab.com/nerdhaltig/gopersist/pool/usecase"
)

type connectionPoolService struct {
	available chan persistance.Port

	availableLock        sync.Mutex
	availableConnections map[persistance.Port]bool

	requestTimeout time.Duration
}

func NewPoolService(size int, requestTimeout time.Duration) usecase.PoolManagement {
	return &connectionPoolService{
		requestTimeout:       requestTimeout,
		available:            make(chan persistance.Port, size),
		availableConnections: make(map[persistance.Port]bool),
	}
}

func (svc *connectionPoolService) Available() int {
	return len(svc.available)
}
