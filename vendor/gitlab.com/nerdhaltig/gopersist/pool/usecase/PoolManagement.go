package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/gopersist/persistance"
)

type PoolManagement interface {

	// Add an connection to the pool
	Add(connection persistance.Port)

	// RequestRaw will request a free connection from the pool
	// or return an error if request of an free connection timed out
	//
	// You don't need to pass an context with timeout, it will be created inside this function
	RequestRaw(ctx context.Context) (connection persistance.Port, err error)

	// WithConnection will request an connection from the pool switch to a tenant and collection.
	// After the connection as aquired from the pool, the handler will be called with this connection.
	WithConnection(ctx context.Context, tenant, collection string, handler func(ctx context.Context, connection persistance.Port))

	// Release an connection.
	// If you release an already released connection, nothing will be done.
	Release(connection persistance.Port)

	Available() int
}
