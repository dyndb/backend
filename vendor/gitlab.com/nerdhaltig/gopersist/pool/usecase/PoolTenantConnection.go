package usecase

import (
	"context"

	"gitlab.com/nerdhaltig/gopersist/persistance"
)

type PoolTenantConnection interface {

	// WithLockedTenant return an connection with switched to tenant and collection.
	// This function LOCK the tenant, so nobody can switch to another tenant during handler is called
	WithLockedTenant(ctx context.Context, tenant, collection string, handler func(ctx context.Context, connection persistance.Port))
}
